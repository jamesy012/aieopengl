#version 410





layout(location=0) in vec4 position;
layout(location=1) in vec4 normal;
layout(location=2) in vec2 texCoord;

uniform mat4 projectionViewMatrix;
uniform mat4 model = mat4(1);
uniform mat4 normalRot = mat4(1);


uniform float time;

out vec4 vPosition;
out vec2 vTexCoord;
out vec4 vNormal;
out vec4 vColor;

//water properties

struct Wave {
	vec2 offset;
	vec2 waveLength;
	vec2 amplitude;
	vec2 speed;
};
uniform Wave waves[AMOUNT_WAVES];

uniform float waterHeight = 1;
uniform float waterSize = 100;
uniform vec3 waterPosition = vec3(0);

void main(){
	vec4 posModel = model*position;
	vec4 waterOffset = vec4(0);
	
	/*	
	{
	float sinWave = 0;
	float xPos = texCoord.x * waterSize;
	float yPos = texCoord.y * waterSize;
	//sinWave += sin((m_Waves[w].offset + i + time*m_Waves[w].speed) / m_Waves[w].waveLength)* m_Waves[w].amplitude;
	
	float maxAmplitude = 0;
	float minAmplitude = 0;
	
	for(int i = 0; i<AMOUNT_WAVES; i++){
		float sinAmount = 0;
		sinAmount = sin(((waves[i].offset.x*waves[i].offset.y) + ((xPos)*(yPos)) + (time * waves[i].speed)) / waves[i].waveLength) * waves[i].amplitude;
		//sinAmount = sin(((waves[i].offset.x*waves[i].offset.y) + ((xPos)*(yPos)) + (time * waves[i].speed)) / waves[i].waveLength) * waves[i].amplitude;
		
		
		sinWave += sinAmount;
		maxAmplitude = max(maxAmplitude,sinAmount);
		minAmplitude = min(minAmplitude,sinAmount);
	}
	waterOffset.y = sinWave;
	waterOffset.y /= AMOUNT_WAVES;
	waterOffset.y *= waterHeight;

		
	float blue;
	//blue = abs(maxAmplitude - minAmplitude);
	blue = posModel.y+waterOffset.y;
	blue += 0.20f;
	blue *= 6;

	
	vColor = vec4(0.85f,0.85f,blue,1.0f);
}
	*/
	
	{
		//sinWave += sin((m_Waves[w].offset + i + time*m_Waves[w].speed) / m_Waves[w].waveLength)* m_Waves[w].amplitude;
		float sinAmount = 0;
		vec2 pos = posModel.xz;
		pos.x += (time * 5) + (waves[0].offset.y);
		//sinAmount += sin(pos.x * time);
		//sinAmount += sin(pos.y * time);
		
		for(int i = 0; i<AMOUNT_WAVES; i++){
			vec2 offset = (pos * waves[i].offset) / waterSize;
			vec2 timeVec = time * waves[i].speed;
			
			sinAmount += sin((offset.x + timeVec.x) / waves[i].waveLength.x) * waves[i].amplitude.x;
			sinAmount += sin((offset.y + timeVec.y) / waves[i].waveLength.y) * waves[i].amplitude.y;
			
			//sinAmount += sin(((posModel.x + waves[i].offset.x) / waterSize) + (time * waves[i].speed.x));
			//sinAmount += sin(((posModel.z + waves[i].offset.y) / waterSize) + (time * waves[i].speed.y));
			//sinAmount += sin(((((pos.x + waves[i].offset.x) / waterSize) + (time * waves[i].speed.x)) / waves[i].waveLength.x)) * waves[i].amplitude.x;
			//sinAmount += sin(((((pos.y + waves[i].offset.y) / waterSize) + (time * waves[i].speed.y)) / waves[i].waveLength.y)) * waves[i].amplitude.y;
			
			//sinAmount += sin(15+(((((pos.y + waves[i].offset.y)*(pos.x + waves[i].offset.x)) / waterSize) + (time * waves[i].speed.x)) / waves[i].waveLength.x)) * waves[i].amplitude.x;
		}
		
		waterOffset.y = sinAmount / (AMOUNT_WAVES);//X and Y
		waterOffset.y *= waterHeight;
		
		
		float blue = (waterOffset.y/2) + 1;
		vColor = vec4(blue/2,blue/2,blue,1);
	}
	
	vPosition = posModel + waterOffset;
	
	gl_Position = projectionViewMatrix * vPosition; 	
	vTexCoord = texCoord;
	vNormal   = normalRot * normal;	

}