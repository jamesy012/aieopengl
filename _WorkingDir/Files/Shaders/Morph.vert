#version 410

layout(location=0) in vec4 position;
layout(location=1) in vec4 normal;
layout(location=2) in vec2 texCoord;

layout(location=3) in vec4 position2;
layout(location=4) in vec4 normal2;

uniform mat4 projectionViewMatrix;
uniform mat4 model = mat4(1);
uniform mat4 normalRot = mat4(1);

uniform float keyTime;

out vec2 vTexCoord;
out vec4 vNormal;

void main(){
	vec4 p = mix(position,position2, keyTime);
	vec4 n = mix(normal,normal2, keyTime);

	vTexCoord = texCoord;	
	vNormal   = normalRot * n;
	
	gl_Position = projectionViewMatrix * model * p; 

}