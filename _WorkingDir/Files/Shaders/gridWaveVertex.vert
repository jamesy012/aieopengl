#version 410

layout(location=0) in vec4 position;
layout(location=1) in vec4 color;

out vec4 vColor;

uniform mat4 projectionViewMatrix;
uniform float time;
uniform float heightScale;

void main(){
	vColor = color;
	vec4 P = position; 
	P.y += sin(time + position.x) * heightScale;
	P.z += sin(time + position.z+(position.x/1.45f)) * heightScale/4;
	gl_Position = projectionViewMatrix * P; 
}