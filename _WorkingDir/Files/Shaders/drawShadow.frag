#version 410

in vec4 vNormal;
in vec4 vShadowCoord;

out vec4 fragColor;

uniform vec3 lightDir;

uniform sampler2D shadowMap;
uniform float bias = 0.0055f;

vec4 boxBlur() {
	vec2 texel = 1.0f / vec2(1024,1024);
	
	vec4 color = texture(shadowMap, vShadowCoord.xy);
	color += texture(shadowMap, vShadowCoord.xy + vec2(-texel.x,  texel.y));
	color += texture(shadowMap, vShadowCoord.xy + vec2(-texel.x,  0));
	color += texture(shadowMap, vShadowCoord.xy + vec2(-texel.x, -texel.y));
	color += texture(shadowMap, vShadowCoord.xy + vec2(0	   ,  texel.y));
	color += texture(shadowMap, vShadowCoord.xy + vec2(0	   , -texel.y));
	color += texture(shadowMap, vShadowCoord.xy + vec2( texel.x,  texel.y));
	color += texture(shadowMap, vShadowCoord.xy + vec2( texel.x, 0));
	color += texture(shadowMap, vShadowCoord.xy + vec2( texel.x, -texel.y));
	
	return color / 9;
}

void main(){
	float d = max(0,dot(normalize(vNormal.xyz),lightDir));
	
	//if(boxBlur().r < vShadowCoord.z - bias)
	//{
	//	d = 0.0f;
	//}
	if(texture(shadowMap, vShadowCoord.xy).r < vShadowCoord.z - bias)
	{
		d = 0.0f;
	}
	//else{
	//d = 1.0f;
	//}
	//if(0.3f < vShadowCoord.z){
	//	d = 0.0f;
	//}


	
	fragColor = vec4(d,d,d,1);
	//fragColor = vec4(vShadowCoord.xxx,1);
	//fragColor = vec4(vShadowCoord.yyy,1);
	//fragColor = vec4(vShadowCoord.zzz,1);
	//fragColor = texture(shadowMap, gl_FragCoord.xy/vec2(1280, 720));
	//fragColor = vec4(gl_FragCoord.xy/vec2(1280, 720),0,1);
	//fragColor = vec4(texture(shadowMap, vShadowCoord.xy).rrr,1);
	//fragColor = vNormal;
	//fragColor = vShadowCoord;
	
	//if(texture(shadowMap, vShadowCoord.xy).r > 0.01f){
	//	fragColor = vec4(1,0,0,1);
	//}else{
	//	fragColor = vec4(0,1,0,1);
	//}
	//fragColor = vec4(vShadowCoord.x,0,0,1);
}