#version 410

layout(location=0) in vec4 position;
layout(location=1) in vec4 normal;
layout(location=2) in vec2 texCoord;
layout(location=3) in vec4 tangent;

uniform mat4 projectionViewMatrix;
uniform mat4 model = mat4(1);
uniform mat4 normalRot = mat4(1);

out vec2 vTexCoord;
out vec4 vNormal;
out vec3 vTangent;
out vec3 vBiTangent;

void main(){	
	vTexCoord = texCoord;
	vNormal   = normalRot * normal;
	vTangent = tangent.xyz;//todo put this in a layout
	vBiTangent = cross(vNormal.xyz, vTangent);
	gl_Position = projectionViewMatrix * model * position;
}