#version 410

layout(location=0) in vec4 position;
layout(location=1) in vec4 normal;
layout(location=2) in vec2 texCoord;

uniform mat4 projectionViewMatrix;

uniform float FARPLANE = 30.0f;
out vec4 vNormal;
out float DEPTH;

void main(){
	gl_Position = projectionViewMatrix * position; 
	vNormal = normal;
	DEPTH = gl_Position.z / FARPLANE ; // do not divide by w
}