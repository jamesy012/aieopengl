#version 410

in vec4 vPosition;
in vec2 vTexCoord; 
in vec4 vNormal;
in vec3 vTangent;
in vec3 vBiTangent;

out vec4 fragColor; 

uniform vec3 lightDir;
uniform sampler2D TexDiffuse1; 
uniform sampler2D TexNormal1;

void main() { 
	mat3 TBN = mat3(normalize(vTangent),normalize(vBiTangent),normalize(vNormal));
	vec3 N = texture(TexNormal1, vTexCoord).xyz *2 - 1;
	
	float d = max(0, dot(normalize(TBN * N),normalize(lightDir)));
	fragColor = texture(TexDiffuse1, vTexCoord);
	fragColor.rgb = fragColor.rgb * d;
	

}