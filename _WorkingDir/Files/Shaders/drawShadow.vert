#version 410

layout(location=0) in vec4 Position;
layout(location=1) in vec4 Normal;

out vec4 vNormal;
out vec4 vShadowCoord;

uniform mat4 projectionViewMatrix;
uniform mat4 model = mat4(1);
uniform mat4 normalRot = mat4(1);

uniform mat4 lightMatrix;

void main(){
	gl_Position = projectionViewMatrix * model * Position; 
	vNormal = normalRot * Normal;
	vShadowCoord = lightMatrix * model * Position;
}