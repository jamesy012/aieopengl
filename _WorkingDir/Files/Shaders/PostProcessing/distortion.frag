#version 410

in vec2 vTexCoord;

out vec4 fragColor;

uniform sampler2D TexDiffuse1;
uniform float time;

vec4 distort() {
	vec2 mid = vec2(0.5f);
	
	float distanceFromCenter = distance(vTexCoord, mid);
	vec2 normalizedCoord = normalize(vTexCoord - mid);
	float bias = distanceFromCenter + sin(distanceFromCenter * 15 + time) * 0.02f;
	
	vec2 newCoord = mid + bias * normalizedCoord;
	
	return texture(TexDiffuse1, newCoord);
}

void main() {
	fragColor = distort();
}