#version 410

in vec2 vTexCoord;

out vec4 fragColor;

uniform sampler2D TexDiffuse1;

vec4 boxBlur() {
	vec2 texel = 1.0f / textureSize(TexDiffuse1, 0).xy;
	
	vec4 x = texture(TexDiffuse1, vTexCoord);
	x += texture(TexDiffuse1, vTexCoord + vec2(-texel.x, -texel.y) 	* -1);
	x += texture(TexDiffuse1, vTexCoord + vec2(0	   , -texel.y) 	*  0);
	x += texture(TexDiffuse1, vTexCoord + vec2( texel.x, -texel.y) 	*  1);
	x += texture(TexDiffuse1, vTexCoord + vec2(-texel.x, 0)			* -2);
	x += texture(TexDiffuse1, vTexCoord + vec2( texel.x, 0)			*  2);
	x += texture(TexDiffuse1, vTexCoord + vec2(-texel.x,  texel.y)	* -1);	
	x += texture(TexDiffuse1, vTexCoord + vec2(0	   ,  texel.y)	*  0);
	x += texture(TexDiffuse1, vTexCoord + vec2( texel.x,  texel.y)	*  1);
	
	vec4 y = texture(TexDiffuse1, vTexCoord);
	y += texture(TexDiffuse1, vTexCoord + vec2(-texel.x, -texel.y) 	* -1);
	y += texture(TexDiffuse1, vTexCoord + vec2(0	   , -texel.y) 	* -2);
	y += texture(TexDiffuse1, vTexCoord + vec2( texel.x, -texel.y) 	* -1);
	y += texture(TexDiffuse1, vTexCoord + vec2(-texel.x, 0)			*  0);
	y += texture(TexDiffuse1, vTexCoord + vec2( texel.x, 0)			*  0);
	y += texture(TexDiffuse1, vTexCoord + vec2(-texel.x,  texel.y)	*  1);	
	y += texture(TexDiffuse1, vTexCoord + vec2(0	   ,  texel.y)	*  2);
	y += texture(TexDiffuse1, vTexCoord + vec2( texel.x,  texel.y)	*  1);

	vec4 color = sqrt(pow(x,vec4(2)) + pow(y,vec4(2)));
	
	return color;
}

void main() {
	fragColor = boxBlur();
}