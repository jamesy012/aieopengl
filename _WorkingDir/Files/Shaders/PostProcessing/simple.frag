#version 410

in vec2 vTexCoord;

out vec4 fragColor;

uniform sampler2D TexDiffuse1;

vec4 simple() {
	return texture(TexDiffuse1, vTexCoord);
}

void main() {
	fragColor = simple();
}