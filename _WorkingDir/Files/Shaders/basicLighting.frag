#version 410

in vec4 vPosition; // P
in vec2 vTexCoord; 
in vec4 vNormal; // N
in vec3 vTangent;
in vec3 vBiTangent;
in float vLightDist;

out vec4 fragColor; 

uniform sampler2D TexDiffuse1; 
uniform sampler2D TexNormal1;
uniform sampler2D TexSpecular1; 

uniform vec3 matAmbient = vec3 (1,1,1); // kA material color
uniform vec3 matDiffuse = vec3 (1,1,1); // kD material color
//uniform vec3 matSpecular = vec3(1,1,1); // kS material color
//uniform vec3 matAmbient = vec3 (1,0,0); // kA material color
//uniform vec3 matDiffuse = vec3 (1,0,0); // kD material color
uniform vec3 matSpecular = vec3(1,0,0); // kS material color

// grey environment ambient light and white diffue light
uniform vec3 enviroAmbient = vec3(0.15f,0.15f,0.15f); // iA
uniform vec3 enviroDiffuse = vec3(1,1,1); // iD
uniform vec3 enviroSpecular = vec3(1,1,1); // iS

//uniform vec3 lightPos = vec3(0); // L
in vec3 vLightDir; // L

uniform vec3 camPos; // CamPos
uniform float iSpecPower = 32.0f;

void main() { 

	vec3 kA = texture(TexDiffuse1, vTexCoord).xyz;
	vec3 kD = texture(TexNormal1, vTexCoord).xyz;
	vec3 kS = texture(TexSpecular1, vTexCoord).rrr;
	
	vec3 Ambient = matAmbient * enviroAmbient; // ambient light

	float Ndl = max(0.0f, dot(vNormal.xyz,-vLightDir));
	
	vec3 Diffuse = matDiffuse * enviroDiffuse * Ndl;
	//Diffuse *= 5;
	
	vec3 R = reflect( vLightDir, vNormal.xyz);
	//R = vLightDir;
	vec3 E = normalize(camPos - vPosition.xyz);
	//E = camPos - vPosition.xyz;
	
	float specTerm = pow(max(0.0f, dot(R,E)),iSpecPower);
	//vec3 Specular = matSpecular * enviroSpecular * specTerm;
	vec3 Specular = kS * enviroSpecular * specTerm;
	//Specular = enviroSpecular * specTerm;
	//Specular = vec3(specTerm,specTerm,specTerm);
	
	//NORMALS
	
	mat3 TBN = mat3(normalize(vTangent),normalize(vBiTangent),normalize(vNormal));
	vec3 N = kD *2 - 1;
	
	float d = max(0, dot(normalize(TBN * N),normalize(vLightDir)));
	//fragColor = texture(TexDiffuse1, vTexCoord);
	
	//fragColor = vec4(vPosition.xyz,1);
	//fragColor = vec4(vLightDir,1);
	//fragColor = vec4(kA.xyz,1);
	//fragColor = vec4(kD.xyz,1);
	//fragColor = vec4(kS.xyz,1);
	//fragColor = vec4(Ambient + Diffuse + Specular, 1);
	//fragColor = vec4((Ambient + Diffuse) * kA, 1);
	//fragColor = vec4(Ambient + Diffuse, 1);
	//fragColor = vec4(Ambient + Diffuse * kA, 1);
	//fragColor = vec4((Ambient + Diffuse + Specular) * kA, 1);
	//fragColor = vec4((Ambient + Diffuse) * kA + Specular, 1);
	//fragColor = vec4((Ambient + Diffuse) * kA + Specular, 1) * d;
	//fragColor = vec4((Ambient + (Diffuse+ Specular)*d) * kA , 1);	
	//fragColor = vec4(Ambient, 1);
	//fragColor = vec4(Diffuse, 1);
	//fragColor = vec4(Specular, 1);
	//fragColor = vec4(vTangent, 1);
	//fragColor = vec4(Ambient * kA, 1);
	//fragColor = vec4(Diffuse * kA, 1);
	//fragColor = vec4(Specular * kA, 1);
	//fragColor = vec4(N,1);
	//fragColor = vec4(E,1);
	//fragColor = vec4(R,1);
	//fragColor = vec4(specTerm,specTerm,specTerm,1);
	//fragColor = vec4(d,d,d,1);
	//fragColor = vec4(TBN * N,1);
	//fragColor = vec4(pow(dot(E,R),iSpecPower),0,0,1);
	//fragColor = vec4(pow(dot(E,R),iSpecPower),0,0,1)*vec4(kS.rrr,1);
	//fragColor = vPosition;
	//fragColor = vec4(Ndl,Ndl,Ndl,1);
	//fragColor = vec4(vLightDist,vLightDist,vLightDist,1);
	
	fragColor = vec4((Ambient + Diffuse) * kA + Specular, 1);
}