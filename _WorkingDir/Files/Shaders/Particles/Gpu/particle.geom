#version 410

//input points, output quads
layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

//input from vertex
in vec3 position[];
in float lifetime[];
in float lifespan[];

//output to fragment
out vec4 Color;
out vec2 TexCoords;

uniform mat4 projectionViewMatrix;
uniform mat4 model = mat4(1);
uniform mat4 cameraTransform;

uniform float sizeStart;
uniform float sizeEnd;
uniform vec4 colorStart;
uniform vec4 colorEnd;

void main() {

	//interpolate color
	Color = mix(colorStart, colorEnd, lifetime[0] / lifespan[0]);
	
	//calculate size of quad
	float halfSize = mix(sizeStart, sizeEnd, lifetime[0] / lifespan[0]) * 0.5f;
	
	//create corners
	vec3 corners[4];
	corners[0] = vec3(  halfSize, -halfSize, 0);
	corners[1] = vec3(  halfSize,  halfSize, 0);
	corners[2] = vec3( -halfSize, -halfSize, 0);
	corners[3] = vec3( -halfSize,  halfSize, 0);
	
	//create billboard
	vec3 zAxis = normalize( cameraTransform[3].xyz - position[0]);
	vec3 xAxis = cross( cameraTransform[1].xyz, zAxis);
	vec3 yAxis = cross( zAxis, xAxis);
	mat3 billboard = mat3(xAxis,yAxis,zAxis);
	
	// emit vertices
	gl_Position = projectionViewMatrix*vec4(billboard*corners[0] + position[0],1);
	TexCoords = vec2(1,1);
	EmitVertex();
	
	gl_Position = projectionViewMatrix*vec4(billboard*corners[1] + position[0],1);
	TexCoords = vec2(1,0);
	EmitVertex();
	
	gl_Position = projectionViewMatrix*vec4(billboard*corners[2] + position[0],1);
	TexCoords = vec2(0,1);
	EmitVertex();
	
	gl_Position = projectionViewMatrix*vec4(billboard*corners[3] + position[0],1);
	TexCoords = vec2(0,0);
	EmitVertex();
	
	}