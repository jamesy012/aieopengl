#version 410

in vec4 Color;
in vec2 TexCoords;
out vec4 fragColor;

uniform sampler2D particleTex;

void main() {
	vec4 tex = texture(particleTex, TexCoords);
	fragColor = Color * tex;
}