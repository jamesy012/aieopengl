#version 410

layout(location=0) in vec4 position;
layout(location=1) in vec4 normal;
layout(location=2) in vec2 texCoord;
layout(location=3) in vec4 tangent;
layout(location=4) in vec4 biTangent;

uniform mat4 projectionViewMatrix;
uniform mat4 model = mat4(1);
uniform mat4 normalRot = mat4(1);

uniform vec3 lightPos = vec3(0); 
out vec3 vLightDir;

out vec4 vPosition;
out vec2 vTexCoord;
out vec4 vNormal;
out vec3 vTangent;
out vec3 vBiTangent;
out float vLightDist;


void main(){
	

	vPosition = model *  position;
	//vPosition = normalize(vPosition);
	gl_Position = projectionViewMatrix * vPosition; 

	
	vTexCoord = texCoord;
	vNormal   = normalRot * normal;
	//vLightDir = vPosition.xyz - lightPos;
	vLightDir = vPosition.xyz - lightPos;
	vLightDist = length(vLightDir)/6;
	
	vLightDir = normalize(vPosition.xyz - lightPos);
	//vLightDir = normalize(lightPos - vPosition.xyz);
	
	vTangent = tangent.xyz;
	vBiTangent = biTangent.xyz;
//	vNormal   = model * normal; // this seems to do the same as above??
	
	//vNormal = abs(normal);
	//vNormal = normal;
}