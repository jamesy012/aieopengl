// VERTEX SHADER � G-PASS
#version 410
layout(location = 0) in vec4 Position;
layout(location = 1) in vec4 Normal;
layout(location = 2) in vec2 texCoord;

// view-space normal and position
out vec4 vPosition;
out vec4 vNormal;
out vec2 vTexCoord;

uniform mat4 view; // needed in addition to Projection * View
uniform mat4 projectionViewMatrix;
uniform mat4 model = mat4(1);
uniform mat4 normalRot = mat4(1);

void main() {
// first store view-space position and normal
vPosition = view * (model *  Position);
vNormal = normalRot * Normal;
vTexCoord = texCoord;
gl_Position = projectionViewMatrix * model * Position;
}
