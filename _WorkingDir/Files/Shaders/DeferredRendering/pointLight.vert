#version 410

layout(location = 0) in vec4 Position;

uniform vec3 lightPosition;

uniform float lightRadius;

uniform mat4 projectionViewMatrix;

void main() {
	
	
	gl_Position = projectionViewMatrix * vec4(Position.xyz * lightRadius + lightPosition,1);
}