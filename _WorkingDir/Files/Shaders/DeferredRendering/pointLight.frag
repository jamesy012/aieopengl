#version 410

out vec3 lightOutput;

// direction in view-space
uniform vec3 lightPositionView;
uniform vec3 lightDiffuse;
uniform float lightRadius;

uniform sampler2D positionTexture;
uniform sampler2D normalTexture;

void main() {	

	vec2 texCoord = gl_FragCoord.xy / textureSize(positionTexture, 0).xy;

	vec3 normal = normalize( texture(normalTexture, texCoord).xyz );
	vec3 position = texture(positionTexture, texCoord).xyz;
	
	vec3 toLight = lightPositionView - position;
	
	float d = max(0, dot(normal, normalize(toLight)));
	
	float falloff = 1 - min(1, length(toLight) / lightRadius);
	
	lightOutput = lightDiffuse * d * falloff;
}