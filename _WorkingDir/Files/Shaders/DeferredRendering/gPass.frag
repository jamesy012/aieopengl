// FRAGMENT SHADER � G-PASS
#version 410
in vec4 vPosition;
in vec4 vNormal;
in vec2 vTexCoord;

uniform sampler2D TexDiffuse1; 
uniform vec3 Color = vec3(1,1,1);

layout(location = 0) out vec3 gpassAlbedo;
layout(location = 1) out vec3 gpassPosition;
layout(location = 2) out vec3 gpassNormal;

void main() {
// we simply output the data
// Note: you could use a material colour,
// or sample a texture for albedo, but this example is white
gpassAlbedo = texture(TexDiffuse1,vTexCoord).xyz * Color; 
gpassPosition = vPosition.xyz;
gpassNormal = vNormal.xyz;
//gpassAlbedo = vNormal.xyz;
}