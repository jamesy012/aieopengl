local M = {}

function M.load(path)		
	-- remove loaded scripts/packages
		package.loaded[path] = nil
	-- load script/package
		loadedPackage = require(path);
		
	return loadedPackage
end

return M