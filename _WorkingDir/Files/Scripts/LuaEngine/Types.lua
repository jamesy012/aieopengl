local M = {}

local types = {
	Transform = 1,
	Mesh = 2,
	Model = 3,
	Camera = 4, -- cameras are transforms :/
	Texture = 5,
	Material = 6,
}

function M.getType(typeName)
	local index = types[typeName];
	if index == nil then
		return 0
	end
	--print(index)
	return index
end

return M