local M = {}

-- keys indexs are all from glfw
local KeyID = {
	-- Arrow keys
	right = 262,
	left = 263,
	down = 264,
	up = 265,
	-- Other
	leftShift = 340,
	rightShift = 344,
	space = 32,
	enter = 257,
	-- Letters
	a = 65,
	b = 66,
	c = 67,
	d = 68,
	e = 69,
	f = 70,
	g = 71,		
	h = 72,
	i = 73,
	j = 74,
	k = 75,
	l = 76,
	m = 77,
	n = 78,
	o = 79,
	p = 80,
	q = 81,
	r = 82,
	s = 83,
	t = 84,
	u = 85,
	v = 86,
	w = 87,
	x = 88,
	y = 89,
	z = 90,
	-- Number Keys
	n0 = 48,
	n1 = 49,
	n2 = 50,
	n3 = 51,
	n4 = 52,
	n5 = 53,
	n6 = 54,
	n7 = 55,
	n8 = 56,
	n9 = 57,
	kp0 = 320,
	kp1 = 321,
	kp2 = 322,
	kp3 = 323,
	kp4 = 324,
	kp5 = 325,
	kp6 = 326,
	kp7 = 327,
	kp8 = 328,
	kp9 = 329,
}

local MouseID = {
 
}


function M.test()
	print("List of keys:")
	for k,v in pairs(KeyID) do
		io.write(k .. ", ")
	end
	print("")
end

function M.isMouseLocked()
	return IsMouseLocked()
end

function M.isKeyDown(a_Key)
	local id = KeyID[a_Key]
	local isDown = IsKeyDown(id)	
	return isDown
end

function M.isKeyRepeated(a_Key)
	local id = KeyID[a_Key]
	local isDown = IsKeyRepeated(id)	
	return isDown
end

function M.wasKeyPressed(a_Key)
	local id = KeyID[a_Key]
	local isDown = WasKeyPressed(id)	
	return isDown
end

function M.isMousePressed(a_MouseButton)
	local isDown = IsMousePressed(a_MouseButton)	
	return isDown
end

function M.wasMousePressed(a_MouseButton)
	local isDown = WasMousePressed(a_MouseButton)	
	return isDown
end

return M
