-- Classes
INPUT = {}

-- Variables
speed = 15
rotateSpeed = 10
totalTime = 0;
outOfScreenForce = {x = 30,y = 20}

screenSize = {x= 0,y = 0}

function Init()	
-- remove loaded scripts/packages
	package.loaded["scripts.keyTest"] = nil
-- load script/package
	INPUT = require("scripts.keyTest");
-- test function for classes
 	INPUT.test();
end

function Destroy()
end

function Update(dt)
local forceX,forceY = GetLocalForce()
forceMag = rotateSpeed*1.5;

carRotSpeed = (forceMag - forceY)
carMoveSpeed = speed

-- Input test
if(INPUT.wasKeyPressed('space'))then
	ApplyForce(-forceY*3,-forceX*3)
end

if(INPUT.isKeyDown('leftShift')) then
	carMoveSpeed = carMoveSpeed * 2
	carRotSpeed = carRotSpeed / 2
end

	if (INPUT.isKeyDown('right')) then
		ApplyRotation(carRotSpeed * dt)
	end
	if (INPUT.isKeyDown('left')) then
		ApplyRotation(-carRotSpeed * dt)
	end
	if (INPUT.isKeyDown('up')) then
		ApplyForce(carMoveSpeed * dt,0);
	end
	if (INPUT.isKeyDown('down')) then
		ApplyForce(-carMoveSpeed * dt,0);
	end
	
	local x,y = GetPostion()
	
	if x > screenSize.x then
		ApplyForceWorld(0,-outOfScreenForce.x)
	end
	if x < 0 then
		ApplyForceWorld(0,outOfScreenForce.x)
	end
	if y > screenSize.y then
		ApplyForceWorld(-outOfScreenForce.y,0)
	end
	if y < 0 then
		ApplyForceWorld(outOfScreenForce.y,0)
	end
	
	local mouseX,mouseY = GetMousePostion()
	mouseY = screenSize.y - mouseY
	
	--[[
	totalTime = totalTime + dt;
ApplyForceWorld(0,math.sin(totalTime* 5) * 1)
ApplyForceWorld(math.cos(totalTime* 5) * 1,0)
		ApplyRotation(carRotSpeed * 100 * dt)
	--]]
	--[[
	rot = GetRotation()
	if rot > 5 then
		ApplyRotation(-1 * dt)
	end
	if rot < -5 then
		ApplyRotation(1 * dt)
	end
	--]]
	--[[
	if mouseX > x then 
		ApplyForceWorld(0,5*dt)
	end
	if mouseX < x then 
		ApplyForceWorld(0,-5*dt)
	end
	if mouseY > y then 
		ApplyForceWorld(5*dt,0)
	end
	if mouseY < y then 
		ApplyForceWorld(-5*dt,0)
	end
	--]]
end

function Draw()
end 

function UpdateScreenSize(x,y)
	screenSize.x = x
	screenSize.y = y
end