-- Classes
SCRIPT_LOAD = require("scripts.LuaEngine.LoadScript")
INPUT = {}
TYPES = {}

--Camera id's
camera = {cameraId = 0, transformId = 0};

-- current id we are controlling when moving with wasd or mouse
rotateAndMoveID = 0;

--plane arrays (also contains all birds)
planeIds = {}
--which plane are we currently controlling
planesControlIndex = 0

--simple plane on the ground
groundPlane = 0;

--model array
model = {modelId = 0, transformId = 0, parentTransformId = 0, meshId = 0, materialId = 0};
--nanoSuit array
nanoSuitModel = {modelId = 0, transformId = 0, parentTransformId = 0, meshId = 0, materialId = 0};

--bird images array
birdTextures = {};

--how long was the program been going
timer = 0;
--current animation timer (goes between 0 and animationTime)
aniTimer = 0
--how long in seconds still the animation flips for the birds
animationTime = 0.2
--current deltaTime
deltaTime = 0

function Init()
	--set seed to OS time
	math.randomseed( os.time() )

	--load scripts and test if they loaded
	INPUT = SCRIPT_LOAD.load("scripts.LuaEngine.Input");
	TYPES = SCRIPT_LOAD.load("scripts.LuaEngine.Types");
	INPUT.test();
	
	--textured plane creation
	--load texture
	local birdTexture1 = ObjectCreateObject(TYPES.getType('Texture'))
	TextureLoadTexture(birdTexture1,"Textures/bird.png");
	
	local birdTexture2 = ObjectCreateObject(TYPES.getType('Texture'))
	TextureLoadTexture(birdTexture2,"Textures/bird2.png");
	
	table.insert(birdTextures, birdTexture1)
	table.insert(birdTextures, birdTexture2)
	
	--create plane
	local plane = createPlane()
	
	--set plane texture
	MaterialSetDiffuse(plane.materialId, birdTextures[1])		
	
	MaterialSetColor(plane.materialId, 0,0,1,1)

	--getting camera
	camera.cameraId = CameraGetMainCamera()
	camera.transformId = CameraGetTransform( camera.cameraId )

	rotateAndMoveID = camera.transformId
		
	--creating groundPlane 
	groundPlane = ObjectCreateObject(TYPES.getType('Mesh'))
	MeshCreatePlane(groundPlane);
	local groundPlaneTransform = ObjectGetTransform(groundPlane);
	TransformSetScale(groundPlaneTransform, 100,100,100)

	TransformSetName(groundPlaneTransform, "Ground Plane");

	local groundPlaneMaterial = MeshGetMaterial(groundPlane);
	MaterialSetColor(groundPlaneMaterial, 0.4,0.4,0.4,1)

	--transform and material are not necessary to keep around in object list
	--so we delete them from the list, they will still be apart of the groundPlane mesh
	ObjectDestroyObject(groundPlaneTransform)
	ObjectDestroyObject(groundPlaneMaterial)
end

function Destroy()
	--test destroy (it will be destroyed after this function anyway by c++) 
	ObjectDestroyObject(model.modelId);
end

function Update(dt)
	--update timers
	deltaTime = dt
	timer = timer + dt
	aniTimer = aniTimer + dt

	--move around camera
	if (INPUT.wasKeyPressed('e')) then
		rotateAndMoveID = camera.transformId
		TransformSetName(camera.transformId,"CAMERA!!! " .. camera.transformId)
	end
	--set camera transform parent to currently moveable Transform
	if (INPUT.wasKeyPressed('q')) then
		if rotateAndMoveID == camera.transformId then
			TransformRemoveParent(camera.transformId);
		else
			TransformSetParent(camera.transformId, rotateAndMoveID)
		end
	end
	--creates a simple bird plane
	if (INPUT.wasKeyPressed('v')) then
		local plane = createPlane()
		
		TransformSetName(plane.transformId,"Lua Plane " .. (tableLength(planeIds)+1))
	end
	--prints currently selected object transformId
	if (INPUT.wasKeyPressed('enter')) then
		print(rotateAndMoveID)
	end
	--loads bunny
	if(INPUT.wasKeyPressed('f')) then
		if(model.modelId == 0) then
			model.modelId = ObjectCreateObject(TYPES.getType('Model'))
			model.transformId = ObjectGetTransform(model.modelId)
			ModelLoadModel(model.modelId,"Models/stanford/", "Bunny.obj")
			
			model.meshId = ModelGetMesh(model.modelId, 0)
			model.materialId = MeshGetMaterial(model.meshId)
			
			model.parentTransformId = ObjectCreateObject(TYPES.getType('Transform'))
			TransformSetName(model.parentTransformId,"model parent")
			TransformSetParent(model.transformId, model.parentTransformId)
			
			TransformSetRotation(model.transformId,0,-90,0)
			TransformSetPosition(model.transformId,0,-4.5,0)
		end
		rotateAndMoveID = model.parentTransformId;
	end
	--changes bunny material color to a random color
	if(INPUT.isKeyDown('g')) then
		if(model.modelId ~= 0) then
			r,g,b = math.random(),math.random(),math.random()
			MaterialSetColor(model.materialId, r,g,b,1)
		end
	end
	--loads nanosuit
	if(INPUT.wasKeyPressed('t')) then
		if(nanoSuitModel.modelId == 0) then
			nanoSuitModel.modelId = ObjectCreateObject(TYPES.getType('Model'))
			nanoSuitModel.transformId = ObjectGetTransform(nanoSuitModel.modelId)
			ModelLoadModel(nanoSuitModel.modelId,"Models/nanosuit/", "nanosuit.obj")
			
			nanoSuitModel.meshId = ModelGetMesh(nanoSuitModel.modelId, 0)
			nanoSuitModel.materialId = MeshGetMaterial(nanoSuitModel.meshId)
			
			nanoSuitModel.parentTransformId = ObjectCreateObject(TYPES.getType('Transform'))
			TransformSetName(nanoSuitModel.parentTransformId,"model parent")
			TransformSetParent(nanoSuitModel.transformId, nanoSuitModel.parentTransformId)
			
			TransformSetRotation(nanoSuitModel.transformId,0,180,0)
			TransformSetPosition(nanoSuitModel.transformId,0,-10,0)
		end
		rotateAndMoveID = nanoSuitModel.parentTransformId;
	end	
	--sets camera to be perspective
	if(INPUT.isKeyDown('p')) then
		CameraSetPerspective(camera.cameraId,45,16.0/9.0,0.3,1000);
	end
	--set the camera to be orthographic
	if(INPUT.isKeyDown('o')) then
		size = 2
		width = size * 16
		height = size * 9
		CameraSetOrthograpic(camera.cameraId,-width,width,-height,height,0.3,1000);
	end

	
	
	-- increment and decrement plane index
	if (INPUT.isKeyRepeated('left')) then
		nextPlane(-1)
	end
	if (INPUT.isKeyRepeated('right')) then
		nextPlane(1)
	end

	-- latest Transform movement
	moveSpeed = 10 * dt;

	if (INPUT.isKeyDown('leftShift')) then
		x,y,z = TransformGetPosition(rotateAndMoveID)
		if (INPUT.isKeyRepeated('w')) then			
			TransformSetPosition(rotateAndMoveID,x,y,z -1)
		end
		if (INPUT.isKeyRepeated('s')) then
			TransformSetPosition(rotateAndMoveID,x,y,z + 1)
		end
		if (INPUT.isKeyRepeated('a')) then
			TransformSetPosition(rotateAndMoveID,x-1,y,z )
		end
		if (INPUT.isKeyRepeated('d')) then
			TransformSetPosition(rotateAndMoveID,x+1,y,z)
		end
		if (INPUT.isKeyRepeated('space')) then
			TransformSetPosition(rotateAndMoveID,x,y+1,z)
		end
		if (INPUT.isKeyRepeated('c')) then
			TransformSetPosition(rotateAndMoveID,x,y-1,z)
		end
	else
		if (INPUT.isKeyDown('w')) then
			TransformTranslate(rotateAndMoveID,0,0,-moveSpeed)
		end
		if (INPUT.isKeyDown('s')) then
			TransformTranslate(rotateAndMoveID,0,0,moveSpeed)
		end
		if (INPUT.isKeyDown('a')) then
			TransformTranslate(rotateAndMoveID,-moveSpeed,0,0 )
		end
		if (INPUT.isKeyDown('d')) then
			TransformTranslate(rotateAndMoveID,moveSpeed,0,0)
		end
		if (INPUT.isKeyDown('space')) then
			TransformTranslate(rotateAndMoveID,0,moveSpeed,0)
		end
		if (INPUT.isKeyDown('c')) then
			TransformTranslate(rotateAndMoveID,0,-moveSpeed,0)
		end
	end
	--mouse movement
	if IsMouseLocked() then 
		local mouseDelta = {x,y}
		mouseDelta.x,mouseDelta.y = GetMouseDelta()
		if (mouseDelta.x ~= 0 or mouseDelta.y ~= 0) then
			x,y,z = TransformGetRotation(rotateAndMoveID)
			TransformSetRotation(rotateAndMoveID,x - mouseDelta.y,y - mouseDelta.x,z)
			--print("Mouse X " .. mouseDelta.x .. " Mouse Y ".. mouseDelta.y)
		end
	end
	--look at test
	if (INPUT.isKeyDown('z')) then
		TransformLookAt(rotateAndMoveID, 0,0,0)
	end
	if(INPUT.isKeyDown('y')) then
		flapTransform(rotateAndMoveID, timer/4, false)
	end

	--plane automated movement
	for i, plane in ipairs(planeIds) do
		if i ~= planesControlIndex then
			flapTransform(plane.transformId, plane.spawnTime, true)
		end
	end
	
	--plane animation
	if aniTimer > animationTime then
		aniTimer = aniTimer - animationTime
		for i, plane in ipairs(planeIds) do
			local index = plane.aniIndex + 1
			if index > 2 then
				index = 1
			end
			MaterialSetDiffuse(plane.materialId, birdTextures[index])
			plane.aniIndex = index
		end
	end
	
end

--all objects that should be drawn
function Draw()
	--normal models first
	if(model.modelId ~= 0) then
		ObjectDraw(model.modelId);
	end
	if(nanoSuitModel.modelId ~= 0) then
		ObjectDraw(nanoSuitModel.modelId);
	end
	ObjectDraw(groundPlane);
	--transparent models last
	--need a way to change shader and turn on/off openGL elements for this to work completely
	for i, plane in ipairs(planeIds) do
		ObjectDraw(plane.id);
	end
end 

--unused (possibly broken)
function lerp(a,b,t)
	print("Time: " .. t)
	return a + (b-a) * t
end

function flapTransform( a_Transform, a_StartTime, a_Rotate)
	TransformTranslateWorld(a_Transform, 0, 0.1 * math.sin((a_StartTime + timer) * 5) , 0)
	if(a_Rotate) then
		TransformRotate(a_Transform, 1 * (math.cos((a_StartTime+ timer) * 5)),30 * deltaTime, 0)
	else
		TransformRotate(a_Transform, 1 * (math.cos((a_StartTime+ timer) * 5)), 0, 0)
	end	
	TransformTranslate(a_Transform, 0, 0 ,-5 * deltaTime)
	--lerpRes =lerp(-1,1,math.fmod(((plane.spawnTime + timer)*0.25),1));
	--TransformRotate(plane.transformId,lerpRes,0,0)
	--print(lerpRes)
end


--creates a simple plane and adds it to the planesIds table
function createPlane()
	local plane = { id, transformId, materialId, aniIndex, spawnTime }
	plane.id = ObjectCreateObject(TYPES.getType('Mesh'))
	MeshCreatePlane(plane.id)
	plane.transformId = ObjectGetTransform(plane.id)
	plane.materialId = MeshGetMaterial(plane.id)
	plane.aniIndex = 1;
	plane.spawnTime = timer
	table.insert(planeIds, plane)
	return plane;
end

--returns the amount of elements in a table
function tableLength(a_Table)
	local count = 0
	for i in pairs(a_Table) do 
		count = count + 1 
	end
	return count
end

--takes an increment and advances the planesControlIndex
--also apply's selected plane to rotateAndMoveID
function nextPlane(a_Increment) 
		length = tableLength(planeIds)
		if length == 0 then			
			planesControlIndex = 0
			--rotateAndMoveID = 0
			return;
		end

		planesControlIndex = planesControlIndex + a_Increment

		if planesControlIndex >= length+1 then
			planesControlIndex = 1
		end
		if(planesControlIndex <= 0) then
			planesControlIndex = length
		end
		
		local p = planeIds[planesControlIndex]
		rotateAndMoveID = p.transformId
		
		print("Current Plane: " .. planesControlIndex)
end
