#define CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

#include "Application.h"

//better memory leak detector
//#include <vld.h>

#define APP_1 1
#define APP_2 2
#define APP_3 3

//CHANGE THIS TO ONE OF THE DEFINCES ABOVE
#define APP_CHOICE APP_1

//PICKES 
#if APP_CHOICE == APP_1
//#include "CarRaceApp.h"
//typedef CarRaceApp CHOSEN_APP;
#elif APP_CHOICE == APP_2
#include "ObjectCreationTestApp.h"
typedef ObjectCreationTestApp CHOSEN_APP;
#elif APP_CHOICE == APP_3
#include "CarRaceApp.h"
typedef CarRaceApp CHOSEN_APP;
#else
#error GAME NOT PICKED, PICK A NUMBER ABOVE
#endif // PICKED


int main() {
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	Application* app = new CHOSEN_APP();
	app->run();
	delete app;

	return 0;
}