#pragma once

#include "Rigidbody.h"

#include <glm\glm.hpp>

class Mesh;

class Box : public Rigidbody {
public:
	Box(glm::vec3 a_Velocity, float a_Mass, float a_Width, float a_Height,float a_Length, glm::vec4 a_Color);
	~Box();

	// Inherited via Rigidbody
	virtual void makeGizmo() override;

	float getHeight() const;
	float getWidth() const;
	float getLength() const;
	glm::vec3 getSize() const;

	glm::vec3 getMax() const;

protected:
	glm::vec3 m_Size;
	glm::vec4 m_Color;

private:
	Mesh* m_Mesh = nullptr;
	glm::vec3 m_Max;
};

