#include "Sphere.h"

#include <aie\Gizmos.h>

#include "Mesh.h"

Sphere::Sphere(glm::vec3 a_Velocity, float a_Mass, float a_Radius, glm::vec4 a_Color) : Rigidbody(ShapeType::SPHERE, a_Velocity, a_Mass) {
	m_Radius = a_Radius;
	m_Color = a_Color;

	if (m_Mesh == nullptr) {
		m_Mesh = new Mesh();
		m_Mesh->createSphere();
		m_Mesh->setColor(m_Color);

		setTransform(&m_Mesh->m_Transform);
		getTransform()->setScale(a_Radius);
	}
}

Sphere::~Sphere() {
	if (m_Mesh != nullptr) {
		delete m_Mesh;
	}
}

void Sphere::makeGizmo() {

	m_Mesh->draw();

	//aie::Gizmos::addAABB(getTransform()->getGlobalPosition(), glm::vec3(m_Radius), glm::vec4(0, 0.5f, 0.5f, 1.0f));
	//aie::Gizmos::addLine(getTransform()->getGlobalPosition(), getTransform()->getGlobalPosition() + m_Velocity * 1.0f, glm::vec4(0, 1, 1, 1));
}

float Sphere::getRadius() const {
	return m_Radius;
}

glm::vec4 Sphere::getColor() const {
	return m_Color;
}
