#pragma once

#include <vector>

#include <glm\glm.hpp>

class PhysicsObject;

class PhysicsScene {
public:
	PhysicsScene();
	~PhysicsScene();

	void addActor(PhysicsObject* a_Actor);
	void RemoveActor(PhysicsObject* a_Actor);
	void update();
	void updateGizmos();
	void debugScene();

	void setGravity(const glm::vec3 a_Gravity);
	glm::vec3 getGravity() const;

	//example: 0.99f 
	void setFriction(const float a_Friction);

	float getTimeScale() const;
	void setTimeScale(const float a_TimeScale);

protected:
	glm::vec3 m_Gravity;
	std::vector<PhysicsObject*> m_Actors;

	float m_Friction = 0;

	float m_TimeScale =1.0f;

};

