#include "Rigidbody.h"

#include "Transform.h"
#include "TimeHandler.h"

#define isTransform() if( Rigidbody::m_Transform == nullptr){ return; }
#define isFrozen() if( Rigidbody::m_Frozen){ return; }


Rigidbody::Rigidbody(ShapeType a_ShapeType, glm::vec3 a_Velocity, float a_Mass) : PhysicsObject(a_ShapeType) {
	m_Velocity = a_Velocity;
	m_Mass = a_Mass;
}

Rigidbody::~Rigidbody() {
}

Transform * Rigidbody::getTransform() {
	return m_Transform;
}

void Rigidbody::setTransform(Transform * a_Transform) {
	m_Transform = a_Transform;
}

void Rigidbody::setPosition(glm::vec3 a_Position) {
	isTransform();
	m_Transform->setPosition(a_Position);
}

void Rigidbody::translate(glm::vec3 a_Position) {
	isTransform();
	m_Transform->translate(a_Position,true);
}

void Rigidbody::setRotation(float a_Rotation) {
	isTransform();
	m_Transform->setRotation(glm::vec3(0, a_Rotation, 0));
}

void Rigidbody::fixedUpdate(glm::vec3 a_Gravity, float a_TimeStep) {
	isTransform();
	isFrozen();
	m_Velocity += a_Gravity * a_TimeStep;

	translate(m_Velocity * a_TimeStep);

}

void Rigidbody::debug() {
}

glm::vec3 Rigidbody::getVelocity() const {
	return m_Velocity;
}

float Rigidbody::getMass() const {
	return m_Mass;
}

void Rigidbody::setMass(float a_NewMass) {
	m_Mass = a_NewMass;
}

void Rigidbody::setVelocity(glm::vec3 a_Velocicy) {
	m_Velocity = a_Velocicy;
}

void Rigidbody::applyForce(glm::vec3 a_Force) {
	m_Velocity += a_Force;
}

void Rigidbody::applyForceToActor(Rigidbody * a_OtherActor, glm::vec3 a_Force) {
}
