#include "PhysicsScene.h"

#include <imgui\imgui.h>
#include <aie\Gizmos.h>

#include "TimeHandler.h"
#include "Transform.h"

#include "PhysicsObject.h"
#include "CollisionCheck.h"
#include "Rigidbody.h"

#define USE_WIKIPEDIA_COLLISION
#undef USE_WIKIPEDIA_COLLISION


PhysicsScene::PhysicsScene() {
}


PhysicsScene::~PhysicsScene() {
}

void PhysicsScene::addActor(PhysicsObject * a_Actor) {
	//todo check if actor is already in array
	m_Actors.push_back(a_Actor);
}

void PhysicsScene::RemoveActor(PhysicsObject * a_Actor) {
	for (size_t i = 0; i < m_Actors.size(); i++) {
		if (m_Actors[i] == a_Actor) {
			m_Actors.erase(m_Actors.begin() + i);
			return;
		}
	}
}

void PhysicsScene::update() {
	glm::vec3 totalVel;
	//update positions
	for (size_t i = 0; i < m_Actors.size(); i++) {
		Rigidbody* rb = (Rigidbody*)m_Actors[i];
		rb->m_Velocity *= 1 - (m_Friction * m_TimeScale);
		m_Actors[i]->fixedUpdate(m_Gravity, 0.016f * m_TimeScale);
		totalVel += abs(rb->getVelocity());
	}
	ImGui::DragFloat3("Total Vel", &totalVel.x);
	//check for collisions
	for (size_t i = 0; i < m_Actors.size(); i++) {
		for (size_t q = i + 1; q < m_Actors.size(); q++) {
			Rigidbody* rbi = (Rigidbody*)m_Actors[i];
			Rigidbody* rbq = (Rigidbody*)m_Actors[q];
			if (rbi->m_Frozen && rbq->m_Frozen) {
				continue;
			}
			if (CollisionCheck::checkCollision(rbi, rbq)) {
				CollisionResultion resultion = CollisionCheck::getCollisionResultion();
				//if there was not a hit
				//tbh this should never hit anyway
				if (!resultion.didHit) {
					continue;
				}
				glm::vec3 res = resultion.mtv;
				glm::vec3 direction = resultion.forceDirection;



				//rbi->setVelocity(glm::vec3(0));
				//rbq->setVelocity(glm::vec3(0));
				glm::vec3 rbiVel = rbi->getVelocity() * 1.0f;
				glm::vec3 rbqVel = rbq->getVelocity() * 1.0f;

				glm::vec3 force = res;
				printf("COLLISION!! %i\n", TimeHandler::getCurrentFrameNumber());
				printf("\t (%f,%f) \n", res.x, res.y);

				unsigned char frozen = 0;
				if (rbi->m_Frozen) {
					frozen++;
				}
				if (rbq->m_Frozen) {
					frozen++;
				}

				//if 0 frozen
				if (frozen == 0) {


					res /= 2;
					rbi->translate(res);
					rbq->translate(-res);

#ifdef USE_WIKIPEDIA_COLLISION
					//https://en.wikipedia.org/wiki/Elastic_collision
					float totalMass = rbi->getMass() + rbq->getMass();

					glm::vec3 resVelI =
						(rbiVel * (rbi->getMass() - rbq->getMass()) + (2 * rbq->getMass() * rbqVel)) / totalMass;
					glm::vec3 resVelQ =
						(rbqVel * (rbq->getMass() - rbi->getMass()) + (2 * rbi->getMass() * rbiVel)) / totalMass;

					rbi->applyForce(resVelI);
					rbq->applyForce(resVelQ);
#else
					//rbi->applyForce(rbqVel/2.0f);
					//rbq->applyForce(rbiVel/2.0f);

					//rbi->applyForce(direction * glm::length(rbiVel) * 2.0f);
					//rbq->applyForce(-direction * glm::length(rbqVel) * 2.0f);
					//rbi->applyForce(direction * glm::length(rbqVel) * 0.5f);
					//rbq->applyForce(-direction * glm::length(rbiVel) * 0.5f);
					rbi->applyForce(-direction * 2.0f);
					rbq->applyForce(direction * 2.0f);
#endif // USE_WIKIPEDIA_COLLISION
				}
				//if 1 frozen
				if (frozen == 1) {
					Rigidbody* unfrozen = nullptr;
					Rigidbody* frozen = nullptr;
					if (rbi->m_Frozen) {
						unfrozen = rbq;
						frozen = rbi;
						unfrozen->translate(-res);
					} else {
						unfrozen = rbi;
						frozen = rbq;

						unfrozen->translate(res);

						direction *= -1;
					}
					direction *= 4.0f;



					//unfrozen->applyForce(direction * (glm::length(unfrozen->getVelocity())*2.0f));
					unfrozen->applyForce(direction);
					//unfrozen->m_Frozen = true;
				}
			}
		}
	}
}

void PhysicsScene::updateGizmos() {

	for (size_t i = 0; i < m_Actors.size(); i++) {
		m_Actors[i]->makeGizmo();
	}
}

void PhysicsScene::debugScene() {
}

void PhysicsScene::setGravity(const glm::vec3 a_Gravity) {
	m_Gravity = a_Gravity;
}

glm::vec3 PhysicsScene::getGravity() const {
	return m_Gravity;
}

void PhysicsScene::setFriction(const float a_Friction) {
	m_Friction = a_Friction;
}

float PhysicsScene::getTimeScale() const {
	return m_TimeScale;
}

void PhysicsScene::setTimeScale(const float a_TimeScale) {
	m_TimeScale = a_TimeScale;
}
