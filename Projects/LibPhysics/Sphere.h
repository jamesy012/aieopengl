#pragma once

#include "Rigidbody.h"

#include <glm\glm.hpp>

class Mesh;

class Sphere : public Rigidbody {
public:
	Sphere(glm::vec3 a_Velocity, float a_Mass, float a_Radius, glm::vec4 a_Color);
	~Sphere();

	// Inherited via Rigidbody
	virtual void makeGizmo() override;

	float getRadius() const;
	glm::vec4 getColor() const;

protected:
	float m_Radius;
	glm::vec4 m_Color;

private:
	Mesh* m_Mesh = nullptr;

};