#pragma once
#include "PhysicsObject.h"

#include <glm\glm.hpp>

class CollisionCheck;
class PhysicsScene;

class Transform;

class Rigidbody : public PhysicsObject {
	friend CollisionCheck;
	friend PhysicsScene;
public:
	Rigidbody(ShapeType a_ShapeType, glm::vec3 a_Velocity, float a_Mass);
	~Rigidbody();

	Transform* getTransform();
	void setTransform(Transform* a_Transform);

	void setPosition(glm::vec3 a_Position);
	void translate(glm::vec3 a_Position);

	void setRotation(float a_Rotation);

	// Inherited via PhysicsObject
	virtual void fixedUpdate(glm::vec3 a_Gravity, float a_TimeStep) override;
	virtual void debug() override;

	glm::vec3 getVelocity() const;
	float getMass() const;

	void setMass(float a_NewMass);

	void setVelocity(glm::vec3 a_Velocicy);
	void applyForce(glm::vec3 a_Force);
	void applyForceToActor(Rigidbody* a_OtherActor, glm::vec3 a_Force);

	bool m_Frozen = false;

protected:
	Transform* m_Transform = nullptr;
	glm::vec3 m_Velocity;
	float m_Mass;


};

