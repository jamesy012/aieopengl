#include "Box.h"

#include <aie\Gizmos.h>

#include "Mesh.h"

Box::Box(glm::vec3 a_Velocity, float a_Mass, float a_Width, float a_Height, float a_Length, glm::vec4 a_Color) : Rigidbody(ShapeType::BOX, a_Velocity, a_Mass) {

	m_Color = a_Color;
	m_Size = glm::vec3(a_Width, a_Height, a_Length);

	if (m_Mesh == nullptr) {
		m_Mesh = new Mesh();
		m_Mesh->createBox();
		m_Mesh->setColor(m_Color);

		setTransform(&m_Mesh->m_Transform);
		getTransform()->setScale(m_Size);
	}

}

Box::~Box() {
	if (m_Mesh != nullptr) {
		delete m_Mesh;
	}
}

void Box::makeGizmo() {
	m_Max = m_Mesh->getMaxVert();
	m_Mesh->draw();

	//aie::Gizmos::addAABB(getTransform()->getGlobalPosition(), getMax(), glm::vec4(1, 0, 0, 1));
}

float Box::getHeight() const {
	return m_Size.y;
}

float Box::getWidth() const {
	return m_Size.x;
}

float Box::getLength() const {
	return m_Size.z;
}

glm::vec3 Box::getSize() const {
	return m_Size;
}

glm::vec3 Box::getMax() const {
	//glm::vec3 max;

	//max = m_Mesh->getMaxVert();

	//for rotated size
	//glm::quat boxRot = m_Transform->getGlobalRotation();
	//glm::vec3 boxRight = m_Size.x * (boxRot * glm::vec3(1, 0, 0));
	//glm::vec3 boxUp = m_Size.y * (boxRot * glm::vec3(0, 1, 0));
	//glm::vec3 boxForward = m_Size.z * (boxRot * glm::vec3(0, 0, 1));
	////max = m_Size;
	//max = glm::vec3(boxRight + boxUp + boxForward);
	//return max;
	return m_Max;
}
