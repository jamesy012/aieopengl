#include "CollisionCheck.h"

#include <math.h>

#include "Transform.h"

#include "Rigidbody.h"
#include "Sphere.h"
#include "Box.h"

#define SP (Sphere*)
#define BX (Box*)

CollisionResultion m_CollisionRes;

CollisionResultion CollisionCheck::getCollisionResultion() {
	return m_CollisionRes;
}

bool CollisionCheck::checkCollision(Rigidbody * a_A, Rigidbody * a_B) {
	m_CollisionRes.didHit = false;
	if (a_A->m_Transform == nullptr) {
		return false;
	}
	if (a_B->m_Transform == nullptr) {
		return false;
	}
	switch (a_A->getType()) {
	case ShapeType::SPHERE:
	{
		return checkSphere(SP a_A, a_B);
	}
	case ShapeType::BOX:
	{
		return checkBox(BX a_A, a_B);
	}
	}
	return false;
}

void CollisionCheck::setCollisionResultion(glm::vec3 a_Mtv, glm::vec3 a_Force) {
	m_CollisionRes.didHit = true;
	m_CollisionRes.mtv = a_Mtv;
	m_CollisionRes.forceDirection = a_Force;
}

void CollisionCheck::flipCollsionResultion() {
	m_CollisionRes.mtv *= -1;
	m_CollisionRes.forceDirection *= -1;
}

bool CollisionCheck::checkSphere(Sphere * a_A, Rigidbody * a_B) {

	switch (a_B->getType()) {
	case ShapeType::SPHERE:
	{
		return sphereSphere(a_A, SP a_B);
	}
	case ShapeType::BOX:
	{
		return sphereBoxAABB(a_A, BX a_B);
	}
	}
	return false;
}

bool CollisionCheck::checkBox(Box * a_A, Rigidbody * a_B) {
	if (a_B->m_Transform == nullptr) {
		return false;
	}
	switch (a_B->getType()) {
	case ShapeType::SPHERE:
	{
		return boxSphereAABB(a_A, SP a_B);
	}
	case ShapeType::BOX:
	{
		return boxBox(a_A, BX a_B);
	}
	}
	return false;
}

bool CollisionCheck::sphereSphere(Sphere * a_A, Sphere * a_B) {
	glm::vec3 pos1 = a_A->m_Transform->getGlobalPosition();
	glm::vec3 pos2 = a_B->m_Transform->getGlobalPosition();

	float dist = glm::distance(pos1, pos2);
	float totalRadius = a_A->getRadius() + a_B->getRadius();
	float radiusDiff = totalRadius - dist;

	glm::vec3 direction = pos1 - pos2;


	if (dist < totalRadius) {
		if (dist == 0) {
			setCollisionResultion(glm::vec3(0), direction);
		} else {
			direction = glm::normalize(direction);
			glm::vec3 relVel = a_A->getVelocity() - a_B->getVelocity();
			glm::vec3 collisionVector = direction * glm::dot(relVel, direction);
			glm::vec3 forceVector = collisionVector * 1.0f / (1.0f / a_A->getMass() + 1.0f / a_B->getMass());

			glm::vec3 diff = pos1 - pos2;
			diff = glm::normalize(diff);
			diff *= radiusDiff;
			setCollisionResultion(diff, forceVector);
		}
		return true;
	}

	return false;
}

bool CollisionCheck::sphereBoxAABB(Sphere * a_A, Box * a_B) {
	bool res = boxSphereAABB(a_B, a_A);
	if (res) {
		flipCollsionResultion();
	}
	return res;
}

bool CollisionCheck::sphereBoxTEST(Sphere * a_A, Box * a_B) {
	//glm::vec3 pos1 = a_A->m_Transform->getGlobalPosition();
	//glm::vec3 pos2 = a_B->m_Transform->getGlobalPosition();
	//glm::vec3 pos2Top = pos2 + glm::vec3(0, a_B->getHeight(), 0);
	//
	//glm::quat boxRot = a_B->getTransform()->getGlobalRotation();
	//
	//glm::vec3 boxNormal = glm::normalize(boxRot * glm::vec3(0, 1, 0));
	//
	//
	//glm::vec3 pos2TopNorm = glm::normalize(pos2Top);
	//float dist = glm::length(pos2Top) * glm::dot(glm::vec3(0, 1, 0), pos2TopNorm);
	//float sphereToPlane = glm::dot(pos1, boxNormal) - dist;
	//
	//if (sphereToPlane < 0) {
	//	boxNormal *= -1;
	//	sphereToPlane *= -1;
	//}
	//float intersection = a_A->getRadius() - sphereToPlane;
	//if (intersection > 0) {
	//	m_CollisionRes.didHit = true;
	//	m_CollisionRes.direction = -boxNormal;
	//	m_CollisionRes.mtv = boxNormal * -intersection;
	//	return true;
	//}
	//
	//
	return false;
}

bool CollisionCheck::boxBox(Box * a_A, Box * a_B) {
	glm::vec3 pos1 = a_A->m_Transform->getGlobalPosition();
	glm::vec3 pos2 = a_B->m_Transform->getGlobalPosition();

	glm::quat p1BoxRot = a_A->getTransform()->getGlobalRotation();
	glm::quat p2BoxRot = a_B->getTransform()->getGlobalRotation();

	glm::vec3 p1Max = a_A->getMax();
	glm::vec3 p1Min = -p1Max + pos1;
	p1Max += pos1;

	glm::vec3 p2Max = a_B->getMax();
	glm::vec3 p2Min = -p2Max + pos2;
	p2Max += pos2;

	bool hasHit = (p1Min.x <= p2Max.x && p1Max.x >= p2Min.x) &&
		(p1Min.y <= p2Max.y && p1Max.y >= p2Min.y) &&
		(p1Min.z <= p2Max.z && p1Max.z >= p2Min.z);

	if (hasHit) {
		glm::vec3 closest;
		closest.x = glm::max(p1Min.x, glm::min(pos2.x, p1Max.x));
		closest.y = glm::max(p1Min.y, glm::min(pos2.y, p1Max.y));
		closest.z = glm::max(p1Min.z, glm::min(pos2.z, p1Max.z));

		glm::vec3 dotPos = (((pos2 - closest) * p1BoxRot) * p2BoxRot) / a_A->getSize();
		float up = glm::dot(dotPos, glm::vec3(0, 1, 0));
		float right = glm::dot(dotPos, glm::vec3(1, 0, 0));
		float forward = glm::dot(dotPos, glm::vec3(0, 0, 1));

		glm::vec3 boxNormal = glm::normalize(glm::vec3(right, up, forward));

		bool isUp = (abs(boxNormal.y) > abs(boxNormal.x) && abs(boxNormal.y) > abs(boxNormal.z));

		if (isUp) {
			boxNormal = glm::vec3(0, up, 0);
			//printf("up ");
		} else {
			bool isRight = (abs(boxNormal.x) > abs(boxNormal.z));
			if (isRight) {
				boxNormal = glm::vec3(right, 0, 0);
				//printf("right ");
			} else {
				if (forward < 0.1f) {
					boxNormal = glm::vec3(0, 1, 0);
				} else {
					boxNormal = glm::vec3(0, 0, forward);
					//printf("forward ");
				}
			}
		}

		//printf("(%f,%f,%f)\n", right, up, forward);

		boxNormal = glm::normalize(p2BoxRot * (p1BoxRot * boxNormal));

		glm::vec3 relVel = a_A->getVelocity() - a_B->getVelocity();
		glm::vec3 collisionVector = boxNormal * glm::dot(relVel, boxNormal);
		glm::vec3 forceVector = collisionVector * 1.0f / (1.0f / a_A->getMass() + 1.0f / a_B->getMass());
		
		setCollisionResultion(-boxNormal, forceVector);
		return true;
	}

	return false;
}

bool CollisionCheck::boxSphereAABB(Box * a_A, Sphere * a_B) {
	//https://developer.mozilla.org/en-US/docs/Games/Techniques/3D_collision_detection
	glm::vec3 pos1 = a_A->m_Transform->getGlobalPosition();
	glm::vec3 pos2 = a_B->m_Transform->getGlobalPosition();

	glm::vec3 direction = pos1 - pos2;

	glm::quat boxRot = a_A->getTransform()->getGlobalRotation();


	glm::vec3 max = a_A->getMax();
	max.x = abs(max.x);
	max.y = abs(max.y);
	max.z = abs(max.z);
	glm::vec3 min = -max + pos1;
	max += pos1;


	glm::vec3 closest;
	closest.x = glm::max(min.x, glm::min(pos2.x, max.x));
	closest.y = glm::max(min.y, glm::min(pos2.y, max.y));
	closest.z = glm::max(min.z, glm::min(pos2.z, max.z));

	float distanceSqr = sqrt(
		((closest.x - pos2.x) * (closest.x - pos2.x)) +
		((closest.y - pos2.y) * (closest.y - pos2.y)) +
		((closest.z - pos2.z) * (closest.z - pos2.z)));

	float diff = a_B->getRadius() - distanceSqr;
	diff += 0.05f;



	if (diff > 0) {
		glm::vec3 dotPos = ((pos2 - closest) * boxRot) / a_A->getSize();
		float up = glm::dot(dotPos, glm::vec3(0, 1, 0));
		float right = glm::dot(dotPos, glm::vec3(1, 0, 0));
		float forward = glm::dot(dotPos, glm::vec3(0, 0, 1));

		glm::vec3 boxNormal = glm::normalize(glm::vec3(right, up, forward));

		bool isUp = (abs(boxNormal.y) > abs(boxNormal.x) && abs(boxNormal.y) > abs(boxNormal.z));

		if (isUp) {
			boxNormal = glm::vec3(0, up, 0);
			//printf("up ");
		} else {
			bool isRight = (abs(boxNormal.x) > abs(boxNormal.z));
			if (isRight) {
				boxNormal = glm::vec3(right, 0, 0);
				//printf("right ");
			} else {
				if (forward < 0.1f) {
					boxNormal = glm::vec3(0, 1, 0);
				} else {
					boxNormal = glm::vec3(0, 0, forward);
					//printf("forward ");
				}
			}
		}

		//printf("(%f,%f,%f)\n", right, up, forward);

		boxNormal = glm::normalize(boxRot * boxNormal);

		//glm::vec3 forceVector = a_A->getMass() * boxNormal * glm::dot(boxNormal, a_A->getVelocity());
		glm::vec3 relVel = a_A->getVelocity() - a_B->getVelocity();
		glm::vec3 collisionVector = boxNormal * glm::dot(relVel, boxNormal);
		glm::vec3 forceVector = collisionVector * 1.0f / (1.0f / a_A->getMass() + 1.0f / a_B->getMass());

		setCollisionResultion(boxNormal * -diff, forceVector);
		return true;
	}


	return false;
}
