#pragma once

#include <glm\glm.hpp>

enum class ShapeType {
	PLANE,
	SPHERE,
	BOX
};

class PhysicsObject {
protected:
	PhysicsObject(ShapeType a_ShapeType) : m_ShapeType(a_ShapeType) {};
public:

	virtual void fixedUpdate(glm::vec3 a_Gravity, float a_TimeStep) = 0;
	virtual void debug() = 0;
	virtual void makeGizmo() = 0;
	virtual void resetPosition() {};

	ShapeType getType() const;

protected:
	ShapeType m_ShapeType;
};