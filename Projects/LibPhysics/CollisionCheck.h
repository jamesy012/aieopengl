#pragma once

#include <glm\glm.hpp>

class Rigidbody;
class Sphere;
class Box;

struct CollisionResultion {
	bool didHit = false;
	glm::vec3 mtv;
	glm::vec3 forceDirection;
};

class CollisionCheck {
public:
	static CollisionResultion getCollisionResultion();

	//checks if there was a collision between two objects
	static bool checkCollision(Rigidbody* a_A, Rigidbody* a_B);
private:
	static void setCollisionResultion(glm::vec3 a_Mtv, glm::vec3 a_Direction);
	static void flipCollsionResultion();

	static bool checkSphere(Sphere* a_A, Rigidbody* a_B);
	static bool checkBox(Box* a_A, Rigidbody* a_B);

	//sphere collisions
	static bool sphereSphere(Sphere* a_A, Sphere* a_B);
	static bool sphereBoxAABB(Sphere* a_A, Box* a_B);
	static bool sphereBoxTEST(Sphere* a_A, Box* a_B);

	//box collisions
	static bool boxBox(Box* a_A, Box* a_B);
	static bool boxSphereAABB(Box* a_A, Sphere* a_B);
};

