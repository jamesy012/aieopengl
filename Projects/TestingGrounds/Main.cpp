#define CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

//better memory leak detector
//#include <vld.h>

#include "WaterGridApp.h"

int main() {
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	Application* app = new WaterGridApp();
	app->run();
	delete app;



	return 0;
}