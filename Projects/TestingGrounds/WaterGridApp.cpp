#include "WaterGridApp.h"

#include <time.h>

#include <imgui\imgui.h>
#include <GLFW\glfw3.h>

#include "Input.h"

#include "Mesh.h"
#include "Shader.h"

float randomValue() {
	return (rand() % 1000) / 1000.0f;
}

void WaterGridApp::startUp() {
	srand((unsigned int)time(NULL));
	m_WaterMesh = new Mesh();
	m_WaterMesh->createGrid(60, 60);
	m_WaterMesh->m_Transform.setScale(glm::vec3(10));

	m_TestMesh = new Mesh();
	m_TestMesh->createBox();
	m_TestMesh->m_Transform.translate(glm::vec3(5, 0, 0));

	m_BasicShader = new Shader();
	m_BasicShader->createBasicProgram();

	m_WaterShader = new Shader();
	loadShader();

	setUpWaves();
}

void WaterGridApp::shutDown() {
	delete m_WaterMesh;
	delete m_TestMesh;

	delete m_BasicShader;
	delete m_WaterShader;
}

void WaterGridApp::update() {
	m_Camera->update();

	ImGui::Checkbox("Wire,", &m_Wireframe);
	if (ImGui::Button("Wire frame")) {
		m_Wireframe ^= 1;
	}

	ImGui::SliderFloat("waterSize#1", &m_WaterSize, -200, 200);
	ImGui::SliderFloat("waterSize#2", &m_WaterSize, -10, 10);
	ImGui::SliderFloat("waterHeight", &m_WaterHeight, 0, 25);
	waveGui();

	if (Input::wasKeyPressed(GLFW_KEY_G)) {
		setUpWaves();
	}
	if (Input::wasKeyPressed(GLFW_KEY_R)) {
		loadShader();
	}
}

void WaterGridApp::draw() {
	if (m_Wireframe) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	} else {
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	m_BasicShader->useProgram();
	m_BasicShader->setUniformProjectionView(m_Camera);

	m_TestMesh->draw();

	m_WaterShader->useProgram();
	m_WaterShader->setUniformProjectionView(m_Camera);
	m_WaterShader->setUniformTime();

	m_WaterShader->setUniform("waterSize", m_WaterSize);
	m_WaterShader->setUniform("waterHeight", m_WaterHeight);
	//m_WaterShader->setUniform("waterPosition", m_WaterMesh->m_Transform.getGlobalPosition());
	applyWave();

	m_WaterMesh->draw();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void WaterGridApp::loadShader() {
	m_WaterShader->deleteShader();
	std::string vertexShader;
	vertexShader += m_WaterShader->loadShaderFromFile("Shaders/water.vert");

	//insert the define for number of waves just after the version
	//version must be first
	//in water.vert there are new lines between version and the first layout
	vertexShader.insert(16, "#define AMOUNT_WAVES " + std::to_string(AMOUNT_WAVES) + " \n\n");

	m_WaterShader->setVertexShader(vertexShader.c_str());
	m_WaterShader->setFragmentShaderFromFile("Shaders/water.frag");
	m_WaterShader->linkShader();
}

void WaterGridApp::applyWave(unsigned int a_Index) {
	m_WaterShader->setUniform(std::string("waves[" + std::to_string(a_Index) + "].offset").c_str(), m_Waves[a_Index].offset.x, m_Waves[a_Index].offset.y);
	m_WaterShader->setUniform(std::string("waves[" + std::to_string(a_Index) + "].speed").c_str(), m_Waves[a_Index].speed.x, m_Waves[a_Index].speed.y);
	m_WaterShader->setUniform(std::string("waves[" + std::to_string(a_Index) + "].waveLength").c_str(), m_Waves[a_Index].waveLength.x, m_Waves[a_Index].waveLength.y);
	m_WaterShader->setUniform(std::string("waves[" + std::to_string(a_Index) + "].amplitude").c_str(), m_Waves[a_Index].amplitude.x, m_Waves[a_Index].amplitude.y);
}

void WaterGridApp::applyWave() {
	for (int i = 0; i < AMOUNT_WAVES; i++) {
		applyWave(i);
	}
}

void WaterGridApp::setUpWaves() {
	for (int i = 0; i < AMOUNT_WAVES; i++) {
		//m_Waves[i].offset.x = float(rand() % 1000);
		//m_Waves[i].offset.y = float(rand() % 1000);
		//m_Waves[i].speed.x = -10.0f + (randomValue() * 20.0f);
		//m_Waves[i].speed.y = -10.0f + (randomValue() * 20.0f);
		//m_Waves[i].waveLength.x = 0.5f + randomValue() * 10.0f;
		//m_Waves[i].waveLength.y = 0.5f + randomValue() * 10.0f;
		//m_Waves[i].amplitude.x = 0.03f + randomValue() * 0.30f;
		//m_Waves[i].amplitude.y = 0.03f + randomValue() * 0.30f;
		m_Waves[i].offset.x = float(rand() % 1000);
		m_Waves[i].offset.y = float(rand() % 1000);
		m_Waves[i].speed.x = -50.0f + (randomValue() * 100.0f);
		m_Waves[i].speed.y = -50.0f + (randomValue() * 100.0f);
		m_Waves[i].waveLength.x = 3.0f + randomValue() * 50.0f;
		m_Waves[i].waveLength.y = 3.0f + randomValue() * 50.0f;
		m_Waves[i].amplitude.x = -1.25f + randomValue() * 2.45f;
		m_Waves[i].amplitude.y = -1.25f + randomValue() * 2.45f;
	}
}

void WaterGridApp::waveGui() {
	for (int i = 0; i < AMOUNT_WAVES; i++) {
		std::string id = std::to_string(i);
		std::string wave = "WAVE! " + id;
		if (ImGui::TreeNodeEx(wave.c_str())) {
			{
				std::string text = "offset#" + id;
				ImGui::DragFloat2(text.c_str(), &m_Waves[i].offset.x);
				text = "waveLength#" + id;
				ImGui::DragFloat2(text.c_str(), &m_Waves[i].waveLength.x);
				text = "amplitude#" + id;
				ImGui::SliderFloat2(text.c_str(), &m_Waves[i].amplitude.x, -1, 1);
				text = "speed#" + id;
				ImGui::DragFloat2(text.c_str(), &m_Waves[i].speed.x);
			}
			ImGui::TreePop();
		}
	}
}
