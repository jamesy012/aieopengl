#pragma once
#include "Application.h"

#include <glm\glm.hpp>

class Mesh;
class Shader;

class WaterGridApp : public Application {
public:

	// Inherited via Application
	virtual void startUp() override;
	virtual void shutDown() override;
	virtual void update() override;
	virtual void draw() override;

private:
	void loadShader();

	Mesh* m_WaterMesh;
	Mesh* m_TestMesh;

	Shader* m_BasicShader;
	Shader* m_WaterShader;

	bool m_Wireframe = false;

	//uniform values
	float m_WaterSize = 8;
	float m_WaterHeight = 1;
	struct Wave {
	public:
		glm::vec2 offset;
		glm::vec2 waveLength;
		glm::vec2 amplitude;
		glm::vec2 speed;
	};
#define AMOUNT_WAVES 5
	Wave m_Waves[AMOUNT_WAVES];
	void applyWave(unsigned int a_Index);
	void applyWave();
	void setUpWaves();
	void waveGui();
};

