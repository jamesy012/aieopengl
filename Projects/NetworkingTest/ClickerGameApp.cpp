#include "ClickerGameApp.h"

#include <string>
#include <iostream>

#include "Application.h"
#include "Gui.h"

#include "Client.h"
#include "Server.h"

#include "ClickerClient.h"
#include "ClickerServer.h"

ClickerGameApp::ClickerGameApp() {

	std::string input;
	std::cout << "Run as (s)erver or (c)lient?\n";
	std::cin >> input;

	if (input[0] == 's' || input[0] == 'S') {
		m_Client = false;
		m_ClickerServer = new ClickerServer();
	} else {
		m_ClickerClient = new ClickerClient();

		Gui::m_ShowHierachy = false;
		m_ClickerClient->run();
	}

}


ClickerGameApp::~ClickerGameApp() {
	if (m_Client) {
		delete m_ClickerClient;
	} else {
		delete m_ClickerServer;
	}

	}