#pragma once

#include "PacketHandling.h"
#include "ClickerCommon.h"

class Server;
class ClickerServerPacket;

class ClickerServer {
public:
	ClickerServer();
	~ClickerServer();

	void userClicked(unsigned int a_Power);

	void sendClickerDataToUsers();
	void sendClickerDataToUser(RakNet::AddressOrGUID a_User);

	void sendEnemyDataToUsers();
	void sendEnemyDataToUser(RakNet::AddressOrGUID a_User);

	//will clear the screen and place information
	void updateText();
	//information about current enemy
	EnemyData m_EnemyData;

	//number of clients connected to the server
	unsigned int m_ClientsConnected = 0;
private:
	//will generate a enemy for the clients
	void generateEnemy();

	void listConnectionIP();

	//reference to the Networking object
	Server* m_Server;
	//the number that is incremented by clients
	unsigned int m_ClickerNumber = 0;
	//packet handler for the server
	ClickerServerPacket* m_Packet;

	//number of enemy's that have been defeated
	unsigned int m_EnemysKilled = 0;
};

class ClickerServerPacket : public PacketHandling {
public:
	//reference to the ClickerServer object
	ClickerServer* m_Server;
	// Inherited via PacketHandling
	virtual void packetReceived(FUNCTIONPARAMATERS) override;
	virtual void pongReceived(RakNet::Packet * a_Packet, uint32_t a_Time, std::string a_Text) override;
};


