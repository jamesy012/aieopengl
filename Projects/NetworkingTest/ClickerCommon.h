#pragma once

#include <string>

#include <MessageIdentifiers.h>

#define SERVERPORT 5534

enum class EnemyImageData {
	Ghost,
	Morty,
	Spider,
	TEXTURE_ENDS
};

enum UserGameMessages {
	ID_STARTMESSAGE = ID_USER_PACKET_ENUM + 1,
	ID_USER_CLICKED,
	ID_SERVER_UPDATE_NUMBER,
	ID_SERVER_SEND_ENEMY,
	ID_SERVER_UPDATE_ENEMY_HEALTH,
	ID_CLIENT_QUIT,
};

struct EnemyData {
	//name for the enemy
	std::string name;

	//texture ID
	unsigned int enemyTextureId;

	//color information
	unsigned int enemyRed;
	unsigned int enemyGreen;
	unsigned int enemyBlue;

	//enemy health data
	unsigned int maxHealth;

	//the number the clicker was at when this was created
	unsigned int clickerDataAtCreation;



};