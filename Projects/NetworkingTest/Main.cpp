#define CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

//better memory leak detector
//#include <vld.h>

#define PINGTEST 0
#define CLICKERGAME 1

#define PROGRAM_TO_RUN CLICKERGAME

#if PROGRAM_TO_RUN == PINGTEST
#include "PingTest.h"
#endif // PROGRAM_TO_RUN == PINGTEST

#if PROGRAM_TO_RUN == CLICKERGAME
#include "ClickerGameApp.h"
#endif // PROGRAM_TO_RUN == CLICKERGAME


int main() {
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

#if PROGRAM_TO_RUN == PINGTEST
	PingTest pt;
	pt.start();
#endif // PROGRAM_TO_RUN == PINGTEST

#if PROGRAM_TO_RUN == CLICKERGAME
	ClickerGameApp cga;
#endif // PROGRAM_TO_RUN == CLICKERGAME




	return 0;
}