#include "ClickerClient.h"

#include <string>
#include <iostream>
#include <vector>
#include <sstream>

#include <imgui\imgui.h>
#include <GLFW\glfw3.h>
#include <glm\glm.hpp>

#include <RakNetTypes.h>//PACKET
#include <MessageIdentifiers.h>
#include <BitStream.h>

#include "Input.h"
#include "Mesh.h"
#include "Shader.h"
#include "Texture.h"
#include "Window.h"

#include "Client.h"



ClickerClient::ClickerClient() {
	m_Client = new Client();
	m_Client->startup();

	m_Packet = new ClickerClientPacket();
	m_Packet->m_Client = this;

	std::string input;

	//get IP??
	//loop until connected
	while (!m_Connected) {
		std::cout << "Enter 'lan' to find local games." << std::endl << "Or enter the ip" << std::endl;
		std::cin >> input;
		if (input == "lan") {
			m_Client->pingServerOnPort(SERVERPORT);
			break;//we will connect later after we have got the ping/pong back
		} else {
			m_Connected = m_Client->connect(input.c_str(), SERVERPORT);
		}
	}

	//if (m_Connected) {
	m_Client->startNetworkThread(m_Packet);
	//}

	m_HandleMouseLock = false;


}


ClickerClient::~ClickerClient() {
	//tell server of disconnect!
	m_Client->startMessage(UserGameMessages::ID_CLIENT_QUIT);
	m_Client->sendMessage();

	delete m_Client;
	delete m_Packet;

	for (int i = 0; i < (int)EnemyImageData::TEXTURE_ENDS; i++) {
		delete m_Textures[i];
	}
	delete m_EnemyMesh;
	delete m_EnemyShader;
}

void ClickerClient::startUp() {
	//update camera
	m_Camera->setLookAt(glm::vec3(0, 0, 1), glm::vec3(0));
	m_PerspetiveOrthographic = true;
	screenSizeChanged();//update camera Perspetive

	for (int i = 0; i < (int)EnemyImageData::TEXTURE_ENDS; i++) {
		m_Textures[i] = new Texture();
	}

	m_Textures[(int)EnemyImageData::Ghost]->loadTexture("Textures/_ClickerGame/ghost.png");
	m_Textures[(int)EnemyImageData::Morty]->loadTexture("Textures/_ClickerGame/morty.png");
	m_Textures[(int)EnemyImageData::Spider]->loadTexture("Textures/_ClickerGame/spider.png");

	m_EnemyMesh = new Mesh();
	m_EnemyMesh->createPlane();
	m_EnemyMesh->m_Transform.setRotation(glm::vec3(90, 0, 0));
	float size = 10.0f;
	m_EnemyMesh->m_Transform.setScale(glm::vec3(size * 9, 1, size * 16));
	m_EnemyMesh->m_Transform.setPosition(glm::vec3(Window::getWidth() * 0.5f, Window::getHeight() * 0.35f,0));

	m_EnemyMesh->setDiffuse(m_Textures[(int)EnemyImageData::Ghost]);
	m_EnemyMesh->setColor(glm::vec4(1, 0, 1, 1));

	m_EnemyShader = new Shader();
	m_EnemyShader->createBasicProgram(true);
}

void ClickerClient::shutDown() {
}

void ClickerClient::update() {
	bool dontUpdateClick = false;

	if (!m_Connected) {
		ImGui::SetNextWindowPos(ImVec2(100, 100));
		ImGui::Begin("SERVER SELECT",0,ImGuiWindowFlags_AlwaysAutoResize);

		int serverCounter = 0;
		for (auto const &server : m_LanServers) {
			serverCounter++;

			ImGui::Text(server.second.Ip.c_str());
			ImGui::SameLine();
			ImGui::Text(server.second.text.c_str());
			ImGui::SameLine();
			std::string buttonName = "Join server:" + std::to_string(serverCounter);
			if (ImGui::Button(buttonName.c_str())) {
				//joins the server
				m_Connected = m_Client->connect(server.second.Ip.c_str(), SERVERPORT);
				m_UserClicks = 0;
			}
		}

		if (serverCounter == 0) {
			ImGui::Text("No servers found!");
		}

		if (ImGui::Button("Refresh")) {
			m_LanServers.clear();
			m_Client->pingServerOnPort(SERVERPORT);
		}

		ImGui::End();
		return;
	} else {
		{
			ImGui::SetNextWindowPos(ImVec2(100, 100));
			ImGui::Begin("CLICKER NUMBER", 0, ImGuiWindowFlags_AlwaysAutoResize);

			std::string number = "Clicker Number: " + std::to_string(m_TotalClicks);
			ImGui::Text(number.c_str());

			number = "Your Clicks: " + std::to_string(m_UserClicks);
			ImGui::Text(number.c_str());

			ImGui::Text("");
			ImGui::Text("Press Space to clicker!");
			ImGui::Text("Or click here -> ");
			ImGui::SameLine();
			ImGui::Button("Click me!");//can click anywhere on the window
			
			ImGui::End();
		}
		{
			ImGui::SetNextWindowPos(ImVec2(400, 100));
			ImGui::Begin("ENEMY!!!", 0, ImGuiWindowFlags_AlwaysAutoResize);

			ImGui::Text("Name: ");
			ImGui::SameLine();
			ImGui::Text(m_EnemyData.name.c_str());

			unsigned int health;
			health = m_TotalClicks - m_EnemyData.clickerDataAtCreation;
			if (health > m_EnemyData.maxHealth-1) {
				ImGui::Text("Enemy Defeated!!");
			} else {
				std::string healthString = "Health " + std::to_string(m_EnemyData.maxHealth - health) + " / " + std::to_string(m_EnemyData.maxHealth);
				ImGui::Text(healthString.c_str());
			}

			ImGui::End();
		}
		{
			ImGui::SetNextWindowPos(ImVec2(100, 250));
			ImGui::Begin("Store:", 0, ImGuiWindowFlags_AlwaysAutoResize);
			
			ImGui::Text("Upgrade Click Power. Current: ");
			ImGui::SameLine();
			ImGui::Text(std::to_string(getClickPower()).c_str());
			ImGui::SameLine();
			ImGui::Text(" Price: ");
			ImGui::SameLine();
			ImGui::Text(std::to_string(m_PurchasePrice).c_str());
			ImGui::SameLine();
			if (ImGui::Button("Buy")) {
				clickerPowerUpgrade();
			}
			if (ImGui::IsItemHovered()) {
				dontUpdateClick = true;
			}
			
			ImGui::End();
		}
	}


	//did the user click??
	if (Input::wasKeyPressed(GLFW_KEY_SPACE)) {
		userClicked();
	}

	if (!dontUpdateClick) {
		if (Input::wasMousePressed(0)) {
			userClicked();
		}
	}
}

void ClickerClient::draw() {
	if (!m_Connected) {
		return;
	}

	//todo draw?!
	m_EnemyShader->useProgram();
	m_EnemyShader->setUniformProjectionView(m_Camera);


	m_EnemyMesh->draw();
}

void ClickerClient::updateClickerNumber(unsigned int a_Number) {
	m_TotalClicks = a_Number;
}

void ClickerClient::updateEnemy() {
	m_EnemyMesh->setDiffuse(m_Textures[m_EnemyData.enemyTextureId]);
	glm::vec4 color;
	//color is sent by 3 chars, convert to 0.0f-1.0f
	color.r = m_EnemyData.enemyRed / 255.0f;
	color.g = m_EnemyData.enemyGreen / 255.0f;
	color.b = m_EnemyData.enemyBlue / 255.0f;
	color.a = 1;
	m_EnemyMesh->setColor(color);
}

void ClickerClient::userClicked() {
	m_UserClicks += getClickPower();

	m_Client->startMessage(UserGameMessages::ID_USER_CLICKED);
	m_Client->addToMessage(getClickPower());
	m_Client->sendMessage();
}

unsigned int ClickerClient::getClickPower() {
	return 1 + (m_PurchaseAmount * 4);
}

void ClickerClient::clickerPowerUpgrade() {
	if (m_UserClicks >= m_PurchasePrice) {
		m_UserClicks -= m_PurchasePrice;
		m_PurchaseAmount++;
		m_PurchasePrice = 50 + (int)(m_PurchaseAmount*72.25f);
	}
}

void ClickerClientPacket::packetReceived(FUNCTIONPARAMATERS) {
	switch (*a_MessageID) {
		case ID_SERVER_UPDATE_NUMBER:
			{
				unsigned int number;
				a_BitStream->Read(number);
				m_Client->updateClickerNumber(number);
				break;
			}
		case ID_SERVER_SEND_ENEMY:
			{
				char* text = new char[100];
				a_BitStream->Read(text);
				m_Client->m_EnemyData.name = text;
				a_BitStream->Read(m_Client->m_EnemyData.maxHealth);
				a_BitStream->Read(m_Client->m_EnemyData.clickerDataAtCreation);
				a_BitStream->Read(m_Client->m_EnemyData.enemyTextureId);
				a_BitStream->Read(m_Client->m_EnemyData.enemyRed);
				a_BitStream->Read(m_Client->m_EnemyData.enemyGreen);
				a_BitStream->Read(m_Client->m_EnemyData.enemyBlue);

				m_Client->updateEnemy();

				delete text;
				break;
			}
	}
}

void ClickerClientPacket::pongReceived(RakNet::Packet * a_Packet, uint32_t a_Time, std::string a_Text) {
	a_Packet->systemAddress.ToString();
	std::cout << "PONG!! from " << a_Packet->systemAddress.ToString() << " " << a_Time << "ms | " << a_Text << std::endl;

	PotentialServers ps;

	std::vector<std::string> strings;
	//split 
	std::istringstream f(a_Packet->systemAddress.ToString());
	std::string s;
	while (getline(f, s, '|')) {
		strings.push_back(s);
	}

	ps.Ip = strings[0];
	ps.text = a_Text;

	m_Client->m_LanServers[strings[0].c_str()] = ps;
}