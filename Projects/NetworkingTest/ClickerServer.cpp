#include "ClickerServer.h"

#include <iostream>
#include <random>
#include <map>
#include <vector>

#include <RakNetTypes.h>//PACKET
#include <RakPeerInterface.h>
#include <BitStream.h>
#include <GetTime.h>

#include <imgui\imgui.h>

#include "Server.h"

#define EnemyAmountSize 8
//list of names
EnemyData EnemyStorage[EnemyAmountSize] = {
	{"Garry",(int)EnemyImageData::Ghost,255,255,255},
	{"Morty",(int)EnemyImageData::Morty,255,255,255},
	{"Barry",(int)EnemyImageData::Ghost,128,255,255},
	{"Terry",(int)EnemyImageData::Ghost,128,128,0},
	{"Larry",(int)EnemyImageData::Ghost,0,128,255},
	{"Rory", (int)EnemyImageData::Ghost,0,255,255},
	{"Corny",(int)EnemyImageData::Ghost,255,255,128},
	{"Bird eating spider",(int)EnemyImageData::Spider,255,255,255 },
};
//name index, number of picks for that name
std::map<int, int> EnemyNamePicks;

ClickerServer::ClickerServer() {
	srand(RakNet::GetTimeMS());

	//server setup
	m_Server = new Server();
	bool serverStart = m_Server->startup(32, SERVERPORT);

	if (!serverStart) {
		std::cout << "Server could not start!";
		system("pause");
		return;
	}

	//packet data
	m_Packet = new ClickerServerPacket();
	m_Packet->m_Server = this;

	//server data
	m_ClickerNumber = 0;
	generateEnemy();

	//start networking
	m_Server->startNetworkThread(m_Packet);


	updateText();

	//tell user everything is fine
	std::cout << std::endl << "SERVER HAS STARTED" << std::endl;

	unsigned int time = 0;

	//make sure the program doesn't close!
	while (true) {
		if (time + 10 < RakNet::GetTimeMS()) {
			time = RakNet::GetTimeMS();
			m_Server->setOfflinePingResponce("Clicker: " + std::to_string(m_ClickerNumber));
		}
	}
}


ClickerServer::~ClickerServer() {
	delete m_Server;
	delete m_Packet;
}

void ClickerServer::userClicked(unsigned int a_Power) {
	m_ClickerNumber += a_Power;
	sendClickerDataToUsers();

	if (int(m_ClickerNumber - m_EnemyData.maxHealth) > int(m_EnemyData.clickerDataAtCreation)) {
		m_EnemysKilled++;
		generateEnemy();
		sendEnemyDataToUsers();
	}

	updateText();
}

void ClickerServer::sendClickerDataToUsers() {
	m_Server->startMessage(UserGameMessages::ID_SERVER_UPDATE_NUMBER);
	m_Server->addToMessage(m_ClickerNumber);
	m_Server->sendMessage();
}

void ClickerServer::sendClickerDataToUser(RakNet::AddressOrGUID a_User) {
	m_Server->startMessage(UserGameMessages::ID_SERVER_UPDATE_NUMBER);
	m_Server->addToMessage(m_ClickerNumber);
	m_Server->sendMessage(&a_User);
}

void ClickerServer::sendEnemyDataToUsers() {
	m_Server->startMessage(UserGameMessages::ID_SERVER_SEND_ENEMY);
	m_Server->addToMessage(m_EnemyData.name);
	m_Server->addToMessage(m_EnemyData.maxHealth);
	m_Server->addToMessage(m_EnemyData.clickerDataAtCreation);
	m_Server->addToMessage(m_EnemyData.enemyTextureId);
	m_Server->addToMessage(m_EnemyData.enemyRed);
	m_Server->addToMessage(m_EnemyData.enemyGreen);
	m_Server->addToMessage(m_EnemyData.enemyBlue);
	m_Server->sendMessage();
}

void ClickerServer::sendEnemyDataToUser(RakNet::AddressOrGUID a_User) {
	m_Server->startMessage(UserGameMessages::ID_SERVER_SEND_ENEMY);
	m_Server->addToMessage(m_EnemyData.name);
	m_Server->addToMessage(m_EnemyData.maxHealth);
	m_Server->addToMessage(m_EnemyData.clickerDataAtCreation);
	m_Server->addToMessage(m_EnemyData.enemyTextureId);
	m_Server->addToMessage(m_EnemyData.enemyRed);
	m_Server->addToMessage(m_EnemyData.enemyGreen);
	m_Server->addToMessage(m_EnemyData.enemyBlue);
	m_Server->sendMessage(&a_User);
}

void ClickerServer::updateText() {
	system("cls");
	std::cout << "SERVER IPS" << std::endl;
	listConnectionIP();
	std::cout << std::endl;
	std::cout << "SERVER STATS" << std::endl;
	std::cout << "ClickerNumber: " << m_ClickerNumber << std::endl;
	std::cout << "ClientsConnected: " << m_ClientsConnected << std::endl;
}

void ClickerServer::generateEnemy() {
	//pick from vector above
	int enemyChoose = rand() % EnemyAmountSize;
	EnemyNamePicks[enemyChoose]++;

	//copy enemy
	EnemyData ed = EnemyStorage[enemyChoose];

	//change name
	ed.name += " Number " + std::to_string(EnemyNamePicks[enemyChoose]);

	//enemy health
	unsigned int healthMultiplyer = m_EnemysKilled + 1;//+1 to stop divide by 0
	float zeroToOne = (rand() % 1000) / 1000.0f;
	ed.maxHealth = 5 + (healthMultiplyer * 5) + (int)(zeroToOne * healthMultiplyer * 10);
	ed.enemyRed = ed.enemyGreen = ed.enemyBlue = 255;

	//color
	ed.enemyRed = EnemyStorage[enemyChoose].enemyRed;
	ed.enemyGreen = EnemyStorage[enemyChoose].enemyGreen;
	ed.enemyBlue = EnemyStorage[enemyChoose].enemyBlue;

	//other data
	ed.clickerDataAtCreation = m_ClickerNumber;

	//update enemy
	m_EnemyData = ed;
}

void ClickerServer::listConnectionIP() {
	//gets all Ip's
	const char* ip;
	int index = 0;
	while (true) {
		ip = m_Server->getPeerInterafce()->GetLocalIP(index);
		if (ip[0] > '9' || ip[0] < '0') {//if not a number, break
			break;
		}
		std::cout << "\t" << index << "\t" << ip << std::endl;
		index++;
	}
}

void ClickerServerPacket::packetReceived(FUNCTIONPARAMATERS) {
	switch (*a_MessageID) {
	case ID_USER_CLICKED:
	{
		unsigned int clickPower;
		a_BitStream->Read(clickPower);
		m_Server->userClicked(clickPower);
		break;
	}
	case ID_NEW_INCOMING_CONNECTION:
	{
		//only send this to the new user??!!?
		m_Server->sendClickerDataToUser(a_Packet->guid);
		m_Server->m_ClientsConnected++;
		m_Server->sendEnemyDataToUser(a_Packet->guid);

		m_Server->updateText();
		break;
	}
	case ID_DISCONNECTION_NOTIFICATION:
	case ID_CONNECTION_LOST:
	case UserGameMessages::ID_CLIENT_QUIT:
	{
		m_Server->m_ClientsConnected--;

		m_Server->updateText();
		break;
	}
	}
}

void ClickerServerPacket::pongReceived(RakNet::Packet * a_Packet, uint32_t a_Time, std::string a_Text) {
}
