#pragma once
#include "Application.h"

#include <string>
#include <map>

#include "PacketHandling.h"

#include "ClickerCommon.h"

class Client;
class ClickerClientPacket;

class Mesh;
class Shader;
class Texture;

struct PotentialServers {
	std::string Ip;
	std::string text;
};

class ClickerClient :
	public Application {
public:
	ClickerClient();
	~ClickerClient();

	// Inherited via Application
	virtual void startUp() override;
	virtual void shutDown() override;
	virtual void update() override;
	virtual void draw() override;

	//updates the m_TotalClicks number to the one supplied here
	//usually from the server
	void updateClickerNumber(unsigned int a_Number);

	//will update the texture and color of the enemy to what is in m_EnemyData
	void updateEnemy();

	//IP, PROTENTIALSERVERS
	std::map<const char*,PotentialServers> m_LanServers;

	EnemyData m_EnemyData;
private:
	void userClicked();

	unsigned int getClickPower();
	void clickerPowerUpgrade();

	Mesh* m_EnemyMesh;
	Shader* m_EnemyShader;
	Texture* m_Textures[(int)EnemyImageData::TEXTURE_ENDS];

	Client* m_Client;
	bool m_Connected = false;
	ClickerClientPacket* m_Packet;

	//store!
	//TODO struct this

	//what is the price of this object
	unsigned int m_PurchasePrice = 50;
	//how many times has the user bought this
	unsigned int m_PurchaseAmount = 0;


	//current clicker number
	unsigned int m_TotalClicks = 0;
	unsigned int m_UserClicks = 0;
};

class ClickerClientPacket : public PacketHandling {
public:
	//reference to the ClickerClient object
	ClickerClient* m_Client;
	// Inherited via PacketHandling
	virtual void packetReceived(FUNCTIONPARAMATERS) override;
	virtual void pongReceived(RakNet::Packet * a_Packet, uint32_t a_Time, std::string a_Text) override;
};

