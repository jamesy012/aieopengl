#pragma once

#include "Networking.h"

class Application;
class ClickerServer;

class ClickerGameApp {
public:
	ClickerGameApp();
	~ClickerGameApp();

private:
	bool m_Client = true;
	Networking* m_Networker;

	Application* m_ClickerClient;
	ClickerServer* m_ClickerServer;
};

