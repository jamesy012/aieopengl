#include "PingTest.h"

#ifdef MYPINGPROGRAM
#include <string>
#include <iostream>

#include <RakNetTypes.h>//PACKET
#include <MessageIdentifiers.h>

#include "Server.h"
#include "Client.h"
#include "Networking.h"
#include "PacketHandling.h"

namespace RakNet {
	struct Packet;
}

#else
#include <RakPeerInterface.h>
#include <MessageIdentifiers.h>
#include <BitStream.h>
#include <RakNetTypes.h>
#include <GetTime.h>
#include <assert.h>
#include <cstdio>
#include <cstring>
#include <stdlib.h>
#include <RakSleep.h>
#include <Gets.h>
#endif


#ifdef MYPINGPROGRAM
class ClientPacket : public PacketHandling {
	// Inherited via PacketHandling
	virtual void packetReceived(FUNCTIONPARAMATERS) {

	}
	virtual void pongReceived(RakNet::Packet* a_Packet, uint32_t a_Time, std::string a_Text) {
		a_Packet->systemAddress.ToString();
		std::cout << "PONG!! from " << a_Packet->systemAddress.ToString() << " " << a_Time << "ms | " << a_Text << std::endl;
	}
};

class ServerPacket : public PacketHandling {
public:
	Networking* m_Networker;
	int m_Pings = 0;

	// Inherited via PacketHandling
	virtual void packetReceived(FUNCTIONPARAMATERS) {
		switch (*a_MessageID) {
			case ID_UNCONNECTED_PING:
			case ID_UNCONNECTED_PING_OPEN_CONNECTIONS:
				m_Pings++;
				m_Networker->setOfflinePingResponce("ping number: " + std::to_string(m_Pings));
				break;
		}
	}
	virtual void pongReceived(RakNet::Packet* a_Packet, uint32_t a_Time, std::string a_Text) {
		std::cout << "THIS SHOULD NOT HAPPEN??" << a_Time << std::endl;
	}
};
#endif


PingTest::PingTest() {
}

void PingTest::start() {
#ifdef MYPINGPROGRAM

	std::string input;
	Server server;
	Client client;
	Networking* networker;

	ClientPacket cp;
	ServerPacket sp;
	PacketHandling* ph;

	sp.m_Networker = &server;

	printf("Run as (s)erver or (c)lient?\n");
	std::cin >> input;

	if (input[0] == 's' || input[0] == 'S') {
		server.startup(2, 5555, 0);
		server.setOfflinePingResponce("first");
		networker = &server;
		ph = &sp;
	} else {//client
		client.startup();
		client.pingServerOnPort(5555);
		networker = &client;
		ph = &cp;
	}

	networker->startNetworkThread(ph);

	while (true) {
		std::cin >> input;
	}

#else

	// Pointers to the interfaces of our server and client.
	// Note we can easily have both in the same program
	RakNet::RakPeerInterface *client;
	RakNet::RakPeerInterface *server;
	bool b;
	char str[256];
	char serverPort[30], clientPort[30];
	RakNet::TimeMS quitTime;
	// Holds packets
	RakNet::Packet* p;

	printf("A client / server sample showing how clients can broadcast offline packets\n");
	printf("to find active servers.\n");
	printf("Difficulty: Beginner\n\n");

	printf("Instructions:\nRun one or more servers on the same port.\nRun a client and it will get pongs from those servers.\n");
	printf("Run as (s)erver or (c)lient?\n");
	Gets(str, sizeof(str));

	if (str[0] == 's' || str[0] == 'S') {
		client = 0;
		server = RakNet::RakPeerInterface::GetInstance();
		// A server
		printf("Enter the server port\n");
		Gets(serverPort, sizeof(serverPort));
		if (serverPort[0] == 0)
			strcpy(serverPort, "60001");

		printf("Starting server.\n");
		// The server has to be started to respond to pings.
		RakNet::SocketDescriptor socketDescriptor(atoi(serverPort), 0);
		socketDescriptor.socketFamily = AF_INET; // Only IPV4 supports broadcast on 255.255.255.255
		b = server->Startup(2, &socketDescriptor, 1) == RakNet::RAKNET_STARTED;
		server->SetMaximumIncomingConnections(2);
		if (b)
			printf("Server started, waiting for connections.\n");
		else {
			printf("Server failed to start.  Terminating.\n");
			exit(1);
		}

		RakNet::RakString str = "whew";
		server->SetOfflinePingResponse(str.C_String(), str.GetLength());
	} else {
		client = RakNet::RakPeerInterface::GetInstance();
		server = 0;

		//// Get our input
		//printf("Enter the client port to listen on, or 0\n");
		//Gets(clientPort, sizeof(clientPort));
		//if (clientPort[0] == 0)
		//	strcpy(clientPort, "60000");
		strcpy(clientPort, "0");
		printf("Enter the port to ping\n");
		Gets(serverPort, sizeof(serverPort));
		if (serverPort[0] == 0)
			strcpy(serverPort, "60001");
		RakNet::SocketDescriptor socketDescriptor(atoi(clientPort), 0);
		socketDescriptor.socketFamily = AF_INET; // Only IPV4 supports broadcast on 255.255.255.255
		client->Startup(1, &socketDescriptor, 1);



		printf("Pinging\n");
	}

	printf("How many seconds to run this sample for?\n");
	Gets(str, sizeof(str));
	if (str[0] == 0) {
		printf("Defaulting to 5 seconds\n");
		quitTime = RakNet::GetTimeMS() + 5000;
	} else
		quitTime = RakNet::GetTimeMS() + atoi(str) * 1000;

	// Loop for input
	while (RakNet::GetTimeMS() < quitTime) {

		if (server)
			p = server->Receive();
		else {
			p = client->Receive();
			// Connecting the client is very simple.  0 means we don't care about
			// a connectionValidationInteger, and false for low priority threads
			// All 255's mean broadcast
			client->Ping("255.255.255.255", atoi(serverPort), false);
		}

		if (p == 0) {
			RakSleep(100);
			continue;
		}
		if (server) {
			//printf("server receive\n");
			server->DeallocatePacket(p);
		} else {
			if (p->data[0] == ID_UNCONNECTED_PONG) {
				RakNet::TimeMS time;
				const size_t stringSize = 10;
				unsigned char string[stringSize] = { "" };
				RakNet::BitStream bsIn(p->data, p->length, false);
				bsIn.IgnoreBytes(1);
				bsIn.Read(time);
				bsIn.ReadBits(string, bsIn.GetNumberOfBitsUsed() - bsIn.GetReadOffset());
				printf("Got pong from %s with time %i ||| %s\n", p->systemAddress.ToString(), RakNet::GetTimeMS() - time, string);
			} else if (p->data[0] == ID_UNCONNECTED_PING) {
				printf("ID_UNCONNECTED_PING from %s\n", p->guid.ToString());
			} else if (p->data[0] == ID_UNCONNECTED_PING_OPEN_CONNECTIONS) {
				printf("ID_UNCONNECTED_PING_OPEN_CONNECTIONS from %s\n", p->guid.ToString());
			}
			client->DeallocatePacket(p);
		}

		RakSleep(100);
	}

	// We're done with the network
	if (server)
		RakNet::RakPeerInterface::DestroyInstance(server);
	if (client)
		RakNet::RakPeerInterface::DestroyInstance(client);

#endif

}


PingTest::~PingTest() {
}
