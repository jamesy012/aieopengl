#include "ObjectCreationTestApp.h"

#include <GLFW\glfw3.h>
#include "Input.h"

#include "Shader.h"
#include "Mesh.h"

void ObjectCreationTestApp::startUp() {
	//m_HandleMouseLock = false;

	m_Object = new ObjectCreationTestObject();
	m_Object->loadScript("Scripts/ObjectCreationTest.lua");

	m_BasicShader = new Shader();
	m_BasicShader->createBasicProgram(true);

	m_Plane = new Mesh();
	m_Plane->createPlane();
	m_Plane->m_Transform.m_Name = "C++ Plane";
}

void ObjectCreationTestApp::shutDown() {
	delete m_Object;
	delete m_BasicShader;
	delete m_Plane;
}

void ObjectCreationTestApp::update() {
	m_Object->update();

	if (Input::wasKeyPressed(GLFW_KEY_R)) {
		m_Object->reloadScript();
	}
}

void ObjectCreationTestApp::draw() {
	//glDisable(GL_DEPTH_TEST);

	m_BasicShader->useProgram();
	m_BasicShader->setUniformProjectionView(m_Camera);

	//m_Plane->draw();

	m_Object->draw();

}

void ObjectCreationTestObject::scriptLoaded() const {

}
