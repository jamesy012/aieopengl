#pragma once

#include <glm\glm.hpp>

#include "LuaBaseObject.h"

class Mesh;
class Texture;

struct lua_State;

class CarObject : public LuaBaseObject {
public:
	CarObject();
	~CarObject();

	void update();
	void draw();

	void setPosition(float a_X, float a_Y);

	const glm::vec2 getPostion();

	void applyForce(const glm::vec2& a_Force);
	void applyForceWorld(const glm::vec2& a_Force);
	void applyRotation(const float a_Force);

	float getRotation();

	void updateScreenSize() const;

	static int luaGetPosition(lua_State* a_State);
	static int luaGetMousePosition(lua_State* a_State);
	static int luaGetRotation(lua_State* a_State);
	static int luaGetLocalForce(lua_State* a_State);

	static int luaApplyForce(lua_State* a_State);
	static int luaApplyRotation(lua_State* a_State);
	static int luaApplyForceWorld(lua_State* a_State);

	//static int luaIsKeyDown(lua_State* a_State);
	//static int luaWasKeyPressed(lua_State* a_State);

private:
	// Inherited via Lua
	virtual void scriptLoaded() const override;
	
	Mesh* m_CarMesh;
	Texture* m_CarTexture;

	//forces to move car with
	glm::vec2 m_Force = glm::vec2(0);
	glm::vec2 m_ForceWorld = glm::vec2(0);
	float m_RotationForce = 0;
};

