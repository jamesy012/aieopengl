#pragma once

#include "Application.h"

class Shader;
class Texture;
class Mesh;
class CarObject;

class CarRaceApp :
	public Application {
public:

	// Inherited via Application
	virtual void startUp() override;
	virtual void shutDown() override;
	virtual void update() override;
	virtual void draw() override;
private:
	Shader* m_Shader;
	Texture* m_TrackTexture;
	Mesh* m_TrackMesh;
	CarObject* m_LuaObject;
};

