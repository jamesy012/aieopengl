#include "CarRaceApp.h"

#include <GLFW\glfw3.h>//keys
#include <glm\glm.hpp>//window size/transform setting

#include "Shader.h"
#include "Mesh.h"
#include "Texture.h"

#include "Input.h"
#include "Window.h"

#include "CarObject.h"

void CarRaceApp::startUp() {
	//tell application to run in Orthographic mode
	m_PerspetiveOrthographic = true;
	m_UpdatePerspetiveToNewWindowSize = false;//always keep it at the same perspective
	screenSizeChanged();
	//stop application from hiding the mouse when we click
	m_HandleMouseLock = false;

	//load shader
	m_Shader = new Shader();
	m_Shader->createBasicProgram(true);//default textured shader

	//load track texture
	m_TrackTexture = new Texture();
	m_TrackTexture->loadTexture("Textures/track.png");

	//get window size
	glm::vec2 windowSize = glm::vec2(Window::getWidth(),Window::getHeight())*0.5f;

	m_TrackMesh = new Mesh();
	m_TrackMesh->createPlane(true);
	m_TrackMesh->m_Transform.m_Name = "Track";

	m_TrackMesh->m_Transform.setPosition(glm::vec3(windowSize.x, 0, windowSize.y));
	m_TrackMesh->m_Transform.setScale(glm::vec3(windowSize.x, 0, windowSize.y));
	m_TrackMesh->setDiffuse(m_TrackTexture);
	m_TrackMesh->setColor(glm::vec4(glm::vec3(0.5f), 1));

	m_Camera->setPosition(glm::vec3(0, 1, 0));
	m_Camera->setRotation(glm::vec3(90, 0, 0));

	//todo load script
	m_LuaObject = new CarObject();
	m_LuaObject->loadScript("scripts/car.lua");
	m_LuaObject->setPosition(windowSize.x,windowSize.y);
}

void CarRaceApp::shutDown() {
	delete m_Shader;
	//delete m_TrackTexture;
	m_TrackTexture->release();//resource manager requires this, if there is more then one
	delete m_TrackMesh;
	delete m_LuaObject;
}

void CarRaceApp::update() {

	m_LuaObject->update();

	if (Input::wasKeyPressed(GLFW_KEY_R)) {
		m_LuaObject->reloadScript();
	}
}

void CarRaceApp::draw() {
	glDisable(GL_DEPTH_TEST);
	m_Shader->useProgram();
	m_Shader->setUniformProjectionView(m_Camera);

	m_TrackMesh->draw();

	m_LuaObject->draw();
}
