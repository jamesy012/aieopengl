#define CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

//better memory leak detector
//#include <vld.h>

#define CAR_RACE_APP 1
#define OBJECT_CREATION_TEST 2
#define GAME_TEST 3

//CHANGE THIS TO ONE OF THE DEFINCES ABOVE
#define APP_CHOICE OBJECT_CREATION_TEST

//PICKES 
#if APP_CHOICE == CAR_RACE_APP
	#include "CarRaceApp.h"
	typedef CarRaceApp CHOSEN_APP;
#elif APP_CHOICE == OBJECT_CREATION_TEST
	#include "ObjectCreationTestApp.h"
	typedef ObjectCreationTestApp CHOSEN_APP;
#elif APP_CHOICE == GAME_TEST
	#include "CarRaceApp.h"
	typedef CarRaceApp CHOSEN_APP;
#else
#error GAME NOT PICKED, PICK A NUMBER ABOVE
#endif // PICKED


int main() {
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	Application* app = new CHOSEN_APP();
	app->run();
	delete app;

	return 0;
}