#include "CarObject.h"

#include "Mesh.h"
#include "Texture.h"

#include "TimeHandler.h"
#include "Input.h"
#include "Window.h"

CarObject::CarObject() {
	m_CarTexture = new Texture();
	m_CarTexture->loadTexture("Textures/car.png");

	m_CarMesh = new Mesh();
	m_CarMesh->createPlane(true);
	m_CarMesh->setDiffuse(m_CarTexture);
	m_CarMesh->setColor(glm::vec4(1, 0, 0, 1));
	m_CarMesh->m_Transform.m_Name = "Car";

	float size = 2.0f;
	m_CarMesh->m_Transform.setScale(glm::vec3(size * 9, 1, size * 16));

	//lua functions 
	//todo, move to libScripting
	registerLuaFunction("GetPostion", luaGetPosition);
	registerLuaFunction("GetMousePostion", luaGetMousePosition);
	registerLuaFunction("GetRotation", luaGetRotation);
	registerLuaFunction("GetLocalForce", luaGetLocalForce);

	registerLuaFunction("ApplyForce", luaApplyForce);
	registerLuaFunction("ApplyRotation", luaApplyRotation);
	registerLuaFunction("ApplyForceWorld", luaApplyForceWorld);

}

CarObject::~CarObject() {
	delete m_CarMesh;
	m_CarTexture->release();
}

void CarObject::update() {
	//update forces
	m_CarMesh->m_Transform.translate(m_CarMesh->m_Transform.getGlobalRotation() * glm::vec3(m_Force.x, 0, m_Force.y), true);
	m_CarMesh->m_Transform.translate(glm::vec3(m_ForceWorld.x, 0, m_ForceWorld.y), true);
	m_CarMesh->m_Transform.rotate(glm::vec3(0, m_RotationForce, 0));

	//deplete forces
	m_Force *= 0.95f;
	m_ForceWorld *= 0.95f;
	m_RotationForce *= 0.95f;
	

	//call update with deltaTime
	pushFunction("Update");
	pushFloat(TimeHandler::getDeltaTime());
	callFunction(1, 0);
}

void CarObject::draw() {
	m_CarMesh->draw();
}

void CarObject::setPosition(float a_X, float a_Y) {
	m_CarMesh->m_Transform.setPosition(glm::vec3(a_X, 0, a_Y));
}

const glm::vec2 CarObject::getPostion() {
	glm::vec3 pos = m_CarMesh->m_Transform.getGlobalPosition();
	return glm::vec2(pos.x, pos.z);
}

void CarObject::applyForce(const glm::vec2 & a_Force) {
	m_Force += a_Force;
}

void CarObject::applyForceWorld(const glm::vec2 & a_Force) {
	m_ForceWorld += a_Force;
}

void CarObject::applyRotation(const float a_Force) {
	m_RotationForce += a_Force;
}

float CarObject::getRotation() {
	return m_CarMesh->m_Transform.getGlobalRotationEulers().y;
}

void CarObject::updateScreenSize() const {
	pushFunction("UpdateScreenSize");

	pushInt(Window::getStartingWidth());
	pushInt(Window::getStartingHeight());

	callFunction(2, 0);
}

int CarObject::luaGetPosition(lua_State * a_State) {
	CarObject* agent = (CarObject*) getPointerVar(a_State, "self");

	agent->pushFloat(agent->m_CarMesh->m_Transform.getGlobalPosition().x);
	agent->pushFloat(agent->m_CarMesh->m_Transform.getGlobalPosition().z);

	return 2;
}

int CarObject::luaGetMousePosition(lua_State * a_State) {
	CarObject* agent = (CarObject*) getPointerVar(a_State, "self");

	agent->pushFloat(Input::getMouseX());
	agent->pushFloat(Input::getMouseY());

	return 2;
}

int CarObject::luaGetRotation(lua_State * a_State) {
	CarObject* agent = (CarObject*) getPointerVar(a_State, "self");

	agent->pushFloat(agent->getRotation());

	return 1;
}

int CarObject::luaGetLocalForce(lua_State * a_State) {
	CarObject* agent = (CarObject*) getPointerVar(a_State, "self");

	agent->pushFloat(agent->m_Force.x);
	agent->pushFloat(agent->m_Force.y);

	return 2;
}

int CarObject::luaApplyForce(lua_State * a_State) {
	CarObject* agent = (CarObject*) getPointerVar(a_State, "self");

	glm::vec2 force;
	force.x = agent->popFloat();
	force.y = agent->popFloat();
	agent->applyForce(force);

	return 0;
}

int CarObject::luaApplyRotation(lua_State * a_State) {
	CarObject* agent = (CarObject*) getPointerVar(a_State, "self");

	float rotation = agent->popFloat();
	agent->applyRotation(rotation);

	return 0;
}

int CarObject::luaApplyForceWorld(lua_State * a_State) {
	CarObject* agent = (CarObject*) getPointerVar(a_State, "self");

	glm::vec2 force;
	force.x = agent->popFloat();
	force.y = agent->popFloat();
	agent->applyForceWorld(force);

	return 0;
}


void CarObject::scriptLoaded() const {
	updateScreenSize();
}
