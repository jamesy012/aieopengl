#pragma once

#include "LuaBaseObject.h"
#include "Application.h"

class Shader;
class Mesh;

class ObjectCreationTestObject;
class ObjectCreationTestApp : public Application {
public:
	
	// Inherited via Application
	virtual void startUp() override;

	virtual void shutDown() override;

	virtual void update() override;

	virtual void draw() override;

private:
	ObjectCreationTestObject* m_Object;

	Shader* m_BasicShader;
	Mesh* m_Plane;
};

class ObjectCreationTestObject : public LuaBaseObject {
	// Inherited via LuaBaseObject
	virtual void scriptLoaded() const override;
};

