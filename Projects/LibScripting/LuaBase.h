#pragma once

//todo change to forward declare
#include <lua.hpp>

class LuaBase {
public:
	LuaBase();
	virtual ~LuaBase();

	//loads file from a_Path,
	//true if file was loaded
	//false if file did not load
	bool loadScript(const char* a_Path);
	//using the path supplied from the last loadScript
	bool reloadScript() const;

	//allows you to add a function call to Lua
	//a_FunctionName is the name in lua
	//a_Function is the c++ function
	void registerLuaFunction(const char* a_FunctionName, const lua_CFunction a_Function) const;

	//adds a function to the lua stack
	//to call this function in lua use callFunction()
	//a_Function is the name of the function in lua
	bool pushFunction(const char* a_Function) const;

	//used after pushFunction()
	//called the function added to lua with a_ArgCount number of arguments
	//use push_ before this function and after pushFunction to add arguments
	//and a_ReturnCount number of variables returned
	//use pop_ after this function to get returned variables 
	bool callFunction(const int a_ArgCount, const int a_ReturnCount) const;

	//sets up the self and other variables in lua
	static void setPointerVar(lua_State* a_State, const char* a_VaraibleName, void* a_Val);

	//gets a_Variable name from lua
	//convert to the class which inherited off this class
	static void* getPointerVar(lua_State* a_State, const char* a_VariableName);

	//types for the push and pop off the lua stack
	//push to add as function parameters
	//pop to get return parameters

	void pushFloat(const float a_Value) const;
	const float popFloat() const;

	void pushBool(const bool a_Value) const;
	const bool popBool() const;

	void pushInt(const int a_Value) const;
	const int popInt() const;

	void pushString(const char* a_Value) const;
	const char* popString() const;



protected:
	//after a script is loaded, this is the first function that will run
	//use it to call lua functions to set data
	virtual void scriptLoaded() const;

private:
	//loads a file into lua from a_Path
	//true if file was loaded
	//false if file did not load
	bool loadFromPath() const;

	//starts lua, calls Init
	//also runs the scriptLoaded function
	void startLua() const;

	lua_State* m_LuaState;
	const char* m_FilePath = nullptr;

public:
	// --- testing conversions
	//lua_State& operator=(const lua_State&) const { return *m_LuaState; };
	//lua_State* operator=(const lua_State*) const { return m_LuaState; };

	//use this class in place of lua_State for lua functions,
	//this should mean it will use m_LuaState instead of the class
	operator lua_State*() const { return m_LuaState; };

	// --- end testing

private:

};

