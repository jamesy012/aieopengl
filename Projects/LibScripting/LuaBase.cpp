#include "LuaBase.h"

#include <iostream>

LuaBase::LuaBase() {
	//create lua object
	m_LuaState = luaL_newstate();
	//include math/io/table library which come with lua
	luaL_openlibs(m_LuaState);

	//add a pointer to this class called self
	//unsure exactly what it does
	setPointerVar(m_LuaState, "self", this);
}


LuaBase::~LuaBase() {
}

bool LuaBase::loadScript(const char * a_Path) {
	//store path for reloads
	m_FilePath = a_Path;
	//then reload the script using the new file path
	return reloadScript();
}

bool LuaBase::reloadScript() const {
	//check to see if we can load the file
	if (loadFromPath()) {
		//if we can then start calling lua functions
		startLua();
		return true;
	}
	//if we can't then return false
	return false;
}

void LuaBase::registerLuaFunction(const char * a_FunctionName, const lua_CFunction a_Function) const {
	lua_register(m_LuaState, a_FunctionName, a_Function);
}

bool LuaBase::callFunction(int a_ArgCount, int a_ReturnCount) const {
	if (!lua_isfunction(m_LuaState, -(a_ArgCount + 1))) {
		std::cout << __FUNCTION__ << ": function is not in the correct position on the stack" << std::endl;
		return false;
	}

	int status = lua_pcall(m_LuaState, a_ArgCount, a_ReturnCount, NULL);

	if (status) {
		std::cout << "Couldn't execute function  " << lua_tostring(m_LuaState, -1) << std::endl;
		return false;
	}

	return true;
}

void LuaBase::setPointerVar(lua_State * a_State, const char * a_VaraibleName, void * a_Val) {
	lua_pushlightuserdata(a_State, a_Val);
	lua_setglobal(a_State, a_VaraibleName);
}

void * LuaBase::getPointerVar(lua_State * a_State, const char * a_VariableName) {
	lua_getglobal(a_State, a_VariableName);

	if (lua_isuserdata(a_State, -1) == false) {
		std::cout << __FUNCTION__ << ": Variable is not a pointer" << std::endl;
		return NULL;
	}

	void* val = (void*) lua_topointer(a_State, -1);
	lua_pop(a_State, 1);
	return val;
}

void LuaBase::pushFloat(const float a_Value) const {
	//unsure if we need more to pushing a number,
	//it works fine as is though
	lua_pushnumber(m_LuaState, a_Value);
}

const float LuaBase::popFloat() const {
	if (lua_isnumber(m_LuaState, -1) == false) {
		int iStackSize = lua_gettop(m_LuaState);
		std::cout << __FUNCTION__ << ": Variable is not a number" << std::endl;
		return 0.0f;
	}

	float val = (float) lua_tonumber(m_LuaState, -1);
	lua_pop(m_LuaState, 1);
	return val;
}

void LuaBase::pushBool(const bool a_Value) const {
	lua_pushboolean(m_LuaState, a_Value);
}

const bool LuaBase::popBool() const {
	if (lua_isboolean(m_LuaState, -1) == false) {
		int iStackSize = lua_gettop(m_LuaState);
		std::cout << __FUNCTION__ << ": Variable is not a bool" << std::endl;
		return 0.0f;
	}

	bool val = lua_toboolean(m_LuaState, -1) != 0;
	lua_pop(m_LuaState, 1);
	return val;
}

void LuaBase::pushInt(const int a_Value) const {
	lua_pushinteger(m_LuaState, a_Value);
}

const int LuaBase::popInt() const {
	if (lua_isinteger(m_LuaState, -1) == false) {
		int iStackSize = lua_gettop(m_LuaState);
		std::cout << __FUNCTION__ << ": Variable is not a number" << std::endl;
		return 0;
	}

	int val = (int) lua_tointeger(m_LuaState, -1);
	lua_pop(m_LuaState, 1);
	return val;
}

void LuaBase::pushString(const char * a_Value) const {
	lua_pushstring(m_LuaState, a_Value);
}

const char * LuaBase::popString() const {
	if (lua_isstring(m_LuaState, -1) == false) {
		int iStackSize = lua_gettop(m_LuaState);
		std::cout << __FUNCTION__ << ": Variable is not a string" << std::endl;
		return "";
	}

	const char* val = (const char*) lua_tostring(m_LuaState, -1);
	lua_pop(m_LuaState, 1);
	return val;
}

bool LuaBase::pushFunction(const char * a_Function) const {
	lua_getglobal(m_LuaState, a_Function);
	if (lua_isfunction(m_LuaState, -1) == false) {
		std::cout << __FUNCTION__ << ": variable is not a function in script: " << a_Function << std::endl;
		return false;
	}
	return true;
}

void LuaBase::scriptLoaded() const {
}

bool LuaBase::loadFromPath()  const {
	//m_FilePath starts as nullptr
	//if this is false then loadScript hasent been run
	if (m_FilePath == nullptr) {
		std::cout << __FUNCTION__ << " file path was nullptr"<< std::endl;
		return false;
	}

	//attempts to load file
	int status = luaL_dofile(m_LuaState, m_FilePath);
	//if status > 0 then there was an error
	if (status) {
		std::cout << "Couldn't load script: " << m_FilePath << std::endl << lua_tostring(m_LuaState, -1) << std::endl;
		return false;
	}
	std::cout << "Script Loaded " << m_FilePath << std::endl;
	return true;
}

void LuaBase::startLua() const {
	//run a script loaded function before init to pass through data that may be necessary 
	scriptLoaded();

	//run init
	lua_getglobal(m_LuaState, "Init");
	//execute
	int status = lua_pcall(m_LuaState, 0, 0, NULL);
	if (status) {
		std::cout << "could not execute function Init " << lua_tostring(m_LuaState, -1) << std::endl;
	}

	
}
