#include "LuaBaseObject.h"

#include <glm\glm.hpp>

#include "Input.h"
#include "Window.h"
#include "TimeHandler.h"
#include "Camera.h"

#include "Transform.h"
#include "Mesh.h"
#include "Model.h"
#include "Texture.h"
#include "Material.h"

//macros
#define getAgent() LuaBaseObject* agent = (LuaBaseObject*) getPointerVar(a_State, "self");

LuaBaseObject::LuaBaseObject() {
	//set a default empty object
	m_Objects[0] = nullptr;

	//common
	registerLuaFunction("IsKeyDown", LuaBaseObject::luaIsKeyDown);
	registerLuaFunction("IsKeyRepeated", LuaBaseObject::luaIsKeyRepeated);
	registerLuaFunction("WasKeyPressed", LuaBaseObject::luaWasKeyPressed);
	registerLuaFunction("IsMouseDown", LuaBaseObject::luaIsMouseDown);
	registerLuaFunction("WasMousePressed", LuaBaseObject::luaWasKeyPressed);
	registerLuaFunction("GetMouseDelta", LuaBaseObject::luaGetMouseDelta);
	registerLuaFunction("GetMousePos", LuaBaseObject::luaGetMousePos);
	registerLuaFunction("IsMouseLocked", LuaBaseObject::luaIsMouseLocked);
	registerLuaFunction("GetWindowSize", LuaBaseObject::luaGetWindowSize);

	//Object Manager
	registerLuaFunction("ObjectCreateObject", LuaBaseObject::objectCreateObject);
	registerLuaFunction("ObjectDestroyObject", LuaBaseObject::objectDeleteObject);
	registerLuaFunction("ObjectGetTransform", LuaBaseObject::objectGetTransform);
	registerLuaFunction("ObjectDraw", LuaBaseObject::objectDraw);

	//Transform functions
	registerLuaFunction("TransformSetName", LuaBaseObject::transformSetName);
	registerLuaFunction("TransformTranslate", LuaBaseObject::transformTranslate);
	registerLuaFunction("TransformTranslateWorld", LuaBaseObject::transformTranslateWorld);
	registerLuaFunction("TransformRotate", LuaBaseObject::transformRotate);
	registerLuaFunction("TransformGetPosition", LuaBaseObject::transformGetPosition);
	registerLuaFunction("TransformSetPosition", LuaBaseObject::transformSetPosition);
	registerLuaFunction("TransformGetScale", LuaBaseObject::transformGetScale);
	registerLuaFunction("TransformSetScale", LuaBaseObject::transformSetScale);
	registerLuaFunction("TransformGetRotation", LuaBaseObject::transformGetRotation);
	registerLuaFunction("TransformSetRotation", LuaBaseObject::transformSetRotation);
	registerLuaFunction("TransformLookAt", LuaBaseObject::transformLookAt);
	registerLuaFunction("TransformGetFromName", LuaBaseObject::transformGetTransformFromName);

	//Transform parent/child Functions
	registerLuaFunction("TransformSetParent", LuaBaseObject::transformSetParent);
	registerLuaFunction("TransformRemoveParent", LuaBaseObject::transformRemoveParent);
	registerLuaFunction("TransformGetParent", LuaBaseObject::transformGetParent);
	registerLuaFunction("TransformAddChild", LuaBaseObject::transformAddChild);
	registerLuaFunction("TransformRemoveChild", LuaBaseObject::transformRemoveChild);
	registerLuaFunction("TransformGetChild", LuaBaseObject::transformGetChild);
	registerLuaFunction("TransformGetChildCount", LuaBaseObject::transformGetChildCount);

	//Mesh functions
	registerLuaFunction("MeshGetMaterial", LuaBaseObject::meshGetMaterial);
	registerLuaFunction("MeshSetMaterial", LuaBaseObject::meshSetMaterial);
	registerLuaFunction("MeshCreatePlane", LuaBaseObject::meshCreatePlane);

	//Model functions
	registerLuaFunction("ModelLoadModel", LuaBaseObject::modelLoadModel);
	registerLuaFunction("ModelGetMesh", LuaBaseObject::modelGetMesh);
	registerLuaFunction("ModelGetMeshCount", LuaBaseObject::modelGetMeshCount);

	//Camera functions
	registerLuaFunction("CameraGetMainCamera", LuaBaseObject::cameraGetMainCamera);
	registerLuaFunction("CameraGetTransform", LuaBaseObject::cameraGetTransform);
	registerLuaFunction("CameraSetPerspective", LuaBaseObject::cameraSetPerspective);
	registerLuaFunction("CameraSetOrthograpic", LuaBaseObject::cameraSetOrthograpic);

	//Texture functions
	registerLuaFunction("TextureLoadTexture", LuaBaseObject::textureLoadTexture);

	//Material functions
	registerLuaFunction("MaterialSetDiffuse", LuaBaseObject::materialSetDiffuse);
	registerLuaFunction("MaterialSetColor", LuaBaseObject::materialSetColor);
}

LuaBaseObject::~LuaBaseObject() {
	if (pushFunction("Destroy")) {
		callFunction(0, 0);
	} else {
		printf("lua script has no Destroy function");
	}

	for (const auto& x : m_Objects) {
		deleteObject(this, x.second, false);
	}
	m_Objects.clear();
}

void LuaBaseObject::update() {
	//call update with deltaTime
	if (pushFunction("Update")) {
		pushFloat(TimeHandler::getDeltaTime());
		callFunction(1, 0);
	} else {
		printf("lua script has no Update function");
	}
}

void LuaBaseObject::draw() {
	//call draw
	if (pushFunction("Draw")) {
		callFunction(0, 0);
	} else {
		printf("lua script has no Draw function");
	}
}

int LuaBaseObject::luaIsKeyDown(L_FUNCTION) {
	getAgent();

	int keyID;
	keyID = agent->popInt();

	agent->pushBool(Input::isKeyDown(keyID));

	return 1;
}

int LuaBaseObject::luaIsKeyRepeated(L_FUNCTION) {
	getAgent();

	int keyID;
	keyID = agent->popInt();

	agent->pushBool(Input::isKeyRepeated(keyID));

	return 1;
}

int LuaBaseObject::luaWasKeyPressed(L_FUNCTION) {
	getAgent();

	int keyID;
	keyID = agent->popInt();

	agent->pushBool(Input::wasKeyPressed(keyID));

	return 1;
}

int LuaBaseObject::luaIsMouseDown(L_FUNCTION) {
	getAgent();

	int mouseId;
	mouseId = agent->popInt();

	agent->pushBool(Input::isMouseDown(mouseId));

	return 1;
}

int LuaBaseObject::luaWasMousePressed(L_FUNCTION) {
	getAgent();

	int mouseId;
	mouseId = agent->popInt();

	agent->pushBool(Input::wasMousePressed(mouseId));

	return 1;
}

int LuaBaseObject::luaGetMouseDelta(L_FUNCTION) {
	getAgent();

	glm::vec2 mouseDelta = Input::getMouseDelta();

	agent->pushFloat(mouseDelta.x);
	agent->pushFloat(mouseDelta.y);

	return 2;
}

int LuaBaseObject::luaGetMousePos(L_FUNCTION) {
	getAgent();

	glm::vec2 mousePos = Input::getMousePos();

	agent->pushFloat(mousePos.x);
	agent->pushFloat(mousePos.y);

	return 2;
}

int LuaBaseObject::luaIsMouseLocked(L_FUNCTION) {
	getAgent();

	bool mouselock = Window::isMouseLocked();

	agent->pushBool(mouselock);

	return 1;
}

int LuaBaseObject::luaGetWindowSize(L_FUNCTION) {
	getAgent();

	agent->pushInt(Window::getWidth());
	agent->pushInt(Window::getHeight());

	return 2;
}

int LuaBaseObject::objectCreateObject(L_FUNCTION) {
	getAgent();

	LuaUseableTypes objectType = (LuaUseableTypes)agent->popInt();
	unsigned int index = 0;

	//if usable type
	if (checkIfUsableType(objectType)) {
		OBJECT_TYPE object = createObjectOfType(objectType);
		index = addObject(agent, object, objectType);
		agent->m_Objects[index]->createdHere = true;
	} else {
		printf("\t\t Unusable type number, %i (Type: %s )\n", (int)objectType, typeToName(objectType));
	}

	agent->pushInt(index);

	return 1;
}

int LuaBaseObject::objectDeleteObject(L_FUNCTION) {
	getAgent();

	LuaObject* lo = getObject(agent);

	if (lo == nullptr) {
		return 0;
	}

	deleteObject(agent, lo, true);

	return 0;
}


int LuaBaseObject::objectGetTransform(L_FUNCTION) {
	getAgent();

	unsigned int index = 0;
	Transform* transformObject = nullptr;

	LuaObject* lo = getObject(agent);

	if (lo == nullptr) {
		printf("%s: Object ID does not exist", __FUNCTION__);
	} else {
		switch (lo->type) {
		case LuaUseableTypes::Mesh:
		{
			Mesh* object = (Mesh*)lo->object;
			transformObject = &object->m_Transform;

			break;
		}
		case LuaUseableTypes::Model:
		{
			Model* object = (Model*)lo->object;
			transformObject = &object->m_Transform;

			break;
		}
		default:
			printf("Could not get transform for object ID %i , Type: %s", lo->index, typeToName(lo->type));
			break;
		}
	}

	if (transformObject != nullptr) {
		index = getObjectIndex(agent, transformObject, LuaUseableTypes::Transform);
	}

	agent->pushInt(index);

	return 1;
}

int LuaBaseObject::objectDraw(L_FUNCTION) {
	getAgent();

	LuaObject* lo = getObject(agent);

	if (lo == nullptr) {
		printf("%s: Object ID does not exist", __FUNCTION__);
	} else {

		IDrawable* object = (IDrawable*)lo->object;
		if (object == nullptr) {
			printf("Object ID:%i is type %s and cant be rendered", lo->index, typeToName(lo->type));
		} else {
			object->draw();
		}
	}

	return 0;
}

int LuaBaseObject::transformSetName(L_FUNCTION) {
	getAgent();

	const char* name = agent->popString();

	LuaObject* lo = getObject(agent);

	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Transform)) {
		return 0;
	}

	Transform* object = (Transform*)lo->object;

	object->m_Name = name;

	return 0;
}

int LuaBaseObject::transformTranslate(L_FUNCTION) {
	getAgent();

	glm::vec3 position;

	position.z = agent->popFloat();
	position.y = agent->popFloat();
	position.x = agent->popFloat();

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Transform)) {
		return 0;
	}
	Transform* object = (Transform*)lo->object;


	object->translate(position, false);

	return 0;
}

int LuaBaseObject::transformTranslateWorld(L_FUNCTION) {
	getAgent();

	glm::vec3 position;

	position.z = agent->popFloat();
	position.y = agent->popFloat();
	position.x = agent->popFloat();

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Transform)) {
		return 0;
	}
	Transform* object = (Transform*)lo->object;


	object->translate(position, true);

	return 0;
}

int LuaBaseObject::transformRotate(L_FUNCTION) {
	getAgent();

	glm::vec3 rotation;

	rotation.z = agent->popFloat();
	rotation.y = agent->popFloat();
	rotation.x = agent->popFloat();

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Transform)) {
		return 0;
	}
	Transform* object = (Transform*)lo->object;


	object->rotate(rotation);

	return 0;
}

int LuaBaseObject::transformGetPosition(L_FUNCTION) {
	getAgent();

	glm::vec3 position;

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Transform)) {
		//push empty vec3 floats
	} else {
		Transform* object = (Transform*)lo->object;
		position = object->getLocalPosition();
	}

	agent->pushFloat(position.x);
	agent->pushFloat(position.y);
	agent->pushFloat(position.z);

	return 3;
}

int LuaBaseObject::transformSetPosition(L_FUNCTION) {
	getAgent();

	glm::vec3 position;

	position.z = agent->popFloat();
	position.y = agent->popFloat();
	position.x = agent->popFloat();

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Transform)) {
		return 0;//might still have to push floats
	}
	Transform* object = (Transform*)lo->object;


	object->setPosition(position);

	return 0;
}

int LuaBaseObject::transformGetScale(L_FUNCTION) {
	getAgent();

	glm::vec3 scale;

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Transform)) {
		//push empty vec3 floats
	} else {
		Transform* object = (Transform*)lo->object;
		scale = object->getLocalPosition();
	}

	agent->pushFloat(scale.x);
	agent->pushFloat(scale.y);
	agent->pushFloat(scale.z);

	return 3;
}

int LuaBaseObject::transformSetScale(L_FUNCTION) {
	getAgent();

	glm::vec3 scale;

	scale.z = agent->popFloat();
	scale.y = agent->popFloat();
	scale.x = agent->popFloat();

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Transform)) {
		return 0;//might still have to push floats
	}
	Transform* object = (Transform*)lo->object;


	object->setScale(scale);

	return 0;
}

int LuaBaseObject::transformGetRotation(L_FUNCTION) {
	getAgent();

	glm::vec3 rotation;

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Transform)) {
		//push empty vec3 floats
	} else {
		Transform* object = (Transform*)lo->object;
		rotation = object->getLocalRotationEulers();
	}

	agent->pushFloat(rotation.x);
	agent->pushFloat(rotation.y);
	agent->pushFloat(rotation.z);

	return 3;
}

int LuaBaseObject::transformSetRotation(L_FUNCTION) {
	getAgent();

	glm::vec3 rotation;

	rotation.z = agent->popFloat();
	rotation.y = agent->popFloat();
	rotation.x = agent->popFloat();

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Transform)) {
		return 0;//might still have to push floats
	}
	Transform* object = (Transform*)lo->object;


	object->setRotation(rotation);

	return 0;
}

int LuaBaseObject::transformLookAt(L_FUNCTION) {
	getAgent();

	glm::vec3 position;

	position.z = agent->popFloat();
	position.y = agent->popFloat();
	position.x = agent->popFloat();

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Transform)) {
		return 0;//might still have to push floats
	}
	Transform* object = (Transform*)lo->object;


	object->setLookAt(position);

	return 0;
}

int LuaBaseObject::transformGetTransformFromName(L_FUNCTION) {
	getAgent();

	const char* name = agent->popString();

	Transform* t = Transform::findTransform(name);

	unsigned int index = 0;

	index = getObjectIndex(agent, t, LuaUseableTypes::Transform);

	agent->pushInt(index);

	return 1;
}

int LuaBaseObject::transformSetParent(L_FUNCTION) {
	getAgent();

	LuaObject* parentObject = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(parentObject, LuaUseableTypes::Transform)) {
		printf("%s Parent does not exist\n", __FUNCTION__);
		return 0;
	}
	Transform* parent = (Transform*)parentObject->object;
	if (parent == nullptr) {
		parent = Transform::getRootTransform();
	}

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Transform)) {
		printf("%s transform does not exist\n", __FUNCTION__);
		return 0;
	}
	Transform* object = (Transform*)lo->object;


	object->setParent(parent);

	return 0;
}

int LuaBaseObject::transformRemoveParent(L_FUNCTION) {
	getAgent();

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Transform)) {
		printf("%s transform does not exist\n", __FUNCTION__);
		return 0;
	}
	Transform* object = (Transform*)lo->object;	

	object->setParent(Transform::getRootTransform());

	return 0;
}
int LuaBaseObject::transformGetParent(L_FUNCTION) {
	getAgent();

	unsigned int parentId = 0;

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Transform)) {
		printf("%s transform does not exist\n", __FUNCTION__);
	} else {
		Transform* object = (Transform*)lo->object;
		Transform* parent = object->getParent();
		if (parent != nullptr) {
			parentId = getObjectIndex(agent, parent, LuaUseableTypes::Transform);
		}
	}

	agent->pushInt(parentId);

	return 1;
}

int LuaBaseObject::transformAddChild(L_FUNCTION) {
	getAgent();

	LuaObject* childObject = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(childObject, LuaUseableTypes::Transform)) {
		printf("%s Child does not exist\n", __FUNCTION__);
		return 0;
	}
	Transform* child = (Transform*)childObject->object;

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Transform)) {
		printf("%s transform does not exist\n", __FUNCTION__);
		return 0;
	}
	Transform* object = (Transform*)lo->object;

	child->setParent(object);

	return 0;
}

int LuaBaseObject::transformRemoveChild(L_FUNCTION) {
	getAgent();

	LuaObject* childObject = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(childObject, LuaUseableTypes::Transform)) {
		printf("%s Child does not exist\n", __FUNCTION__);
		return 0;
	}
	Transform* child = (Transform*)childObject->object;

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Transform)) {
		printf("%s transform does not exist\n", __FUNCTION__);
		return 0;
	}
	Transform* object = (Transform*)lo->object;

	if (object->isChild(child)) {
		child->removeParent();
	} else {
		printf("%s transform is not a child of parent\n", __FUNCTION__);
	}

	return 0;
}

int LuaBaseObject::transformGetChild(L_FUNCTION) {
	getAgent();

	unsigned int childId = 0;
	unsigned int childIndex = agent->popInt();

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Transform)) {
		printf("%s transform does not exist\n", __FUNCTION__);
	} else {
		Transform* object = (Transform*)lo->object;
		Transform* child = object->getChild(childIndex);
		if (child != nullptr) {
			childId = getObjectIndex(agent, child, LuaUseableTypes::Transform);
		}
	}

	agent->pushInt(childId);

	return 1;
}

int LuaBaseObject::transformGetChildCount(L_FUNCTION) {
	getAgent();

	unsigned int childCount = 0;

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Transform)) {
		printf("%s transform does not exist\n", __FUNCTION__);
	} else {
		Transform* object = (Transform*)lo->object;
		childCount = object->getChildrenCount();
	}

	agent->pushInt(childCount);

	return 1;
}

int LuaBaseObject::meshGetMaterial(L_FUNCTION) {
	getAgent();

	unsigned int index = 0;

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (checkIfType(lo, LuaUseableTypes::Mesh)) {
		Mesh* object = (Mesh*)lo->object;
		Material* t = object->m_Material;

		index = getObjectIndex(agent, t, LuaUseableTypes::Material);
	}

	agent->pushInt(index);

	return 1;
}

int LuaBaseObject::meshSetMaterial(L_FUNCTION) {
	getAgent();

	LuaObject* materialObject = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(materialObject, LuaUseableTypes::Material)) {
		materialObject = nullptr;
	}


	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Mesh)) {
		lo = nullptr;
	}
	if (materialObject == nullptr || lo == nullptr) {
		printf("%s material or mesh was does not exist\n", __FUNCTION__);
		return 0;
	}
	Material* material = (Material*)materialObject->object;

	Mesh* object = (Mesh*)lo->object;
	object->setMaterial(material);

	return 0;
}

int LuaBaseObject::meshCreatePlane(L_FUNCTION) {
	getAgent();

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Mesh)) {
		return 0;
	}

	Mesh* object = (Mesh*)lo->object;
	object->createPlane();
	object->m_Transform.m_Name = "LUA PLANE";

	return 0;
}

int LuaBaseObject::modelLoadModel(L_FUNCTION) {
	getAgent();

	const char* fileName = agent->popString();
	const char* filePath = agent->popString();

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Model)) {
		return 0;
	}

	Model* object = (Model*)lo->object;
	object->loadModel(filePath, fileName);

	return 0;
}

int LuaBaseObject::modelGetMesh(L_FUNCTION) {
	getAgent();

	unsigned int meshIndex = agent->popInt();
	unsigned int meshObjectIndex = 0;

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Model)) {
		printf("%s did not receive a %s object index", __FUNCTION__, typeToName(LuaUseableTypes::Model));
	} else {
		Model* object = (Model*)lo->object;

		//if index is within size
		if (meshIndex < object->m_Meshs.size()) {

			Mesh* mesh = object->m_Meshs[meshIndex];

			meshObjectIndex = getObjectIndex(agent, mesh, LuaUseableTypes::Mesh);
		}
	}

	agent->pushInt(meshObjectIndex);

	return 1;
}

int LuaBaseObject::modelGetMeshCount(L_FUNCTION) {
	getAgent();

	unsigned int meshCount = 0;

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Model)) {
		printf("%s Model does not exist\n", __FUNCTION__);
	} else {
		Model* object = (Model*)lo->object;
		meshCount = object->m_Meshs.size();
	}

	agent->pushInt(meshCount);

	return 1;
}

int LuaBaseObject::cameraGetMainCamera(L_FUNCTION) {
	getAgent();

	Camera* camera = Camera::m_MainCamera;

	unsigned int index = 0;

	index = getObjectIndex(agent, camera, LuaUseableTypes::Camera);

	agent->pushInt(index);

	return 1;
}

int LuaBaseObject::cameraGetTransform(L_FUNCTION) {
	getAgent();

	unsigned int index = 0;

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (checkIfType(lo, LuaUseableTypes::Camera)) {
		Camera* object = (Camera*)lo->object;
		Transform* t = object;

		index = getObjectIndex(agent, t, LuaUseableTypes::Transform);
	}

	agent->pushInt(index);

	return 1;
}

int LuaBaseObject::cameraSetPerspective(L_FUNCTION) {
	getAgent();

	float far = agent->popFloat();
	float near = agent->popFloat();
	float aspect = agent->popFloat();
	float fov = agent->popFloat();

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Camera)) {
		return 0;//might still have to push floats
	}

	Camera* object = (Camera*)lo->object;
	object->setPerspective(fov, aspect, near, far);

	return 0;
}

int LuaBaseObject::cameraSetOrthograpic(L_FUNCTION) {
	getAgent();

	float far = agent->popFloat();
	float near = agent->popFloat();
	float top = agent->popFloat();
	float bot = agent->popFloat();
	float right = agent->popFloat();
	float left = agent->popFloat();

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Camera)) {
		return 0;//might still have to push floats
	}

	Camera* object = (Camera*)lo->object;
	object->setOthrographic(left, right, bot, top, near, far);

	return 0;
}


int LuaBaseObject::textureLoadTexture(L_FUNCTION) {
	getAgent();

	const char* pathName = agent->popString();

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Texture)) {
		return 0;
	}

	Texture* object = (Texture*)lo->object;
	object->loadTexture(pathName);

	return 0;
}

int LuaBaseObject::materialSetDiffuse(L_FUNCTION) {
	getAgent();

	LuaObject* textureObject = getObject(agent);


	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Material)) {
		return 0;
	}
	if (!checkIfType(textureObject, LuaUseableTypes::Texture)) {
		return 0;
	}

	Material* object = (Material*)lo->object;
	Texture* texture = (Texture*)textureObject->object;

	object->setTexture(TextureSlots::Diffuse, texture);

	return 0;
}

int LuaBaseObject::materialSetColor(L_FUNCTION) {
	getAgent();

	glm::vec4 color;

	//backwards popping
	color.a = agent->popFloat();
	color.b = agent->popFloat();
	color.g = agent->popFloat();
	color.r = agent->popFloat();

	LuaObject* lo = getObject(agent);
	//check if lo is a nullptr or the type we are checking
	if (!checkIfType(lo, LuaUseableTypes::Material)) {
		return 0;
	}

	Material* object = (Material*)lo->object;

	object->setColor(color);

	return 0;
}


void LuaBaseObject::deleteObject(L_AGENT, LuaObject * a_ObjectToDelete, bool a_Remove) {
	if (a_ObjectToDelete != nullptr) {
		if (a_ObjectToDelete->createdHere) {
			switch (a_ObjectToDelete->type) {
			case LuaUseableTypes::Transform:
			{
				delete (Transform*)a_ObjectToDelete->object;
				break;
			}
			case LuaUseableTypes::Mesh:
			{
				delete (Mesh*)a_ObjectToDelete->object;
				break;
			}
			case LuaUseableTypes::Model:
			{
				delete (Model*)a_ObjectToDelete->object;
				break;
			}
			case LuaUseableTypes::Texture:
			{
				Texture* tex = (Texture*)a_ObjectToDelete->object;
				tex->release();
				break;
			}
			case LuaUseableTypes::Material:
			{
				delete (Material*)a_ObjectToDelete->object;
				break;
			}
			default:
			{
				printf("default deletion of object type %s\n", typeToName(a_ObjectToDelete->type));
				delete a_ObjectToDelete->object;
				break;
			}
			}

		}


		if (a_Remove) {
			a_Agent->m_Objects.erase(a_ObjectToDelete->index);
		}

		delete a_ObjectToDelete;

		a_ObjectToDelete->index = 0;
		a_ObjectToDelete->createdHere = false;
		a_ObjectToDelete->type = LuaUseableTypes::NO_TYPE;
		a_ObjectToDelete->object = nullptr;



	}
}

OBJECT_TYPE LuaBaseObject::createObjectOfType(LuaUseableTypes a_Type) {
	OBJECT_TYPE object;

	switch (a_Type) {
	case LuaUseableTypes::Transform:
		object = new Transform();
		break;
	case LuaUseableTypes::Mesh:
		object = new Mesh();
		break;
	case LuaUseableTypes::Model:
		object = new Model();
		break;
	case LuaUseableTypes::Texture:
		object = new Texture();
		break;
	case LuaUseableTypes::Camera:
		//creating a camera is pretty useless since we cant swap between them for rendering
	case LuaUseableTypes::NO_TYPE:
	default:
		//call virtual function here so other classes can add their own types
		printf("%s. type number was unidentified and cant be created!: %i \n", __FUNCTION__, a_Type);
		return nullptr;
	}

	return object;
}

unsigned int LuaBaseObject::addObject(L_AGENT, OBJECT_TYPE a_Object, LuaUseableTypes a_Type) {
	//todo check if a_Object is already in m_Objects
	unsigned int index = a_Agent->m_ObjectCounter++;

	LuaObject* lo = new LuaObject();
	lo->index = index;
	lo->type = a_Type;
	lo->object = a_Object;

	a_Agent->m_Objects[index] = lo;

	return index;
}

LuaBaseObject::LuaObject* LuaBaseObject::getObject(L_AGENT) {
	LuaObject* lo;

	CounterTypeIndex index = a_Agent->popInt();

	if (index == 0) {
		printf("Object Index was 0\n");
		return nullptr;
	}

	lo = a_Agent->m_Objects.at(index);

	//if lo is not nullptr
	if (lo != nullptr) {
		//check if it's type is in the usable range
		if (!checkIfUsableType(lo->type)) {
			return nullptr;
		}
		//else return normaly
	}

	return lo;
}

bool LuaBaseObject::checkIfUsableType(LuaUseableTypes a_Type) {

	if (a_Type >= LuaUseableTypes::END_TYPES) {
		return false;
	}
	if (a_Type <= LuaUseableTypes::NO_TYPE) {
		return false;
	}
	return true;
}

bool LuaBaseObject::checkIfType(LuaObject * a_Object, LuaUseableTypes a_Type) {
	if (a_Object == nullptr) {

		return false;
	}
	return a_Object->type == a_Type;
}

unsigned int LuaBaseObject::checkIfObject(L_AGENT, OBJECT_TYPE a_Object, LuaUseableTypes a_Type) {
	unsigned int index = -1;
	for (const auto& x : a_Agent->m_Objects) {
		index++;
		if (x.second == nullptr) {
			continue;
		}
		if (x.second->type == a_Type) {
			if (x.second->object == a_Object) {
				return index;
			}
		}
	}

	return 0;
}

unsigned int LuaBaseObject::getObjectIndex(L_AGENT, OBJECT_TYPE a_Object, LuaUseableTypes a_Type) {
	unsigned int index = 0;
	if (a_Object != nullptr) {
		index = checkIfObject(a_Agent, a_Object, a_Type);

		//if we have found the object, but it's not in m_Objects
		//then we add it to m_Objects
		if (index == 0) {
			//add the object
			index = addObject(a_Agent, a_Object, a_Type);
		}
	}
	return index;
}

const char * LuaBaseObject::typeToName(LuaUseableTypes a_Type) {
	switch (a_Type) {
	case LuaUseableTypes::Transform:
		return "Transform";
		break;
	case LuaUseableTypes::Mesh:
		return "Mesh";
		break;
	case LuaUseableTypes::Texture:
		return "Texture";
		break;
	case LuaUseableTypes::NO_TYPE:
	default:
		return "UNKNOWN";
	}
}