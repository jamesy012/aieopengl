#pragma once
#include <map>

#include "LuaBase.h"

struct lua_State;

enum class LuaUseableTypes {
	NO_TYPE,
	Transform = 1,
	Mesh,
	Model,
	Camera,//cameras are transforms
	Texture,
	Material,
	END_TYPES
};

//macros
#define OBJECT_TYPE void*
#define L_FUNCTION lua_State* a_State
#define L_AGENT LuaBaseObject* a_Agent

//adds common functions to lua
//and sets up the object manager
class LuaBaseObject : public LuaBase {
	//holder for all objects used by lua
	struct LuaObject {
		OBJECT_TYPE object;
		LuaUseableTypes type;
		unsigned int index;
		//flag to check if we created this object
		bool createdHere;
	};
	//typedef LuaBaseObject::LuaObject LUA_OBJECT;
public:
	LuaBaseObject();
	virtual ~LuaBaseObject();

	void update();
	void draw();
private:
	//counter type
	typedef unsigned int CounterTypeIndex;
	//list of objects
	std::map <CounterTypeIndex, LuaObject*> m_Objects;
	//how many objects have been created
	CounterTypeIndex m_ObjectCounter = 1;

	///--- LUA FUNCTIONS
	///return is how many variables were pushed
private:
	///common
	//check to see if a key is down
	//in: int key number
	//out: bool was key down
	static int luaIsKeyDown(L_FUNCTION);
	//check to see if a key is repeating
	//in: int key number
	//out: bool was key repeating
	static int luaIsKeyRepeated(L_FUNCTION);
	//check to see if a key was pressed this frame
	//in: int key number
	//out: bool was was pressed this frame
	static int luaWasKeyPressed(L_FUNCTION);
	//check to see if a mouse button is down
	//in: int mouse number
	//out: bool was mouse down
	static int luaIsMouseDown(L_FUNCTION);
	//check to see if a mouse button was pressed this frame
	//in: int mouse number
	//out: bool was pressed this frame
	static int luaWasMousePressed(L_FUNCTION);
	//gets mouse movement from last frame and this frame
	//out: 2 ints, x,y
	static int luaGetMouseDelta(L_FUNCTION);
	//gets mouse position relative to the window
	//out: 2 ints, x,y
	static int luaGetMousePos(L_FUNCTION);
	//check to see if the mouse is currently hidden and locked to window
	//out: bool was mouse locked
	static int luaIsMouseLocked(L_FUNCTION);
	//gets the size of the window
	//out: 2 ints, x,y
	static int luaGetWindowSize(L_FUNCTION);

	///---Object Manager
private:
	//creates object of type
	//in: int version of LuaUseableTypes
	//out: index to m_Objects of created object
	static int objectCreateObject(L_FUNCTION);
	//deletes object memory if created here and removes from m_Objecs
	//in: int object index
	static int objectDeleteObject(L_FUNCTION);
	//checks known types of objects to get their transform object
	//will create a new index for it if object did not exist before
	//in: int object index
	static int objectGetTransform(L_FUNCTION);
	//draws all objects which are of type IDrawable
	//in: int object index
	static int objectDraw(L_FUNCTION);

	///Transform
	//sets name for a transform
	//in: int Transform object index, const char* name
	static int transformSetName(L_FUNCTION);
	//moves transform relative to the local axis
	//in: int Transform object index, (3 floats) vec3 translate amount
	static int transformTranslate(L_FUNCTION);
	//moves transform relative to the World axis
	//in: int Transform object index, (3 floats) vec3 translate amount
	static int transformTranslateWorld(L_FUNCTION);
	//rotates transform
	//in: int Transform object index, (3 floats) vec3 rotation amount
	static int transformRotate(L_FUNCTION);
	//returns the local position of a transform
	//in: int Transform object index
	//out (3 float) vec3 position
	static int transformGetPosition(L_FUNCTION);
	//sets the local position of a transform
	//in: int Transform object index, (3 floats) vec3 new position
	static int transformSetPosition(L_FUNCTION);
	//returns the local scale of a transform
	//in: int Transform object index
	//out (3 float) vec3 position
	static int transformGetScale(L_FUNCTION);
	//sets the local scale of a transform
	//in: int Transform object index, (3 floats) vec3 new scale
	static int transformSetScale(L_FUNCTION);
	//returns the local rotation of a transform
	//in: int Transform object index
	//out (3 float) vec3 rotation in degrees
	static int transformGetRotation(L_FUNCTION);
	//sets the local rotation of a transform in degrees
	//in: int Transform object index, (3 floats) vec3 new rotation
	static int transformSetRotation(L_FUNCTION);
	//sets the local rotation of a transform in degrees
	//in: int Transform object index, (3 floats) vec3 pos to look at 
	static int transformLookAt(L_FUNCTION);
	//finds a transform by name, returns 0 if transform not found
	//if transform found and not in objects then it will create a new LuaObject and index for it
	//in: const char* name
	//out: transform index
	static int transformGetTransformFromName(L_FUNCTION);

	///Transform parent/child Functions
	//sets the parent transform of a object
	//if parent transform is nullptr then parent will be the root transform
	//in: int Transform object index, int parent transform index
	static int transformSetParent(L_FUNCTION);
	//sets the parent to be the rootTransform
	//in: int Transform object index
	static int transformRemoveParent(L_FUNCTION);
	//gets the parent transform index,
	//if object did not exist in m_Objects before a new index will be created
	//in: int Transform object index
	//out: int parent transform index
	static int transformGetParent(L_FUNCTION);
	//adds a child to a transform
	//in: int Transform object index, int child transform index
	static int transformAddChild(L_FUNCTION);
	//removes child from transform
	//in: int Transform object index, int child transform index
	static int transformRemoveChild(L_FUNCTION);
	//gets a child from a index
	//if child did not exist in m_Objects before a new index will be created
	//in: int Transform object index, int child index
	//out: int child object transform index
	static int transformGetChild(L_FUNCTION);
	//gets number of children attached to this transform
	//in: int Transform object index
	//out: int number of children
	static int transformGetChildCount(L_FUNCTION);

	///Mesh
	//gets the material for this mesh
	//if the material did not exist in m_Objects before a new index will be created
	//in: int Mesh object index
	//out: int material Index
	static int meshGetMaterial(L_FUNCTION);
	//sets the material for this mesh
	//in: int Mesh object index, int Material object index
	static int meshSetMaterial(L_FUNCTION);
	//creates a flat plane facing up
	//in: int Mesh object index
	static int meshCreatePlane(L_FUNCTION);

	///Model
	//loads a model
	//in: int Model object index, const char* filePath, const char* fileName
	static int modelLoadModel(L_FUNCTION);
	//gets the Mesh from this model using the mesh index
	//if the mesh did not exist in m_Objects before a new index will be created
	//in: int Model object index, int mesh index
	//out: int Mesh object Index
	static int modelGetMesh(L_FUNCTION);
	//gets number of mesh's attached to this model
	//in: int Model object index
	//out: int number of mesh's
	static int modelGetMeshCount(L_FUNCTION);

	///Camera
	//gets the first created camera for this scene
	//out: int Camera Object index
	static int cameraGetMainCamera(L_FUNCTION);
	//gets the cameras transform
	//in: int Camera Object index
	//out: int Transform Object Index
	static int cameraGetTransform(L_FUNCTION);
	//sets the cameras projectionTransform to be perspective
	//in: int Camera Object index, float field of view, float aspect ratio, float near plane, float far plane
	static int cameraSetPerspective(L_FUNCTION);
	//sets the cameras projectionTransform to be orthographic
	//in: int Camera Object index, float left,float right, float bottom, float top, float near plane, float far plane
	static int cameraSetOrthograpic(L_FUNCTION);

	///Textures
	//loads the texture
	//in: int object index, const char* filePath
	static int textureLoadTexture(L_FUNCTION);

	///Materials
	//sets the diffuse of the material to a texture
	//in: int Transform object index, int texture object index
	static int materialSetDiffuse(L_FUNCTION);
	//sets the color of the material
	//in: int Transform object index, (4 floats) r,g,b,a
	static int materialSetColor(L_FUNCTION);

	///--- OTHER/COMMON
	//deletes memory objects from m_Objects
	//if a_Remove then it will erase object from m_Objects
	static void deleteObject(L_AGENT, LuaObject* a_ObjectToDelete,bool a_Remove = false);
	//creates a new version of the object from a_Type
	//returns pointer to object
	static OBJECT_TYPE createObjectOfType(LuaUseableTypes a_Type);
	//adds object to m_Objects and creates a new LuaObject
	static unsigned int addObject(L_AGENT, OBJECT_TYPE a_Object, LuaUseableTypes a_Type);
	//Lua Function: in: object index
	//returns the object from an int by popping from Lua
	//returns: LuaObject, nullptr if no object in index slot
	static LuaObject* getObject(L_AGENT);
	//check to see if a_Type is a accepted type
	static bool checkIfUsableType(LuaUseableTypes a_Type);
	//checks if the type of a_Object is same as a_Type
	//returns false if a_Object is a nullptr or the type is not the same
	static bool checkIfType(LuaObject* a_Object, LuaUseableTypes a_Type);
	//goes through m_Objects to find any objects that match a_Object
	static unsigned int checkIfObject(L_AGENT, OBJECT_TYPE a_Object,LuaUseableTypes a_Type);
	//will find the object from m_Objects
	//if not there it will create one for that transform object in m_Objects
	static unsigned int getObjectIndex(L_AGENT, OBJECT_TYPE a_Object, LuaUseableTypes a_Type);
	//converts a LuaUsableType into a const char*
	//returns name of Type
	static const char* typeToName(LuaUseableTypes a_Type);
};
