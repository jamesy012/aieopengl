#pragma once

#include <functional>//std::function
#include <string>

namespace RakNet {
	class RakPeerInterface;
	struct SocketDescriptor;
	enum StartupResult;
	class BitStream;
	struct Packet;
	enum ConnectionAttemptResult;
	struct AddressOrGUID;
}


namespace std {
	class thread;
}

class PacketHandling;

class Networking {
	//RakNet::MessageID
#define FUNCTIONPARAMATERS unsigned char* a_MessageID, RakNet::BitStream* a_BitStream, RakNet::Packet* a_Packet
	using FunctionCallback = std::function<void(FUNCTIONPARAMATERS)>;
public:
	Networking();
	virtual ~Networking();

	void setOfflinePingResponce(std::string a_Text);

	//a user made class that deals with what happens when packets arrive
	virtual void startNetworkThread(PacketHandling* a_PacketHandling);

	//this will start a while loop that will only end during the deconstructor
	void startNetworkingLoop();

	//this will end the networking loop
	void endNetworkingLoop();

	//this will attempt to connect to a_IP
	//returns false if it couldn't connect
	bool connect(const char* a_IP, unsigned short a_Port, std::string a_Password = "");

	//this will start a message with the id of MessageID
	void startMessage(unsigned char* a_MessageID);
	//this will start a message with the id of MessageID
	void startMessage(unsigned int a_MessageID);
	//adds a_Data to the current message
	void addToMessage(std::string a_Data);
	//adds a_Data to the current message
	void addToMessage(float a_Data);
	//adds a_Data to the current message
	void addToMessage(unsigned int a_Data);
	//adds a_Data to the current message
	void addToMessage(unsigned char a_Data);
	//returns a reference to the current message to add data to it
	//Do not store this
	RakNet::BitStream** getCurrentMessage();
	//sends message to all clients
	void sendMessage();
	//sends message to a client
	void sendMessage(RakNet::Packet* a_Client);
	//sends message to a client
	void sendMessage(RakNet::AddressOrGUID* a_Client);
	//sends message to all clients, except this one
	void sendMessageExclude(RakNet::AddressOrGUID a_Client);

	//pings one port on lan
	void pingServerOnPort(unsigned short a_Port, bool a_ReplyForIncomingConnections = false);
	//pings one port for address
	void pingServerOnPort(const char* a_Address,unsigned short a_Port, bool a_ReplyForIncomingConnections = false);
	//checks every port
	//FUNCTION CURRENTLY EMPTY
	void pingServersAllPorts(bool a_ReplyForIncomingConnections = false);

	//returns a true/false based on if the thread is running
	bool isThreadRunning();

	//returns a pointer to the peer interface
	RakNet::RakPeerInterface* getPeerInterafce();

	//returns the results from the RakPeerInterface startup
	RakNet::StartupResult getStartupResult();
protected:
	void setSocketDecriptorInformation(unsigned short a_Port, const char* a_Address = 0);
	//checks to see if the startup was a success 
	bool wasStartupGood();
	
	virtual void networkingInheritedLoop(FUNCTIONPARAMATERS) = 0;

	//thread for all multitasking tasks
	std::thread* m_NetworkingThread = nullptr;

	//lifeline of raknet, most things go though this
	RakNet::RakPeerInterface* m_PeerInterface = nullptr;
	//handles ports
	RakNet::SocketDescriptor* m_SocketDescriptor = nullptr;

	PacketHandling* m_PacketHandler = nullptr;

	//a flag to check if the peerInterface has been started
	bool m_HasStarted = false;

	//a flag for the networking thread, true when the thread is running
	bool m_ShouldEnd = false;

	//bitStream for creating messages
	//will be nullptr
	RakNet::BitStream* m_BitStream = nullptr;

	//storage of the result from the startup
	//(default value is PORT_CANNOT_BE_ZERO)
	RakNet::StartupResult m_StartupResult = (RakNet::StartupResult)8;

	RakNet::ConnectionAttemptResult m_ConnectionResult = (RakNet::ConnectionAttemptResult)0;
};

