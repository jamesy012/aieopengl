#pragma once

#include <string>

#include "Networking.h"

namespace RakNet {
	struct Packet;
}

class PacketHandling {
public:
	//FUNCTIONPARAMATERS //from networking
	virtual void packetReceived(FUNCTIONPARAMATERS) = 0;

	virtual void pongReceived(RakNet::Packet* a_Packet, uint32_t a_Time, std::string a_Text) = 0;

};