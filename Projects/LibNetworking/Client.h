#pragma once
#include "Networking.h"
class Client :
	public Networking {
public:
	//clients dont really need a port, and the address here is not useful either
	bool startup(unsigned short a_Port = 0, const char* a_Address = 0);

	// Inherited via Networking
	virtual void networkingInheritedLoop(FUNCTIONPARAMATERS) override;
};

