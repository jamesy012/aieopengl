#include "Client.h"

#include <RakPeerInterface.h>
#include <MessageIdentifiers.h>
#include <BitStream.h>
#include <GetTime.h>

#include "PacketHandling.h"

bool Client::startup(unsigned short a_Port, const char * a_Address) {
	setSocketDecriptorInformation(a_Port, a_Address);

	m_StartupResult = m_PeerInterface->Startup(1, m_SocketDescriptor, 1);

	return wasStartupGood();
}

void Client::networkingInheritedLoop(FUNCTIONPARAMATERS) {
	switch (*a_MessageID) {
		case ID_UNCONNECTED_PONG:
			{
				RakNet::TimeMS time;
				a_BitStream->Read(time);

				const size_t stringSize = ((a_BitStream->GetNumberOfBitsUsed() - a_BitStream->GetReadOffset()) / 8) + 2;
				char* string = new char[stringSize];
				a_BitStream->ReadBits((unsigned char*)string, a_BitStream->GetNumberOfBitsUsed() - a_BitStream->GetReadOffset());
				string[stringSize-1] = '\0';

				time = RakNet::GetTimeMS() - time;

				m_PacketHandler->pongReceived(a_Packet,time,string);
				
				delete string;
				break;
			}
	}
}
