#include "Networking.h"

#include <RakPeerInterface.h>
#include <MessageIdentifiers.h>
#include <BitStream.h>
#include <RakPeer.h>
#include <GetTime.h>

#include <thread>

#include "PacketHandling.h"

Networking::Networking() {
	m_PeerInterface = RakNet::RakPeerInterface::GetInstance();
	m_SocketDescriptor = new RakNet::SocketDescriptor();
}

Networking::~Networking() {
	endNetworkingLoop();
	if (m_PeerInterface != nullptr) {
		RakNet::RakPeerInterface::DestroyInstance(m_PeerInterface);
	}
	if (m_SocketDescriptor != nullptr) {
		delete m_SocketDescriptor;
	}

	if (m_BitStream != nullptr) {
		delete m_BitStream;
		m_BitStream = nullptr;
	}
}

void Networking::setOfflinePingResponce(std::string a_Text) {
	m_PeerInterface->SetOfflinePingResponse(a_Text.c_str(), a_Text.size());
}

void Networking::startNetworkThread(PacketHandling * a_PacketHandling) {
	assert(m_HasStarted);//only continue if the peer interface has been started
	assert(!m_ShouldEnd);//only continue if there is a thread not currently running
	m_PacketHandler = a_PacketHandling;
	m_NetworkingThread = new std::thread(std::bind(&Networking::startNetworkingLoop, this));
}


bool Networking::connect(const char * a_IP, unsigned short a_Port, std::string a_Password) {
	m_ConnectionResult = m_PeerInterface->Connect(a_IP, a_Port, a_Password.c_str(), a_Password.size());

	//return true if m_ConnectionResult is RakNet::ConnectionAttemptResult::CONNECTION_ATTEMPT_STARTED
	return (m_ConnectionResult == RakNet::ConnectionAttemptResult::CONNECTION_ATTEMPT_STARTED);
}

void Networking::startMessage(unsigned char * a_MessageID) {
	assert(m_BitStream == nullptr);
	m_BitStream = new RakNet::BitStream;
	//Untested warning fix
	//m_BitStream->Write((RakNet::MessageID)a_MessageID);
	m_BitStream->Write(a_MessageID);
}

void Networking::startMessage(unsigned int a_MessageID) {
	assert(m_BitStream == nullptr);
	m_BitStream = new RakNet::BitStream;
	m_BitStream->Write((RakNet::MessageID)a_MessageID);
}

void Networking::addToMessage(std::string a_Data) {
	assert(m_BitStream != nullptr);
	m_BitStream->Write(a_Data.c_str());
}

void Networking::addToMessage(float a_Data) {
	assert(m_BitStream != nullptr);
	m_BitStream->Write(a_Data);
}

void Networking::addToMessage(unsigned int a_Data) {
	assert(m_BitStream != nullptr);
	m_BitStream->Write(a_Data);
}

void Networking::addToMessage(unsigned char a_Data) {
	assert(m_BitStream != nullptr);
	m_BitStream->Write(a_Data);
}

RakNet::BitStream** Networking::getCurrentMessage() {
	assert(m_BitStream != nullptr);
	return &m_BitStream;
}

void Networking::sendMessage() {
	assert(m_BitStream != nullptr);
	m_PeerInterface->Send(m_BitStream, MEDIUM_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);//send to all
	delete m_BitStream;
	m_BitStream = nullptr;
}

void Networking::sendMessage(RakNet::Packet * a_Client) {
	assert(m_BitStream != nullptr);
	m_PeerInterface->Send(m_BitStream, MEDIUM_PRIORITY, RELIABLE_ORDERED, 0, a_Client->systemAddress, false);
	delete m_BitStream;
	m_BitStream = nullptr;
}

void Networking::sendMessage(RakNet::AddressOrGUID* a_Client) {
	assert(m_BitStream != nullptr);
	m_PeerInterface->Send(m_BitStream, MEDIUM_PRIORITY, RELIABLE_ORDERED, 0, *a_Client, false);
	delete m_BitStream;
	m_BitStream = nullptr;
}

void Networking::sendMessageExclude(RakNet::AddressOrGUID a_Client) {
	assert(m_BitStream != nullptr);
	m_PeerInterface->Send(m_BitStream, MEDIUM_PRIORITY, RELIABLE_ORDERED, 0, a_Client, true);
	delete m_BitStream;
	m_BitStream = nullptr;
}

void Networking::pingServerOnPort(unsigned short a_Port, bool a_ReplyForIncomingConnections) {
	//255.255... will ping every lan ip on that port
	m_PeerInterface->Ping("255.255.255.255", a_Port, false);
}

void Networking::pingServerOnPort(const char * a_Address, unsigned short a_Port, bool a_ReplyForIncomingConnections) {
	m_PeerInterface->Ping(a_Address, a_Port, false);
}

void Networking::pingServersAllPorts(bool a_ReplyForIncomingConnections) {
	//for(int )
}

bool Networking::isThreadRunning() {
	//if should end is true, then the while loop for the networking would not run 
	return !m_ShouldEnd;
}

RakNet::RakPeerInterface * Networking::getPeerInterafce() {
	return m_PeerInterface;
}

RakNet::StartupResult Networking::getStartupResult() {
	if (!m_HasStarted) {
		return RakNet::StartupResult::STARTUP_OTHER_FAILURE;
	}
	return m_StartupResult;
}

void Networking::setSocketDecriptorInformation(unsigned short a_Port, const char * a_Address) {
	m_SocketDescriptor->port = a_Port;
	if (a_Address != nullptr) {
		strcpy_s(m_SocketDescriptor->hostAddress, a_Address);
	}

	//if (a_Port == 0) {
	//	m_Port = m_SocketDescriptor->port;
	//} else {
	//	m_Port = a_Port;
	//}
}

bool Networking::wasStartupGood() {
	if (m_StartupResult == RakNet::StartupResult::RAKNET_STARTED) {
		m_HasStarted = true;
		return true;
	}
	return false;
}

void Networking::startNetworkingLoop() {
	//this should only run if the m_PeerInterface has been setup
	assert(m_HasStarted);
	m_ShouldEnd = false;
	RakNet::Packet* packet = nullptr;
	while (!m_ShouldEnd) {
		//initialize a receive
		//check to see if packet is true (exists)
		//deallocate old packet, get new packet
		for (packet = m_PeerInterface->Receive(); packet; m_PeerInterface->DeallocatePacket(packet), packet = m_PeerInterface->Receive()) {

			RakNet::MessageID messageType = packet->data[0];
			RakNet::BitStream bsin(packet->data, packet->length, false);
			bsin.IgnoreBytes(sizeof(RakNet::MessageID));

			//common handles here
			switch (messageType) {
				default:
					//client/server specif handles here
					networkingInheritedLoop(&messageType, &bsin, packet);
					//^ user defined PacketHandling class called in there
					break;
				case ID_CONNECTION_LOST://<-- to remove warning
					break;
			}

			m_PacketHandler->packetReceived(&messageType, &bsin, packet);

		}
	}
}

void Networking::endNetworkingLoop() {
	if (m_NetworkingThread != nullptr) {
		m_ShouldEnd = true;
		m_NetworkingThread->join();
		delete m_NetworkingThread;
		m_NetworkingThread = nullptr;
	}
}
