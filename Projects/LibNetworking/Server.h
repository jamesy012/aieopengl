#pragma once
#include "Networking.h"
class Server :	public Networking {
public:

	bool startup(unsigned int a_MaxConnections, unsigned short a_Port, const char* a_Address = 0);

	// Inherited via Networking
	virtual void networkingInheritedLoop(FUNCTIONPARAMATERS) override;

};

