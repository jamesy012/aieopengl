#include "Server.h"

#include <RakPeerInterface.h>
#include <MessageIdentifiers.h>
#include <BitStream.h>

#include "PacketHandling.h"


bool Server::startup(unsigned int a_MaxConnections, unsigned short a_Port, const char * a_Address) {
	setSocketDecriptorInformation(a_Port, a_Address);

	m_StartupResult = m_PeerInterface->Startup(a_MaxConnections, m_SocketDescriptor, 1);
	m_PeerInterface->SetMaximumIncomingConnections(a_MaxConnections);

	return wasStartupGood();
}

void Server::networkingInheritedLoop(FUNCTIONPARAMATERS) {
	switch (*a_MessageID) {
		case ID_CONNECTION_LOST://<-- to remove warning
			break;
	}
}
