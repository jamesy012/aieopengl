#include "Window.h"
#include "Application.h"

#include <GLFW\glfw3.h>
#include <stdio.h>


static GLFWwindow* m_Window = nullptr;

static int m_WindowWidth = 0;
static int m_WindowHeight = 0;

static int m_StartingWidth = 0;
static int m_StartingHeight = 0;

static int m_FrameBufferWidth = 0;
static int m_FrameBufferHeight = 0;

static int m_StartingFrameBufferWidth = 0;
static int m_StartingFrameBufferHeight = 0;

static bool m_MouseLocked = false;
static bool m_IsWindowFocused = true;

void Window::createWindow(int a_Width, int a_Height, char * a_Title) {
	if (m_Window != nullptr) {
		glfwDestroyWindow(m_Window);
		m_Window = nullptr;
	}

	m_Window = glfwCreateWindow(a_Width, a_Height, a_Title, nullptr, nullptr);

	if (m_Window == nullptr) {
		printf("Unable to create window!\n");
	}

	m_StartingWidth = m_WindowWidth = a_Width;
	m_StartingHeight = m_WindowHeight = a_Height;

	glfwGetFramebufferSize(m_Window, &m_StartingFrameBufferWidth, &m_StartingFrameBufferHeight);
	m_FrameBufferWidth = m_StartingFrameBufferWidth;
	m_FrameBufferHeight = m_StartingFrameBufferHeight;

}

void Window::destroyWindow() {
	if (m_Window != nullptr) {
		glfwDestroyWindow(m_Window);
		m_Window = nullptr;
	}
}

void Window::windowSizeCallback(GLFWwindow * a_Window, int a_Width, int a_Height) {
	m_WindowWidth = a_Width;
	m_WindowHeight = a_Height;
	Application::getCurrentApplcation()->screenSizeChanged();
}

void Window::framebufferSizeCallback(GLFWwindow * a_Window, int a_Width, int a_Height) {
	m_FrameBufferWidth = a_Width;
	m_FrameBufferHeight = a_Height;


	Application::getCurrentApplcation()->frameBufferChanged();
}

void Window::windowFocusCallback(GLFWwindow * a_Window, int a_Focused) {
	//printf("Window Focus callback: %i\n", a_Focused);
	if (a_Focused) {
		//gained input
		m_IsWindowFocused = true;
	} else {
		//lost input
		m_IsWindowFocused = false;
	}
	Application::getCurrentApplcation()->windowFocusChanged(m_IsWindowFocused);
}

GLFWwindow * Window::getWindow() {
	return m_Window;
}

int Window::getWidth() {
	return m_WindowWidth;
}

int Window::getHeight() {
	return m_WindowHeight;
}

int Window::getFramebufferWidth() {
	return m_FrameBufferWidth;
}

int Window::getFramebufferHeight() {
	return m_FrameBufferHeight;
}

int Window::getStartingWidth() {
	return m_StartingWidth;
}

int Window::getStartingHeight() {
	return m_StartingHeight;
}

int Window::getStartingFramebufferWidth() {
	return m_StartingFrameBufferWidth;
}

int Window::getStartingFramebufferHeight() {
	return m_StartingFrameBufferHeight;
}

bool Window::hasFocus() {
	return m_IsWindowFocused;
}

void Window::toggleMouseLock() {
	m_MouseLocked = !m_MouseLocked;
	if (m_MouseLocked) {
		glfwSetInputMode(m_Window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	} else {
		glfwSetInputMode(m_Window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}
}

bool Window::isMouseLocked() {
	return m_MouseLocked;
}
