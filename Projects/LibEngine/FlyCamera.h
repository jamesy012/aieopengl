#pragma once
#include "Camera.h"

#include <glm\glm.hpp>

class FlyCamera : public Camera {
public:
	FlyCamera();
	~FlyCamera();

	void update();
	void setSpeed(float a_Speed);

	bool m_MovementOnlyWhileMouseLocked = true;
	bool m_CameraRotationOnlyWhileMouseLocked = true;
	bool m_AllowChangeOfFovByScroll = true;
private:
	//float speed of Player
	float m_Speed = 10;
};

