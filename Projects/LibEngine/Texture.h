#pragma once

#include "IResource.h"

class Texture : public IResource {
public:
	Texture();
	~Texture();

	void loadTexture(const char* a_FilePath);
	void createWhite();

	static void useTexture(Texture* a_Tex, unsigned int a_TextureSpot, unsigned int a_UniformLocation);
	static void useTexture(Texture* a_Tex, unsigned int a_TextureSpot, const char* a_UniformName);
	static void useTexture(unsigned int a_TextureID, unsigned int a_TextureSpot, const char* a_UniformName);

	static void bindTexture(unsigned int a_TextureSpot, Texture* a_Tex);
	static void bindTexture(unsigned int a_TextureSpot, unsigned int a_ID = 0);

	int m_ImageWidth = 0;
	int m_ImageHeight = 0;
	int m_ImageFormat = 0;

	unsigned int m_TextureId = 0;

	const char* m_TexturePath;

	virtual void release() override;
private:


	// Inherited via IResource
	virtual unsigned int getTypeId() override;

	virtual void load() override;

	virtual void copy(IResource * a_Res) override;

	virtual void destroy() override;


};

