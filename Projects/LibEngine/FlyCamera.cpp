#include "FlyCamera.h"

#include <cmath>

#include <GLFW\glfw3.h>

#include <glm\ext.hpp>

#include "Input.h"
#include "TimeHandler.h"
#include "Window.h"

FlyCamera::FlyCamera() {
	m_Name = "Fly Camera";
}


FlyCamera::~FlyCamera() {
}

void FlyCamera::update() {
	//check if we should move the camera?
	bool mouseMovement = true;
	//is the flag for only moving while locked on?
	if (m_CameraRotationOnlyWhileMouseLocked) {
		//set mouse movement bool to if the mouse is locked
		mouseMovement = Window::isMouseLocked();
	}
	//if we should move the mouse
	if (mouseMovement) {
		//move mouse
		glm::vec2 movement = -glm::vec2(Input::getMouseDeltaX(), Input::getMouseDeltaY());

		if (movement != glm::vec2(0)) {
			setDirty();

#if USE_QUATERNIONS
			movement *= 0.0025f;//sensitivity
			//rotate Up and down
			rotate(glm::quat(glm::rotate(movement.y, glm::vec3(1, 0, 0))));

			//calc up
			glm::vec4 up = glm::inverse(getLocalRotation()) * glm::vec4(0, 1, 0, 0);

			//rotate left and right using the up vector
			rotate(glm::quat(glm::rotate(movement.x, glm::vec3(up[0], up[1], up[2]))));
#else

			//attempt to stop the weird left/right controls when upside down
			glm::vec3 rotation = getLocalRotationEulers();
			int xRot = (int) abs(rotation.x) % 360;
			if (xRot > 90 && xRot < 270) {
				movement.x *= -1;
			}

			//up/down
			rotate(glm::vec3(movement.y, 0, 0));
			//left/right
			rotate(glm::vec3(0, movement.x, 0));
#endif // USE_QUATERNIONS

		}
		//scroll
		if (m_AllowChangeOfFovByScroll) {
			float scrollDelta = -Input::getMouseScrollY();//scroll is backwards for what we want
			if (scrollDelta != 0) {
				scrollDelta *= 0.1f;//sensitivity
				float fov = m_FieldOfView + scrollDelta;
				fov = fov < 0.1f ? 0.1f : fov > 2 ? 2 : fov;//clamp
				setPerspective(fov, m_Aspect, m_NearPlane, m_FarPlane);
			}
		}
	}

	bool keyMovement = true;
	if (m_MovementOnlyWhileMouseLocked) {
		keyMovement = Window::isMouseLocked();
	}
	if (!keyMovement) {
		return;
	}

	//z will center view to (0,0,0)
	if (Input::isKeyDown(GLFW_KEY_Z)) {
		setLookAt(m_Position, glm::vec3(0));
		//return;
	}

	//how much to move in each direction
	glm::vec3 movement;
	if (Input::isKeyDown(GLFW_KEY_W)) {
		movement.z -= m_Speed;
	}
	if (Input::isKeyDown(GLFW_KEY_S)) {
		movement.z += m_Speed;
	}
	if (Input::isKeyDown(GLFW_KEY_A)) {
		movement.x -= m_Speed;
	}
	if (Input::isKeyDown(GLFW_KEY_D)) {
		movement.x += m_Speed;
	}
	if (Input::isKeyDown(GLFW_KEY_SPACE)) {
		movement.y += m_Speed;
	}
	if (Input::isKeyDown(GLFW_KEY_LEFT_CONTROL)) {
		movement.y -= m_Speed;
	}

	if (movement == glm::vec3(0)) {
		return;
	}

	translate(movement * TimeHandler::getDeltaTime(), false);


}

void FlyCamera::setSpeed(float a_Speed) {
	m_Speed = a_Speed;
}
