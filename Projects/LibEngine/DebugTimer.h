#pragma once

#include <map>
#include <chrono>

#define DEBUG_TIMER_CLOCK_TYPE std::chrono::steady_clock

class DebugTimer {
public:

	static void startTimer(const unsigned int a_Index, const std::string a_Desc = "Timer");
	static void endTimer(const unsigned int a_Index, const bool a_DisplayInConsole = true);
	static void lapTimer(const unsigned int a_Index, const bool a_DisplayInConsole = true);
	static bool isTimerActive(const unsigned int a_Index);
	

protected:
	struct Time {
		std::chrono::time_point<DEBUG_TIMER_CLOCK_TYPE> m_Start;
		std::chrono::time_point<DEBUG_TIMER_CLOCK_TYPE> m_End;
		std::chrono::duration<float> m_TotalTime;
		std::string m_Desc;
		unsigned int m_Index;
		bool m_CurrentlyTimed;
		bool m_HasLapped;
		unsigned int m_LapNumber;
	};

	static std::chrono::time_point<DEBUG_TIMER_CLOCK_TYPE> getCurrentTime();
	static void displayDiff(const Time* a_Time);
	static Time* getTimer(const unsigned int a_Index);

	static std::chrono::duration<float> m_LastTime;
	static std::map<int, Time> m_Times;

};

