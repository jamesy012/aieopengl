#pragma once

#include <glm\glm.hpp>

class Shader;

class Grid {
public:
	Grid();
	~Grid();

	void generateGrid(unsigned int a_Rows, unsigned int a_Cols);
	void loadShader();
	void draw(glm::mat4 a_ProjectionViewMatrix);
private:
	unsigned int m_VAO,m_VBO,m_IBO;

	unsigned int m_ProgramID;
	Shader* m_Shader = nullptr;

	unsigned int m_Rows, m_Cols;
};

