#pragma once

#include <map>
#include <vector>

class IResource;


class ResourceManager {
public:
	struct ResourceRef {
		IResource* resource;
		unsigned int resourceCount = 0;
	};
	static void addResource(IResource* a_Resource);
	static bool hasResource(IResource* a_Resource);
	static void removeResource(IResource* a_Resource,bool a_DeleteBaseResource = true);
	static ResourceRef* getLoadedResource(IResource* a_ResourcePathToCopy);

	static void destroy();

private:
	static ResourceRef createReference(IResource* a_Resource);
	static void createResource(ResourceRef* a_ResRef);
	static void copyResource(ResourceRef* a_Reference, IResource* a_Resource);
	static void removeReference(ResourceRef* a_Reference);
	static std::map<unsigned int, std::vector<ResourceRef>> m_ResourceList;
};

