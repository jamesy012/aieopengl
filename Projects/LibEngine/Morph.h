#pragma once

#include <vector>

#include "Model.h"
#include "Vertex.h"

class Morph {
public:
	struct GLInfo {
		unsigned int vao;
		unsigned int vbo1;
		unsigned int vbo2;
		unsigned int faceCount;
	};

	~Morph();

	void load(const char* a_Dir, const char* a_Model1, const char* a_Model2);
	void draw();


private:
	unsigned int createVertex(Model* a_Model, unsigned int a_MeshIndex);

	Model* m_Models[2];
	std::vector<GLInfo> m_Meshes;
};

