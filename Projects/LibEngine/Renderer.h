#pragma once

#include <vector>
//#include <map>
#include <unordered_map>
#include <string>

//for RenderUniform
//for the UniformTypes enum
#include "Shader.h"

class IDrawable;
class BoundingObject;
class Camera;



struct RenderUniform {
	std::unordered_map<std::string, ShaderUniform*> uniforms;
};
struct RenderObject : public RenderUniform {
	IDrawable* drawable;
	BoundingObject* bounding;

};

struct RenderShaderBatch : public RenderUniform {
	Shader* shader;
	Camera* camera;
	std::unordered_map<IDrawable*, RenderObject*> objects;
};

class Renderer {
public:
	~Renderer();


	//starts a render batch for the drawList
	//if there is one already started then it will end that and start a new one
	//sets the projectionView for shader from the camera every frame
	RenderShaderBatch* startRenderShaderBatch(Shader* a_Shader, Camera* a_Camera);

	//adds a drawable object to the draw list for the current shaderbatch
	RenderObject* addObject(IDrawable* a_Drawable, BoundingObject* a_BoundingObject = nullptr);

	//adds a drawable object to the draw list for a_Rsb
	RenderObject* addObject(RenderShaderBatch* a_Rsb, IDrawable* a_Drawable, BoundingObject* a_BoundingObject = nullptr);

	void setUniform(RenderUniform* a_Object, const UniformTypes a_Type, std::string a_Name, const float* a_Data);

	//ends the current shader batch
	void endRenderShaderBatch();

	//draw after binding the mesh
	//will check for frustum and cull
	void draw();

private:
	void useUniforms(const RenderUniform* a_UniformList,Shader* a_Shader);

	//list of shader's and their objects
	std::vector<RenderShaderBatch*> m_DrawList;

	//current RenderShaderBatch object we are making
	RenderShaderBatch* m_CurrentShaderBatch = nullptr;
	RenderObject* m_LastRenderObject = nullptr;
};

