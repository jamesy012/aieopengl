#pragma once

class Transform;

class Gui {
public:
	//TODO make my own methods for all of these instead of using the example implemtation
	static void startUp();
	static void shutdown();
	static void newFrame();
	static void draw();

	static void drawHierarchy();
	static void drawTransformChildren(Transform* a_Transform, int& a_TransformDrawID, Transform** a_SelectedTransform);
	static void drawInspector();

	//flag to turn the hierarchy off, if it is called it wont run
	static bool m_ShowHierachy;
};

