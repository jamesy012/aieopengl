#pragma once

//TODO 
#define LOGGER_USE_CONSOLE

#include <queue>

namespace std {
	class thread;
	class mutex;
}


enum LogType {
	Normal,
	Warning,
	Error
};

class Logger {
public:
	Logger();
	~Logger();

	void setFilePath(const char* a_FilePath);
	void log(char* a_Message, LogType a_LogType = Normal);

private:
	struct logMessage {
		char* message;
		LogType logType;
	};

	//called when there is something that needs to be added to the log file
	//just unlocks the mutex
	void allowFileWriting();

	void writeToLog(logMessage* a_LogMessage);

	void writeToFile();
	void clearFile();
	//path to location file is stored
	char* m_FilePath = "/Log/";
	//name of file
	char* m_FileName = "Logger";
	//extension of file
	char* m_FileExtension = "Log";


	std::thread* m_IoThread;
	std::mutex* m_IoMutex;
	bool m_EndThread = false;

	std::queue<logMessage> m_MessageList;
};

