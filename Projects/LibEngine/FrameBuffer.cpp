#include "FrameBuffer.h"

#include <gl_core_4_4.h>

#include <stdio.h>

#include "Texture.h"
#include "Window.h"

FrameBuffer::FrameBuffer() {
}


FrameBuffer::~FrameBuffer() {
	if (m_Texture != nullptr) {
		delete m_Texture;
	}
}

void FrameBuffer::setSize(const int a_Width, const int a_Height) {
	m_Ratio = false;
	m_Width = (float)a_Width;
	m_Height = (float)a_Height;
}

void FrameBuffer::setSize(const float a_RatioWidth, const float a_RatioHeight) {
	m_Ratio = true;
	m_Width = a_RatioWidth;
	m_Height = a_RatioHeight;
}

void FrameBuffer::createFB(const unsigned int a_TextureFormat, const bool a_AddDepthRenderBuffer) {

	//Delete all framebuffer data
	if (m_Fbo != 0) {
		glDeleteFramebuffers(1, &m_Fbo);
		if (m_FboDepth != 0) {
			glDeleteRenderbuffers(1, &m_FboDepth);
			m_FboDepth = 0;
		}
		if (m_Texture != nullptr) {
			delete m_Texture;
			m_Texture = nullptr;
		}
	}

	//create Texture
	int width = getWidth();
	int height = getHeight();
	unsigned int texID;

	glGenTextures(1, &texID);
	glBindTexture(GL_TEXTURE_2D, texID);

	//look at http://docs.gl/gl4/glTexImage2D for examples
	glTexImage2D(GL_TEXTURE_2D, 0, a_TextureFormat, width, height, 0, a_TextureFormat, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	//create Depth Buffer
	if (a_AddDepthRenderBuffer) {
		glGenRenderbuffers(1, &m_FboDepth);
		glBindRenderbuffer(GL_RENDERBUFFER, m_FboDepth);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, width, height);
	}

	//create framebuffer and bind textures
	glGenFramebuffers(1, &m_Fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, m_Fbo);

	//Attach depth render buffer
	if (a_AddDepthRenderBuffer) {
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_FboDepth);
	}

	//TODO bind textures to framebuffer in a better way then this
	if (a_TextureFormat == GL_DEPTH_COMPONENT) {
		glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, texID, 0);
		glDrawBuffer(GL_NONE);
	} else {//it's a color?!
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texID, 0);

		GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0 };
		glDrawBuffers(1, drawBuffers);
	}

	//add depth if it's requested?


	//check errors? doesn't really tell us much tho
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE) {
		printf("Framebuffer Error\n");
	}


	//unbind
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	//set up other data for FrameBufferClass
	m_Texture = new Texture();
	m_Texture->m_TextureId = texID;
}

void FrameBuffer::use(FrameBuffer * a_FB) {
	if (a_FB == nullptr) {
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0, 0, Window::getFramebufferWidth(), Window::getFramebufferHeight());
		glClearColor(0.25f, 0.25f, 0.25f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		return;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, a_FB->m_Fbo);
	glViewport(0, 0, a_FB->getWidth(), a_FB->getHeight());
	glClearColor(0.75f, 0.75f, 0.75f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

int FrameBuffer::getWidth() {
	if (m_Ratio) {
		return int(Window::getFramebufferWidth() * m_Width);
	}
	return (int) m_Width;
}

int FrameBuffer::getHeight() {
	if (m_Ratio) {
		return (int) (Window::getFramebufferHeight() * m_Height);
	}
	return (int) m_Height;
}
