#pragma once

#include <vector>
#include <glm\glm.hpp>

#include "Vertex.h"
#include "Transform.h"

#include "IDrawable.h"

class Model;
class Morph;
class Texture;
class Material;

class Mesh : public IDrawable {
	friend Morph;
	friend Model;
public:
	Mesh();
	~Mesh();
	Mesh(const Mesh& a_Copy);

	//transform for this mesh
	Transform m_Transform;

	//creates a plane for this mesh
	//if there is already a mesh here then it will overwrite the old mesh
	void createPlane(bool a_FlipYCoords = false);

	//creates a grid for this mesh
	//width and depth is 1x1, with a vertices's every a_X and a_Y
	//if there is already a mesh here then it will overwrite the old mesh
	void createGrid(unsigned int a_X, unsigned int a_Y);

	//creates a box for this mesh
	//if there is already a mesh here then it will overwrite the old mesh
	void createBox();

	//creates a sphere for this mesh
	//if there is already a mesh here then it will overwrite the old mesh
	//currently creates sphere
	void createSphere();

	//merges another mesh into this mesh
	//if a_KeepMaterial is false this mesh with take the other mesh's material
	void merge(const Mesh* a_Other, bool a_KeepMaterial);

	//this will create tangent data for all vertices's 
	void genTangentsAndSet();

	//removes all data and unbinds VAO,VBO,IBO's
	void clear();

	//unbinds all buffers (VAO,VBO,IBO)
	void unbind();

	//goes through all verts and looks for the most max vert positions
	glm::vec3 getMaxVert();

	//will set this mesh's current material to
	//if a_Material = nullptr then Mesh will make a new white material
	void setMaterial(Material* a_Material);

	void setDiffuse(Texture* a_Texture);
	void setNormal(Texture* a_Texture);
	void setSpecular(Texture* a_Texture);
	//sets the color of the material
	//if color is black or alpha is 0 then it will remove color from material
	void setColor(glm::vec4 a_Color);

	void bind();

	// Inherited via IDrawable
	virtual void draw() override;
	virtual bool isTransparent() override;
	virtual const char * getName() override;

	//reference to the material
	Material* m_Material = nullptr;

protected:
	unsigned int m_VAO=0, m_VBO=0, m_IBO=0;


	std::vector<VertexTex> m_Vertexs;
	std::vector<unsigned int> m_Indices;

private:
	//will deal with deleting the material if this mesh made it
	void removeMaterial();

	//if true that means that this mesh should delete the material because it made it
	bool m_CreatedMaterial = false;




};

