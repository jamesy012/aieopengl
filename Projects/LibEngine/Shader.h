#pragma once

//#define glUseProgram(a_Program) Shader::useProgram(a_Program)

//for UniformTypes enum
//TODO perhaps just hardcore the number to not include this massive file??
#include <gl_core_4_4.h>

#include <glm\glm.hpp>
#include <string>
#include <vector>

class Camera;
class Transform;

enum class UniformTypes {
	FLOAT_MAT4 = GL_FLOAT_MAT4,
	FLOAT_VEC4 = GL_FLOAT_VEC4,
	FLOAT_VEC3 = GL_FLOAT_VEC3,
	INT_VEC2 = GL_INT_VEC2,
	FLOAT_VEC2 = GL_FLOAT_VEC2,
	FLOAT = GL_FLOAT,
	INT = GL_INT,
	UNSIGNED_INT = GL_UNSIGNED_INT,
	SAMPLER_2D = GL_SAMPLER_2D,
};

struct ShaderUniform {
	UniformTypes dataType;
	std::string name;
	float* data = nullptr;
};

class Shader {
	enum ShaderTypes {
		Vertex,
		Fragment,
		Geometry,
		END_SHADER_TYPES
	};

	struct ShaderStruct {
	public:
		unsigned int shaderId = 0;
		std::string path = "";
	};

	//list of common uniforms
	enum CommonUniforms {
		ProjectionView,
		Time,
		Model,
		NormalRot,
		END
	};

public:
	//gets the size of each type
	static unsigned int getUniformTypeSize(const UniformTypes a_Type);
	//converts openGL types into UniformTypes enum
	static UniformTypes getUnifromTypeFromGl(const GLenum a_Type);

public:
	Shader();
	~Shader();

	//takes in a const char* containing the shader code
	//and sets up the shader program using that shader code
	void setShader(const char* a_VertexShader, const char* a_FragmentShader);
	//takes in a file location of shader text files
	//then calls setShader with loaded files
	void setShaderFromFile(const char* a_VertexFileLocation, const char* a_FragmentShaderLocation);

	void setVertexShader(const char* a_VertexShader);
	void setFragmentShader(const char* a_FragmentShader);
	void setGeometryShader(const char* a_GeometryShader);
	void setVertexShaderFromFile(const char* a_VertexShader);
	void setFragmentShaderFromFile(const char* a_FragmentShader);
	void setGeometryShaderFromFile(const char* a_GeometryShader);
	std::string loadShaderFromFile(const char* a_ShaderFileLocation);

	//final step of shader creation
	//will link the vertex/fragment shaders into one program
	void linkShader();
	//will create the shader program on the GPU
	//does not set anything
	void createProgram();

	//creates a basic shader
	//if a_Textrured is false then the fragment shader will have a vec4 color uniform
	//if true a Sampler2D TexDiffuse1
	//it takes vec4 position in location 0
	//and a vec2 texCoord in location 2
	//mat4 uniform projectionViewMatrix
	//mat4 model
	void createBasicProgram(bool a_Textured = false);

	//static function to set current shader
	//static void useProgram(const unsigned int a_ProgramID);
	//static function to set current shader
	static void useProgram(Shader* a_Shader);
	//static function to set current shader
	void useProgram();

	//find common uniform locations within shaders
	//todo deprecate this, use getUniformInformation instead
	void findCommonUniformLocations();
	void getUniformInformation();

	void setUniformTime();
	void setUniformTime(float time);
	void setUniformProjectionView(const glm::mat4& a_ProjectionView);
	void setUniformProjectionView(Camera* a_Camera);
	void setNormalRotation(const glm::mat4& a_ModelMatrix);
	void setModelMatrix(const glm::mat4& a_ModelMatrix);
	void setModelAndNormalMatrix(const glm::mat4& a_ModelMatrix);
	void setNormalRotation(Transform& a_ModelMatrix);
	void setModelMatrix(Transform& a_ModelMatrix);
	void setModelAndNormalMatrix(Transform& a_ModelMatrix);

	unsigned int getUniformLocation(const char* a_Name);
	void setUniform(const ShaderUniform* a_Uniform);
	void setUniform(const char* a_Name, float a_A0);
	void setUniform(const char* a_Name, unsigned int a_A0);
	void setUniform(const char* a_Name, float a_A0, float a_A1);
	void setUniform(const char* a_Name, int a_A0, int a_A1);
	void setUniform(const char* a_Name, float a_A0, float a_A1, float a_A2);
	void setUniform(const char* a_Name, const glm::vec3& a_Vec3);
	void setUniform(const char* a_Name, float a_A0, float a_A1, float a_A2, float a_A3);
	void setUniform(const char* a_Name, const glm::vec4& a_Vec4);
	void setUniform(const char* a_Name, const glm::mat4& a_mat4);

	void deleteShader();

	const int getProgramId() const;

	//gets currently used shader
	static Shader* getCurrentShader();

	bool m_ShowUniformErrorMessages = true;
private:
	bool m_Linked = false;
	ShaderStruct m_Shaders[ShaderTypes::END_SHADER_TYPES];

	//ID of the shader program
	unsigned int m_ProgramID = 0;

	//contains location of common uniforms
	unsigned int m_CommonUniformLoc[CommonUniforms::END] = { 0 };

	//returns the ShaderUniformStruct of a object that fits both criteria from the m_Uniforms vector
	ShaderUniform* getUniformObject(const UniformTypes a_Type, const std::string a_Name);
	//returns the ShaderUniformStruct of a object that fits both criteria from the m_Uniforms vector
	ShaderUniform* getUniformObject(const UniformTypes a_Type, const unsigned int a_Location);
	//float version
	//sets ShaderUniform data from a_Data
	void setUniformObjectData(const UniformTypes a_Type, const std::string a_Name, const float* a_Data);
	//int version
	//sets ShaderUniform data from a_Data
	void setUniformObjectData(const UniformTypes a_Type, const std::string a_Name, const int* a_Data);
	//unsigned int version
	//sets ShaderUniform data from a_Data
	void setUniformObjectData(const UniformTypes a_Type, const std::string a_Name, const unsigned int* a_Data);
	//float version
	//sets ShaderUniform data from a_Data
	void setUniformObjectData(const UniformTypes a_Type, const unsigned int m_Location, const float* a_Data);
	//int version	
	//sets ShaderUniform data from a_Data
	void setUniformObjectData(const UniformTypes a_Type, const unsigned int m_Location, const int* a_Data);
	//unsigned int version		
	//sets ShaderUniform data from a_Data
	void setUniformObjectData(const UniformTypes a_Type, const unsigned int m_Location, const unsigned int* a_Data);

	std::vector<ShaderUniform*> m_Uniforms;

};

