#pragma once

class Application;

class TimeHandler {
	friend Application;
public:

	static float getCurrentTime();
	static int getCurrentFrameNumber();
	static float getPreviousTime();
	static float getDeltaTime();
	static float getUnscaledDeltaTime();
	static float getDeltaTimeScale();

	static void setScale(float a_Scale);
protected:
	static void update();

	//static variables for deltaTime are in .cpp
};
