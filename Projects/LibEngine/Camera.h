#pragma once

#include "Transform.h"

class Application;

class Camera : public Transform {
	friend Application;
public:
	Camera();
	virtual ~Camera();

	virtual void update() = 0;
	void setPerspective(float a_FieldOfView, float a_Aspect, float a_Near, float a_Far);
	void setOthrographic(const float a_Left,const float a_Right,const float a_Bottom,const float a_Top,const float a_Near = -1.0f,const float a_Far = 1.0f);

	glm::mat4 getView();
	glm::mat4 getProjection();
	glm::mat4 getProjectionView();

	void updateTransform();

	//reference to the main camera
	static Camera* m_MainCamera;
protected:
	void updateProjectionViewTransform();

	float m_FieldOfView;
	float m_Aspect;
	float m_NearPlane;
	float m_FarPlane;

	glm::mat4 m_ViewTransform;
	glm::mat4 m_ProjectionTransform;
	glm::mat4 m_ProjectionViewTransform;

};

