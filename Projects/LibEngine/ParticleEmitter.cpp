#include "ParticleEmitter.h"

#include <gl_core_4_4.h>

#include <glm/ext.hpp>


#include "Transform.h"
#include "TimeHandler.h"

ParticleEmitter::ParticleEmitter()
	: m_Particles(nullptr), m_FirstDead(0), m_MaxParticles(0),
	m_Position(0, 0, 0), m_Vao(0), m_Vbo(0), m_Ibo(0),
	m_VertexData(nullptr) {
}


ParticleEmitter::~ParticleEmitter() {
	delete[] m_Particles;
	delete[] m_VertexData;

	glDeleteVertexArrays(1, &m_Vao);
	glDeleteBuffers(1, &m_Vbo);
	glDeleteBuffers(1, &m_Ibo);
}

void ParticleEmitter::initalise(unsigned int a_MaxPatricles, unsigned int a_EmitRate, float a_LifetimeMin, float a_LifetimeMax, float a_VelocityMin, float a_VelocityMax, float a_StartSize, float a_EndSize, const glm::vec4 & a_StartColor, const glm::vec4 & a_EndColor) {

	m_EmitTimer = 0;
	m_EmitRate = 1.0f / a_EmitRate;

	m_StartColor = a_StartColor;
	m_EndColor = a_EndColor;

	m_StartSize = a_StartSize;
	m_EndSize = a_EndSize;

	m_VelocityMin = a_VelocityMin;
	m_VelocityMax = a_VelocityMax;

	m_LifespanMin = a_LifetimeMin;
	m_LifeSpanMax = a_LifetimeMax;

	m_MaxParticles = a_MaxPatricles;

	m_Particles = new Particle[m_MaxParticles];
	m_FirstDead = 0;

	m_VertexData = new ParticleVertex[m_MaxParticles * 4];
	unsigned int* indexData = new unsigned int[m_MaxParticles * 6];
	for (unsigned int i = 0; i < m_MaxParticles; i++) {
		indexData[i * 6 + 0] =i * 4 + 0;
		indexData[i * 6 + 1] =i * 4 + 1;
		indexData[i * 6 + 2] =i * 4 + 2;
							  
		indexData[i * 6 + 3] =i * 4 + 0;
		indexData[i * 6 + 4] =i * 4 + 2;
		indexData[i * 6 + 5] =i * 4 + 3;
	}
	glGenVertexArrays(1, &m_Vao);
	glBindVertexArray(m_Vao);

	glGenBuffers(1, &m_Vbo);
	glGenBuffers(1, &m_Ibo);

	glBindBuffer(GL_ARRAY_BUFFER, m_Vbo);
	glBufferData(GL_ARRAY_BUFFER, m_MaxParticles * 4 * sizeof(ParticleVertex), m_VertexData, GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_Ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_MaxParticles * 6 * sizeof(unsigned int), indexData, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);//pos
	glEnableVertexAttribArray(1);//col

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(ParticleVertex), 0);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(ParticleVertex), ((char*)0) + 16);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	delete[] indexData;

}

void ParticleEmitter::emit() {
	if (m_FirstDead >= m_MaxParticles) {
		return;
	}

	Particle& particle = m_Particles[m_FirstDead++];

	particle.position = m_Position;

	particle.lifeTime = 0;
	particle.lifeSpan = (rand() / (float)RAND_MAX) * (m_LifeSpanMax - m_LifespanMin) + m_LifespanMin;

	particle.color = m_StartColor;
	particle.size = m_StartSize;

	float velocity = (rand() / (float)RAND_MAX) * (m_VelocityMax - m_VelocityMin) + m_VelocityMin;

	particle.velocity.x = (rand() / (float)RAND_MAX) * 2 - 1;
	particle.velocity.y = (rand() / (float)RAND_MAX) * 2 - 1;
	particle.velocity.z = (rand() / (float)RAND_MAX) * 2 - 1;

	particle.velocity = glm::normalize(particle.velocity) * velocity;
}

void ParticleEmitter::update(Transform & a_BillboardTransform) {

	m_EmitTimer += TimeHandler::getDeltaTime();
	while (m_EmitTimer > m_EmitRate) {
		emit();
		m_EmitTimer -= m_EmitRate;
	}

	//glm::mat4 globalPos = a_BillboardTransform.getGlobalTransform();

	unsigned int quad = 0;

	//update and set billboards
	for (unsigned int i = 0; i < m_FirstDead; i++) {
		Particle* particle = &m_Particles[i];

		particle->lifeTime += TimeHandler::getDeltaTime();
		if (particle->lifeTime >= particle->lifeSpan) {
			*particle = m_Particles[m_FirstDead - 1];
			m_FirstDead--;
		} else {

			particle->position += particle->velocity * TimeHandler::getDeltaTime();
			particle->size = glm::mix(m_StartSize, m_EndSize, particle->lifeTime / particle->lifeSpan);
			particle->color = glm::mix(m_StartColor, m_EndColor, particle->lifeTime / particle->lifeSpan);

			//billboard
			float halfSize = particle->size * 0.5f;


			m_VertexData[quad * 4 + 0].position = glm::vec4(halfSize, halfSize, 0, 1);
			m_VertexData[quad * 4 + 1].position = glm::vec4(-halfSize, halfSize, 0, 1);
			m_VertexData[quad * 4 + 2].position = glm::vec4(-halfSize, -halfSize, 0, 1);
			m_VertexData[quad * 4 + 3].position = glm::vec4(halfSize, -halfSize, 0, 1);

			m_VertexData[quad * 4 + 0].color = particle->color;
			m_VertexData[quad * 4 + 1].color = particle->color;
			m_VertexData[quad * 4 + 2].color = particle->color;
			m_VertexData[quad * 4 + 3].color = particle->color;

			//glm::vec3 zAxis = glm::normalize(glm::vec3(globalPos[3])) - particle->position;
			//
			//glm::vec3 xAxis = glm::cross(glm::vec3(globalPos[1]), zAxis);
			//glm::vec3 yAxis = glm::cross(zAxis, xAxis);
			//
			//glm::mat4 billboard = glm::mat4(
			//	glm::vec4(xAxis, 0),
			//	glm::vec4(yAxis, 0),
			//	glm::vec4(zAxis, 0),
			//	glm::vec4(0, 0, 0, 1));


			//m_VertexData[quad * 4 + 0].position = billboard * m_VertexData[quad * 4 + 0].position + glm::vec4(particle->position, 0);
			//m_VertexData[quad * 4 + 1].position = billboard * m_VertexData[quad * 4 + 1].position + glm::vec4(particle->position, 0);
			//m_VertexData[quad * 4 + 2].position = billboard * m_VertexData[quad * 4 + 2].position + glm::vec4(particle->position, 0);
			//m_VertexData[quad * 4 + 3].position = billboard * m_VertexData[quad * 4 + 3].position + glm::vec4(particle->position, 0);

			//billboard = glm::lookAt(glm::vec3(m_VertexData[quad * 4 + 0].position) + particle->position, a_BillboardTransform.getGlobalPosition(), glm::vec3(0, 1, 0));

			glm::quat rot = a_BillboardTransform.getGlobalRotation();

			m_VertexData[quad * 4 + 0].position = rot * m_VertexData[quad * 4 + 0].position + glm::vec4(particle->position,0);
			m_VertexData[quad * 4 + 1].position = rot * m_VertexData[quad * 4 + 1].position + glm::vec4(particle->position, 0);
			m_VertexData[quad * 4 + 2].position = rot * m_VertexData[quad * 4 + 2].position + glm::vec4(particle->position, 0);
			m_VertexData[quad * 4 + 3].position = rot * m_VertexData[quad * 4 + 3].position + glm::vec4(particle->position, 0);

			quad++;

		}
	}

}

void ParticleEmitter::draw() {
	//update vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, m_Vbo);
	glBufferSubData(GL_ARRAY_BUFFER, 0, m_FirstDead * 4 * sizeof(ParticleVertex), m_VertexData);

	glBindVertexArray(m_Vao);
	glDrawElements(GL_TRIANGLES, m_FirstDead * 6, GL_UNSIGNED_INT, 0);
}
