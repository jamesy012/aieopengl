#pragma once

#include <glm/glm.hpp>

struct VertexColor {
	glm::vec4 position;
	glm::vec4 color;
};

struct VertexTex {
	glm::vec4 position;
	glm::vec4 normal;
	glm::vec2 texCoord;
	glm::vec4 tangent;
	glm::vec4 biTangent;
};