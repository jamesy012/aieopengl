#pragma once

#include <glm\glm.hpp>

class Transform;

struct Particle {
	glm::vec3 position;
	glm::vec3 velocity;
	glm::vec4 color;
	float size;
	float lifeTime;
	float lifeSpan;
};

struct ParticleVertex {
	glm::vec4 position;
	glm::vec4 color;
};

class ParticleEmitter {
public:
	ParticleEmitter();
	virtual ~ParticleEmitter();

	void initalise(unsigned int a_MaxPatricles, unsigned int a_EmitRate,
		float a_LifetimeMin, float a_LifetimeMax,
		float a_VelocityMin, float a_VelocityMax,
		float a_StartSize, float a_EndSize,
		const glm::vec4& a_StartColor, const glm::vec4& a_EndColor);

	void emit();

	void update(Transform& a_BillboardTransform);

	void draw();

protected:

	Particle* m_Particles;
	unsigned int m_FirstDead;
	unsigned int m_MaxParticles;

	unsigned int m_Vao, m_Vbo, m_Ibo;
	ParticleVertex* m_VertexData;

	glm::vec3 m_Position;
	
	float m_EmitTimer;
	float m_EmitRate;

	float m_LifespanMin;
	float m_LifeSpanMax;

	float m_VelocityMin;
	float m_VelocityMax;

	float m_StartSize;
	float m_EndSize;

	glm::vec4 m_StartColor;
	glm::vec4 m_EndColor;
};

