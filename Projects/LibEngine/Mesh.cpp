#include "Mesh.h"

#include <gl_core_4_4.h>
#include <glm\ext.hpp>

#include "Texture.h"
#include "Material.h"
#include "Shader.h"

Mesh::Mesh() {
	setMaterial(nullptr);
	m_Transform.m_Name = "Mesh";
}


Mesh::~Mesh() {
	removeMaterial();
	clear();
}

Mesh::Mesh(const Mesh & a_Copy) {
	//copy transform data
	m_Transform = a_Copy.m_Transform;

	//resize indices so it wont keep resizing after every few adds
	m_Indices.resize(a_Copy.m_Indices.size());
	for (size_t i = 0; i < a_Copy.m_Indices.size(); i++) {
		//set this index to a_Copys index
		m_Indices[i] = a_Copy.m_Indices[i];
	}

	//resize indices so it wont keep resizing after every few adds
	m_Vertexs.resize(a_Copy.m_Vertexs.size());
	for (size_t i = 0; i < a_Copy.m_Vertexs.size(); i++) {
		//copy a_Copys
		m_Vertexs[i] = a_Copy.m_Vertexs[i];
	}

	if (m_CreatedMaterial) {
		delete m_Material;
	}
	m_Material = new Material(*a_Copy.m_Material);
	m_CreatedMaterial = true;

	bind();

}

void Mesh::createPlane(bool a_FlipYCoords) {
	clear();
	//data from a obj file made in blender
	glm::vec4 vertPos[] = {
		glm::vec4(-1, 0,  1, 1),
		glm::vec4(1,  0,  1, 1),
		glm::vec4(-1, 0, -1, 1),
		glm::vec4(1,  0, -1, 1),
	};
	glm::vec4 normals[]{
		glm::vec4(0, 1,  0, 0),
	};
	glm::vec2 texCoords[]{//y coords have been flipped
		glm::vec2(1,1),
		glm::vec2(1,0),
		glm::vec2(0,0),
		glm::vec2(0,1),
	};
	unsigned int indexSize = 18;//by mousing over data below
	unsigned int indexData[]{
		2 , 1, 1, 4 , 2, 1, 3 , 3 ,1,
		1 , 4, 1, 2 , 1, 1, 3 , 3 ,1,
	};
	m_Vertexs.reserve(indexSize);
	m_Indices.reserve(indexSize);
	for (unsigned int i = 0; i < indexSize; i += 3) {
		VertexTex vert;
		vert.position = vertPos[indexData[i] - 1];
		vert.texCoord = texCoords[indexData[i + 1] - 1];
		vert.normal = normals[indexData[i + 2] - 1];

		if (a_FlipYCoords) {
			vert.texCoord.y = -vert.texCoord.y;
		}

		m_Vertexs.push_back(vert);
		m_Indices.push_back(i / 3);
	}

	bind();
	m_Transform.m_Name = "Plane";
}

void Mesh::createGrid(unsigned int a_X, unsigned int a_Z) {
	//input error checking
	if (a_X == 0 || a_Z == 0) {
		printf("Cant Create plane, x or y is 0. (%u,%u)", a_X, a_Z);
		return;
	}
	//set up class for function
	clear();
	//pre-calculated information
	const unsigned int verts = (a_X) * (a_Z);
	const unsigned int indices = (a_X - 1) * (a_Z - 1) * 6;
	const glm::vec4 normal(0, 1, 0, 0);
	//common data
	glm::vec4 vertPos(0, 0, 0, 1);//vertex Position
	glm::vec2 texCoord;//texture coord
	//set up arrays
	m_Vertexs.resize(verts);
	m_Indices.resize(indices);
	//loop, create verts
	for (size_t z = 0; z < a_Z; z++) {
		for (size_t x = 0; x < a_X; x++) {
			//work out texCoords (between 0,1)
			float xPercentage = (float)x / a_X;
			float zPercentage = (float)z / a_Z;
			//set tex coord (between 0,1)
			texCoord = glm::vec2(xPercentage, zPercentage);
			//set position, between -1,1
			vertPos.x = (xPercentage*2.0f) - 1;
			vertPos.z = (zPercentage*2.0f) - 1;


			VertexTex vt;
			vt.normal = normal;
			vt.texCoord = texCoord;
			vt.position = vertPos;
			m_Vertexs[z * a_X + x] = vt;
		}
	}
	unsigned int index = 0;
	for (unsigned int z = 0; z < a_Z - 1; ++z) {
		for (unsigned int x = 0; x < a_X - 1; ++x) {
			//tri 1
			m_Indices[index++] = z * a_X + x;
			m_Indices[index++] = (z + 1) * a_X + x;
			m_Indices[index++] = (z + 1) * a_X + (x + 1);
			//tri 2
			m_Indices[index++] = z * a_X + x;
			m_Indices[index++] = (z + 1) * a_X + (x + 1);
			m_Indices[index++] = z * a_X + (x + 1);
		}
	}
	bind();
	m_Transform.m_Name = "Grid";
}

void Mesh::createBox() {
	clear();
	//data from a obj file made in blender
	glm::vec4 vertPos[] = {
		glm::vec4(1, -1, -1, 1),
		glm::vec4(1, -1,  1, 1),
		glm::vec4(-1, -1,  1, 1),
		glm::vec4(-1, -1, -1, 1),
		glm::vec4(1,  1, -1, 1),
		glm::vec4(1,  1,  1, 1),
		glm::vec4(-1,  1,  1, 1),
		glm::vec4(-1,  1, -1, 1),
	};
	glm::vec4 normals[]{
		glm::vec4(0, -1,  0, 0),
		glm::vec4(0,  1,  0, 0),
		glm::vec4(1, -0,  0, 0),
		glm::vec4(-0, -0,  1, 0),
		glm::vec4(-1, -0, -0, 0),
		glm::vec4(0,  0, -1, 0),
	};
	glm::vec2 texCoords[]{//y coords have been flipped
		glm::vec2(1,0),
		glm::vec2(0,0),
		glm::vec2(0,1),
		glm::vec2(1,1),
	};
	unsigned int indexSize = 108;//by mousing over data below
	unsigned int indexData[]{
		2 , 1 , 1,	 3 , 2 , 1,	 4 , 3 , 1,
		8 , 2 , 2,	 7 , 3 , 2,	 6 , 4 , 2,
		1 , 4 , 3,	 5 , 1 , 3,	 6 , 2 , 3,
		2 , 4 , 4,	 6 , 1 , 4,	 7 , 2 , 4,
		7 , 1 , 5,	 8 , 2 , 5,	 4 , 3 , 5,
		1 , 3 , 6,	 4 , 4 , 6,	 8 , 1 , 6,
		1 , 4 , 1,	 2 , 1 , 1,	 4 , 3 , 1,
		5 , 1 , 2,	 8 , 2 , 2,	 6 , 4 , 2,
		2 , 3 , 3,	 1 , 4 , 3,	 6 , 2 , 3,
		3 , 3 , 4,	 2 , 4 , 4,	 7 , 2 , 4,
		3 , 4 , 5,	 7 , 1 , 5,	 4 , 3 , 5,
		5 , 2 , 6,	 1 , 3 , 6,	 8 , 1 , 6,
	};
	m_Vertexs.reserve(indexSize);
	m_Indices.reserve(indexSize);
	for (unsigned int i = 0; i < indexSize; i += 3) {
		VertexTex vert;
		vert.position = vertPos[indexData[i] - 1];
		vert.texCoord = texCoords[indexData[i + 1] - 1];
		vert.normal = normals[indexData[i + 2] - 1];

		m_Vertexs.push_back(vert);
		m_Indices.push_back(i / 3);
	}
	bind();
	m_Transform.m_Name = "Box";
}

void Mesh::createSphere() {
	//TODO make this dynamic like the createGrid function
	clear();
	//data from a obj file made in blender
	glm::vec4 vertPos[] = {
		glm::vec4(-0.707107, 0.707107, 0.000000  ,1),
		glm::vec4(-0.923880, 0.382683, 0.000000  ,1),
		glm::vec4(-0.923880, -0.382684, 0.000000 ,1),
		glm::vec4(-0.707107, -0.707107, 0.000000 ,1),
		glm::vec4(-0.382683, -0.923880, 0.000000 ,1),
		glm::vec4(-0.309597, 0.923880, -0.224936 ,1),
		glm::vec4(-0.572061, 0.707107, -0.415627 ,1),
		glm::vec4(-0.747434, 0.382683, -0.543043 ,1),
		glm::vec4(-0.809017, -0.000000, -0.587785,1),
		glm::vec4(-0.747434, -0.382684, -0.543043,1),
		glm::vec4(-0.572061, -0.707107, -0.415627,1),
		glm::vec4(-0.309597, -0.923880, -0.224936,1),
		glm::vec4(-0.118256, 0.923880, -0.363953 ,1),
		glm::vec4(-0.218508, 0.707107, -0.672498 ,1),
		glm::vec4(-0.285495, 0.382683, -0.878661 ,1),
		glm::vec4(-0.309017, -0.000000, -0.951056,1),
		glm::vec4(-0.285495, -0.382684, -0.878661,1),
		glm::vec4(-0.218508, -0.707107, -0.672498,1),
		glm::vec4(-0.118256, -0.923880, -0.363953,1),
		glm::vec4(-0.000000, 1.000000, 0.000000  ,1),
		glm::vec4(0.118255, 0.923880, -0.363954  ,1),
		glm::vec4(0.218508, 0.707107, -0.672498  ,1),
		glm::vec4(0.285494, 0.382683, -0.878661  ,1),
		glm::vec4(0.309017, -0.000000, -0.951056 ,1),
		glm::vec4(0.285494, -0.382684, -0.878661 ,1),
		glm::vec4(0.218508, -0.707107, -0.672498 ,1),
		glm::vec4(0.118255, -0.923880, -0.363954 ,1),
		glm::vec4(0.309597, 0.923880, -0.224936  ,1),
		glm::vec4(0.572061, 0.707107, -0.415627  ,1),
		glm::vec4(0.747434, 0.382683, -0.543043  ,1),
		glm::vec4(0.809017, -0.000000, -0.587785 ,1),
		glm::vec4(0.747434, -0.382684, -0.543043 ,1),
		glm::vec4(0.572061, -0.707107, -0.415627 ,1),
		glm::vec4(0.309597, -0.923880, -0.224936 ,1),
		glm::vec4(0.382683, 0.923880, -0.000000  ,1),
		glm::vec4(0.707107, 0.707107, -0.000000  ,1),
		glm::vec4(0.923879, 0.382683, -0.000000  ,1),
		glm::vec4(1.000000, -0.000000, 0.000000  ,1),
		glm::vec4(0.923879, -0.382684, -0.000000 ,1),
		glm::vec4(0.707107, -0.707107, -0.000000 ,1),
		glm::vec4(0.382683, -0.923880, -0.000000 ,1),
		glm::vec4(0.309597, 0.923880, 0.224936	  ,1),
		glm::vec4(0.572061, 0.707107, 0.415627	  ,1),
		glm::vec4(0.747434, 0.382683, 0.543043	  ,1),
		glm::vec4(0.809017, -0.000000, 0.587785  ,1),
		glm::vec4(0.747434, -0.382684, 0.543043  ,1),
		glm::vec4(0.572061, -0.707107, 0.415627  ,1),
		glm::vec4(0.309597, -0.923880, 0.224936  ,1),
		glm::vec4(0.118256, 0.923880, 0.363953	  ,1),
		glm::vec4(0.218508, 0.707107, 0.672498	  ,1),
		glm::vec4(0.285494, 0.382683, 0.878662	  ,1),
		glm::vec4(0.309017, -0.000000, 0.951056  ,1),
		glm::vec4(0.285494, -0.382684, 0.878662  ,1),
		glm::vec4(0.218508, -0.707107, 0.672498  ,1),
		glm::vec4(0.118256, -0.923880, 0.363953  ,1),
		glm::vec4(-0.118256, 0.923880, 0.363954  ,1),
		glm::vec4(-0.218508, 0.707107, 0.672498  ,1),
		glm::vec4(-0.285495, 0.382683, 0.878662  ,1),
		glm::vec4(-0.309017, -0.000000, 0.951056 ,1),
		glm::vec4(-0.285495, -0.382684, 0.878662 ,1),
		glm::vec4(-0.218508, -0.707107, 0.672498 ,1),
		glm::vec4(-0.118256, -0.923880, 0.363954 ,1),
		glm::vec4(-0.000000, -1.000000, 0.000000 ,1),
		glm::vec4(-0.309597, 0.923880, 0.224936  ,1),
		glm::vec4(-0.572061, 0.707107, 0.415627  ,1),
		glm::vec4(-0.747434, 0.382683, 0.543043  ,1),
		glm::vec4(-0.809017, -0.000000, 0.587785 ,1),
		glm::vec4(-0.747434, -0.382684, 0.543043 ,1),
		glm::vec4(-0.572061, -0.707107, 0.415627 ,1),
		glm::vec4(-0.309597, -0.923880, 0.224936 ,1),
		glm::vec4(-0.382684, 0.923880, 0.000000  ,1),
		glm::vec4(-1.000000, -0.000000, 0.000000 ,1)
	};
	glm::vec4 normals[]{
		glm::vec4(-0.546700,-0.818200,-0.17760,0),
		glm::vec4(-0.802700,-0.536300,-0.26080,0),
		glm::vec4(-0.802700,0.536300,-0.260800,0),
		glm::vec4(-0.337900,-0.818200,-0.46510,0),
		glm::vec4(-0.496100,-0.536300,-0.68280,0),
		glm::vec4(-0.577500,-0.185900,-0.79490,0),
		glm::vec4(-0.577500,0.185900,-0.794900,0),
		glm::vec4(-0.496100,0.536300,-0.682800,0),
		glm::vec4(-0.337900,0.818200,-0.465100,0),
		glm::vec4(0.000000,-0.818200,-0.574900,0),
		glm::vec4(0.000000,-0.536300,-0.844000,0),
		glm::vec4(0.000000,-0.185900,-0.982600,0),
		glm::vec4(0.000000,0.185900,-0.982600 ,0),
		glm::vec4(0.000000,0.536300,-0.844000 ,0),
		glm::vec4(-0.000000,0.818200,-0.574900,0),
		glm::vec4(0.337900,-0.818200,-0.465100,0),
		glm::vec4(0.496100,-0.536300,-0.682800,0),
		glm::vec4(0.577500,-0.185900,-0.794900,0),
		glm::vec4(0.577500,0.185900,-0.794900 ,0),
		glm::vec4(0.496100,0.536300,-0.682800 ,0),
		glm::vec4(0.337900,0.818200,-0.465100 ,0),
		glm::vec4(0.546700,-0.818200,-0.177600,0),
		glm::vec4(0.802700,-0.536300,-0.260800,0),
		glm::vec4(0.934500,-0.185900,-0.303600,0),
		glm::vec4(0.934500,0.185900,-0.303600 ,0),
		glm::vec4(0.802700,0.536300,-0.260800 ,0),
		glm::vec4(0.546700,0.818200,-0.177600 ,0),
		glm::vec4(0.546700,-0.818200,0.177600 ,0),
		glm::vec4(0.802700,-0.536300,0.260800 ,0),
		glm::vec4(0.934500,-0.185900,0.303600 ,0),
		glm::vec4(0.934500,0.185900,0.303600  ,0),
		glm::vec4(0.802700,0.536300,0.260800  ,0),
		glm::vec4(0.546700,0.818200,0.177600  ,0),
		glm::vec4(0.337900,-0.818200,0.465100 ,0),
		glm::vec4(0.496100,-0.536300,0.682800 ,0),
		glm::vec4(0.577500,-0.185900,0.794900 ,0),
		glm::vec4(0.577500,0.185900,0.794900  ,0),
		glm::vec4(0.496100,0.536300,0.682800  ,0),
		glm::vec4(0.337900,0.818200,0.465100  ,0),
		glm::vec4(-0.000000,-0.818200,0.574900,0),
		glm::vec4(0.000000,-0.536300,0.844000 ,0),
		glm::vec4(0.000000,-0.185900,0.982600 ,0),
		glm::vec4(0.000000,0.185900,0.982600  ,0),
		glm::vec4(0.000000,0.536300,0.844000  ,0),
		glm::vec4(0.000000,0.818200,0.574900  ,0),
		glm::vec4(-0.337900,-0.818200,0.465100,0),
		glm::vec4(-0.496100,-0.536300,0.682800,0),
		glm::vec4(-0.577500,-0.185900,0.794900,0),
		glm::vec4(-0.577500,0.185900,0.794900 ,0),
		glm::vec4(-0.496100,0.536300,0.682800 ,0),
		glm::vec4(-0.337900,0.818200,0.465100 ,0),
		glm::vec4(-0.194700,-0.978800,-0.06330,0),
		glm::vec4(-0.934500,-0.185900,-0.30360,0),
		glm::vec4(-0.934500,0.185900,-0.303600,0),
		glm::vec4(-0.546700,0.818200,-0.177600,0),
		glm::vec4(-0.194700,0.978800,-0.063300,0),
		glm::vec4(-0.120300,-0.978800,-0.16560,0),
		glm::vec4(-0.120300,0.978800,-0.165600,0),
		glm::vec4(-0.000000,-0.978800,-0.20470,0),
		glm::vec4(-0.000000,0.978800,-0.204700,0),
		glm::vec4(0.120300,-0.978800,-0.165600,0),
		glm::vec4(0.120300,0.978800,-0.165600 ,0),
		glm::vec4(0.194700,-0.978800,-0.063300,0),
		glm::vec4(0.194700,0.978800,-0.063300 ,0),
		glm::vec4(0.194700,-0.978800,0.063300 ,0),
		glm::vec4(0.194700,0.978800,0.063300  ,0),
		glm::vec4(0.120300,-0.978800,0.165600 ,0),
		glm::vec4(0.120300,0.978800,0.165600  ,0),
		glm::vec4(0.000000,-0.978800,0.204700 ,0),
		glm::vec4(0.000000,0.978800,0.204700  ,0),
		glm::vec4(-0.120300,-0.978800,0.165600,0),
		glm::vec4(-0.120300,0.978800,0.165600 ,0),
		glm::vec4(-0.194700,-0.978800,0.063300,0),
		glm::vec4(-0.546700,-0.818200,0.177600,0),
		glm::vec4(-0.802700,-0.536300,0.260800,0),
		glm::vec4(-0.934500,-0.185900,0.303600,0),
		glm::vec4(-0.934500,0.185900,0.303600 ,0),
		glm::vec4(-0.802700,0.536300,0.260800 ,0),
		glm::vec4(-0.546700,0.818200,0.177600 ,0),
		glm::vec4(-0.194700,0.978800,0.063300 ,0)
	};
	glm::vec2 texCoords[]{//y coords have been flipped
		glm::vec2(0.494850, 0.292104),
		glm::vec2(0.533179, 0.174138),
		glm::vec2(0.607673, 0.228261),
		glm::vec2(0.433324, 0.292104),
		glm::vec2(0.483404, 0.137974),
		glm::vec2(0.096152, 0.716411),
		glm::vec2(0.135224, 0.836662),
		glm::vec2(0.084485, 0.873527),
		glm::vec2(0.633526, 0.101232),
		glm::vec2(0.661980, 0.188804),
		glm::vec2(0.614514, 0.042717),
		glm::vec2(0.465925, 0.125275),
		glm::vec2(0.607838, 0.022170),
		glm::vec2(0.218135, 0.970630),
		glm::vec2(0.211330, 0.991576),
		glm::vec2(0.237516, 0.910982),
		glm::vec2(0.211162, 0.781491),
		glm::vec2(0.266522, 0.821712),
		glm::vec2(0.757562, 0.101232),
		glm::vec2(0.729108, 0.188804),
		glm::vec2(0.776575, 0.042717),
		glm::vec2(0.783251, 0.022170),
		glm::vec2(0.383337, 0.970630),
		glm::vec2(0.390142, 0.991576),
		glm::vec2(0.363956, 0.910982),
		glm::vec2(0.334950, 0.821712),
		glm::vec2(0.857910, 0.174138),
		glm::vec2(0.783416, 0.228261),
		glm::vec2(0.907685, 0.137974),
		glm::vec2(0.925164, 0.125275),
		glm::vec2(0.516987, 0.873527),
		glm::vec2(0.534805, 0.886472),
		glm::vec2(0.466248, 0.836662),
		glm::vec2(0.390310, 0.781491),
		glm::vec2(0.896239, 0.292104),
		glm::vec2(0.957764, 0.292104),
		glm::vec2(0.979370, 0.292104),
		glm::vec2(0.568038, 0.716411),
		glm::vec2(0.590061, 0.716411),
		glm::vec2(0.505320, 0.716411),
		glm::vec2(0.411456, 0.716411),
		glm::vec2(0.804160, 0.292104),
		glm::vec2(0.857910, 0.410069),
		glm::vec2(0.907685, 0.446233),
		glm::vec2(0.925164, 0.458932),
		glm::vec2(0.516987, 0.559295),
		glm::vec2(0.534805, 0.546350),
		glm::vec2(0.466248, 0.596160),
		glm::vec2(0.390310, 0.651331),
		glm::vec2(0.757562, 0.482976),
		glm::vec2(0.729108, 0.395403),
		glm::vec2(0.776575, 0.541490),
		glm::vec2(0.783251, 0.562038),
		glm::vec2(0.383337, 0.462192),
		glm::vec2(0.390143, 0.441246),
		glm::vec2(0.363956, 0.521840),
		glm::vec2(0.334950, 0.611110),
		glm::vec2(0.633526, 0.482976),
		glm::vec2(0.661980, 0.395403),
		glm::vec2(0.614514, 0.541490),
		glm::vec2(0.607838, 0.562038),
		glm::vec2(0.218135, 0.462192),
		glm::vec2(0.211330, 0.441246),
		glm::vec2(0.237516, 0.521840),
		glm::vec2(0.266522, 0.611110),
		glm::vec2(0.533179, 0.410069),
		glm::vec2(0.483404, 0.446233),
		glm::vec2(0.465925, 0.458932),
		glm::vec2(0.084485, 0.559295),
		glm::vec2(0.066667, 0.546350),
		glm::vec2(0.135224, 0.596160),
		glm::vec2(0.211162, 0.651331),
		glm::vec2(0.695544, 0.292104),
		glm::vec2(0.586929, 0.292104),
		glm::vec2(0.411720, 0.292104),
		glm::vec2(0.033434, 0.716411),
		glm::vec2(0.066667, 0.886472),
		glm::vec2(0.190016, 0.716411),
		glm::vec2(0.300736, 0.716411),
		glm::vec2(0.783416, 0.355946),
		glm::vec2(0.607673, 0.355946),
		glm::vec2(0.011411, 0.716411)
	};
	unsigned int indexSize = 1260;//by mousing over data below
	unsigned int indexData[]{
		4,1,1,11,2,1,12,3,1		  ,
		3,4,2,10,5,2,11,2,2		  ,
		1,6,3,7,7,3,8,8,3		  ,
		11,2,4,18,9,4,19,10,4	  ,
		11,2,5,10,5,5,17,11,5	  ,
		10,5,6,9,12,6,16,13,6	  ,
		8,8,7,15,14,7,16,15,7	  ,
		7,7,8,14,16,8,15,14,8	  ,
		6,17,9,13,18,9,14,16,9	  ,
		18,9,10,26,19,10,27,20,10 ,
		17,11,11,25,21,11,26,19,11,
		16,13,12,24,22,12,25,21,12,
		15,14,13,23,23,13,24,24,13,
		14,16,14,22,25,14,23,23,14,
		13,18,15,21,26,15,22,25,15,
		26,19,16,33,27,16,34,28,16,
		25,21,17,32,29,17,33,27,17,
		24,22,18,31,30,18,32,29,18,
		23,23,19,30,31,19,31,32,19,
		23,23,20,22,25,20,29,33,20,
		21,26,21,28,34,21,29,33,21,
		34,28,22,33,27,22,40,35,22,
		32,29,23,39,36,23,40,35,23,
		31,30,24,38,37,24,39,36,24,
		30,31,25,37,38,25,38,39,25,
		30,31,26,29,33,26,36,40,26,
		28,34,27,35,41,27,36,40,27,
		41,42,28,40,35,28,47,43,28,
		39,36,29,46,44,29,47,43,29,
		38,37,30,45,45,30,46,44,30,
		37,38,31,44,46,31,45,47,31,
		37,38,32,36,40,32,43,48,32,
		35,41,33,42,49,33,43,48,33,
		47,43,34,54,50,34,55,51,34,
		46,44,35,53,52,35,54,50,35,
		45,45,36,52,53,36,53,52,36,
		44,46,37,51,54,37,52,55,37,
		44,46,38,43,48,38,50,56,38,
		42,49,39,49,57,39,50,56,39,
		54,50,40,61,58,40,62,59,40,
		53,52,41,60,60,41,61,58,41,
		53,52,42,52,53,42,59,61,42,
		51,54,43,58,62,43,59,63,43,
		50,56,44,57,64,44,58,62,44,
		49,57,45,56,65,45,57,64,45,
		62,59,46,61,58,46,69,66,46,
		61,58,47,60,60,47,68,67,47,
		59,61,48,67,68,48,68,67,48,
		58,62,49,66,69,49,67,70,49,
		57,64,50,65,71,50,66,69,50,
		56,65,51,64,72,51,65,71,51,
		63,73,52,5,74,52,12,3,52  ,
		72,75,53,9,12,53,10,5,53  ,
		2,76,54,8,8,54,9,77,54	  ,
		1,6,55,71,78,55,6,17,55	  ,
		71,78,56,20,79,56,6,17,56 ,
		63,73,57,12,3,57,19,10,57 ,
		6,17,58,20,79,58,13,18,58 ,
		63,73,59,19,10,59,27,20,59,
		13,18,60,20,79,60,21,26,60,
		63,73,61,27,20,61,34,28,61,
		21,26,62,20,79,62,28,34,62,
		63,73,63,34,28,63,41,42,63,
		28,34,64,20,79,64,35,41,64,
		63,73,65,41,42,65,48,80,65,
		35,41,66,20,79,66,42,49,66,
		63,73,67,48,80,67,55,51,67,
		42,49,68,20,79,68,49,57,68,
		63,73,69,55,51,69,62,59,69,
		49,57,70,20,79,70,56,65,70,
		63,73,71,62,59,71,70,81,71,
		56,65,72,20,79,72,64,72,72,
		63,73,73,70,81,73,5,74,73 ,
		70,81,74,69,66,74,4,1,74  ,
		68,67,75,3,4,75,4,1,75	  ,
		67,68,76,72,75,76,3,4,76  ,
		67,70,77,66,69,77,2,76,77 ,
		65,71,78,1,6,78,2,76,78	  ,
		64,72,79,71,78,79,1,6,79  ,
		64,72,80,20,79,80,71,78,80,
		5,74,1,4,1,1,12,3,1		  ,
		4,1,2,3,4,2,11,2,2		  ,
		2,76,3,1,6,3,8,8,3		  ,
		12,3,4,11,2,4,19,10,4	  ,
		18,9,5,11,2,5,17,11,5	  ,
		17,11,6,10,5,6,16,13,6	  ,
		9,77,7,8,8,7,16,15,7	  ,
		8,8,8,7,7,8,15,14,8		  ,
		7,7,9,6,17,9,14,16,9	  ,
		19,10,10,18,9,10,27,20,10 ,
		18,9,11,17,11,11,26,19,11 ,
		17,11,12,16,13,12,25,21,12,
		16,15,13,15,14,13,24,24,13,
		15,14,14,14,16,14,23,23,14,
		14,16,15,13,18,15,22,25,15,
		27,20,16,26,19,16,34,28,16,
		26,19,17,25,21,17,33,27,17,
		25,21,18,24,22,18,32,29,18,
		24,24,19,23,23,19,31,32,19,
		30,31,20,23,23,20,29,33,20,
		22,25,21,21,26,21,29,33,21,
		41,42,22,34,28,22,40,35,22,
		33,27,23,32,29,23,40,35,23,
		32,29,24,31,30,24,39,36,24,
		31,32,25,30,31,25,38,39,25,
		37,38,26,30,31,26,36,40,26,
		29,33,27,28,34,27,36,40,27,
		48,80,28,41,42,28,47,43,28,
		40,35,29,39,36,29,47,43,29,
		39,36,30,38,37,30,46,44,30,
		38,39,31,37,38,31,45,47,31,
		44,46,32,37,38,32,43,48,32,
		36,40,33,35,41,33,43,48,33,
		48,80,34,47,43,34,55,51,34,
		47,43,35,46,44,35,54,50,35,
		46,44,36,45,45,36,53,52,36,
		45,47,37,44,46,37,52,55,37,
		51,54,38,44,46,38,50,56,38,
		43,48,39,42,49,39,50,56,39,
		55,51,40,54,50,40,62,59,40,
		54,50,41,53,52,41,61,58,41,
		60,60,42,53,52,42,59,61,42,
		52,55,43,51,54,43,59,63,43,
		51,54,44,50,56,44,58,62,44,
		50,56,45,49,57,45,57,64,45,
		70,81,46,62,59,46,69,66,46,
		69,66,47,61,58,47,68,67,47,
		60,60,48,59,61,48,68,67,48,
		59,63,49,58,62,49,67,70,49,
		58,62,50,57,64,50,66,69,50,
		57,64,51,56,65,51,65,71,51,
		3,4,53,72,75,53,10,5,53	  ,
		72,82,54,2,76,54,9,77,54  ,
		7,7,55,1,6,55,6,17,55	  ,
		5,74,74,70,81,74,4,1,74	  ,
		69,66,75,68,67,75,4,1,75  ,
		68,67,76,67,68,76,3,4,76  ,
		72,82,77,67,70,77,2,76,77 ,
		66,69,78,65,71,78,2,76,78 ,
		65,71,79,64,72,79,1,6,79
	};
	m_Vertexs.reserve(indexSize);
	m_Indices.reserve(indexSize);
	for (unsigned int i = 0; i < indexSize; i += 3) {
		VertexTex vert;
		vert.position = vertPos[indexData[i] - 1];
		vert.texCoord = texCoords[indexData[i + 1] - 1];
		vert.normal = normals[indexData[i + 2] - 1];

		m_Vertexs.push_back(vert);
		m_Indices.push_back(i / 3);
	}
	bind();
	m_Transform.m_Name = "Sphere";
}

void Mesh::merge(const Mesh* a_Other, bool a_KeepMaterial) {
	unbind();

	unsigned int currIndicesSize = m_Indices.size();
	unsigned int otherIndicesSize = a_Other->m_Indices.size();
	//resize indices so it wont keep resizing after every few adds
	m_Indices.resize(currIndicesSize + otherIndicesSize);
	for (size_t i = 0; i < otherIndicesSize; i++) {
		//set this index to a_Copys index
		m_Indices[currIndicesSize + i] = a_Other->m_Indices[i];
	}

	unsigned int currVertssSize = m_Vertexs.size();
	unsigned int otherVertsSize = a_Other->m_Vertexs.size();
	//resize indices so it wont keep resizing after every few adds
	m_Vertexs.resize(currVertssSize + otherVertsSize);
	for (size_t i = 0; i < otherVertsSize; i++) {
		//copy a_Copys
		m_Vertexs[currVertssSize + i] = a_Other->m_Vertexs[i];
	}

	bind();
}

void Mesh::genTangentsAndSet() {
	//if m_Vertex's is not a multiple of 3
	if (m_Vertexs.size() % 3 != 0) {
		printf("m_Vertex's is not a multiple of 3, it has %u number of verts", (unsigned int)m_Vertexs.size());
		return;
	}
	//http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-13-normal-mapping/
	//for every face
	for (size_t i = 0; i < m_Vertexs.size(); i += 3) {
		glm::vec4& v0 = m_Vertexs[i + 0].position;
		glm::vec4& v1 = m_Vertexs[i + 1].position;
		glm::vec4& v2 = m_Vertexs[i + 2].position;

		glm::vec2& uv0 = m_Vertexs[i + 0].texCoord;
		glm::vec2& uv1 = m_Vertexs[i + 1].texCoord;
		glm::vec2& uv2 = m_Vertexs[i + 2].texCoord;

		glm::vec4 deltaPos1 = v1 - v0;
		glm::vec4 deltaPos2 = v2 - v0;

		glm::vec2 deltaUv1 = uv1 - uv0;
		glm::vec2 deltaUv2 = uv2 - uv0;

		//float r = 1.0f / (deltaUv1.x * deltaUv2.y - deltaUv1.y * deltaUv2.x);
		////glm::vec4 tangent = (deltaPos1 * deltaUv2.y - deltaPos2 * deltaUv1.y) * r;
		////glm::vec4 biTangent = (deltaPos2 * deltaUv1.x - deltaPos1 * deltaUv2.x) * r;
		//glm::vec4 tangent = (deltaPos2 * deltaUv1.x - deltaPos1 * deltaUv2.x) * r;
		//
		//m_Vertexs[i + 0].tangent = tangent;
		//m_Vertexs[i + 1].tangent = tangent;
		//m_Vertexs[i + 2].tangent = tangent;

		// https://learnopengl.com/#!Advanced-Lighting/Normal-Mapping
		float f = 1.0f / (deltaUv1.x * deltaUv2.y - deltaUv2.x * deltaUv1.y);

		glm::vec4 tangent;
		tangent.x = f * (deltaUv2.y * deltaPos1.x - deltaUv1.y * deltaPos2.x);
		tangent.y = f * (deltaUv2.y * deltaPos1.y - deltaUv1.y * deltaPos2.y);
		tangent.z = f * (deltaUv2.y * deltaPos1.z - deltaUv1.y * deltaPos2.z);
		tangent = glm::normalize(tangent);

		glm::vec4 biTangent;
		biTangent.x = f * (-deltaUv2.x * deltaPos1.x + deltaUv1.x * deltaPos2.x);
		biTangent.y = f * (-deltaUv2.x * deltaPos1.y + deltaUv1.x * deltaPos2.y);
		biTangent.z = f * (-deltaUv2.x * deltaPos1.z + deltaUv1.x * deltaPos2.z);
		biTangent = glm::normalize(biTangent);


		m_Vertexs[i + 0].tangent = tangent;
		m_Vertexs[i + 1].tangent = tangent;
		m_Vertexs[i + 2].tangent = tangent;

		m_Vertexs[i + 0].biTangent = biTangent;
		m_Vertexs[i + 1].biTangent = biTangent;
		m_Vertexs[i + 2].biTangent = biTangent;
	}
}

void Mesh::clear() {
	unbind();
	m_Vertexs.clear();
	m_Indices.clear();
}

void Mesh::unbind() {
	if (m_VAO != 0) {
		glDeleteVertexArrays(1, &m_VAO);
		m_VAO = 0;
	}
	if (m_VBO != 0) {
		glDeleteBuffers(1, &m_VBO);
		m_VBO = 0;
	}
	if (m_IBO != 0) {
		glDeleteBuffers(1, &m_IBO);
		m_IBO = 0;
	}
}

glm::vec3 Mesh::getMaxVert() {
	glm::vec3 max = glm::vec3(0, 0, 0);

	glm::vec4 scaleData = glm::vec4(m_Transform.getLocalScale(), 1);
	glm::quat rotationData = m_Transform.getLocalRotation();

	for (size_t i = 0; i < m_Vertexs.size(); i++) {
		glm::vec4 pos = (m_Vertexs[i].position  *scaleData)  * rotationData;
		if (pos.x > max.x) {
			max.x = pos.x;
		}
		if (pos.y > max.y) {
			max.y = pos.y;
		}
		if (pos.z > max.z) {
			max.z = pos.z;
		}
	}

	return max;
}

void Mesh::setMaterial(Material * a_Material) {
	//if setting this material to nullptr
	if (a_Material == nullptr) {
		//if there is no current material
		//then make one
		if (m_Material == nullptr) {
			m_Material = new Material();
			m_CreatedMaterial = true;
		}
	} else {//setting from a_Material
		removeMaterial();
		m_Material = a_Material;
	}
}

void Mesh::setDiffuse(Texture * a_Texture) {
	m_Material->setTexture(TextureSlots::Diffuse, a_Texture);
}

void Mesh::setNormal(Texture * a_Texture) {
	m_Material->setTexture(TextureSlots::Normal, a_Texture);
}

void Mesh::setSpecular(Texture * a_Texture) {
	m_Material->setTexture(TextureSlots::Specular, a_Texture);
}

void Mesh::setColor(glm::vec4 a_Color) {
	if (glm::vec3(a_Color) == glm::vec3(0) || a_Color.a == 0) {
		m_Material->usingColor(false);
		return;
	}
	m_Material->setColor(a_Color);
}

void Mesh::removeMaterial() {
	if (m_CreatedMaterial) {
		delete m_Material;
	}
	m_CreatedMaterial = false;
	m_Material = nullptr;
}

const char * Mesh::getName() {
	return m_Transform.m_Name.c_str();
}

void Mesh::draw() {
	m_Material->useTextures();

	Shader::getCurrentShader()->setModelAndNormalMatrix(m_Transform);

	glBindVertexArray(m_VAO);
	glDrawElements(GL_TRIANGLES, (GLsizei)m_Indices.size(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

bool Mesh::isTransparent() {
	return false;
}

void Mesh::bind() {
	if (m_VAO != 0) {
		glDeleteVertexArrays(1, &m_VAO);
		m_VAO = 0;
	}
	if (m_VBO != 0) {
		glDeleteBuffers(1, &m_VBO);
		m_VBO = 0;
	}
	if (m_IBO != 0) {
		glDeleteBuffers(1, &m_IBO);
		m_IBO = 0;
	}

	glGenVertexArrays(1, &m_VAO);
	glBindVertexArray(m_VAO);

	glGenBuffers(1, &m_IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_Indices.size() * sizeof(unsigned int), &m_Indices[0], GL_STATIC_DRAW);

	glGenBuffers(1, &m_VBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, m_Vertexs.size() * sizeof(VertexTex), &m_Vertexs[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(VertexTex), 0);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(VertexTex), (GLvoid*)offsetof(VertexTex, normal));
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(VertexTex), (GLvoid*)offsetof(VertexTex, texCoord));
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(VertexTex), (GLvoid*)offsetof(VertexTex, tangent));
	glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(VertexTex), (GLvoid*)offsetof(VertexTex, biTangent));

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
