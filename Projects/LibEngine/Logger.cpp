#include "Logger.h"

#include <thread>
#include <mutex>

Logger::Logger() {
	m_IoMutex = new std::mutex();
	m_IoMutex->lock();//cant lock in writeToFile, error will occur

	m_IoThread = new std::thread(std::bind(&Logger::writeToFile, this));
}

Logger::~Logger() {
	//std::this_thread::sleep_for(std::chrono::seconds(1));
	m_EndThread = true;
	m_IoMutex->unlock();
	m_IoThread->join();

	delete m_IoThread;
	delete m_IoMutex;
}

void Logger::log(char * a_Message, LogType a_LogType) {
	logMessage lm;
	lm.message = a_Message;
	lm.logType = a_LogType;
	m_MessageList.push(lm);


	allowFileWriting();
	writeToLog(&lm);
}

void Logger::allowFileWriting() {
	m_IoMutex->unlock();

	m_IoMutex->lock();
}

void Logger::writeToLog(logMessage * a_LogMessage) {
	//if(lm == nullptr)
#ifdef LOGGER_USE_CONSOLE
	char* types[] = { "Normal","Warning","Error" };

	printf("DEBUG: %s, %s\n", types[(int) a_LogMessage->logType], a_LogMessage->message);
#else
	//imgui
#endif // LOGGER_USE_CONSOLE
}


void Logger::writeToFile() {
	//m_IoMutex->lock(); //cant start lock here, lock starts in Logger
	while (!m_EndThread) {
			m_IoMutex->lock();

		while (!m_MessageList.empty()) {
			logMessage* lm = &m_MessageList.front();

			printf("FILE WRITE: %s\n", lm->message);

			m_MessageList.pop();
		}
			m_IoMutex->unlock();
		
	}

	//m_IoMutex->unlock();
}
