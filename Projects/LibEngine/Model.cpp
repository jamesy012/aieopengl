#include "Model.h"
#include "Model.h"

#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h>

#include <string>
#include <iostream>
#include <math.h>

#include "Vertex.h"
#include "Texture.h"
#include "Material.h"
#include "Mesh.h"

#include "BoundingSphere.h"

Model::Model() {
}


Model::~Model() {
	for (size_t i = 0; i < m_Materials.size(); i++) {
		delete m_Materials[i];
	}
	for (size_t i = 0; i < m_Meshs.size(); i++) {
		delete m_Meshs[i];
	}
	for (size_t i = 0; i < m_Textures.size(); i++) {
		//delete m_Textures[i];
		m_Textures[i]->release();
	}
}

Model::Model(const Model & a_Copy) {
	//take the data from a_Copy's transform and put it in ours
	m_Transform = a_Copy.m_Transform;

	//resize mesh to it's final size so it doesn't keep resizing after a few adds
	m_Meshs.resize(a_Copy.m_Meshs.size());
	for (size_t i = 0;i< a_Copy.m_Meshs.size(); i++) {
		//
		Mesh* copy = new Mesh(*a_Copy.m_Meshs[i]);

		copy->m_Transform.setParent(&m_Transform);

		m_Meshs[i] = copy;
	}

	//TODO copy over materials from mesh's
	//and copy over Textures

	//TODO also work out if we should use a reference to the textures or load news one
	//maybe make a texture manager that handles all textures
}

void Model::loadModel(const char * a_FilePath, const char * a_FileName, const bool a_Bind) {

	std::string fileName = a_FilePath;
	fileName += a_FileName;

	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;


	std::string err;
	bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, fileName.c_str(), a_FilePath);

	if (!err.empty()) {
		std::cerr << err << std::endl;
	}
	if (!ret) {
		return;
	}

	//load materials
	//TODO load more materials then just the diffuse
	for (size_t m = 0; m < materials.size(); m++) {
		tinyobj::material_t mat = materials[m];
		Material* material = new Material();

		std::string fileName;

		if (mat.diffuse_texname != "") {
			fileName = a_FilePath + mat.diffuse_texname;
			Texture* tex = new Texture();
			tex->loadTexture(fileName.c_str());
			m_Textures.push_back(tex);
			material->setTexture(TextureSlots::Diffuse, tex);
		}


		if (mat.bump_texname != "") {
			fileName = a_FilePath + mat.bump_texname;
			Texture* tex = new Texture();
			tex->loadTexture(fileName.c_str());
			m_Textures.push_back(tex);
			material->setTexture(TextureSlots::Normal, tex);
		}


		if (mat.specular_texname != "") {
			fileName = a_FilePath + mat.specular_texname;
			Texture* tex = new Texture();
			tex->loadTexture(fileName.c_str());
			m_Textures.push_back(tex);
			material->setTexture(TextureSlots::Specular, tex);
		}

		m_Materials.push_back(material);
	}

	//load indices for each shape/model
	for (size_t s = 0; s < shapes.size(); s++) {
		Mesh* mesh = new Mesh();
		mesh->m_Transform.m_Name = shapes[s].name;
		mesh->m_Transform.setParent(&m_Transform);

		//set up sizes for vectors so they don't remake the array with a larger size every few pushes
		mesh->m_Vertexs.reserve(shapes[s].mesh.indices.size());
		mesh->m_Indices.reserve(shapes[s].mesh.indices.size());

		//printf("Indices: ");
		for (size_t index = 0; index < shapes[s].mesh.indices.size(); index++) {
			tinyobj::index_t vertInfo = shapes[s].mesh.indices[index];

			VertexTex vert;

			vert.position.x = attrib.vertices[3 * vertInfo.vertex_index + 0];
			vert.position.y = attrib.vertices[3 * vertInfo.vertex_index + 1];
			vert.position.z = attrib.vertices[3 * vertInfo.vertex_index + 2];
			vert.position.w = 1;

			if (vertInfo.normal_index != -1) {
				vert.normal.x = attrib.normals[3 * vertInfo.normal_index + 0];
				vert.normal.y = attrib.normals[3 * vertInfo.normal_index + 1];
				vert.normal.z = attrib.normals[3 * vertInfo.normal_index + 2];
				vert.normal.w = 0;
			}

			if (vertInfo.texcoord_index != -1) {
				vert.texCoord.x = attrib.texcoords[2 * vertInfo.texcoord_index + 0];
				//flip y
				vert.texCoord.y = 1 - attrib.texcoords[2 * vertInfo.texcoord_index + 1];
			}

			mesh->m_Vertexs.push_back(vert);
			//the data above is data for the current index, so add index
			mesh->m_Indices.push_back((const unsigned int)index);
		}

		//set which material this uses
		//TODO have it go through material_ids and add which ever materials this shape uses
		if (m_Materials.size() != 0) {
			int id = shapes[s].mesh.material_ids[0];
			if (id != -1) {
				mesh->setMaterial(m_Materials[id]);
			}
		}

		mesh->genTangentsAndSet();

		if (a_Bind) {
			//bind mesh data into opengl
			mesh->bind();
		}
		//add binded mesh to meshes vector
		m_Meshs.push_back(mesh);
	}


	m_Transform.m_Name = a_FileName;
	m_Transform.m_Name.resize(m_Transform.m_Name.size() - 4);//remove.obj
}

void Model::fit(BoundingSphere * a_BoundingSphere) {
	for (unsigned int i = 0; i < m_Meshs.size(); i++) {
		a_BoundingSphere->fit(m_Meshs[i]->m_Vertexs);
	}
}

/*
void Model::loadModel(const char * a_FilePath, const char* a_FileName) {

	std::string fileName = a_FilePath;
	fileName += a_FileName;

	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;


	std::string err;
	bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, fileName.c_str(), a_FilePath);

	if (!err.empty()) {
		std::cerr << err << std::endl;
	}
	if (!ret) {
		return;
	}

	glm::vec4* vertexData = new glm::vec4[attrib.vertices.size() / 3];
	glm::vec4* normalData = new glm::vec4[attrib.normals.size() / 3];
	glm::vec2* texCoordData = new glm::vec2[attrib.texcoords.size() / 2];

	//load vertex data in order they came in
	for (size_t vIndex = 0; vIndex < attrib.vertices.size()/3; vIndex++) {
		vertexData[vIndex].x = attrib.vertices[3 * vIndex + 0];
		vertexData[vIndex].y = attrib.vertices[3 * vIndex + 1];
		vertexData[vIndex].z = attrib.vertices[3 * vIndex + 2];
		vertexData[vIndex].w = 1;
		//printf("Vertex %i: (%f,%f,%f)\n", vIndex, vertexData[vIndex].x, vertexData[vIndex].y, vertexData[vIndex].z);
	}

	//load normal data in order they came in
	for (size_t nIndex = 0; nIndex < attrib.normals.size() / 3; nIndex++) {
		normalData[nIndex].x = attrib.normals[3 * nIndex + 0];
		normalData[nIndex].y = attrib.normals[3 * nIndex + 1];
		normalData[nIndex].z = attrib.normals[3 * nIndex + 2];
		normalData[nIndex].w = 0;
		//printf("normal %i: (%f,%f,%f)\n", nIndex, normalData[nIndex].x, normalData[nIndex].y, normalData[nIndex].z);
	}

	//load texture coord data in order they came in
	for (size_t tIndex = 0; tIndex < attrib.texcoords.size() / 2; tIndex++) {
		texCoordData[tIndex].x = attrib.texcoords[2 * tIndex + 0];
		texCoordData[tIndex].y = attrib.texcoords[2 * tIndex + 1];
		//printf("texture coords %i: (%f,%f)\n", tIndex, texCoordData[tIndex].x, texCoordData[tIndex].y);
	}

	//load materials
	for (size_t m = 0; m < materials.size(); m++) {
		tinyobj::material_t mat = materials[m];
		Texture* tex = new Texture();
		std::string fileName = a_FilePath;
		fileName += mat.diffuse_texname;
		tex->loadTexture(fileName.c_str());
		m_Texture.push_back(tex);
	}


	//load indices for each shape/model
	for (size_t s = 0; s < shapes.size(); s++) {
		//printf("\nShape: %i, %s\n", s, shapes[s].name.c_str());
		size_t index_offset = 0;
		Mesh* mesh = new Mesh();

		//min starts at int_max and goes down
		//max starts at 0 and goes up
		unsigned int minVertIndex = -1;
		unsigned int maxVertIndex = 0;

		mesh->m_Indices.reserve(shapes[s].mesh.m_Indices.size());

		//printf("Indices: ");
		for (size_t index = 0; index < shapes[s].mesh.m_Indices.size(); index++) {
			tinyobj::index_t vertInfo = shapes[s].mesh.m_Indices[index];
			maxVertIndex = (unsigned int)fmax(maxVertIndex, vertInfo.vertex_index);
			minVertIndex = (unsigned int)fmin(minVertIndex, vertInfo.vertex_index);

			mesh->m_Indices.push_back(vertInfo.vertex_index);
			//printf("%i, ", vertInfo.vertex_index);
		}
		//maxVerts is one less then it should be from that, so lets add one to it
		maxVertIndex++;
		//printf("\nIndex Start and end Pos: min %i, max %i\n", minVertIndex, maxVertIndex);



		mesh->m_Vertexs.reserve(maxVertIndex - minVertIndex);

		for (unsigned int i = 0; i < maxVertIndex - minVertIndex; i++) {
			VertexTex vertInfo;
			vertInfo.position = vertexData[minVertIndex + i];
			//vertInfo.normal = glm::vec4(1,1,1,1);
			vertInfo.normal = glm::vec4(0);
			//vertInfo.normal = normalData[minVertIndex + i];

			//vertInfo.texCoord = texCoordData[shapes[s].mesh.m_Indices[minVertIndex + i].texcoord_index];

			mesh->m_Vertexs.push_back(vertInfo);
			//printf("Vertex %i: (%f,%f,%f)\n", minVertIndex + i, vertInfo.position.x, vertInfo.position.y, vertInfo.position.z);

		}

		for (unsigned int i = 0; i < shapes[s].mesh.m_Indices.size(); i++) {
			int vertInfo = shapes[s].mesh.m_Indices[i].vertex_index - minVertIndex;

			int nor = shapes[s].mesh.m_Indices[i].normal_index;
			if (nor > attrib.normals.size() / 3) {
				printf("OVER NORMALS! %i: ", nor);
			}
			mesh->m_Vertexs[vertInfo].normal += normalData[nor];
			mesh->m_Vertexs[vertInfo].normal = glm::normalize(mesh->m_Vertexs[vertInfo].normal);


			int tex = shapes[s].mesh.m_Indices[i].texcoord_index;
			if (tex > attrib.texcoords.size() / 2) {
				printf("OVER! %i: ", nor);
			}
			mesh->m_Vertexs[vertInfo].texCoord = texCoordData[tex];

			mesh->m_TextureId = m_Texture[shapes[s].mesh.material_ids[i / 3]];
		}

		//make index count go from 0 - maxVertIndex
		//so it reads the smaller array of vertInfo positions
		//instead of getting m_Indices from the big list of verts
		for (unsigned int i = 0; i < mesh->m_Indices.size(); i++) {
			mesh->m_Indices[i] -= minVertIndex;
		}

		mesh->bind();
		//add binded mesh to meshes vector
		m_Meshs.push_back(mesh);
	}



	delete[] vertexData;
	delete[] normalData;
	delete[] texCoordData;
}
*/

void Model::draw() {
	for (unsigned int i = 0; i < m_Meshs.size(); i++) {
		m_Meshs[i]->draw();
	}
}

bool Model::isTransparent() {
	//TODO store transparent value and update when one of the meshes update

	//if one of the meshes are transparent then return true
	for (unsigned int i = 0; i < m_Meshs.size(); i++) {
		if (m_Meshs[i]->isTransparent()) {
			return true;
		}
	}
	return false;
}

const char * Model::getName() {
	return m_Transform.m_Name.c_str();
}
