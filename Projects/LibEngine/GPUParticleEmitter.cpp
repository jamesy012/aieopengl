#include "GPUParticleEmitter.h"

#include <gl_core_4_4.h>

#include "TimeHandler.h"
#include "Shader.h"
#include "Camera.h"
#include "Texture.h"

GPUParticleEmitter::GPUParticleEmitter() :
	m_Particles(nullptr), m_MaxParticles(0),
	m_Position(0, 0, 0), m_DrawShader(0), m_UpdateShader(0),
	m_LastDrawTime(0)
{
	m_Vao[0] = 0;
	m_Vao[1] = 0;
	m_Vbo[0] = 0;
	m_Vbo[1] = 0;
}


GPUParticleEmitter::~GPUParticleEmitter() {
	delete[] m_Particles;

	glDeleteVertexArrays(2, m_Vao);
	glDeleteBuffers(2, m_Vbo);

	delete m_DrawShader;
	delete m_UpdateShader;

}

void GPUParticleEmitter::initalise(const unsigned int a_MaxParticles, const float a_LifespanMin, const float a_LifespanMax, const float a_VelocityMin, const float a_VelocityMax, const float a_StartSize, const float a_EndSize, const glm::vec4 & a_StartColor, const glm::vec4 & a_EndColor) {

	m_StartColor = a_StartColor;
	m_EndColor = a_EndColor;

	m_StartSize = a_StartSize;
	m_EndSize = a_EndSize;

	m_VelocityMin = a_VelocityMin;
	m_VelocityMax = a_VelocityMax;

	m_LifespanMin = a_LifespanMin;
	m_LifespanMax = a_LifespanMax;

	m_MaxParticles = a_MaxParticles;

	m_Particles = new GPUParticle[m_MaxParticles];

	m_ActiveBuffer = 0;
	createBuffers();
	assert(glGetError() == GL_NO_ERROR);
	createUpdateShader();
	assert(glGetError() == GL_NO_ERROR);
	createDrawShader();
	assert(glGetError() == GL_NO_ERROR);

}

void GPUParticleEmitter::draw(Camera & a_Camera) {

	float time = TimeHandler::getCurrentTime();
	glm::mat4& cameraTransform = a_Camera.getGlobalTransform();
	glm::mat4& projectionView = a_Camera.getProjectionView();

	m_UpdateShader->useProgram();
	assert(glGetError() == GL_NO_ERROR);
	m_UpdateShader->setUniformTime();

	float deltatime = time - m_LastDrawTime;
	m_LastDrawTime = time;
	assert(glGetError() == GL_NO_ERROR);
	m_UpdateShader->setUniform("deltaTime", deltatime);
	assert(glGetError() == GL_NO_ERROR);
	m_UpdateShader->setUniform("emitterPosition", m_Position);
	assert(glGetError() == GL_NO_ERROR);
	m_UpdateShader->setUniform("lifeMin", m_LifespanMin);
	assert(glGetError() == GL_NO_ERROR);
	m_UpdateShader->setUniform("lifeMax", m_LifespanMax);

	assert(glGetError() == GL_NO_ERROR);
	//stop rasterisation
	glEnable(GL_RASTERIZER_DISCARD);
	assert(glGetError() == GL_NO_ERROR);
	glBindVertexArray(m_Vao[m_ActiveBuffer]);

	//swap buffers
	unsigned int otherBuffer = (m_ActiveBuffer + 1) % 2;
	assert(glGetError() == GL_NO_ERROR);
	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, m_Vbo[otherBuffer]);
	assert(glGetError() == GL_NO_ERROR);
	glBeginTransformFeedback(GL_POINTS);
	assert(glGetError() == GL_NO_ERROR);
	glDrawArrays(GL_POINTS, 0, m_MaxParticles);
	assert(glGetError() == GL_NO_ERROR);
	glEndTransformFeedback();
	assert(glGetError() == GL_NO_ERROR);
	glDisable(GL_RASTERIZER_DISCARD);
	assert(glGetError() == GL_NO_ERROR);
	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);
	assert(glGetError() == GL_NO_ERROR);

	m_DrawShader->useProgram();

	glEnable(GL_BLEND);
	glDepthMask(GL_FALSE);

	m_DrawShader->setUniformProjectionView(&a_Camera);
	m_DrawShader->setUniform("cameraTransform", cameraTransform);

	Texture::useTexture(m_ParticleTexture, 0, "particleTex");

	glBindVertexArray(m_Vao[otherBuffer]);
	assert(glGetError() == GL_NO_ERROR);
	glDrawArrays(GL_POINTS, 0, m_MaxParticles);
	assert(glGetError() == GL_NO_ERROR);

	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);

	m_ActiveBuffer = otherBuffer;


}

void GPUParticleEmitter::createBuffers() {

	glGenVertexArrays(2, m_Vao);
	glGenBuffers(2, m_Vbo);

	//set up first buffer
	glBindVertexArray(m_Vao[0]);
	glBindBuffer(GL_ARRAY_BUFFER, m_Vao[0]);
	glBufferData(GL_ARRAY_BUFFER, m_MaxParticles * sizeof(GPUParticle), m_Particles, GL_STREAM_DRAW);

	glEnableVertexAttribArray(0);//pos
	glEnableVertexAttribArray(1);//vel
	glEnableVertexAttribArray(2);//lifetime
	glEnableVertexAttribArray(3);//lifespan
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), (GLvoid*)offsetof(GPUParticle, velocity));
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), (GLvoid*)offsetof(GPUParticle, lifetime));
	glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), (GLvoid*)offsetof(GPUParticle, lifespan));
	assert(glGetError() == GL_NO_ERROR);
	//set up second buffer
	glBindVertexArray(m_Vao[1]);
	glBindBuffer(GL_ARRAY_BUFFER, m_Vao[1]);
	glBufferData(GL_ARRAY_BUFFER, m_MaxParticles * sizeof(GPUParticle), m_Particles, GL_STREAM_DRAW);

	glEnableVertexAttribArray(0);//pos
	glEnableVertexAttribArray(1);//vel
	glEnableVertexAttribArray(2);//lifetime
	glEnableVertexAttribArray(3);//lifespan
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), (GLvoid*)offsetof(GPUParticle, velocity));
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), (GLvoid*)offsetof(GPUParticle, lifetime));
	glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), (GLvoid*)offsetof(GPUParticle, lifespan));
	assert(glGetError() == GL_NO_ERROR);
}

void GPUParticleEmitter::createUpdateShader() {
	m_UpdateShader = new Shader();
	assert(glGetError() == GL_NO_ERROR);
	m_UpdateShader->setVertexShaderFromFile("Shaders/Particles/Gpu/vertUpdate.vert");
	assert(glGetError() == GL_NO_ERROR);
	m_UpdateShader->createProgram();

	// specify the data that we will stream back
	const char* varyings[] = { "position", "velocity", "lifetime", "lifespan" }; 
	glTransformFeedbackVaryings(m_UpdateShader->getProgramId(), 4, varyings, GL_INTERLEAVED_ATTRIBS);
	assert(glGetError() == GL_NO_ERROR);
	m_UpdateShader->linkShader();
	assert(glGetError() == GL_NO_ERROR);
}

void GPUParticleEmitter::createDrawShader() {
	m_DrawShader = new Shader();

	m_DrawShader->setVertexShaderFromFile("Shaders/Particles/Gpu/particle.vert");
	m_DrawShader->setFragmentShaderFromFile("Shaders/Particles/Gpu/particle.frag");
	m_DrawShader->setGeometryShaderFromFile("Shaders/Particles/Gpu/particle.geom");
	assert(glGetError() == GL_NO_ERROR);
	m_DrawShader->linkShader();
	assert(glGetError() == GL_NO_ERROR);
	m_DrawShader->useProgram();
	assert(glGetError() == GL_NO_ERROR);

	m_DrawShader->setUniform("sizeStart", m_StartSize);
	m_DrawShader->setUniform("sizeEnd", m_EndSize);
	m_DrawShader->setUniform("colorStart", m_StartColor);
	m_DrawShader->setUniform("colorEnd", m_EndColor);

}
