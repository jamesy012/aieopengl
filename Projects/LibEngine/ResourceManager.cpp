#include "ResourceManager.h"

#include "IResource.h"

#define ResourceRef ResourceManager::ResourceRef

std::map<unsigned int, std::vector<ResourceRef>> ResourceManager::m_ResourceList;

void ResourceManager::addResource(IResource * a_Resource) {

	ResourceRef* ref = getLoadedResource(a_Resource);
	if (ref == nullptr) {//if there is no reference, create one
		createResource(&createReference(a_Resource));
		return;
	} else {
		copyResource(ref, a_Resource);
	}

	////if we have already loaded this resource
	//if (hasResource(a_Resource)) {
	//	a_Resource->copy(getLoadedResource(a_Resource));
	//} else {//else add it to loaded
	//	a_Resource->load();
	//}

}

bool ResourceManager::hasResource(IResource * a_Resource) {
	//std::vector<IResource*>* res = &m_ResourceList[a_Resource->getTypeId()];
	//for (unsigned int i = 0; i < res->size(); i++) {
	//	if (res->at(i)->m_FilePath == a_Resource->m_FilePath) {
	//		return true;
	//	}
	//}
	return false;
}

void ResourceManager::removeResource(IResource * a_Resource, bool a_DeleteBaseResource) {
	ResourceRef* ref = getLoadedResource(a_Resource);
	if (ref != nullptr) {
		ref->resourceCount--;
		bool isReferenceResource = ref->resource == a_Resource;
		if (ref->resourceCount <= 0) {

			//get data from reference to delete it
			ResourceRef startingRef = *ref;
			//remove reference from resource array
			removeReference(ref);

			unsigned resourceID = startingRef.resource->getTypeId();
			a_Resource->destroy();
			startingRef.resource->m_CanBeDeleated = true;
			if (a_DeleteBaseResource) {
				delete startingRef.resource;//also destroy the reference resource
			}

		}
		if (!isReferenceResource) {//if this is not the reference resource
			a_Resource->m_CanBeDeleated = true;
			delete a_Resource;
		}


	}
}

ResourceRef* ResourceManager::getLoadedResource(IResource * a_ResourcePathToCopy) {
	std::vector<ResourceManager::ResourceRef>* res = &m_ResourceList[a_ResourcePathToCopy->getTypeId()];
	for (unsigned int i = 0; i < res->size(); i++) {
		if (res->at(i).resource->m_FilePath == a_ResourcePathToCopy->m_FilePath) {
			return &res->at(i);
		}
	}
	return nullptr;
}

ResourceRef ResourceManager::createReference(IResource * a_Resource) {
	ResourceRef ref;
	ref.resource = a_Resource;
	ref.resourceCount = 0;
	return ref;
}

void ResourceManager::createResource(ResourceRef * a_ResRef) {
	a_ResRef->resource->load();
	a_ResRef->resource->m_CanBeDeleated = false;
	a_ResRef->resourceCount++;

	m_ResourceList[a_ResRef->resource->getTypeId()].push_back(*a_ResRef);
}

void ResourceManager::copyResource(ResourceRef * a_Reference, IResource * a_Resource) {
	a_Reference->resourceCount++;
	a_Resource->copy(a_Reference->resource);
	a_Resource->m_CanBeDeleated = false;
}

void ResourceManager::removeReference(ResourceRef * a_Reference) {
	std::vector<ResourceManager::ResourceRef>* res = &m_ResourceList[a_Reference->resource->getTypeId()];
	for (unsigned int i = 0; i < res->size(); i++) {
		if (res->at(i).resource->m_FilePath == a_Reference->resource->m_FilePath) {
			res->erase(res->begin()+i);
			return;
		}
	}
}
