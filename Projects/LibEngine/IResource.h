#pragma once

#include <string>
#include "ResourceManager.h"

class IResource {
	friend ResourceManager;
public:
	virtual unsigned int getTypeId() = 0;
	virtual void release() = 0;
	bool m_UseResourceManager = true;
protected:
	virtual void load() = 0;
	virtual void copy(IResource* a_Res) = 0;
	virtual void destroy() = 0;
	std::string m_FilePath;
	bool m_CanBeDeleated = false;
};

