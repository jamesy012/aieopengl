#pragma once
#include "BoundingObject.h"
class BoundingSphere :
	public BoundingObject {
public:

	BoundingSphere();

	// Inherited via BoundingObject
	virtual void fit(const std::vector<VertexTex>& a_Points) override;

	glm::vec3 getCenter();
	float getRadius();

private:
	glm::vec3 m_Center;
	float m_Radius;
};

