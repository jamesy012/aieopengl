#pragma once

class IDrawable {
public:
	//called when we want to render this Drawable object
	//will set up drawable objects
	virtual void draw() = 0;
	virtual bool isTransparent() = 0;
	virtual const char* getName() = 0;
};

