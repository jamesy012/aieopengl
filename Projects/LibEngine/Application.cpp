#include "Application.h"

#include <GLFW\glfw3.h>
#include <glm\glm.hpp>
#include <glm\ext.hpp>
#include <imgui\imgui.h>

#include <stdio.h> //printf

#include "TimeHandler.h"
#include "Input.h"
#include "Window.h"

#include "Transform.h"
#include "FlyCamera.h"
#include "Gui.h"

static Application* m_CurrentApplication;

Application::Application() {
}


Application::~Application() {
	if (m_Camera != nullptr) {
		delete m_Camera;
	}
	Window::destroyWindow();
	delete m_StartingRoot;
}

void Application::run() {
	m_StartingRoot = new Transform("Root Transform");
	Transform::setRootTransform(m_StartingRoot);

	m_CurrentApplication = this;

	//glfw startup
	if (glfwInit() == false) {
		return;
	}

	windowCreation();

	glfwMakeContextCurrent(Window::getWindow());

	if (ogl_LoadFunctions() == ogl_LOAD_FAILED) {
		Window::destroyWindow();
		return;
	}

	//if there was no camera
	if (m_Camera == nullptr) {
		m_Camera = new FlyCamera();
		m_Camera->setLookAt(glm::vec3(10, 10, 10), glm::vec3(0), glm::vec3(0, 1, 0));
		m_Camera->setPerspective(glm::radians(60.0f), 16 / 9.f, 0.1f, 1000.f);
	}

	//start up the gui
	Gui::startUp();
	//application startup
	startUp();

	//if there was no window made
	if (Window::getWindow() == nullptr) {
		printf("No Window creation in main for current application\n");
		printf("call createWindow from startUp()\n");
		shutDown();
		return;
	}


	//set up common drawing data
	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);
	glEnable(GL_DEPTH_TEST);

	//this was causing issues with the nano suit, please enable and disable blending when
	//drawing textures with alpha
	//glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	while (!glfwWindowShouldClose(Window::getWindow()) && !m_Quit) {

		//clear
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		//update delta time
		TimeHandler::update();
		//tell gui there is a new frame
		Gui::newFrame();
		//check flags that are common and are handled by Application
		handles();

		//application update
		update();
		//application draw
		draw();

		//draw gui
		Gui::draw();

		//will set the flag for getting focus on last frame to false
		m_GotFocusThisFrame = false;

		//update input
		Input::update();
		//glfw buffer swap
		glfwSwapBuffers(Window::getWindow());
		//update events eg: Inputs from glfw
		glfwPollEvents();

	}

	//end gui
	Gui::shutdown();

	shutDown();

	glfwTerminate();
}

void Application::windowCreation() {

	Window::createWindow(1280, 720, "Computer Graphics");

	setCallbacksForWindow();
}

void Application::frameBufferChanged() {
	glViewport(0, 0, Window::getFramebufferWidth(), Window::getFramebufferHeight());
}

void Application::screenSizeChanged() {
	if (!m_UpdatePerspectiveOnWindowResize) {
		return;
	}
	if (m_PerspetiveOrthographic) {
		if (m_UpdatePerspetiveToNewWindowSize) {
			m_Camera->setOthrographic(0, (const float)Window::getWidth(), 0, (const float)Window::getHeight(),-10000,10000);
		} else {
			m_Camera->setOthrographic(0, (const float) Window::getStartingWidth(), 0, (const float) Window::getStartingHeight(), -10000, 10000);
		}
	} else {
		if (Window::getFramebufferHeight() != 0) {
			if (m_UpdatePerspetiveToNewWindowSize) {
				m_Camera->setPerspective(m_Camera->m_FieldOfView, float(Window::getFramebufferWidth()) / Window::getFramebufferHeight(), m_Camera->m_NearPlane, m_Camera->m_FarPlane);
			} else {
				m_Camera->setPerspective(m_Camera->m_FieldOfView, float(Window::getStartingFramebufferWidth()) / Window::getStartingFramebufferHeight(), m_Camera->m_NearPlane, m_Camera->m_FarPlane);
			}
		}
	}

}

void Application::windowFocusChanged(bool a_GainedFocus) {
	if (a_GainedFocus) {
		m_GotFocusThisFrame = true;
	} else {
		//lost focus
		if (Window::isMouseLocked()) {
			Window::toggleMouseLock();
			Input::resetMouseDelta();
		}
	}

}

void Application::setCallbacksForWindow() {
	GLFWwindow* window = Window::getWindow();
	if (window == nullptr) {
		printf("cant set callbacks when there is no window\n");
		return;
	}
	glfwSetKeyCallback(window, Input::keyCallback);
	glfwSetCursorPosCallback(window, Input::cursorPositionCallback);
	glfwSetMouseButtonCallback(window, Input::mouseButtonCallback);
	glfwSetCharCallback(window, Input::charCallback);
	glfwSetScrollCallback(window, Input::scrollCallback);
	glfwSetWindowSizeCallback(window, Window::windowSizeCallback);
	glfwSetFramebufferSizeCallback(window, Window::framebufferSizeCallback);
	glfwSetWindowFocusCallback(window, Window::windowFocusCallback);
}

Application * Application::getCurrentApplcation() {
	return m_CurrentApplication;
}

void Application::handles() {
	if (m_GotFocusThisFrame) {
		return;
	}
	if (m_HandleMouseLock) {
		mouseLock();
	}
	if (m_HandleEscapeQuit) {
		escapeQuit();
	}
}

void Application::mouseLock() {
	//if mouse locked?
	if (Window::isMouseLocked()) {
		if (Input::wasKeyPressed(GLFW_KEY_ESCAPE)) {
			Window::toggleMouseLock();
			Input::resetMouseDelta();
			Input::removeKeyButton(GLFW_KEY_ESCAPE);
		}
	} else {
		if (Input::wasMousePressed(GLFW_MOUSE_BUTTON_1)) {

			ImGuiIO& io = ImGui::GetIO();

			if (io.WantCaptureMouse) {//if over window
				return;
			}

			Window::toggleMouseLock();
			Input::removeMouseButton(GLFW_MOUSE_BUTTON_1);
		}
	}
}

void Application::escapeQuit() {
	if (Input::wasKeyPressed(GLFW_KEY_ESCAPE)) {
		//if mouse is not locked
		if (!Window::isMouseLocked()) {
			m_Quit = true;
			Input::removeKeyButton(GLFW_KEY_ESCAPE);
		}
	}
}
