#pragma once

struct GLFWwindow;

class Window {
public:
	static void createWindow(int a_Width, int a_Height, char* a_Title);
	static void destroyWindow();

	//called when window is resized (do not use for glViewPort)
	static void windowSizeCallback(GLFWwindow* a_Window, int a_Width, int a_Height);
	//called when window is resized
	static void framebufferSizeCallback(GLFWwindow* a_Window, int a_Width, int a_Height);

	static void windowFocusCallback(GLFWwindow* a_Window, int a_Focused);

	static GLFWwindow* getWindow();

	static int getWidth();
	static int getHeight();
	static int getFramebufferWidth();
	static int getFramebufferHeight();
	static int getStartingWidth();
	static int getStartingHeight();
	static int getStartingFramebufferWidth();
	static int getStartingFramebufferHeight();

	//does the window have focus
	static bool hasFocus();
	static void toggleMouseLock();
	static bool isMouseLocked();
 
};

