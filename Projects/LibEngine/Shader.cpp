#include "Shader.h"


#include <GLFW\glfw3.h>
#include <glm\ext.hpp>

#include <fstream> //std::ifstream
#include <sstream> //std::stringstream
#include <string> //std::string

#include <stdio.h>

#include "TimeHandler.h"
#include "Camera.h"

//last shader to be used
//does not update if you call gluseShader
static Shader* m_CurrentlyUsedProgram;

Shader::Shader() {

}

Shader::~Shader() {

	if (m_ProgramID != 0) {
		glDeleteProgram(m_ProgramID);
	}
	for (size_t i = 0; i < m_Uniforms.size(); i++) {
		delete		m_Uniforms[i]->data;
		delete m_Uniforms[i];
	}

}

void Shader::setShader(const char * a_VertexShader, const char * a_FragmentShader) {
	setVertexShader(a_VertexShader);
	setFragmentShader(a_FragmentShader);
	m_Shaders[ShaderTypes::Vertex].path = "Vertex Text Path";
	m_Shaders[ShaderTypes::Fragment].path = "Fragment Text Path";
	linkShader();
	//findCommonUniformLocations();
}

void Shader::setShaderFromFile(const char * a_VertexFileLocation, const char * a_FragmentShaderLocation) {
	setVertexShader(loadShaderFromFile(a_VertexFileLocation).c_str());
	setFragmentShader(loadShaderFromFile(a_FragmentShaderLocation).c_str());
	m_Shaders[ShaderTypes::Vertex].path = a_VertexFileLocation;
	m_Shaders[ShaderTypes::Fragment].path = a_FragmentShaderLocation;
	linkShader();
	//findCommonUniformLocations();
}

void Shader::setVertexShader(const char * a_VertexShader) {
	if (m_Linked) {
		printf("Cant Add Vertex Shader to a linked program");
		return;
	}
	if (m_Shaders[ShaderTypes::Vertex].shaderId != 0) {
		glDeleteShader(m_Shaders[ShaderTypes::Vertex].shaderId);
	}
	unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, (const char**) &a_VertexShader, 0);
	glCompileShader(vertexShader);

	m_Shaders[ShaderTypes::Vertex].shaderId = vertexShader;
	m_Shaders[ShaderTypes::Vertex].path = "Vertex path undefined";
}

void Shader::setFragmentShader(const char * a_FragmentShader) {
	if (m_Linked) {
		printf("Cant Add fragment Shader to a linked program");
		return;
	}
	if (m_Shaders[ShaderTypes::Fragment].shaderId != 0) {
		glDeleteShader(m_Shaders[ShaderTypes::Fragment].shaderId);
	}
	unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, (const char**) &a_FragmentShader, 0);
	glCompileShader(fragmentShader);

	m_Shaders[ShaderTypes::Fragment].shaderId = fragmentShader;
	m_Shaders[ShaderTypes::Fragment].path = "Fragment path undefined";
}

void Shader::setGeometryShader(const char * a_GeometryShader) {
	if (m_Linked) {
		printf("Cant Add fragment Shader to a linked program");
		return;
	}
	if (m_Shaders[ShaderTypes::Geometry].shaderId != 0) {
		glDeleteShader(m_Shaders[ShaderTypes::Geometry].shaderId);
	}
	unsigned int geometryShader = glCreateShader(GL_GEOMETRY_SHADER);
	glShaderSource(geometryShader, 1, (const char**) &a_GeometryShader, 0);
	glCompileShader(geometryShader);

	m_Shaders[ShaderTypes::Geometry].shaderId = geometryShader;
	m_Shaders[ShaderTypes::Geometry].path = "Geometry path undefined";
}

void Shader::setVertexShaderFromFile(const char * a_VertexShader) {
	setVertexShader(loadShaderFromFile(a_VertexShader).c_str());
	m_Shaders[ShaderTypes::Vertex].path = a_VertexShader;
}

void Shader::setFragmentShaderFromFile(const char * a_FragmentShader) {
	setFragmentShader(loadShaderFromFile(a_FragmentShader).c_str());
	m_Shaders[ShaderTypes::Fragment].path = a_FragmentShader;
}

void Shader::setGeometryShaderFromFile(const char * a_GeometryShader) {
	setGeometryShader(loadShaderFromFile(a_GeometryShader).c_str());
	m_Shaders[ShaderTypes::Geometry].path = a_GeometryShader;
}

std::string Shader::loadShaderFromFile(const char * a_ShaderFileLocation) {
	std::ifstream shaderFile(a_ShaderFileLocation);
	std::stringstream fileBuffer;
	fileBuffer << shaderFile.rdbuf();//get text from file into buffer
	std::string shaderText = fileBuffer.str();

	if (shaderText.size() <= 5) {
		printf("shader loaded from file was empty %s\n", a_ShaderFileLocation);
		return "";
	}
	return shaderText;
}

void Shader::linkShader() {
	if (m_Linked) {
		printf("Shader already linked");
		return;
	}

	if (m_ProgramID == 0) {
		createProgram();
	}

	//m_...Shader is 0 if not set, if it's 0 then opengl will not do anything
	for (int i = 0; i < ShaderTypes::END_SHADER_TYPES; i++) {
		if (m_Shaders[i].shaderId != 0) {
			glAttachShader(m_ProgramID, m_Shaders[i].shaderId);
		}
	}

	glLinkProgram(m_ProgramID);

	int success = GL_FALSE;


	glGetProgramiv(m_ProgramID, GL_LINK_STATUS, &success);
	if (success == GL_FALSE) {
		int infoLogLength = 0;
		glGetProgramiv(m_ProgramID, GL_INFO_LOG_LENGTH, &infoLogLength);
		char* infoLog = new char[infoLogLength];

		glGetProgramInfoLog(m_ProgramID, infoLogLength, 0, infoLog);
		printf("Error: Failed to link shader program\n");
		printf("%s\n", infoLog);
		delete[] infoLog;

		m_Linked = false;
	} else {
		m_Linked = true;
	}


	if (m_Linked) {
		printf("==========================\n");
		printf("Shader Loaded (ID:%i)\n", m_ProgramID);
		if (m_Shaders[ShaderTypes::Vertex].shaderId != 0) {
			printf("\tVertex: %s\n", m_Shaders[ShaderTypes::Vertex].path.c_str());
		}
		if (m_Shaders[ShaderTypes::Fragment].shaderId != 0) {
			printf("\tFragment: %s\n", m_Shaders[ShaderTypes::Fragment].path.c_str());
		}
		if (m_Shaders[ShaderTypes::Geometry].shaderId != 0) {
			printf("\tGeometry: %s\n", m_Shaders[ShaderTypes::Geometry].path.c_str());
		}

		getUniformInformation();
		findCommonUniformLocations();
	}

	//delete shaders
	for (int i = 0; i < ShaderTypes::END_SHADER_TYPES; i++) {
		glDeleteShader(m_Shaders[i].shaderId);
		m_Shaders[i].shaderId = 0;
	}
}

void Shader::createProgram() {
	if (m_ProgramID == 0) {
		m_ProgramID = glCreateProgram();
	} else {
		printf("cant create program if there is already a made program");
	}
}

//void Shader::useProgram(const unsigned int a_ProgramID) {
//	m_CurrentlyUsedProgram = a_ProgramID;
//	glUseProgram(m_CurrentlyUsedProgram);
//}

void Shader::createBasicProgram(bool a_Textured) {
	std::string vertex = "#version 410\n"
		"layout(location=0) in vec4 position; "
		"layout(location = 2) in vec2 texCoord; "
		" "
		"uniform mat4 projectionViewMatrix; "
		"uniform mat4 model = mat4(1); "
		" "
		"out vec2 vTexCoord; "
		" "
		"void main() { "
		"	gl_Position = projectionViewMatrix * model * position; "
		"	vTexCoord = texCoord; "
		"} ";

	std::string fragment;

	if (a_Textured) {
		fragment = "#version 410\n"
			" "
			"	in vec2 vTexCoord; "
			"out vec4 fragColor; "
			" "
			"uniform vec4 color = vec4(1,1,1,1); "
			"uniform sampler2D TexDiffuse1; "
			" "
			"void main() { "
			"	vec4 col = texture(TexDiffuse1, vTexCoord); "
			"	fragColor = (col * color)*col.a; "
			"} ";
	} else {
		fragment = "#version 410\n"
			" "
			"uniform vec4 color = vec4(1,1,1,1); "
			"out vec4 fragColor; "
			" "
			"void main() { "
			"	fragColor = color; "
			"} ";
	}

	setVertexShader(vertex.c_str());
	setFragmentShader(fragment.c_str());

	m_Shaders[ShaderTypes::Vertex].path = "Basic Vertex";
	if (a_Textured) {
		m_Shaders[ShaderTypes::Fragment].path = "Basic Textured Fragment";
	} else {
		m_Shaders[ShaderTypes::Fragment].path = "Basic Untextured Fragment";
	}

	linkShader();
}

void Shader::useProgram(Shader * a_Shader) {
	m_CurrentlyUsedProgram = a_Shader;
	glUseProgram(a_Shader->m_ProgramID);
}

void Shader::useProgram() {
	//DEBUG INFORMATION ABOUT UNIFORM DATA
	//for (size_t i = 0; i < m_Uniforms.size(); i++) {
	//	ShaderUniform* u = m_Uniforms[i];
	//	switch (m_Uniforms[i]->dataType)
	//	{
	//	case GL_FLOAT_MAT4:
	//		printf("mat4: Name: %s, values:(%f,%f,%f,%f),(%f,%f,%f,%f),(%f,%f,%f,%f),(%f,%f,%f,%f)\n", u->name.c_str(), u->data[0], u->data[1], u->data[2], u->data[3], u->data[4], u->data[5], u->data[6], u->data[7], u->data[8], u->data[9], u->data[10], u->data[11], u->data[12], u->data[13], u->data[14], u->data[15]);
	//		break;
	//	case GL_FLOAT_VEC3:
	//		printf("vec3: Name: %s, values: (%f,%f,%f)\n", u->name.c_str(), u->data[0], u->data[1], u->data[2]);
	//		break;
	//	case GL_FLOAT_VEC4:
	//		printf("vec4: Name: %s, values: (%f,%f,%f,%f)\n", u->name.c_str(), u->data[0], u->data[1], u->data[2], u->data[3]);
	//		break;
	//	case GL_FLOAT:
	//		printf("float: Name: %s, values: (%f)\n", u->name.c_str(), u->data[0]);
	//		break;
	//	default:
	//		break;
	//	}
	//}

	m_CurrentlyUsedProgram = this;
	glUseProgram(m_ProgramID);
}

void Shader::setUniformTime() {
	if (m_CommonUniformLoc[CommonUniforms::Time] != -1) {
		float time = TimeHandler::getCurrentTime();
		glUniform1f(m_CommonUniformLoc[CommonUniforms::Time], time);
		setUniformObjectData(UniformTypes::FLOAT,
							 m_CommonUniformLoc[CommonUniforms::Model], &time);
	}
}

void Shader::setUniformTime(float time) {
	if (m_CommonUniformLoc[CommonUniforms::Time] != -1) {
		glUniform1f(m_CommonUniformLoc[CommonUniforms::Time], time);
		setUniformObjectData(UniformTypes::FLOAT,
							 m_CommonUniformLoc[CommonUniforms::Model], &time);
	}
}

void Shader::setNormalRotation(const glm::mat4& a_ModelMatrix) {
	if (m_CommonUniformLoc[CommonUniforms::NormalRot] != -1) {
		//http://stackoverflow.com/questions/24351858/normal-rotation-in-glsl
		glm::mat4 trasp = glm::transpose(glm::inverse(a_ModelMatrix));
		glUniformMatrix4fv(m_CommonUniformLoc[CommonUniforms::NormalRot], 1, GL_FALSE, glm::value_ptr(trasp));
		setUniformObjectData(UniformTypes::FLOAT_MAT4,
							 m_CommonUniformLoc[CommonUniforms::Model], glm::value_ptr(a_ModelMatrix));
	}
}

void Shader::setModelMatrix(const glm::mat4& a_ModelMatrix) {
	if (m_CommonUniformLoc[CommonUniforms::Model] != -1) {
		glUniformMatrix4fv(m_CommonUniformLoc[CommonUniforms::Model], 1, GL_FALSE, glm::value_ptr(a_ModelMatrix));
		setUniformObjectData(UniformTypes::FLOAT_MAT4,
							 m_CommonUniformLoc[CommonUniforms::Model], glm::value_ptr(a_ModelMatrix));
	}
}

void Shader::setModelAndNormalMatrix(const glm::mat4& a_ModelMatrix) {
	setNormalRotation(a_ModelMatrix);
	setModelMatrix(a_ModelMatrix);
}

void Shader::setNormalRotation(Transform & a_ModelMatrix) {
	setNormalRotation(a_ModelMatrix.getGlobalTransform());
}

void Shader::setModelMatrix(Transform & a_ModelMatrix) {
	setModelMatrix(a_ModelMatrix.getGlobalTransform());
}

void Shader::setModelAndNormalMatrix(Transform & a_ModelMatrix) {
	glm::mat4* transform = &a_ModelMatrix.getGlobalTransform();
	setModelAndNormalMatrix(*transform);
}

void Shader::setUniformProjectionView(const glm::mat4& a_ProjectionView) {
	if (m_CommonUniformLoc[CommonUniforms::ProjectionView] != -1) {
		glUniformMatrix4fv(m_CommonUniformLoc[CommonUniforms::ProjectionView], 1, GL_FALSE, glm::value_ptr(a_ProjectionView));
		setUniformObjectData(UniformTypes::FLOAT_MAT4,
							 m_CommonUniformLoc[CommonUniforms::ProjectionView], glm::value_ptr(a_ProjectionView));
	}
}

void Shader::setUniformProjectionView(Camera * a_Camera) {
	if (m_CommonUniformLoc[CommonUniforms::ProjectionView] != -1) {
		glm::mat4 proj = a_Camera->getProjectionView();
		glUniformMatrix4fv(m_CommonUniformLoc[CommonUniforms::ProjectionView], 1, GL_FALSE, glm::value_ptr(proj));
		setUniformObjectData(UniformTypes::FLOAT_MAT4,
							 m_CommonUniformLoc[CommonUniforms::ProjectionView], glm::value_ptr(proj));
	}
}

unsigned int Shader::getUniformLocation(const char * a_Name) {
	int loc = glGetUniformLocation(m_ProgramID, a_Name);
	if (loc == -1 && m_ShowUniformErrorMessages) {
		printf("Attempted to get location of uniform %s\nIt is not in the shader\n", a_Name);
	}
	return loc;
}

void Shader::setUniform(const ShaderUniform * a_Uniform) {
	int loc = getUniformLocation(a_Uniform->name.c_str());
	switch (a_Uniform->dataType) {
		case UniformTypes::FLOAT_MAT4:
			glUniformMatrix4fv(loc, 1, GL_FALSE, a_Uniform->data);
		case UniformTypes::FLOAT_VEC4:
			glUniform4fv(loc, 1, a_Uniform->data);
		case UniformTypes::FLOAT_VEC3:
			glUniform3fv(loc, 1, a_Uniform->data);
		case UniformTypes::INT_VEC2:
			glUniform2iv(loc, 1, (int*) a_Uniform->data);
		case UniformTypes::FLOAT_VEC2:
			glUniform2fv(loc, 1, a_Uniform->data);
		case UniformTypes::FLOAT:
			glUniform1fv(loc, 1, a_Uniform->data);
		case UniformTypes::INT:
		case UniformTypes::SAMPLER_2D:
		case UniformTypes::UNSIGNED_INT:
			glUniform1iv(loc, 1, (int*) a_Uniform->data);
		default:
			//no uniform set up for this data type
			assert(true);
			break;
	}
	setUniformObjectData(a_Uniform->dataType, a_Uniform->name.c_str(), a_Uniform->data);
}

void Shader::setUniform(const char * a_Name, float a_A0) {
	unsigned int loc = getUniformLocation(a_Name);
	glUniform1f(loc, a_A0);

	setUniformObjectData(UniformTypes::UNSIGNED_INT, a_Name, &a_A0);
}

void Shader::setUniform(const char * a_Name, unsigned int a_A0) {
	unsigned int loc = getUniformLocation(a_Name);
	glUniform1i(loc, a_A0);

	setUniformObjectData(UniformTypes::INT, a_Name, &a_A0);
}

void Shader::setUniform(const char * a_Name, float a_A0, float a_A1) {
	unsigned int loc = getUniformLocation(a_Name);
	glUniform2f(loc, a_A0, a_A1);

	setUniformObjectData(UniformTypes::FLOAT_VEC2, a_Name, &a_A0);
}

void Shader::setUniform(const char * a_Name, int a_A0, int a_A1) {
	unsigned int loc = getUniformLocation(a_Name);
	glUniform2i(loc, a_A0, a_A1);

	setUniformObjectData(UniformTypes::INT_VEC2, a_Name, &a_A0);
}

void Shader::setUniform(const char * a_Name, float a_A0, float a_A1, float a_A2) {
	unsigned int loc = getUniformLocation(a_Name);
	glUniform3f(loc, a_A0, a_A1, a_A2);

	setUniformObjectData(UniformTypes::FLOAT_VEC3, a_Name, &a_A0);
}

void Shader::setUniform(const char * a_Name, const glm::vec3& a_Vec3) {
	unsigned int loc = getUniformLocation(a_Name);
	glUniform3fv(loc, 1, glm::value_ptr(a_Vec3));


	setUniformObjectData(UniformTypes::FLOAT_VEC3, a_Name, glm::value_ptr(a_Vec3));
}

void Shader::setUniform(const char * a_Name, float a_A0, float a_A1, float a_A2, float a_A3) {
	unsigned int loc = getUniformLocation(a_Name);
	glUniform4f(loc, a_A0, a_A1, a_A2, a_A3);

	setUniformObjectData(UniformTypes::FLOAT_VEC4, a_Name, &a_A0);
}

void Shader::setUniform(const char * a_Name, const glm::vec4 & a_Vec4) {
	unsigned int loc = getUniformLocation(a_Name);
	glUniform4fv(loc, 1, glm::value_ptr(a_Vec4));

	setUniformObjectData(UniformTypes::FLOAT_VEC4, a_Name, glm::value_ptr(a_Vec4));
}

void Shader::setUniform(const char * a_Name, const glm::mat4 & a_mat4) {
	unsigned int loc = getUniformLocation(a_Name);
	glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(a_mat4));

	setUniformObjectData(UniformTypes::FLOAT_MAT4, a_Name, glm::value_ptr(a_mat4));
}

void Shader::deleteShader() {
	m_Linked = false;
	if (m_CurrentlyUsedProgram == this) {
		glUseProgram(0);
		m_CurrentlyUsedProgram = nullptr;
	}
	if (m_ProgramID != 0) {
		glDeleteProgram(m_ProgramID);
		m_ProgramID = 0;
	}
}

const int Shader::getProgramId() const {
	return m_ProgramID;
}

void Shader::findCommonUniformLocations() {
	if (m_ProgramID == 0) {
		return;
	}
	m_CommonUniformLoc[CommonUniforms::ProjectionView] = glGetUniformLocation(m_ProgramID, "projectionViewMatrix");
	m_CommonUniformLoc[CommonUniforms::Time] = glGetUniformLocation(m_ProgramID, "time");
	m_CommonUniformLoc[CommonUniforms::Model] = glGetUniformLocation(m_ProgramID, "model");
	m_CommonUniformLoc[CommonUniforms::NormalRot] = glGetUniformLocation(m_ProgramID, "normalRot");
}

void Shader::getUniformInformation() {
	//http://stackoverflow.com/questions/440144/in-opengl-is-there-a-way-to-get-a-list-of-all-uniforms-attribs-used-by-a-shade
	printf("==========================\n");
	GLint i;
	GLint count;

	GLint size;
	GLenum type;

	const GLsizei bufSize = 32;
	GLchar name[bufSize];
	GLsizei length;
	//get uniforms
	glGetProgramiv(m_ProgramID, GL_ACTIVE_UNIFORMS, &count);

	for (i = 0; i < count; i++) {
		glGetActiveUniform(m_ProgramID, (GLuint) i, bufSize, &length, &size, &type, name);

		std::string typeName;
		ShaderUniform* uniform = new ShaderUniform();

		switch (type) {
			case GL_FLOAT_MAT4:
				typeName = "mat4";
				uniform->data = new float[16]{ 1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1 };
				break;
			case GL_FLOAT_VEC4:
				typeName = "vec4";
				uniform->data = new float[4]{ 0 };
				break;
			case GL_FLOAT_VEC3:
				typeName = "vec3";
				uniform->data = new float[3]{ 0 };
				break;
			case GL_FLOAT_VEC2:
				typeName = "vec2";
				uniform->data = new float[2]{ 0 };
				break;
			case GL_FLOAT:
				typeName = "float";
				uniform->data = new float[1]{ 0 };
				break;
			case GL_SAMPLER_2D:
				typeName = "sampler 2D";
				uniform->data = new float[1]{ 0 };
				break;
			default:
				typeName = "unknown type";
				uniform->data = new float[0];
				break;
		}
		if (uniform != nullptr) {
			uniform->name = name;
			uniform->dataType = getUnifromTypeFromGl(type);
			m_Uniforms.push_back(uniform);
		}
		printf("Uniform #%u Type: %u/%s, \tName: %s\n", i, type, typeName.c_str(), name);
	}
}

Shader* Shader::getCurrentShader() {
	return m_CurrentlyUsedProgram;
}

ShaderUniform * Shader::getUniformObject(const UniformTypes a_Type, const  std::string a_Name) {
	//go through each uniform
	for (size_t i = 0; i < m_Uniforms.size(); i++) {
		//check type
		if (m_Uniforms[i]->dataType == a_Type) {
			//check name
			if (m_Uniforms[i]->name == a_Name) {
				//return uniform
				return m_Uniforms[i];
			}
		}
	}
	return nullptr;
}

ShaderUniform * Shader::getUniformObject(const UniformTypes a_Type, const unsigned int a_Location) {
	////go through each uniform
	//for (size_t i = 0; i < m_Uniforms.size(); i++) {
	//	//check type
	//	if (m_Uniforms[i]->dataType == a_Type) {
	//		//check name
	//		if (m_Uniforms[i]->name == a_Name) {
	//			//return uniform
	//			return m_Uniforms[i];
	//		}
	//	}
	//}
	if (a_Location > m_Uniforms.size()) {
		return nullptr;
	}
	return m_Uniforms[a_Location];
}

void Shader::setUniformObjectData(const UniformTypes a_Type, const  std::string a_Name, const float * a_Data) {
	ShaderUniform* uniform = getUniformObject(a_Type, a_Name);
	if (uniform != nullptr) {
		size_t size = getUniformTypeSize(a_Type);

		//copy data
		for (size_t i = 0; i < size; i++) {
			uniform->data[i] = a_Data[i];
		}

		//memcpy(uniform->data, a_Data, size);

		//uniform->data = a_Data;
	}
}

void Shader::setUniformObjectData(const UniformTypes a_Type, const  std::string a_Name, const int * a_Data) {
	ShaderUniform* uniform = getUniformObject(a_Type, a_Name);
	if (uniform != nullptr) {
		size_t size = getUniformTypeSize(a_Type);

		//copy data
		for (size_t i = 0; i < size; i++) {
			uniform->data[i] = (float) a_Data[i];
		}
	}
}

void Shader::setUniformObjectData(const UniformTypes a_Type, std::string a_Name, const unsigned int * a_Data) {
	ShaderUniform* uniform = getUniformObject(a_Type, a_Name);
	if (uniform != nullptr) {
		size_t size = getUniformTypeSize(a_Type);
		//copy data
		for (size_t i = 0; i < size; i++) {
			uniform->data[i] = (float) a_Data[i];
		}
	}
}

void Shader::setUniformObjectData(const UniformTypes a_Type, const unsigned int m_Location, const float * a_Data) {
	ShaderUniform* uniform = getUniformObject(a_Type, m_Location);
	if (uniform != nullptr) {
		size_t size = getUniformTypeSize(a_Type);
		//copy data
		for (size_t i = 0; i < size; i++) {
			uniform->data[i] = (float) a_Data[i];
		}
	}
}

void Shader::setUniformObjectData(const UniformTypes a_Type, const unsigned int m_Location, const int * a_Data) {
	ShaderUniform* uniform = getUniformObject(a_Type, m_Location);
	if (uniform != nullptr) {
		size_t size = getUniformTypeSize(a_Type);
		//copy data
		for (size_t i = 0; i < size; i++) {
			uniform->data[i] = (float) a_Data[i];
		}
	}
}

void Shader::setUniformObjectData(const UniformTypes a_Type, const unsigned int m_Location, const unsigned int * a_Data) {
	ShaderUniform* uniform = getUniformObject(a_Type, m_Location);
	if (uniform != nullptr) {
		size_t size = getUniformTypeSize(a_Type);
		//copy data
		for (size_t i = 0; i < size; i++) {
			uniform->data[i] = (float) a_Data[i];
		}
	}
}

unsigned int Shader::getUniformTypeSize(const UniformTypes a_Type) {
	size_t size = 0;
	//get size
	switch (a_Type) {
		case UniformTypes::FLOAT_MAT4:
			size = 16;
			break;
		case UniformTypes::FLOAT_VEC4:
			size = 4;
			break;
		case UniformTypes::FLOAT_VEC3:
			size = 3;
			break;
		case UniformTypes::INT_VEC2:
		case UniformTypes::FLOAT_VEC2:
			size = 2;
			break;
		case UniformTypes::FLOAT:
		case UniformTypes::INT:
		case UniformTypes::UNSIGNED_INT:
		case UniformTypes::SAMPLER_2D:
			size = 1;
			break;
		default:
			return 0;
	}

	return size;
}

UniformTypes Shader::getUnifromTypeFromGl(const GLenum a_Type) {
	switch (a_Type) {
		case GL_FLOAT_MAT4:
			return UniformTypes::FLOAT_MAT4;
		case GL_FLOAT_VEC4:
			return UniformTypes::FLOAT_VEC4;
		case GL_FLOAT_VEC3:
			return UniformTypes::FLOAT_VEC3;
		case GL_INT_VEC2:
			return UniformTypes::INT_VEC2;
		case GL_FLOAT_VEC2:
			return UniformTypes::FLOAT_VEC2;
		case GL_FLOAT:
			return UniformTypes::FLOAT;
		case GL_INT:
			return UniformTypes::INT;
		case GL_UNSIGNED_INT:
			return UniformTypes::UNSIGNED_INT;
		case GL_SAMPLER_2D:
			return UniformTypes::SAMPLER_2D;
	}

	//should not get to this point
	//wrong a_Type or not set up yet
	assert(false);
	return UniformTypes::INT;
}
