#include "Morph.h"

#include <gl_core_4_4.h>

#include "Mesh.h"
#include "Model.h"

#include "Vertex.h"

Morph::~Morph() {
	for (int i = 0; i < 2; i++) {
		delete m_Models[i];
	}
}

void Morph::load(const char * a_Dir, const char * a_Model1, const char * a_Model2) {
	m_Models[0] = new Model();
	m_Models[1] = new Model();

	m_Models[0]->m_Transform.m_ShowInHierarchy = false;
	m_Models[1]->m_Transform.m_ShowInHierarchy = false;

	//load
	m_Models[0]->loadModel(a_Dir, a_Model1, false);
	m_Models[1]->loadModel(a_Dir, a_Model2, false);

	m_Meshes.resize(m_Models[0]->m_Meshs.size());

	for (size_t i = 0; i < m_Models[0]->m_Meshs.size(); i++) {
		printf("1: %u  \t2: %u\n", (unsigned int)m_Models[0]->m_Meshs[i]->m_Vertexs.size(), (unsigned int)m_Models[1]->m_Meshs[i]->m_Vertexs.size());

		//bind
		GLInfo gl;

		gl.vbo1 = createVertex(m_Models[0], (unsigned int)i);
		gl.vbo2 = createVertex(m_Models[1], (unsigned int)i);
		gl.faceCount = (unsigned int)m_Models[0]->m_Meshs[i]->m_Indices.size();

		//VAO
		glGenVertexArrays(1, &gl.vao);
		glBindVertexArray(gl.vao);

		//bind first VBO
		glBindBuffer(GL_ARRAY_BUFFER, gl.vbo1);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(VertexTex), 0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(VertexTex), (GLvoid*)offsetof(VertexTex, normal));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(VertexTex), (GLvoid*)offsetof(VertexTex, texCoord));

		//bind second VBO
		glBindBuffer(GL_ARRAY_BUFFER, gl.vbo2);
		glEnableVertexAttribArray(3);
		glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(VertexTex), 0);
		glEnableVertexAttribArray(4);
		glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(VertexTex), (GLvoid*)offsetof(VertexTex, normal));

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
		m_Meshes.push_back(gl);
	}



}

void Morph::draw() {
	for (auto& gl : m_Meshes) {
		glBindVertexArray(gl.vao);
		glDrawArrays(GL_TRIANGLES, 0, gl.faceCount);
		glBindVertexArray(0);
	}
}

unsigned int Morph::createVertex(Model * a_Model, unsigned int a_MeshIndex) {
	unsigned int buffer = 0;

	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER,
		a_Model->m_Meshs[a_MeshIndex]->m_Vertexs.size() * sizeof(VertexTex),
		&a_Model->m_Meshs[a_MeshIndex]->m_Vertexs[0],
		GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	return buffer;
}
