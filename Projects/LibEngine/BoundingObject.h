#pragma once

#include <vector>

#include "Vertex.h"

class BoundingObject {
public:
	BoundingObject() { reset(); }
	virtual void reset() {
		m_Min.x = m_Min.y = m_Min.z = 1e37f;
		m_Max.x = m_Max.y = m_Max.z = -1e37f;

	}
	virtual void fit(const std::vector<VertexTex>& a_Points) = 0;

	glm::vec3 getMin() {
		return m_Min;
	}
	glm::vec3 getMax() {
		return m_Max;
	}
	

protected:
	glm::vec3 m_Min, m_Max;

};