#include "DebugTimer.h"

//http://en.cppreference.com/w/cpp/chrono

std::chrono::duration<float> DebugTimer::m_LastTime;
std::map<int, DebugTimer::Time> DebugTimer::m_Times;

void DebugTimer::startTimer(unsigned int a_Index, const std::string a_Desc) {
	Time t;
	t.m_Desc = a_Desc;
	t.m_Index = a_Index;
	t.m_CurrentlyTimed = true;
	t.m_TotalTime = std::chrono::duration<float>(0);
	t.m_HasLapped = false;
	t.m_LapNumber = 0;
	m_Times[a_Index] = t;
	m_Times[a_Index].m_Start = getCurrentTime();
}

void DebugTimer::endTimer(const unsigned int a_Index, const bool a_DisplayInConsole) {
	//this is here so it doesnt count getting the struct in the total time
	std::chrono::time_point<DEBUG_TIMER_CLOCK_TYPE> endTime = getCurrentTime();

	Time* t = getTimer(a_Index);
	if (t == nullptr) {
		return;
	}
	//only update if timer is on
	if (t->m_CurrentlyTimed) {
		t->m_End = endTime;

		t->m_TotalTime += t->m_End - t->m_Start;

		if (a_DisplayInConsole) {
			displayDiff(t);
		}

		t->m_CurrentlyTimed = false;
	}
	m_Times.erase(a_Index);
}

void DebugTimer::lapTimer(const unsigned int a_Index, const bool a_DisplayInConsole) {
	std::chrono::time_point<DEBUG_TIMER_CLOCK_TYPE> lapTime = getCurrentTime();

	//get timer
	Time* t = &m_Times.at(a_Index);
	//check if that timer exists
	if (t == nullptr) {
		return;
	}
	//only update timer if it's currently turned on
	if (t->m_CurrentlyTimed) {
		//update end times
		t->m_End = lapTime;

		t->m_HasLapped = true;

		t->m_TotalTime += t->m_End - t->m_Start;

		//check if it will be printed?
		if (a_DisplayInConsole) {
			displayDiff(t);
		}
		t->m_LapNumber++;
		//reset timer
		t->m_Start = getCurrentTime();
	}
}

bool DebugTimer::isTimerActive(const unsigned int a_Index) {
	Time* t = getTimer(a_Index);
	//check if that timer exists
	if (t == nullptr) {
		return false;
	}
	return true;
}

std::chrono::time_point<DEBUG_TIMER_CLOCK_TYPE> DebugTimer::getCurrentTime() {
	//return std::chrono::system_clock::now();
	return DEBUG_TIMER_CLOCK_TYPE::now();
}

void DebugTimer::displayDiff(const Time* a_Time) {
	//if (a_Time->m_CurrentlyTimed) {
	std::chrono::duration<float> elapsed = a_Time->m_End - a_Time->m_Start;
	printf("DebugTimer #%u/%u, Name: %s, Time: (%f)ms", a_Time->m_Index, a_Time->m_LapNumber, a_Time->m_Desc.c_str(), elapsed.count() * 1000);

	if (a_Time->m_HasLapped) {
		printf(", Total time: (%f)ms", a_Time->m_TotalTime.count() * 1000);
	}

	printf("\n");

	//} else {
//	printf("Timer %i is not currently active\n",a_Time->m_Index);
//}
}

DebugTimer::Time * DebugTimer::getTimer(const unsigned int a_Index) {
	if (!m_Times.count(a_Index)) {//check if a_Index is apart of m_Times
		return nullptr;
	}
	//get timer, this way we dont create a timer to end it if it doesn't exist
	Time* t = &m_Times.at(a_Index);
	return t;
}
