#include "Camera.h"

#include <glm/ext.hpp>

#include <stdio.h>

Camera* Camera::m_MainCamera = nullptr;

Camera::Camera() {
	if (m_MainCamera == nullptr) {
		m_MainCamera = this;
	}
}

Camera::~Camera() {
	if (this == m_MainCamera) {
		m_MainCamera = nullptr;
	}
}

void Camera::setPerspective(float a_FieldOfView, float a_Aspect, float a_Near, float a_Far) {
	m_ProjectionTransform = glm::perspective(a_FieldOfView, a_Aspect, a_Near, a_Far);
	m_FieldOfView = a_FieldOfView;
	m_Aspect = a_Aspect;
	m_NearPlane = a_Near;
	m_FarPlane = a_Far;
	setDirty();
}

void Camera::setOthrographic(const float a_Left, const float a_Right, const float a_Bottom, const float a_Top, const float a_Near, const float a_Far) {
	m_ProjectionTransform = glm::ortho(a_Left, a_Right, a_Bottom, a_Top, a_Near, a_Far);
	setDirty();
}

glm::mat4 Camera::getView() {
	if (isParentDirty()) {
		updateTransform();
	}
	return m_ViewTransform;
}

glm::mat4 Camera::getProjection() {
	return m_ProjectionTransform;
}

glm::mat4 Camera::getProjectionView() {
	if (isParentDirty()) {
		updateTransform();
	}
	return m_ProjectionViewTransform;
}

void Camera::updateTransform() {
	Transform::updateTransform();

	m_ViewTransform = glm::inverse(m_GlobalTransform);
	updateProjectionViewTransform();
}

void Camera::updateProjectionViewTransform() {

	m_ProjectionViewTransform = m_ProjectionTransform * m_ViewTransform;
}