#include "Texture.h"

#include <gl_core_4_4.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <stdio.h>
#include <string>

#include "Shader.h"

#include "ResourceManager.h"


//32 is from gl_core_4_4.h, it is the highest number listed with GL_TEXTURE__
static unsigned int m_UsedTexID[32];

//Texture::~Texture()
//{
//	//http://en.cppreference.com/w/cpp/memory/new/operator_delete
//	//this->release();
//}

Texture::Texture() {
}

Texture::~Texture() {
	//if this is not the only resource then assert of the resource can be deleted
	ResourceManager::ResourceRef* ref = ResourceManager::getLoadedResource(this);

	if (ref == nullptr) {
		return;
	}

	if (ref->resourceCount != 1) {
		//resource manager has not allowed this resource to be deleted
		//to delete this use texture->release
		assert(m_CanBeDeleated);
	} else {
		//else remove this resource from the list but don't get the Resource manager to delete
		ResourceManager::removeResource(this,false);
	}

	//if it gets here then it will delete
}

void Texture::loadTexture(const char * a_FilePath) {
	m_FilePath = a_FilePath;

	ResourceManager::addResource(this);
}

void Texture::createWhite() {
	if (m_TextureId != 0) {
		printf("Currently overriding texture %s with texture %s\n", m_TexturePath, "Code White");
		glDeleteTextures(1, &m_TextureId);
	}

	GLubyte data[] = { 255,255,255,255 };

	glGenTextures(1, &m_TextureId);
	glBindTexture(GL_TEXTURE_2D, m_TextureId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
}


void Texture::useTexture(Texture * a_Tex, unsigned int a_TextureSpot, unsigned int a_UniformLocation) {
	unsigned int texID = 0;

	if (a_Tex != nullptr) {
		texID = a_Tex->m_TextureId;
	}

	// texID starts at 0, so if this texture doesn't have a texture then it should set it to 0
	bindTexture(a_TextureSpot, texID);

	//if (texID != 0) {
	glUniform1i(a_UniformLocation, a_TextureSpot);
	//}
}

void Texture::useTexture(Texture * a_Tex, unsigned int a_TextureSpot, const char * a_UniformName) {
	unsigned int loc = glGetUniformLocation(Shader::getCurrentShader()->getProgramId(), a_UniformName);
	useTexture(a_Tex, a_TextureSpot, loc);
}

void Texture::useTexture(unsigned int a_TextureID, unsigned int a_TextureSpot, const char * a_UniformName) {

	bindTexture(a_TextureSpot, a_TextureID);

	unsigned int loc = glGetUniformLocation(Shader::getCurrentShader()->getProgramId(), a_UniformName);
	glUniform1i(loc, a_TextureSpot);
}

void Texture::bindTexture(unsigned int a_TextureSpot, Texture * a_Tex) {
	//assert(a_Tex == nullptr && "a_Tex cant be nullptr");
	bindTexture(a_TextureSpot, a_Tex->m_TextureId);
}

void Texture::bindTexture(unsigned int a_TextureSpot, unsigned int a_ID) {
	//if this texture is already bound
	if (m_UsedTexID[a_TextureSpot] == a_ID) {
		return;
	}

	//else bind that texture
	glActiveTexture(GL_TEXTURE0 + a_TextureSpot);
	glBindTexture(GL_TEXTURE_2D, a_ID);

	//update m_UsedTexId
	m_UsedTexID[a_TextureSpot] = a_ID;
}

unsigned int Texture::getTypeId() {
	return 0;
}

void Texture::load() {
	m_ImageWidth = m_ImageHeight = m_ImageFormat = 0;

	unsigned char* data = stbi_load(m_FilePath.c_str(), &m_ImageWidth, &m_ImageHeight, &m_ImageFormat, STBI_default);

	if (data == nullptr || data == '\0') {
		return;
	}

	if (m_TextureId != 0) {
		//printf("Currently overriding texture %s with texture %s\n", m_TexturePath, a_FilePath);
		glDeleteTextures(1, &m_TextureId);
	}

	unsigned int textureType = GL_RGB;
	if (m_ImageFormat == STBI_rgb_alpha) {
		textureType = GL_RGBA;
	}
	if (m_ImageFormat == STBI_grey) {
		textureType = GL_RED;
	}


	glGenTextures(1, &m_TextureId);
	glBindTexture(GL_TEXTURE_2D, m_TextureId);
	glTexImage2D(GL_TEXTURE_2D, 0, textureType, m_ImageWidth, m_ImageHeight, 0, textureType, GL_UNSIGNED_BYTE, data);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	stbi_image_free(data);
}

void Texture::copy(IResource * a_Res) {
	Texture* tex = (Texture*)a_Res;
	m_FilePath = tex->m_FilePath;
	m_ImageFormat = tex->m_ImageFormat;
	m_ImageWidth = tex->m_ImageWidth;
	m_ImageHeight = tex->m_ImageHeight;
	m_TextureId = tex->m_TextureId;
}

void Texture::destroy() {
	if (m_TextureId != 0) {
		glDeleteTextures(1, &m_TextureId);
	}
}

void Texture::release() {
	ResourceManager::removeResource(this);
}
