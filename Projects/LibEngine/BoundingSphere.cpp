#include "BoundingSphere.h"



BoundingSphere::BoundingSphere() : m_Center(0), m_Radius(0) {

}

void BoundingSphere::fit(const std::vector<VertexTex>& a_Points) {

	for (auto& vertex : a_Points) {
		const glm::vec4* p = &vertex.position;
		if (p->x < m_Min.x) {
			m_Min.x = p->x;
		}
		if (p->y < m_Min.y) {
			m_Min.y = p->y;
		}
		if (p->z < m_Min.z) {
			m_Min.z = p->z;
		}
		if (p->x > m_Max.x) {
			m_Max.x = p->x;
		}
		if (p->y > m_Max.y) {
			m_Max.y = p->y;
		}
		if (p->z > m_Max.z) {
			m_Max.z = p->z;
		}
	}
	
	m_Center = (m_Min + m_Max) * 0.5f;
	m_Radius = glm::distance(m_Min, m_Center);

}

glm::vec3 BoundingSphere::getCenter() {
	return m_Center;
}

float BoundingSphere::getRadius() {
	return m_Radius;
}
