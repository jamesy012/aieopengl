#pragma once

#include <glm\glm.hpp>

class Camera;
class Shader;
class Texture;

class GPUParticleEmitter {

	struct GPUParticle {
		GPUParticle() : lifetime(1), lifespan(0) {}

		glm::vec3 position;
		glm::vec3 velocity;
		float lifetime;
		float lifespan;

	};

public:
	GPUParticleEmitter();
	~GPUParticleEmitter();

	void initalise(const unsigned int a_MaxParticles,
		const float a_LifespanMin, const float a_LifespanMax,
		const float a_VelocityMin, const float a_VelocityMax,
		const float a_StartSize, const float a_EndSize,
		const glm::vec4& a_StartColor, const glm::vec4& a_EndColor);


	void draw(Camera& a_Camera);

	Texture* m_ParticleTexture;
private:
	void createBuffers();
	void createUpdateShader();
	void createDrawShader();


	GPUParticle* m_Particles;

	unsigned int m_MaxParticles;

	glm::vec3 m_Position;

	float m_LifespanMin;
	float m_LifespanMax;

	float m_VelocityMin;
	float m_VelocityMax;

	float m_StartSize;
	float m_EndSize;

	glm::vec4 m_StartColor;
	glm::vec4 m_EndColor;

	unsigned int m_ActiveBuffer;
	unsigned int m_Vao[2];
	unsigned int m_Vbo[2];

	//unsigned int m_UpdateShader;
	//unsigned int m_DrawShader;

	Shader* m_UpdateShader;
	Shader*	m_DrawShader;

	float m_LastDrawTime;
};

