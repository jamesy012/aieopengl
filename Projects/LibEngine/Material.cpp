#include "Material.h"

#include "Texture.h"
#include "Shader.h"

static unsigned int m_ObjectCreated = 0;
static Texture* m_DefaultWhiteTexture = nullptr;

Material::Material() {
	if (m_ObjectCreated == 0) {
		m_DefaultWhiteTexture = new Texture();
		m_DefaultWhiteTexture->createWhite();
	}

	setTexture(TextureSlots::Diffuse, m_DefaultWhiteTexture);
	setTexture(TextureSlots::Specular, m_DefaultWhiteTexture);
	setTexture(TextureSlots::Normal, m_DefaultWhiteTexture);

	m_ObjectCreated++;
}

Material::~Material() {
	m_ObjectCreated--;

	if (m_ObjectCreated == 0) {
		delete m_DefaultWhiteTexture;
	}
}

Material::Material(const Material & a_Copy) {
	//copy all pointers
	m_Textures[TextureSlots::Diffuse] = a_Copy.m_Textures[TextureSlots::Diffuse];
	m_Textures[TextureSlots::Specular] = a_Copy.m_Textures[TextureSlots::Specular];
	m_Textures[TextureSlots::Normal] = a_Copy.m_Textures[TextureSlots::Normal];
}

Material & Material::operator=(const Material & a_Copy) {
	//copy all pointers
	m_Textures[TextureSlots::Diffuse] = a_Copy.m_Textures[TextureSlots::Diffuse];
	m_Textures[TextureSlots::Specular] = a_Copy.m_Textures[TextureSlots::Specular];
	m_Textures[TextureSlots::Normal] = a_Copy.m_Textures[TextureSlots::Normal];
	return *this;
}

void Material::setColor(const glm::vec4 a_Color) {
	m_Color.color = a_Color;
	m_Color.isUsing = true;
}

void Material::usingColor(const bool a_UseColor) {
	m_Color.isUsing = a_UseColor;
}

void Material::setTexture(TextureSlots a_Slot, Texture * a_Texture) {
	if (a_Texture == nullptr) {
		m_Textures[a_Slot] = m_DefaultWhiteTexture;
	} else {
		m_Textures[a_Slot] = a_Texture;
	}
}

void Material::useTextures() const {

	//if (m_Color.isUsing) {
	//todo check what the color is currently set to, only change if it's different
		Shader* shader = Shader::getCurrentShader();
		shader->setUniform("color", m_Color.color);
	
	//}

	Texture::useTexture(m_Textures[TextureSlots::Diffuse], TextureSlots::TextureSlot0, "TexDiffuse1");
	Texture::useTexture(m_Textures[TextureSlots::Specular], TextureSlots::TextureSlot1, "TexSpecular1");
	Texture::useTexture(m_Textures[TextureSlots::Normal], TextureSlots::TextureSlot2, "TexNormal1");
}
