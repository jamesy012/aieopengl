#pragma once

#include <vector>

#include "Transform.h"

#include "IDrawable.h"

class Mesh;
class Material;
class Texture;
class BoundingSphere;

class Model : public IDrawable {
public:
	Model();
	~Model();
	Model(const Model& a_Copy);

	void loadModel(const char* a_FilePath, const char* a_FileName, const bool a_Bind = true);

	void fit(BoundingSphere* a_BoundingSphere);

	//a transform for the model
	Transform m_Transform;

	//list of all mesh's part of this model
	std::vector<Mesh*> m_Meshs;
	//list of all materials used by these meshes
	std::vector<Material*> m_Materials;
	//a reference to all textures loaded by the model to delete in deconstruction
	std::vector<Texture*> m_Textures;


	// Inherited via IDrawable
	virtual void draw() override;
	virtual bool isTransparent() override;
	virtual const char * getName() override;

};

