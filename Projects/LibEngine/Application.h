#pragma once

#include <gl_core_4_4.h>
#include <glm/glm.hpp>

#include "Camera.h"
class Application;

class Application {
public:
	Application();
	virtual ~Application();

	void run();

	virtual void windowCreation();
	virtual void startUp() = 0;
	virtual void shutDown() = 0;
	virtual void update() = 0;
	virtual void draw() = 0;

	virtual void frameBufferChanged();
	virtual void screenSizeChanged();
	
	virtual void windowFocusChanged(bool a_GainedFocus);

	void setCallbacksForWindow();

	static Application* getCurrentApplcation();

protected:
	//reference to a common camera used between scenes
	//by default if not set within windowCreation then it will be a fly camera
	Camera* m_Camera = nullptr;

	//set this to true to quit game, will continue to run game loop if false
	bool m_Quit = false;

	//set by classes inheriting from Application
	//tells this class to handle the mouse locking into screen
	//click on window to lock
	//if locked escape will unlock
	bool m_HandleMouseLock = true;
	//set by classes inheriting from Application
	//will handle pressing escape to quit application
	//will not quit if mouse is locked
	bool m_HandleEscapeQuit = true;

	//if set to true then the application will change the glViewPort
	//to be from 0,0 to the new width and height
	bool m_UpdatePerspectiveOnWindowResize = true;
	//if set to true the application will use the new frame buffer size from the resize
	//if set to false the application will use the frame buffer size from when the application was created
	bool m_UpdatePerspetiveToNewWindowSize = true;

	//when the window is resized should we use orthographic or perspective when updating the camera
	bool m_PerspetiveOrthographic = false;
	//when updating the perspective of the camera, should we update it to the new screen size
	bool m_UpdateOrthographicToWidnowSize = true;

private:
	//checks for all the handles done by application
	void handles();
	//handles mouseLock to screen and release of mouse
	void mouseLock();
	//handles when the escape key is pressed and will quit program
	void escapeQuit();

	bool m_GotFocusThisFrame = false;

	//this is the starting root of the program, it sticks around so we can delete it while the program ends
	Transform* m_StartingRoot;
};

