#include "Grid.h"

#include <gl_core_4_4.h>
#include <GLFW\glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include <iostream>

#include "Vertex.h"
#include "Shader.h"

Grid::Grid() {
}


Grid::~Grid() {
	if (m_VAO != 0) {
		glDeleteBuffers(1, &m_VAO);
	}
	if (m_IBO != 0) {
		glDeleteBuffers(1, &m_IBO);
	}
	if (m_VAO != 0) {
		glDeleteVertexArrays(1, &m_VAO);
	}
	if (m_Shader != nullptr) {
		delete m_Shader;
	}
}

void Grid::generateGrid(unsigned int a_Rows, unsigned int a_Cols) {
	m_Rows = a_Rows;
	m_Cols = a_Cols;

	//VERTEX POS/COLOR
	VertexColor* aoVertices = new VertexColor[a_Rows * a_Cols];
	for (unsigned int y = 0; y < a_Rows; ++y) {
		for (unsigned int x = 0; x < a_Cols; ++x) {
			glm::vec4 pos((float)x, 0, (float)y, 1);

			glm::vec3 color(rand()%1000/1000.0f,0,sinf((x / (float)(a_Cols - 1)) *
				y / (float)(a_Rows - 1)));

			aoVertices[y * a_Cols + x].position = pos;
			aoVertices[y * a_Cols + x].color = glm::vec4(color, 1);
		}
	}
	//INDICES/INDEX
	unsigned int* auiIndices = new unsigned int[(a_Rows - 1) * (a_Cols - 1) * 6];

	unsigned int index = 0;
	for (unsigned int y = 0; y < a_Rows-1; ++y) {
		for (unsigned int x = 0; x < a_Cols-1; ++x) {
			//tri 1
			auiIndices[index++] = y * a_Cols + x;
			auiIndices[index++] = (y + 1) * a_Cols + x;
			auiIndices[index++] = (y + 1) * a_Cols + (x + 1);
			//tri 2
			auiIndices[index++] = y * a_Cols + x;
			auiIndices[index++] = (y + 1) * a_Cols + (x + 1);
			auiIndices[index++] = y * a_Cols + (x + 1);
		}
	}


	//bind
	if (m_VAO != 0) {
		glDeleteBuffers(1, &m_VAO);
	}
	if (m_IBO != 0) {
		glDeleteBuffers(1, &m_IBO);
	}
	if (m_VAO != 0) {
		glDeleteVertexArrays(1, &m_VAO);
	}

	glGenBuffers(1, &m_VBO);
	glGenBuffers(1, &m_IBO);
	glGenVertexArrays(1, &m_VAO);

	glBindVertexArray(m_VAO);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, (a_Rows * a_Cols) * sizeof(VertexColor), aoVertices, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(VertexColor), 0);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(VertexColor), (void*)(sizeof(glm::vec4)));

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, (a_Rows - 1) * (a_Cols - 1) * 6 * sizeof(unsigned int), auiIndices, GL_STATIC_DRAW);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	delete[] auiIndices;
	delete[] aoVertices;
}

void Grid::loadShader() {
	const char* vsSource = "#version 410\n \
	layout(location=0) in vec4 position; \
	layout(location=1) in vec4 colour; \
	out vec4 vColour; \
	uniform mat4 projectionViewWorldMatrix; \
	uniform float time; \
	uniform float heightScale; \
	void main() { \
	vColour = colour; \
	vec4 P = position; \
	P.y += sin(time + position.x) * heightScale;\
	P.y += sin(time + position.z) * heightScale/4;\
	gl_Position = projectionViewWorldMatrix * P; \
	}";

	const char* fsSource = "#version 410\n \
	in vec4 vColour; \
	out vec4 fragColor; \
	void main() { \
	fragColor = vColour; \
	}";

	if (m_Shader != nullptr) {
		delete m_Shader;
	}

	Shader shaderText;
	m_Shader = new Shader();

	m_Shader->setShaderFromFile("Shaders/gridWaveVertex.vert", "Shaders/basicFragment.frag");

	//m_Shader->setShader(vsSource, fsSource);
	m_ProgramID = m_Shader->getProgramId();

	m_Shader->findCommonUniformLocations();

	glUseProgram(m_ProgramID);


	unsigned int heightScaleUniform = glGetUniformLocation(m_ProgramID, "heightScale");
	glUniform1f(heightScaleUniform, 2.0f);

	//int success = GL_FALSE;
	//unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
	//unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	//
	//glShaderSource(vertexShader, 1, (const char**)&vsSource, 0);
	//glCompileShader(vertexShader);
	//glShaderSource(fragmentShader, 1, (const char**)&fsSource, 0);
	//glCompileShader(fragmentShader);
	//
	//m_ProgramID = glCreateProgram();
	//glAttachShader(m_ProgramID, vertexShader);
	//glAttachShader(m_ProgramID, fragmentShader);
	//glLinkProgram(m_ProgramID);
	//
	//glGetProgramiv(m_ProgramID, GL_LINK_STATUS, &success);
	//if (success == GL_FALSE) {
	//	int infoLogLength = 0;
	//	glGetProgramiv(m_ProgramID, GL_INFO_LOG_LENGTH, &infoLogLength);
	//	char* infoLog = new char[infoLogLength];
	//
	//	glGetProgramInfoLog(m_ProgramID, infoLogLength, 0, infoLog);
	//	printf("Error: Failed to link shader program\n");
	//	printf("%s\n", infoLog);
	//	delete[] infoLog;
	//}
	//
	//glDeleteShader(fragmentShader);
	//glDeleteShader(vertexShader);
}

void Grid::draw(glm::mat4 a_ProjectionViewMatrix) {

	m_Shader->useProgram();

	m_Shader->setUniformProjectionView(a_ProjectionViewMatrix);
	m_Shader->setUniformTime();


	glBindVertexArray(m_VAO);
	unsigned int indexCount = (m_Rows - 1) * (m_Cols - 1) * 6;
	glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);

}
