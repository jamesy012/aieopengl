#include "Renderer.h"

#include <assert.h>

#include "IDrawable.h"

#include "Camera.h"
#include "Shader.h"


Renderer::~Renderer() {
	endRenderShaderBatch();
	//shaders
	for (size_t i = 0; i < m_DrawList.size();++i) {
		//sdl
		RenderShaderBatch* shaderDrawList = m_DrawList[i];

		//shader objects
		//iterator, sdl, object
		auto itSdlObj = shaderDrawList->objects.begin();
		while (itSdlObj != shaderDrawList->objects.end()) {

			//object uniforms
			//iterator, sdl, object, uniforms
			auto itSdlObjUni = itSdlObj->second->uniforms.begin();
			while (itSdlObjUni != itSdlObj->second->uniforms.end()) {
				//delete and next uniform
				delete itSdlObjUni->second;
				++itSdlObjUni;
			}

			//delete and next object
			delete itSdlObj->second;
			++itSdlObj;
		}
		//shader uniforms
		//iterator, sdl, uniforms
		auto itSdlUni = shaderDrawList->uniforms.begin();
		while (itSdlUni != shaderDrawList->uniforms.end()) {
			//delete and next uniform
			delete itSdlUni->second;
			++itSdlUni;
		}
		
		//deletes the RenderShaderBatch
		delete shaderDrawList;
	}
}

RenderShaderBatch* Renderer::startRenderShaderBatch(Shader * a_Shader, Camera * a_Camera) {
	endRenderShaderBatch();
	m_CurrentShaderBatch = new RenderShaderBatch;
	m_CurrentShaderBatch->shader = a_Shader;
	m_CurrentShaderBatch->camera = a_Camera;
	return m_CurrentShaderBatch;
}

RenderObject* Renderer::addObject(IDrawable * a_Drawable, BoundingObject * a_BoundingObject) {
	//assert if there is no currentShaderBatch since there is nothing to add to
	assert(m_CurrentShaderBatch != nullptr);

	//else add normally
	RenderObject* ro = new RenderObject;
	ro->drawable = a_Drawable;
	ro->bounding = a_BoundingObject;
	m_CurrentShaderBatch->objects[a_Drawable] = ro;
	m_LastRenderObject = ro;
	return ro;
}

void Renderer::setUniform(RenderUniform* a_Object, const UniformTypes a_Type, std::string a_Name,const float * a_Data) {
	//contains key
	if (a_Object->uniforms.count(a_Name) == 1) {
		ShaderUniform* su = a_Object->uniforms.at(a_Name);
		su->data = (float*) a_Data;
	} else {//does not contain key
		ShaderUniform* su = new ShaderUniform;
		su->dataType = a_Type;
		su->data = (float*) a_Data;
		su->name = a_Name;
		a_Object->uniforms[a_Name] = su;
	}
}

void Renderer::endRenderShaderBatch() {
	if (m_CurrentShaderBatch == nullptr) {
		return;
	}
	m_DrawList.push_back(m_CurrentShaderBatch);
	m_CurrentShaderBatch = nullptr;
}

void Renderer::draw() {
	endRenderShaderBatch();

	for (size_t i = 0; i < m_DrawList.size(); ++i) {
		RenderShaderBatch* shaderDrawList = m_DrawList[i];

		//shader startup
		shaderDrawList->shader->useProgram();
		shaderDrawList->shader->setUniformProjectionView(shaderDrawList->camera);

		//shader uniforms to set
		//auto itSdlUni = shaderDrawList->uniforms.begin();
		//while (itSdlUni != shaderDrawList->uniforms.end()) {
		//	//set uniforms
		//	shaderDrawList->shader->setUniform(itSdlUni->second);
		//	++itSdlUni;
		//}
		useUniforms(shaderDrawList, shaderDrawList->shader);

		//objects to draw
		auto itSdlObj = shaderDrawList->objects.begin();
		while (itSdlObj != shaderDrawList->objects.end()) {
			////object uniforms
			//auto itSdlObjUni = itSdlObj->second->uniforms.begin();
			//while (itSdlObjUni != itSdlObj->second->uniforms.end()) {
			//
			//	//set uniforms
			//	shaderDrawList->shader->setUniform(itSdlObjUni->second);
			//	++itSdlObjUni;
			//}
			useUniforms(itSdlObj->second, shaderDrawList->shader);

			//draw object
			itSdlObj->second->drawable->draw();
			++itSdlObj;
		}


		//for (int q = 0; q < shaderDrawList->objects.size(); ++q) {
		//	shaderDrawList->objects[q]->drawable->draw();
		//}
	}
}

void Renderer::useUniforms(const RenderUniform * a_UniformList,Shader* a_Shader) {
	//object uniforms
	auto itSdlObjUni = a_UniformList->uniforms.begin();
	while (itSdlObjUni != a_UniformList->uniforms.end()) {

		//set uniforms
		a_Shader->setUniform(itSdlObjUni->second);
		++itSdlObjUni;
	}
}
