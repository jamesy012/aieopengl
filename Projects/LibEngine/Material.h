#pragma once

#include <glm\vec4.hpp>

class Texture;

enum TextureSlots {
	Diffuse,
	TextureSlot0 = 0,
	Specular,
	TextureSlot1 = 1,
	Normal,
	TextureSlot2 = 2,
	TextureTypesEnd
};

class Material {
	//TODO perhaps put this in Texture so each texture can have it's own color
	struct Color {
		glm::vec4 color = glm::vec4(1);
		bool isUsing = false;
	};
public:
	Material();
	~Material();

	Material(const Material& a_Copy);
	Material& operator=(const Material& a_Copy);

	//sets this materials color, also sets the isUsing flag to true
	void setColor(const glm::vec4 a_Color);
	//sets weather this material will set the color uniform or not
	void usingColor(const bool a_UseColor);

	//sets a_Texture into the TextureSlot of a_Slot
	void setTexture(TextureSlots a_Slot, Texture* a_Texture);

	//applies the textures to the graphics card
	//in their slots
	void useTextures() const;

private:
	//list of texture types that the material can use
	//use TextureSlots to set which slot you want to use
	Texture* m_Textures[TextureSlots::TextureTypesEnd] = { nullptr };

	Color m_Color;
};

