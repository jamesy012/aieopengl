#include "Gui.h"

#include <imgui\imgui.h>
#include <../Examples/opengl3_example/imgui_impl_glfw_gl3.h>

#include <glm\ext.hpp>
#include <array>

#include "Window.h"
#include "Transform.h"

//the Transform selected by the hierarchy, used in drawInspector
static Transform* m_SelectedTransform;
bool Gui::m_ShowHierachy = true;
static glm::vec3 m_RotStart;
static bool m_IsRotating = false;

void Gui::startUp() {
	ImGui_ImplGlfwGL3_Init(Window::getWindow(), false);
}

void Gui::shutdown() {
	ImGui_ImplGlfwGL3_Shutdown();
}

void Gui::newFrame() {
	ImGui_ImplGlfwGL3_NewFrame();

	if (m_ShowHierachy) {
		drawHierarchy();
		drawInspector();
	}
}

void Gui::draw() {
	ImGui::Render();
}

void Gui::drawHierarchy() {
	Transform* root = Transform::getRootTransform();

	if (root == nullptr) {
		return;
	}

	ImGui::SetNextWindowSize(ImVec2(200, 500), ImGuiSetCond_Appearing);
	ImGui::Begin("Hierarchy");

	int drawID = 0;
	Transform* selectedTransform = m_SelectedTransform;

	drawTransformChildren(root, drawID, &selectedTransform);

	m_SelectedTransform = selectedTransform;

	//if (ImGui::TreeNode("Trees"))
	//{
	//	if (ImGui::TreeNode("Basic trees"))
	//	{
	//		for (int i = 0; i < 5; i++)
	//			if (ImGui::TreeNode((void*)(intptr_t)i, "Child %d", i))
	//			{
	//				ImGui::Text("blah blah");
	//				ImGui::SameLine();
	//				if (ImGui::SmallButton("print")) printf("Child %d pressed", i);
	//				ImGui::TreePop();
	//			}
	//		ImGui::TreePop();
	//	}
	//	ImGui::TreePop();
	//}

	ImGui::End();
}

void Gui::drawTransformChildren(Transform * a_Transform, int& a_TransformDrawID, Transform ** a_SelectedTransform) {
	//flag to turn off hierarchy
	if (!a_Transform->m_ShowInHierarchy) {
		return;
	}

	ImGuiTreeNodeFlags nodeFlags = ImGuiTreeNodeFlags_OpenOnDoubleClick;

	if (a_Transform == Transform::getRootTransform()) {
		nodeFlags |= ImGuiTreeNodeFlags_DefaultOpen;
	}

	//nodeFlags |= ((m_SelectedTransform & (1 < *a_Selected)) ? ImGuiTreeNodeFlags_Selected : 0);
	if (m_SelectedTransform == a_Transform) {
		nodeFlags |= ImGuiTreeNodeFlags_Selected;
	}

	//if no children
	if (a_Transform->getChildrenCount() == 0) {
		nodeFlags |= ImGuiTreeNodeFlags_NoTreePushOnOpen | ImGuiTreeNodeFlags_Leaf;

		ImGui::TreeNodeEx((void*)(intptr_t)a_TransformDrawID, nodeFlags, a_Transform->m_Name.c_str());

		if (ImGui::IsItemClicked()) {

			*a_SelectedTransform = a_Transform;
		}


		//if (ImGui::IsItemHovered()) {
		//	printf("HOVER!\n");
		//	if (ImGui::IsMouseDragging()) {
		//		printf("DRAG!\n");
		//	}
		//}

	} else {

		nodeFlags |= ImGuiTreeNodeFlags_OpenOnArrow;
		//weird (void*) (intptr_t) conversion taken from line 284 of imgui_demo
		bool isOpen = ImGui::TreeNodeEx((void*)(intptr_t)a_TransformDrawID, nodeFlags, a_Transform->m_Name.c_str());
		if (ImGui::IsItemClicked()) {
			*a_SelectedTransform = a_Transform;
		}

		if (isOpen) {
			for (unsigned int i = 0; i < a_Transform->getChildrenCount(); i++) {
				drawTransformChildren(a_Transform->getChild(i), ++a_TransformDrawID, a_SelectedTransform);
			}

			ImGui::TreePop();
		}
	}
}

void Gui::drawInspector() {
	if (m_SelectedTransform == nullptr) {
		return;
	}

	glm::vec3 lPosition = m_SelectedTransform->getLocalPosition();
	glm::vec3 lRotation = m_SelectedTransform->getLocalRotationEulers();
	glm::vec3 lRotationCopy = lRotation;
	glm::vec3 lScale = m_SelectedTransform->getLocalScale();
	glm::vec3 gPosition = m_SelectedTransform->getGlobalPosition();
	glm::vec3 gRotation = m_SelectedTransform->getGlobalRotationEulers();
	glm::vec3 gScale = m_SelectedTransform->getGlobalScale();
	if (!m_IsRotating) {
		m_RotStart = lRotation;
	}

	//	printf("%f,%f,%f\n", lRotation.x, lRotation.y, lRotation.z);

	ImGui::SetNextWindowSize(ImVec2(400, 500), ImGuiSetCond_Appearing);

	bool isRot = false;

	bool close = true;

	ImGui::Begin("Inspector", &close);

	ImGui::Text("Transform:");
	ImGui::Indent();
	ImGui::Text("Name:");
	ImGui::SameLine();
	//max size of transform is 50 + 1 for \0
	std::string* str = &m_SelectedTransform->m_Name;
	std::vector<char> data(str->c_str(), str->c_str() + 51);
	if (ImGui::InputText("", &data[0], 51)) {
		m_SelectedTransform->m_Name = data.data();
	}

	if (ImGui::TreeNodeEx("Local Data", ImGuiTreeNodeFlags_DefaultOpen)) {
		if (ImGui::DragFloat3("Position", glm::value_ptr(lPosition), 0.1f)) {
			m_SelectedTransform->setPosition(lPosition);
		}
		if (ImGui::DragFloat3("Rotation", glm::value_ptr(m_RotStart), 0.1f)) {

			//printf("diff: %f\n", lRotation.y - lRotationCopy.y);
			//printf("new (%f,%f,%f)\n",lRotation.x,lRotation.y,lRotation.z);
			//printf("old (%f,%f,%f)\n", lRotationCopy.x, lRotationCopy.y, lRotationCopy.z);

			m_SelectedTransform->setRotation(m_RotStart);
		}
		if (ImGui::IsItemActive()) {
			isRot = true;
			m_IsRotating = true;
		}
		//this is no longer necessary since transform now uses a vec3 for it's rotation instead of a quaternion
		//if (ImGui::DragFloat("Rot Y", &lRotation.y, 0.1f)) {
		//	m_SelectedTransform->setRotation(lRotation);
		//}
		if (ImGui::DragFloat3("Scale", glm::value_ptr(lScale), 0.1f)) {
			m_SelectedTransform->setScale(lScale);
		}
		ImGui::TreePop();
	}
	if (ImGui::TreeNode("Global Data:")) {
		ImGui::Indent();
		ImGui::Text("Position (%f, %f, %f)", gPosition.x, gPosition.y, gPosition.z);
		ImGui::Text("Rotation (%f, %f, %f)", gRotation.x, gRotation.y, gRotation.z);
		ImGui::Text("Scale (%f, %f, %f)", gScale.x, gScale.y, gScale.z);
		ImGui::TreePop();
		ImGui::Unindent();
	}
	if (ImGui::TreeNode("Transform Data:")) {
		if (ImGui::TreeNode("Local")) {
			glm::mat4* local = &m_SelectedTransform->getLocalTransform();
			ImGui::Text("[0] (%f, %f, %f, %f)", local[0][0].x, local[0][0].y, local[0][0].z, local[0][0].w);
			ImGui::Text("[1] (%f, %f, %f, %f)", local[0][1].x, local[0][1].y, local[0][1].z, local[0][1].w);
			ImGui::Text("[2] (%f, %f, %f, %f)", local[0][2].x, local[0][2].y, local[0][2].z, local[0][2].w);
			ImGui::Text("[3] (%f, %f, %f, %f)", local[0][3].x, local[0][3].y, local[0][3].z, local[0][3].w);
			ImGui::TreePop();
		}
		if (ImGui::TreeNode("Global")) {
			glm::mat4* global = &m_SelectedTransform->getGlobalTransform();
			ImGui::Text("[0] (%f, %f, %f, %f)", global[0][0].x, global[0][0].y, global[0][0].z, global[0][0].w);
			ImGui::Text("[1] (%f, %f, %f, %f)", global[0][1].x, global[0][1].y, global[0][1].z, global[0][1].w);
			ImGui::Text("[2] (%f, %f, %f, %f)", global[0][2].x, global[0][2].y, global[0][2].z, global[0][2].w);
			ImGui::Text("[3] (%f, %f, %f, %f)", global[0][3].x, global[0][3].y, global[0][3].z, global[0][3].w);
			ImGui::TreePop();
		}
		ImGui::TreePop();
	}
	if (ImGui::TreeNode("Parent Data:")) {
		ImGui::Indent();
		ImGui::Text("Unimplemented");
		ImGui::Unindent();
		ImGui::TreePop();
	}
	if (ImGui::TreeNode("Child Data:")) {
		if (m_SelectedTransform->getChildrenCount() == 0) {
			ImGui::Indent();
			ImGui::Text("No Children");
			ImGui::Unindent();
		} else {
			for (unsigned int i = 0; i < m_SelectedTransform->getChildrenCount(); i++) {
				if (ImGui::TreeNodeEx((void*)(intptr_t)i, ImGuiTreeNodeFlags_OpenOnArrow, m_SelectedTransform->getChild(i)->m_Name.c_str())) {
					ImGui::Indent();
					ImGui::Text("Unimplemented");
					ImGui::Unindent();
					ImGui::TreePop();
				}
			}
		}
		ImGui::TreePop();
	}

	ImGui::End();

	if (!isRot) {
		m_IsRotating = false;
	}


	if (!close) {
		m_SelectedTransform = nullptr;
	}
}
