#pragma once

#include <vector>

class Texture;

//a framebuffer object
//defaults to screen width and height
class FrameBuffer {
public:
	FrameBuffer();
	~FrameBuffer();

	//sets size of framebuffer in pixel size's
	void setSize(const int a_Width, const int a_Height);
	//sets size of framebuffer with a ratio of the screen size
	//floats are multiplied by screen size to get the final size
	void setSize(const float a_RatioWidth, const float a_RatioHeight);

	//updates size of framebuffer using ratio
	void updateSize();

	//creates the frameBuffer
	//a_TextureFormat = GL_RED,GL_RG,GL_RGB,GL_RBGA, GL_DEPTH_COMPONENT,GL_DEPTH_STENCIL
	void createFB(const unsigned int a_TextureFormat,const bool a_AddDepthRenderBuffer);

	//static function to use a framebuffer
	//if a_Fb is a nullptr then it will set up the default back buffer fbo
	static void use(FrameBuffer* a_Fb = nullptr);

	//reference to the Texture this Framebuffer uses
	Texture* m_Texture = nullptr;
private:
	//returns width of framebuffer in pixels
	int getWidth();
	//returns height of framebuffer in pixels
	int getHeight();

	//a flag to know if we are using a ratio or actual size for the width and height
	bool m_Ratio = true;
	//width of framebuffer
	//can be a ratio or a width
	float m_Width = 1.0f;
	//height of framebuffer
	//can be a ratio or a height
	float m_Height = 1.0f;

	//opengl framebuffer object reference
	//it's the lifeline to our fbo
	//starts at 0 which is the back buffer
	unsigned int m_Fbo = 0;
	//render buffer for depth
	unsigned int m_FboDepth = 0;

	struct FBTexture {
		Texture* Tex;
		unsigned int ColorAttachment;
	};

	std::vector<FBTexture> m_ColorAttachments;
};

