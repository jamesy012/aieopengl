#pragma once
#include "Application.h"

class Shader;
class Texture;

class TutTextures : public Application {
public:
	TutTextures();
	~TutTextures();

	void startUp();
	void shutDown();
	void update();
	void draw();

private: 
	Shader* m_Shader;
	unsigned int m_VAO, m_VBO, m_IBO;

	Texture* m_EarthTexture;
	Texture* m_EarthDayNight;
};

