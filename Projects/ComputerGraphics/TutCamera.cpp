#include "TutCamera.h"

#include <gl_core_4_4.h>
#include <GLFW\glfw3.h>
#include <glm\glm.hpp>
#include <glm/ext.hpp>

#include <aie\Gizmos.h>

#include "Grid.h"
#include "FlyCamera.h"

void TutCamera::startUp() {

	m_Grid = new Grid();
	m_Grid->loadShader();
	m_Grid->generateGrid(5, 30);

	aie::Gizmos::create(1000, 1000, 0, 0);

	//m_Camera = new FlyCamera();
	//m_Camera->setLookAt(glm::vec3(40, 20, 40), glm::vec3(15, 0, 15), glm::vec3(0, 1, 0));
	m_Camera->setLookAt(glm::vec3(0, 0, 5), glm::vec3(0), glm::vec3(0, 1, 0));
	m_Camera->setPerspective(glm::pi<float>() * 0.25f, 16 / 9.f, 0.1f, 1000.f);

}

void TutCamera::shutDown() {
	delete m_Grid;
	aie::Gizmos::destroy();
}


void TutCamera::draw() {
	m_Grid->draw(m_Camera->getProjectionView());

	aie::Gizmos::clear();

	aie::Gizmos::addTransform(glm::mat4(1));

	aie::Gizmos::addSphere(glm::vec3(1, 1, 0), 1.0f, 10, 10, glm::vec4(0.5f, 0.5f, 0.5f, 1.0f));

	aie::Gizmos::draw(m_Camera->getProjectionView());
}

void TutCamera::update() {
	m_Camera->update();
}
