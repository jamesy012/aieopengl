#pragma once
#include "Application.h"

#include <glm\glm.hpp>

//#include "Transform.h"
class Transform;
class Shader;
class Model;
class Mesh;
class Texture;
class FrameBuffer;

class TutDeferredRendering :
	public Application {
public:
	void startUp();
	void shutDown();
	void update();
	void draw();
private:
	unsigned int m_GpassFbo;
	unsigned int m_AlbedoTexture;
	unsigned int m_PositionTexture;
	unsigned int m_NormalTexture;
	unsigned int m_GpassDepth;

	unsigned int m_LightFbo;
	unsigned int m_LightTexture;

	unsigned int m_QuadVao;
	unsigned int m_QuadVbo;

	unsigned int m_PointLightVao;
	unsigned int m_PointLightVbo;
	unsigned int m_PointLightIbo;

	Shader* m_GpassShader;
	Shader* m_directionalLightShader;
	Shader* m_PointLightShader;
	Shader* m_CompositeShader;

	Model* m_Model;
	Mesh* m_Plane;
	glm::vec3 m_PlaneColor = glm::vec3(1.0f, 0.6f, 0.1f);

	Texture* m_WhiteTexture;

	void drawDirectionalLight(const glm::vec3& a_Direction, const glm::vec3& a_Diffuse);
	void drawPointLight(const glm::vec3& a_Position, float a_Radius, const glm::vec3 a_Diffuse);

	Transform* m_PointLights[10][10];
	Transform* m_PointLightHolder;

	FrameBuffer* m_Fb;
	Shader* m_NormalShader;
	Mesh* m_FbQuad;
	Mesh* m_FbQuad2;
	Camera* m_FbCamera;
};

