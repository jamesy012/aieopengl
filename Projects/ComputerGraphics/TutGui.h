#pragma once

#include "Application.h"

#include <imgui\imgui.h>

class Shader;
class Mesh;

class TutGui : public Application {
public:
	void startUp();
	void shutDown();
	void update();
	void draw();



private:
	bool show_test_window = false;
	bool show_another_window = false;
	ImVec4 clear_color = ImColor(114, 144, 154);



	Mesh* m_Model;
	Mesh* m_Plane;
	Mesh* m_LightBox;
	Shader* m_UseShadowProgram;
	Shader* m_ShadowGenProgram;

	unsigned int m_Fbo;
	unsigned int m_FboDepth;

	glm::vec3 m_LightDirection;
	glm::mat4 m_LightMatrix;
};

