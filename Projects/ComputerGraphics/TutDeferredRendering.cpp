#include "TutDeferredRendering.h"

#include <GLFW\glfw3.h>

#include <imgui\imgui.h>
#include <glm/ext.hpp>

#include "Shader.h"
#include "Model.h"
#include "Mesh.h"
#include "Texture.h"
#include "Input.h"
#include "TimeHandler.h"

#include "FlyCamera.h"
#include "FrameBuffer.h"

#include "Transform.h"

void TutDeferredRendering::startUp() {

	//GPASS FRAMEBUFFER
	glGenFramebuffers(1, &m_GpassFbo);
	glBindFramebuffer(GL_FRAMEBUFFER, m_GpassFbo);
	glGenTextures(1, &m_AlbedoTexture);
	glBindTexture(GL_TEXTURE_2D, m_AlbedoTexture);
	//glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGB8, 1280, 720);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, 1280, 720, 0, GL_RGB, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
		GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
		GL_NEAREST);
	glGenTextures(1, &m_PositionTexture);
	glBindTexture(GL_TEXTURE_2D, m_PositionTexture);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGB32F, 1280, 720);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
		GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
		GL_NEAREST);
	glGenTextures(1, &m_NormalTexture);
	glBindTexture(GL_TEXTURE_2D, m_NormalTexture);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGB32F, 1280, 720);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
		GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
		GL_NEAREST);
	glGenRenderbuffers(1, &m_GpassDepth);
	glBindRenderbuffer(GL_RENDERBUFFER, m_GpassDepth);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT,
		1280, 720);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
		m_AlbedoTexture, 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1,
		m_PositionTexture, 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2,
		m_NormalTexture, 0);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
		GL_RENDERBUFFER, m_GpassDepth);

	GLenum gpassTargets[] = { GL_COLOR_ATTACHMENT0,
		GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
	glDrawBuffers(3, gpassTargets);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// setup light framebuffer
	glGenFramebuffers(1, &m_LightFbo);
	glBindFramebuffer(GL_FRAMEBUFFER, m_LightFbo);
	glGenTextures(1, &m_LightTexture);
	glBindTexture(GL_TEXTURE_2D, m_LightTexture);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGB8, 1280, 720);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
		GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
		GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
		GL_TEXTURE_2D, m_LightTexture, 0);
	GLenum lightTargets[] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, lightTargets);
	auto status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE)
		printf("Framebuffer Error!\n");
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	//left and right quad
	m_Fb = new FrameBuffer();
	m_FbQuad = new Mesh();
	m_FbQuad2 = new Mesh();
	m_FbCamera = new FlyCamera();
	m_NormalShader = new Shader();
	m_NormalShader->setShaderFromFile("Shaders/basicVertexTex.vert", "Shaders/basicDiffuse.frag");
	m_FbCamera->setRotation(glm::vec3(-90, 0, 0));
	m_FbCamera->setPosition(glm::vec3(0, 10, 0));
	m_FbCamera->setPerspective(glm::radians(60.0f), 16 / 9.f, 0.1f, 1000.f);
	m_FbCamera->m_Name = "FrameBuffer Camera";
	m_FbQuad->createPlane(true);
	m_FbQuad->m_Transform.setScale(glm::vec3(5));
	m_FbQuad->m_Transform.setPosition(glm::vec3(-5, 0, 0));
	m_FbQuad->m_Transform.m_Name = "Left quad";
	m_FbQuad2->createPlane(true);
	m_FbQuad2->m_Transform.setScale(glm::vec3(5));
	m_FbQuad2->m_Transform.setPosition(glm::vec3(5, 0, 0));
	m_FbQuad2->m_Transform.m_Name = "Right quad";
	m_Fb->setSize(512, 512);
	m_Fb->createFB(GL_RGB, true);

	//full screen quad
	glm::vec2 halfTexel = 1.0f / glm::vec2(1280, 720) * 0.5f;

	int vertexDataWidth = 6;
	int vertexDataHeight = 6;
	float vertexData[] = {
		-1, -1, 0, 1,     halfTexel.x,     halfTexel.y,
		1,  1, 0, 1, 1 - halfTexel.x, 1 - halfTexel.y,
		-1,  1, 0, 1,     halfTexel.x, 1 - halfTexel.y,

		-1, -1, 0, 1,     halfTexel.x,     halfTexel.y,
		1, -1, 0, 1, 1 - halfTexel.x,     halfTexel.y,
		1,  1, 0, 1, 1 - halfTexel.x, 1 - halfTexel.y,
	};

	glGenVertexArrays(1, &m_QuadVao);
	glBindVertexArray(m_QuadVao);

	glGenBuffers(1, &m_QuadVbo);
	glBindBuffer(GL_ARRAY_BUFFER, m_QuadVbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * vertexDataWidth * vertexDataHeight, vertexData, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(float) * vertexDataWidth, 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * vertexDataWidth, ((char*)0) + sizeof(float) * 4);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//point light cube
	float cubeVertexData[] = {
		-1, -1, 1, 1,
		1, -1, 1, 1,
		1, -1, -1, 1,
		-1, -1, -1, 1,
		-1, 1, 1, 1,
		1, 1, 1, 1,
		1, 1, -1, 1,
		-1, 1, -1, 1,
	};
	unsigned int cubeIndexData[] = {
		0, 5, 4,
		0, 1, 5,
		1, 6, 5,
		1, 2, 6,
		2, 7, 6,
		2, 3, 7,
		3, 4, 7,
		3, 0, 4,
		4, 6, 7,
		4, 5, 6,
		3, 1, 0,
		3, 2, 1
	};

	glGenVertexArrays(1, &m_PointLightVao);
	glBindVertexArray(m_PointLightVao);


	glGenBuffers(1, &m_PointLightIbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_PointLightIbo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * 36, cubeIndexData, GL_STATIC_DRAW);

	glGenBuffers(1, &m_PointLightVbo);
	glBindBuffer(GL_ARRAY_BUFFER, m_PointLightVbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 32, cubeVertexData, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 4, 0);

	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//OTHER
	m_GpassShader = new Shader();
	m_GpassShader->setVertexShaderFromFile("Shaders/DeferredRendering/gPass.vert");
	m_GpassShader->setFragmentShaderFromFile("Shaders/DeferredRendering/gPass.frag");
	m_GpassShader->linkShader();

	m_CompositeShader = new Shader();
	m_CompositeShader->setVertexShaderFromFile("Shaders/DeferredRendering/fsQuad.vert");
	m_CompositeShader->setFragmentShaderFromFile("Shaders/DeferredRendering/fsQuad.frag");
	m_CompositeShader->linkShader();

	m_directionalLightShader = new Shader();
	m_directionalLightShader->setVertexShaderFromFile("Shaders/DeferredRendering/fsQuad.vert");
	m_directionalLightShader->setFragmentShaderFromFile("Shaders/DeferredRendering/directionalLight.frag");
	m_directionalLightShader->linkShader();

	m_PointLightShader = new Shader();
	m_PointLightShader->setVertexShaderFromFile("Shaders/DeferredRendering/pointLight.vert");
	m_PointLightShader->setFragmentShaderFromFile("Shaders/DeferredRendering/pointLight.frag");
	m_PointLightShader->linkShader();

	m_Model = new Model();
	m_Model->loadModel("Models/stanford/", "Bunny.obj");
	//m_Model->loadModel("Models/nanosuit/", "nanosuit.obj");

	m_Plane = new Mesh();
	m_Plane->createPlane();
	m_Plane->m_Transform.setScale(glm::vec3(50, 1, 50));


	m_WhiteTexture = new Texture();
	m_WhiteTexture->createWhite();

	m_Plane->setDiffuse(m_WhiteTexture);
	m_Model->m_Meshs[0]->setDiffuse(m_WhiteTexture);

	m_PointLightHolder = new Transform("Lights");

	for (int y = 0; y < 10; y += 1) {
		for (int x = 0; x < 10; x += 1) {
			m_PointLights[x][y] = new Transform("LIGHT " + std::to_string(x + (y * 10)));
			int xPos = x * 5;
			int yPos = y * 5;
			m_PointLights[x][y]->setPosition(glm::vec3(xPos - 25, 1, yPos - 25));
			m_PointLights[x][y]->setParent(m_PointLightHolder);
		}
	}
}

void TutDeferredRendering::shutDown() {
	delete m_GpassShader;
	delete m_directionalLightShader;
	delete m_PointLightShader;
	delete m_CompositeShader;
	delete m_Model;
	delete m_Plane;
	delete m_WhiteTexture;
	delete m_PointLightHolder;

	delete m_Fb;
	delete m_NormalShader;
	delete m_FbQuad;
	delete m_FbQuad2;
	delete m_FbCamera;

	for (int y = 0; y < 10; y += 1) {
		for (int x = 0; x < 10; x += 1) {
			delete m_PointLights[x][y];
		}
	}

	//TODO delete opengl buffers
}

void TutDeferredRendering::update() {
	m_Camera->update();

	ImGui::DragFloat3("Plane Color", glm::value_ptr(m_PlaneColor), 0.05f, 0.0f, 1.0f);

	if (Input::wasKeyPressed(GLFW_KEY_P)) {
		m_GpassShader->deleteShader();
		m_GpassShader->setVertexShaderFromFile("Shaders/DeferredRendering/gPass.vert");
		m_GpassShader->setFragmentShaderFromFile("Shaders/DeferredRendering/gPass.frag");
		m_GpassShader->linkShader();
	}
	if (Input::wasKeyPressed(GLFW_KEY_O)) {
		m_directionalLightShader->deleteShader();
		m_directionalLightShader->setVertexShaderFromFile("Shaders/DeferredRendering/fsQuad.vert");
		m_directionalLightShader->setFragmentShaderFromFile("Shaders/DeferredRendering/directionalLight.frag");
		m_directionalLightShader->linkShader();
	}
	if (Input::wasKeyPressed(GLFW_KEY_I)) {
		m_CompositeShader->deleteShader();
		m_CompositeShader->setVertexShaderFromFile("Shaders/DeferredRendering/fsQuad.vert");
		m_CompositeShader->setFragmentShaderFromFile("Shaders/DeferredRendering/fsQuad.frag");
		m_CompositeShader->linkShader();
	}

	//m_PointLightHolder->rotate(glm::vec3(0,sinf(TimeHandler::getCurrentTime())*5,0));
	//m_PointLightHolder->setScale(glm::vec3(cosf(TimeHandler::getCurrentTime()*2.25f),1,1));
}

void TutDeferredRendering::draw() {
	//1st pass, G-Pass: Render albedo, position and normal
	glEnable(GL_DEPTH_TEST);

	glBindFramebuffer(GL_FRAMEBUFFER, m_GpassFbo);
	glClearColor(0, 0, 0, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_GpassShader->useProgram();

	//m_GpassShader->setUniform("projectionView", m_Camera->getProjectionView());
	m_GpassShader->setUniformProjectionView(m_Camera);
	m_GpassShader->setUniform("view", m_Camera->getView());

	m_GpassShader->setUniform("Color", 1, 1, 1);

	m_Model->draw();

	m_GpassShader->setUniform("Color", m_PlaneColor);

	m_Plane->draw();

	//2nd pass, LightPass: render lights as geometry, sampling position and normals
	//disable depth and enable additive blending
	glBindFramebuffer(GL_FRAMEBUFFER, m_LightFbo);
	glClear(GL_COLOR_BUFFER_BIT);

	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);

	//direction light render

	m_directionalLightShader->useProgram();

	Texture::useTexture(m_PositionTexture, 0, "positionTexture");
	Texture::useTexture(m_NormalTexture, 1, "normalTexture");

	drawDirectionalLight(glm::vec3(-1), glm::vec3(0.0f));

	//point light render

	glCullFace(GL_FRONT);

	m_PointLightShader->useProgram();

	m_PointLightShader->setUniformProjectionView(m_Camera);

	Texture::useTexture(m_PositionTexture, 0, "positionTexture");
	Texture::useTexture(m_NormalTexture, 1, "normalTexture");

	//bind point light color
	glBindVertexArray(m_PointLightVao);

	//drawPointLight(glm::vec3(-3), 5 , glm::vec3(1));

	float t = TimeHandler::getCurrentTime();

#if false

	drawPointLight(glm::vec3(sinf(t) * 5, 3, cosf(t) * 5), 5, glm::vec3(1, 0, 0));
	drawPointLight(glm::vec3(sinf(t) * -5, 3, cosf(t) * -5), 5, glm::vec3(0, 1, 0));
	drawPointLight(glm::vec3(cosf(t / 5) * 5, 3, sinf(t / 5) * 5), 5, glm::vec3(1));
#else
	int colorSize = 7;
	glm::vec3 colors[] = {
		glm::vec3(1,0,0),
		glm::vec3(1,1,0),
		glm::vec3(1,1,1),
		glm::vec3(1,0,1),
		glm::vec3(0,1,1),
		glm::vec3(0,1,0),
		glm::vec3(0,0,1),
	};

	static int seed = 0;
	if (seed == 0 || TimeHandler::getCurrentFrameNumber() % 300) {
		seed = (int)TimeHandler::getCurrentTime();
		srand(seed);
	}

	for (int y = 0; y < 10; y += 1) {
		for (int x = 0; x < 10; x += 1) {
			drawPointLight(m_PointLights[x][y]->getGlobalPosition() * sinf(x + (y * 50)*0.25f + TimeHandler::getCurrentTime()) + sin(TimeHandler::getCurrentTime()*0.462f),
				//drawPointLight(m_PointLights[x][y]->getGlobalPosition(),
				abs(sin(TimeHandler::getCurrentFrameNumber() / 20.0f + x * y) * -cos(TimeHandler::getCurrentTime()*10.0f)*0.5f + 0.5f) * 8,
				colors[rand() % colorSize]);
		}
	}
#endif

	glCullFace(GL_BACK);

	glDisable(GL_BLEND);

	// 3rd pass, composite pass: render a quad combine albedo * light
	FrameBuffer::use(m_Fb);
	//glBindFramebuffer(GL_FRAMEBUFFER, 0);
	//glClear(GL_COLOR_BUFFER_BIT);

	m_CompositeShader->useProgram();

	Texture::useTexture(m_AlbedoTexture, 0, "albedoTexture");
	Texture::useTexture(m_LightTexture, 1, "lightTexture");

	glBindVertexArray(m_QuadVao);
	glDrawArrays(GL_TRIANGLES, 0, 6);

	//backbuffer
	//glBindFramebuffer(GL_FRAMEBUFFER, 0);
	//glClear(GL_COLOR_BUFFER_BIT);
	FrameBuffer::use();

	m_NormalShader->useProgram();
	m_NormalShader->setUniformProjectionView(m_FbCamera);

	Texture tex;


	static int texID = 0;
	unsigned int textures[] = {
		m_AlbedoTexture,m_NormalTexture,m_LightTexture,m_PositionTexture
	};
	if (TimeHandler::getCurrentFrameNumber() % 100 == 0) {
		texID = (texID + 1) % 4;
	}
	tex.m_TextureId = textures[texID];

	m_FbQuad->setDiffuse(&tex);
	m_FbQuad->draw();

	tex.m_TextureId = m_LightTexture;

	m_FbQuad2->setDiffuse(&tex);
	m_FbQuad2->draw();

	tex.m_TextureId = 0;
}

void TutDeferredRendering::drawDirectionalLight(const glm::vec3 & a_Direction, const glm::vec3 & a_Diffuse) {

	glm::vec4 viewSpaceLight = glm::vec4(glm::normalize(a_Direction), 1);

	m_directionalLightShader->setUniform("lightDirection", glm::vec3(viewSpaceLight));
	m_directionalLightShader->setUniform("lightDiffuse", a_Diffuse);

	//draw quad
	glBindVertexArray(m_QuadVao);
	glDrawArrays(GL_TRIANGLES, 0, 6);
}

void TutDeferredRendering::drawPointLight(const glm::vec3 & a_Position, float a_Radius, const glm::vec3 a_Diffuse) {

	glm::vec3 viewSpacePosition = glm::vec3(m_Camera->getView() * glm::vec4(a_Position, 1));

	m_PointLightShader->setUniform("lightPosition", a_Position);

	m_PointLightShader->setUniform("lightPositionView", viewSpacePosition);

	m_PointLightShader->setUniform("lightRadius", a_Radius);

	m_PointLightShader->setUniform("lightDiffuse", a_Diffuse);

	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);

}
