#include "TutGpuParticles.h"
#include <aie\Gizmos.h>

#include "GPUParticleEmitter.h"
#include "Texture.h"
#include "Model.h"
#include "Mesh.h"
#include "Shader.h"

void TutGpuParticles::startUp() {
	aie::Gizmos::create(1000, 1000, 0, 0);
	m_Camera->setLookAt(glm::vec3(-10, 0, 0), glm::vec3(0), glm::vec3(0, 1, 0));

	m_Particles = new GPUParticleEmitter();

	m_Particles->initalise(10000,
		0.1f,5.0f,5,20,0.2f,0.1f,
		glm::vec4(1,1,1,0),glm::vec4(1,1,1,1));


	m_Texture = new Texture();
	m_Texture->loadTexture("Textures/particle.png");
	m_Eyes = new Texture();
	m_Eyes->loadTexture("Textures/kawaiiEyes.png");

	m_Particles->m_ParticleTexture = m_Texture;

	m_Model = new Model();
	m_Model->loadModel("Models/Nanosuit/","nanosuit.obj");
	m_Model->m_Transform.setPosition(glm::vec3(0, -14.3f, 0.7f));

	m_Plane = new Mesh();
	m_Plane->createPlane();
	m_Plane->m_Transform.m_Name = "Eyes";
	m_Plane->m_Transform.setRotation(glm::vec3(90, 0, 0));
	m_Plane->m_Transform.setPosition(glm::vec3(0, 14.4f, 0.75f));
	m_Plane->m_Transform.setScale(glm::vec3(0.5f, 1.0f, 0.5f));
	m_Plane->setDiffuse(m_Eyes);
	m_Plane->m_Transform.setParent(&m_Model->m_Transform);

	m_Shader = new Shader();
	m_Shader->setShaderFromFile("Shaders/basicVertexTex.vert", "Shaders/basicDiffuse.frag");

}

void TutGpuParticles::shutDown() {
	delete m_Particles;
	delete m_Texture;
	delete m_Eyes;
	delete m_Model;
	delete m_Plane;
	delete m_Shader;
	aie::Gizmos::destroy();
}

void TutGpuParticles::update() {
	m_Camera->update();
}

void TutGpuParticles::draw() {
	//aie::Gizmos::addTransform(glm::mat4(1),5.0f);
	//aie::Gizmos::draw(m_Camera->getProjectionView());

	m_Shader->useProgram();
	m_Shader->setUniformProjectionView(m_Camera);
	
	m_Model->draw();


	glEnable(GL_BLEND);
	glDepthMask(GL_FALSE);
	m_Plane->draw();
	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);

	m_Particles->draw(*m_Camera);
}
