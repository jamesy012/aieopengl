#pragma once
#include "Application.h"

class Model;
class Shader;
class BoundingSphere;

class TutSceneManagement :
	public Application {
public:
	// Inherited via Application
	virtual void startUp() override;
	virtual void shutDown() override;
	virtual void update() override;
	virtual void draw() override;


private:
	//a_Planes is an array of 6 vec4's
	void getFrustumPlanes(const glm::mat4& a_Transform, glm::vec4* a_Planes);
	bool checkBoundingSphere(BoundingSphere* a_Sphere,Transform* a_Transform, glm::vec4* a_Planes);

	Model* m_Model;
	Shader* m_Shader;

	BoundingSphere* m_BoundingSphere;

	
};

