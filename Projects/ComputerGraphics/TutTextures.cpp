#include "TutTextures.h"

#include <gl_core_4_4.h>
#include <GLFW\glfw3.h>
#include <glm\ext.hpp>

#include <aie\Gizmos.h>

#include "Shader.h"
#include "Input.h"
#include "TimeHandler.h"
#include "Window.h"

#include "Texture.h"


TutTextures::TutTextures() {
}


TutTextures::~TutTextures() {

}

void TutTextures::startUp() {

	aie::Gizmos::create(10000, 10000, 0, 0);

	m_EarthTexture = new Texture();
	m_EarthTexture->loadTexture("Textures/earth_diffuse.jpg");

	m_EarthDayNight = new Texture();
	m_EarthDayNight->loadTexture("Textures/blackWhiteTexture2.tga");


	const char* vsSource = "#version 410\n \
 layout(location=0) in vec4 position; \
 layout(location=1) in vec2 texCoord; \
 out vec2 vTexCoord; \
 uniform mat4 projectionViewMatrix; \
 void main() { \
 vTexCoord = texCoord; \
 gl_Position= projectionViewMatrix * position;\
 }";

	const char* fsSource = "#version 410\n \
 in vec2 vTexCoord; \
 out vec4 fragColor; \
 uniform sampler2D diffuse; \
 uniform sampler2D bwTex; \
 uniform float time; \
 void main() { \
 vec2 coords = vec2(sin(vTexCoord.x+mod(time,100)-50)-0.5f,tan(vTexCoord.y+time/10));\
	vec4 col = texture(diffuse,coords); \
 float dist = distance(vec2(0.5f,0.5f),coords);\
col.g *= dist;\
col.b *= dist;\
col *= 1-texture(bwTex,vTexCoord-vec2(time,0)).a; \
 fragColor = col; \
}";
	// vec4 col = texture(diffuse,vTexCoord+vec2(time*0.1f,0)); \

	m_Shader = new Shader();
	m_Shader->setShader(vsSource, fsSource);
	m_Shader->findCommonUniformLocations();

	float vertexData[] = {
		-5, 0,  5, 1, 0, 1,
		 5, 0,  5, 1, 1, 1,
		 5, 0, -5, 1, 1, 0,
		-5, 0, -5, 1, 0, 0
	};
	unsigned int indexData[] = {
		0,1,2,
		0,2,3
	};

	glGenVertexArrays(1, &m_VAO);
	glGenBuffers(1, &m_VBO);
	glGenBuffers(1, &m_IBO);
	glBindVertexArray(m_VAO);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 6 * 4, vertexData, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * 6, indexData, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 6, 0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 6, ((char*)0) + 16);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void TutTextures::shutDown() {
	aie::Gizmos::destroy();
	delete m_Shader;
	delete m_EarthTexture;
	delete m_EarthDayNight;
}

void TutTextures::update() {
	m_Camera->update();
}

void TutTextures::draw() {

	m_Shader->useProgram();

	m_Shader->setUniformProjectionView(m_Camera->getProjectionView());
	m_Shader->setUniformTime();

	unsigned int loc = 0;

	Texture::useTexture(m_EarthTexture,0, "diffuse");
	Texture::useTexture(m_EarthDayNight,1, "bwTex");

	glBindVertexArray(m_VAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);

}
