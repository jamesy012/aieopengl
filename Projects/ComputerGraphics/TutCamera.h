#pragma once
#include "Application.h"

class Grid;

class TutCamera :
	public Application {
public:

	void startUp();
	void shutDown();
	void draw();
	void update();

private:
	Grid* m_Grid;
};

