#pragma once
#include "Application.h"

#include <GLFW\glfw3.h>

#include <glm/glm.hpp>
#include <glm/ext.hpp>


using glm::vec3;
using glm::vec4;
using glm::mat4;



class MyApplication : public Application {
public:
	MyApplication();
	~MyApplication();

	virtual void startUp();
	virtual void shutDown();
	virtual void draw();
	virtual void update();

private:
	mat4 sun;
	mat4 mercury;
	mat4 venus;
	mat4 earth;
	mat4 moon;
	mat4 mars;
	mat4 jupiter;
	mat4 saturn;
	mat4 uranus;
	mat4 neptune;
	mat4 pluto;
};

