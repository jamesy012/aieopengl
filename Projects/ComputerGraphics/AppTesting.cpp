#include "AppTesting.h"

#include "Shader.h"
#include "Transform.h"
#include "Mesh.h"
#include "Model.h"
#include "Texture.h"

void AppTesting::startUp() {
	m_Shader = new Shader();
	m_Shader->setShaderFromFile("Shaders/basicVertexTex.vert", "Shaders/colorUniform.frag");

	m_Transform = new Transform("TEST!@!");

	m_Transform->setPosition(glm::vec3(10, 10, 0));

	m_CopyTransform = new Transform(*m_Transform);

	m_Mesh = new Mesh();
	m_Mesh->createPlane();
	m_Mesh->m_Transform.setScale(glm::vec3(5, 1, 5));

	m_CopyMesh = new Mesh(*m_Mesh);
	m_CopyMesh->m_Transform.setPosition(glm::vec3(-10, 0, 0));

	m_Model = new Model();
	//m_Model->loadModel("Models/", "boxTex.obj");
	m_Model->loadModel("Models/stanford/", "bunny.obj");

	m_CopyModel = new Model(*m_Model);
	//m_CopyModel = new Model();
	//m_CopyModel->loadModel("Models/stanford/", "bunny.obj");

	m_Model->m_Transform.setPosition(glm::vec3(-10, 0, 0));

	m_WorldDiff = new Texture();
	m_WorldSpec = new Texture();
	m_WorldDiffManagerLoad = new Texture();

	m_WorldDiff->loadTexture("Textures/earth_diffuse.jpg");
	m_WorldSpec->loadTexture("Textures/earth_specular.jpg");
	//m_WorldDiffManagerLoad = m_WorldDiff;
	m_WorldDiffManagerLoad->loadTexture("Textures/earth_diffuse.jpg");
}

void AppTesting::shutDown() {
	delete m_Shader;
	delete m_Transform;
	delete m_CopyTransform;
	delete m_Mesh;
	delete m_CopyMesh;
	delete m_Model;
	delete m_CopyModel;

	delete m_WorldDiff;
	delete m_WorldSpec;
	delete m_WorldDiffManagerLoad;
}

void AppTesting::update() {
	m_Camera->update();
}

void AppTesting::draw() {
	m_Shader->useProgram();
	m_Shader->setUniformProjectionView(m_Camera);

	m_Shader->setUniform("color", 1, 0, 0, 1);
	m_Model->draw();
	m_Mesh->draw();

	m_Shader->setUniform("color", 0, 1, 0, 1);
	m_CopyModel->draw();
	m_CopyMesh->draw();
}
