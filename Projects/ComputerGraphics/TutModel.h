#pragma once
#include "Application.h"

#include "Transform.h"

class Model;
class Shader;

class TutModel : public Application{
public:
	TutModel();
	~TutModel();

	void startUp();
	void shutDown();
	void update();
	void draw();
private:
	Model* m_Model;
	Shader* m_Shader;

	Transform m_ModelTransform;
};

