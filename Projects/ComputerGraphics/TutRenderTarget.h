#pragma once
#include "Application.h"

class Mesh;
class Shader;
class Model;
class Texture;
class Transform;
class FrameBuffer;

class TutRenderTarget : public Application {
public:

	void startUp();
	void shutDown();
	void update();
	void draw();
private:
	void drawScene(Camera* a_Camera);

	//unsigned int m_Fbo;
	//unsigned int m_FboTexture;
	//unsigned int m_FboDepth;
	//
	//Texture* m_RenderBufferTexture;
	FrameBuffer* m_FrameBuffer;

	//plane
	Mesh* m_RenderMesh;
	Mesh* m_ColorBoard;

	Shader* m_DiffuseShader;
	Shader* m_ColorShader;

	Model* m_Model;

	Camera* m_RenderTargetCamera;

	Model* m_RenderTargetModel;
	Model* m_CameraModel;

	Transform* m_RenderTargetHolder;
};

