#include "TutRenderTarget.h"

#include <gl_core_4_4.h>

#include "Window.h"
#include "TimeHandler.h"

#include "Texture.h"

#include "Mesh.h"
#include "Shader.h"
#include "Model.h"
#include "Transform.h"

#include "FlyCamera.h"

#include "FrameBuffer.h"

void TutRenderTarget::startUp() {
	////make texture for framebuffer
	//glGenTextures(1, &m_FboTexture);
	//glBindTexture(GL_TEXTURE_2D, m_FboTexture);
	//
	////set up texture formats
	//glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, 512, 512);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//
	////unbind
	//glBindTexture(GL_TEXTURE_2D, 0);
	//
	////DEPTH BUFFER
	//glGenRenderbuffers(1, &m_FboDepth);
	//glBindRenderbuffer(GL_RENDERBUFFER, m_FboDepth);
	//glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, 512, 512);
	//
	////unbind
	//glBindRenderbuffer(GL_RENDERBUFFER, 0);
	//
	////make frame buffer
	//glGenFramebuffers(1, &m_Fbo);
	//glBindFramebuffer(GL_FRAMEBUFFER, m_Fbo);
	//
	////bind targets for frameBuffer
	////attach texture to frame buffer
	//glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_FboTexture, 0);
	////attach depth buffer
	//glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_FboDepth);
	//
	////??? tell the FBO how many color attachments we have assigned??
	//GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0 };
	//glDrawBuffers(1, drawBuffers);
	//
	//GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	//if (status != GL_FRAMEBUFFER_COMPLETE) {
	//	printf("Framebuffer Error\n");
	//}
	//
	////unbind frame buffer
	////0 will make it render like normal (to the back buffer)
	//glBindFramebuffer(GL_FRAMEBUFFER, 0);
	//
	////create a Texture object for the FBO tex
	//m_RenderBufferTexture = new Texture();
	//m_RenderBufferTexture->m_TextureId = m_FboTexture;

	m_FrameBuffer = new FrameBuffer();
	m_FrameBuffer->setSize(512,512);
	m_FrameBuffer->createFB(GL_RGB, true);

	//load and setup information for the render target plane
	m_RenderMesh = new Mesh();
	m_RenderMesh->createPlane(true);
	m_RenderMesh->m_Transform.m_Name = "Render target";
	m_RenderMesh->m_Transform.setScale(glm::vec3(5, 2, 10));
	m_RenderMesh->m_Transform.setPosition(glm::vec3(-20, -20, 0));
	m_RenderMesh->m_Transform.rotate(glm::vec3(90, 89, 0));

	//m_RenderMesh->setDiffuse(m_RenderBufferTexture);
	m_RenderMesh->setDiffuse(m_FrameBuffer->m_Texture);

	//load and set up the information for the color changing plane
	m_ColorBoard = new Mesh();
	m_ColorBoard->createPlane();
	m_ColorBoard->m_Transform.m_Name = "Color board";
	//m_ColorBoard->m_Transform.setParent(&m_Model->m_Transform);
	m_ColorBoard->m_Transform.setScale(glm::vec3(5, 5, 5));
	m_ColorBoard->m_Transform.setPosition(glm::vec3(0, 2.0f, -5));
	m_ColorBoard->m_Transform.setRotation(glm::vec3(90, 0, 0));

	//load shaders
	m_DiffuseShader = new Shader();
	m_DiffuseShader->setShaderFromFile("Shaders/basicVertex.vert", "Shaders/basicDiffuse.frag");
	m_ColorShader = new Shader();
	m_ColorShader->setShaderFromFile("Shaders/basicVertex.vert", "Shaders/colorUniform.frag");

	//create camera for the render target
	m_RenderTargetCamera = new FlyCamera();
	m_RenderTargetCamera->m_Name = "Render target Camera";
	m_RenderTargetCamera->setPerspective(glm::radians(60.0f), 16 / 9.0f, 0.1f, 1000.f);
	m_RenderTargetCamera->setLookAt(glm::vec3(0, 0, 8), glm::vec3(0));
	m_RenderTargetCamera->translate(glm::vec3(0, 2.5f, 0));
	//m_RenderTargetCamera->setParent(&m_Model->m_Transform);

	//load models and set up transforms for model
	m_Model = new Model();
	m_Model->loadModel("Models/hand/", "hand_00.obj");

	//copy model data so far
	m_CameraModel = new Model(*m_Model);

	//continue setting up m_Model
	m_Model->m_Transform.setScale(glm::vec3(5, 5, 5));

	//set up transform data for camera model
	m_CameraModel->m_Transform.setPosition(glm::vec3(0, -1.5f, 0));
	m_CameraModel->m_Transform.setScale(glm::vec3(3, 3, 3));
	m_CameraModel->m_Transform.setRotation(glm::vec3(0, -90, 0));

	//copy information from m_CameraModel into m_RenderTargetModel
	m_RenderTargetModel = new Model(*m_CameraModel);

	m_CameraModel->m_Transform.setParent(m_Camera);
	m_RenderTargetModel->m_Transform.setParent(m_RenderTargetCamera);

	//create a transform to hold all the render target stuff
	m_RenderTargetHolder = new Transform("Render Target Holder");
	m_RenderTargetHolder->setPosition(glm::vec3(0, -20, 0));
	m_Model->m_Transform.setParent(m_RenderTargetHolder);
	m_RenderTargetCamera->setParent(m_RenderTargetHolder);
	m_ColorBoard->m_Transform.setParent(m_RenderTargetHolder);
}

void TutRenderTarget::shutDown() {
	delete m_RenderMesh;
	delete m_DiffuseShader;
	delete m_Model;
	//delete m_RenderBufferTexture;
	delete m_RenderTargetCamera;
	delete m_ColorShader;
	delete m_ColorBoard;
	delete m_RenderTargetModel;
	delete m_CameraModel;
	delete m_RenderTargetHolder;
	delete m_FrameBuffer;
}

void TutRenderTarget::update() {
	m_Camera->update();

	float speed = 45.0f * sin(TimeHandler::getCurrentTime());
	//if ((TimeHandler::getCurrentFrameNumber()) % 120 > 60) {
	m_RenderTargetCamera->setRotation(glm::vec3(0, speed, 0));
	//} else {
	//	m_RenderTargetCamera->rotate(glm::vec3(0, -speed, 0)* TimeHandler::getDeltaTime());
	//}

}

void TutRenderTarget::draw() {

	FrameBuffer::use(m_FrameBuffer);
	//glBindFramebuffer(GL_FRAMEBUFFER, m_Fbo);
	//glViewport(0, 0, 512, 512);
	//glClearColor(0.75f, 0.75f, 0.75f, 1.0f);
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	drawScene(m_RenderTargetCamera);

	////draw camera hand, only in render target
	//Transform camPos;
	//camPos.setPosition(m_Camera->getGlobalPosition());
	//camPos.setRotation(m_Camera->getGlobalRotation());
	//camPos.rotate(glm::vec3(0, -90, 0));//model is facing the wrong way
	//camPos.setScale(glm::vec3(3, 3, 3));
	//
	//m_DiffuseShader->setModelAndNormalMatrix(camPos.getGlobalTransform());
	//
	//m_Model->draw();
	m_CameraModel->draw();
	
	//glBindFramebuffer(GL_FRAMEBUFFER, 0);
	//glViewport(0, 0, Window::getFramebufferWidth(), Window::getFramebufferHeight());
	//glClearColor(0.25f, 0.25f, 0.25f, 1.0f);
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	FrameBuffer::use();

	//draw a hand to the screen to show what the other camera sees
	drawScene(m_Camera);

	m_DiffuseShader->setModelAndNormalMatrix(m_RenderMesh->m_Transform.getGlobalTransform());
	m_RenderMesh->draw();

	//Transform camModelLoc;
	//camModelLoc.setParent(m_RenderTargetCamera);
	//camModelLoc.setRotation(glm::vec3(0, -90, 0));
	//camModelLoc.setScale(glm::vec3(0.5f, 0.5f, 0.5f));
	//
	//m_DiffuseShader->setModelAndNormalMatrix(camModelLoc.getGlobalTransform());
	//m_Model->draw();
	m_RenderTargetModel->draw();

	//m_DiffuseShader->setModelAndNormalMatrix(m_RelectedObject->getGlobalTransform());
	//m_Model->draw();


}

void TutRenderTarget::drawScene(Camera* a_Camera) {
	m_ColorShader->useProgram();

	m_ColorShader->setUniformProjectionView(a_Camera->getProjectionView());

	m_ColorShader->setUniform("color", sin(TimeHandler::getCurrentTime()*3.14f)*0.5f + 0.5f, sin(TimeHandler::getCurrentTime()*0.25f)*0.5f + 0.5f, sin(TimeHandler::getCurrentTime())*0.5f + 0.5f, 1);

	m_ColorBoard->draw();

	m_DiffuseShader->useProgram();

	m_DiffuseShader->setUniformProjectionView(a_Camera->getProjectionView());
	m_DiffuseShader->setModelAndNormalMatrix(m_Model->m_Transform.getGlobalTransform());

	m_Model->draw();
}
