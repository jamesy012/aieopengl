#include "TutShadows.h"

#include <gl_core_4_4.h>
#include <GLFW\glfw3.h>

#include <glm\ext.hpp>

#include "Model.h"
#include "Mesh.h"
#include "Shader.h"

#include "Texture.h"
#include "TimeHandler.h"
#include "Input.h"
#include "Window.h"


void TutShadows::startUp() {

	m_Model = new Model();
	m_Plane = new Mesh();
	m_Plane2 = new Mesh();
	m_LightBox = new Mesh();
	m_UseShadowProgram = new Shader();
	m_ShadowGenProgram = new Shader();

	m_Plane->createPlane();
	m_Plane->m_Transform.setScale(glm::vec3(10, 1, 10));

	m_Plane2->createPlane();
	m_Plane2->m_Transform.setScale(glm::vec3(10, 1, 10));
	m_Plane2->m_Transform.setRotation(glm::vec3(90, 0, 0));
	m_Plane2->m_Transform.setPosition(glm::vec3(0, 5, -10));

	m_LightBox->createBox();

	m_UseShadowProgram->setVertexShaderFromFile("Shaders/drawShadow.vert");
	m_UseShadowProgram->setFragmentShaderFromFile("Shaders/drawShadow.frag");
	m_UseShadowProgram->linkShader();

	m_ShadowGenProgram->setVertexShaderFromFile("Shaders/depthOnlyVert.vert");
	m_ShadowGenProgram->setFragmentShaderFromFile("Shaders/depthOnlyFrag.frag");
	m_ShadowGenProgram->linkShader();

	m_UseShadowProgram->m_ShowUniformErrorMessages = false;

	m_Model->loadModel("Models/stanford/", "Bunny.obj");



	glGenFramebuffers(1, &m_Fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, m_Fbo);

	glGenTextures(1, &m_FboDepth);
	glBindTexture(GL_TEXTURE_2D, m_FboDepth);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, 1024, 1024, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, m_FboDepth, 0);

	glDrawBuffer(GL_NONE);

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE) {
		printf("Framebuffer Error\n");
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);




}

void TutShadows::shutDown() {
	delete m_Model;
	delete m_LightBox;
	delete m_Plane;
	delete m_Plane2;
	delete m_UseShadowProgram;
	delete m_ShadowGenProgram;
}

void TutShadows::update() {
	m_Camera->update();

	if (Input::wasKeyPressed(GLFW_KEY_P)) {
		m_ShadowGenProgram->deleteShader();
		m_ShadowGenProgram->setVertexShaderFromFile("Shaders/depthOnlyVert.vert");
		m_ShadowGenProgram->setFragmentShaderFromFile("Shaders/depthOnlyFrag.frag");
		m_ShadowGenProgram->linkShader();
	}
	if (Input::wasKeyPressed(GLFW_KEY_O)) {
		m_UseShadowProgram->deleteShader();
		m_UseShadowProgram->setVertexShaderFromFile("Shaders/drawShadow.vert");
		m_UseShadowProgram->setFragmentShaderFromFile("Shaders/drawShadow.frag");
		m_UseShadowProgram->linkShader();
	}
	if (Input::isKeyDown(GLFW_KEY_LEFT)) {
		m_Model->m_Transform.rotate(glm::vec3(0, -50, 0) * TimeHandler::getDeltaTime());
	}
	if (Input::isKeyDown(GLFW_KEY_RIGHT)) {
		m_Model->m_Transform.rotate(glm::vec3(0, 50, 0) * TimeHandler::getDeltaTime());
	}


	m_LightDirection = glm::normalize(glm::vec3(1.0f, 1.5f, 1.0f));
	m_LightDirection = glm::normalize(glm::vec3(sin(TimeHandler::getCurrentTime()), 1.5f, cos(TimeHandler::getCurrentTime())));

	glm::mat4 lightProjection = glm::ortho<float>(-10, 10, -10, 15, -10, 10);
	glm::mat4 lightView = glm::lookAt(m_LightDirection, glm::vec3(0), glm::vec3(0, 1, 0));

	m_LightMatrix = lightProjection * lightView;

	m_LightBox->m_Transform.setPosition(m_LightDirection * 10);

}

void TutShadows::draw() {
	glViewport(0, 0, 1024, 1024);
	glBindFramebuffer(GL_FRAMEBUFFER, m_Fbo);
	glClear(GL_DEPTH_BUFFER_BIT);

	m_ShadowGenProgram->useProgram();
	m_ShadowGenProgram->setUniformProjectionView(m_LightMatrix);

	//DRAW ALL SHADOW CASTING GEROMERTY
	m_ShadowGenProgram->setModelAndNormalMatrix(m_Model->m_Transform.getGlobalTransform());
	m_Model->draw();
	//
	//m_ShadowGenProgram->setModelAndNormalMatrix(m_Plane->m_Transform.getGlobalTransform());
	//m_Plane->draw(false);
	//NO MORE SHADOWS PAST THIS POINT!

	glViewport(0, 0, Window::getFramebufferWidth(), Window::getFramebufferHeight());
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	m_UseShadowProgram->useProgram();
	m_UseShadowProgram->setUniformProjectionView(m_Camera);

	glm::mat4 textureSpaceOffset = glm::mat4(
		0.5f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.5f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.5f, 0.0f,
		0.5f, 0.5f, 0.5f, 1.0f
	);

	glm::mat4 lightMatrix = textureSpaceOffset * m_LightMatrix;
	m_UseShadowProgram->setUniform("lightMatrix", lightMatrix);
	m_UseShadowProgram->setUniform("lightDir", m_LightDirection);

	//Texture::useTexture(m_FboDepth, 1, "shadowMap");


	unsigned int loc = glGetUniformLocation(m_UseShadowProgram->getProgramId(), "shadowMap");
	glUniform1i(loc, 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_FboDepth);


	m_UseShadowProgram->setModelAndNormalMatrix(m_Plane->m_Transform.getGlobalTransform());
	m_Plane->draw();

	m_UseShadowProgram->setModelAndNormalMatrix(m_Plane2->m_Transform.getGlobalTransform());
	m_Plane2->draw();

	m_UseShadowProgram->setModelAndNormalMatrix(m_Model->m_Transform.getGlobalTransform());
	m_Model->draw();



	//glm::mat4 lightmat;
	//lightmat = glm::translate(m_LightDirection*20);
	//
	//m_UseShadowProgram->setModelAndNormalMatrix(lightmat);
	m_LightBox->draw();
}
