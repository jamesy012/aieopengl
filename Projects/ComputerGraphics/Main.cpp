#define CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

//better memory leak detector
//#include <vld.h>

#include "Application.h"

#define PROJECTION_TEST 1
#define APP_TEST 2

#define NO_APPLICATION

//Rendering with opengl 4
#define MY_APPLICATION 4//planets
#define TUT_RENDERING_GEOM 5//grid
//Cameras and Projection
#define TUT_CAMERA 6//movable camera
//Texturing
#define TUT_TEXTURES 7//plane with a texture
#define TUT_ADVANCED_TEXTURING 8
//___Model loading
#define TUT_MODEL 9//model loading
//Lighting
#define TUT_LIGHTING 10//model lighting
//Animation
#define TUT_ANIMATION 11//quaternion example and Morph animation
//Particle effects
#define TUT_CPU_PARTICLES 12
#define TUT_GPU_PARTICLES 13
//post-processing
#define TUT_RENDER_TARGET 14//first render target tut
#define TUT_POST_PROCESSING_EFFECTS 15//post-processing full-screen effects tut
//Shadows
#define TUT_SHADOWS 16
//Deferred Rendering
#define TUT_DEFFERRED_RENDERING 17
//Scene Management
#define TUT_SCENE_MANAGEMENT 18
//Graphical User Interfaces
#define TUT_GUI 19
//Procedural Generation
//Tessellation
//Volume Rendering

#define APP_CHOICE TUT_DEFFERRED_RENDERING

#if APP_CHOICE == PROJECTION_TEST
	#include "ProjectTestApp.h"
	typedef ProjectTestApp CHOSEN_APP;
#elif APP_CHOICE == APP_TEST
	#include "AppTesting.h"
	typedef ProjectTestApp CHOSEN_APP;
#elif APP_CHOICE == MY_APPLICATION
	#include "MyApplication.h"
	typedef MyApplication CHOSEN_APP;
#elif APP_CHOICE == TUT_RENDERING_GEOM
	#include "TutRenderingGeometry.h"
	typedef TutRenderingGeometry CHOSEN_APP;
#elif APP_CHOICE == TUT_CAMERA
	#include "TutCamera.h"
	typedef TutCamera CHOSEN_APP;
#elif APP_CHOICE == TUT_TEXTURES
	#include "TutTextures.h"
	typedef TutTextures CHOSEN_APP;
#elif APP_CHOICE == TUT_ADVANCED_TEXTURING
	#include "TutAdvancedTexturing.h"
	typedef TutAdvancedTexturing CHOSEN_APP;
#elif APP_CHOICE == TUT_MODEL
	#include "TutModel.h"
	typedef TutModel CHOSEN_APP;
#elif APP_CHOICE == TUT_LIGHTING
	#include "TutLighting.h"
	typedef TutLighting CHOSEN_APP;
#elif APP_CHOICE == TUT_ANIMATION
	#include "TutAnimation.h"
	typedef TutAnimation CHOSEN_APP;
#elif APP_CHOICE == TUT_CPU_PARTICLES
	#include "TutCpuParticles.h"
	typedef TutCpuParticles CHOSEN_APP;
#elif APP_CHOICE == TUT_GPU_PARTICLES
	#include "TutGpuParticles.h"
	typedef TutGpuParticles CHOSEN_APP;
#elif APP_CHOICE == TUT_RENDER_TARGET
	#include "TutRenderTarget.h"
	typedef TutRenderTarget CHOSEN_APP;
#elif APP_CHOICE == TUT_POST_PROCESSING_EFFECTS
	#include "TutPPEffects.h"
	typedef TutPPEffects CHOSEN_APP;
#elif APP_CHOICE == TUT_SHADOWS
	#include "TutShadows.h"
	typedef TutShadows CHOSEN_APP;
#elif APP_CHOICE == TUT_DEFFERRED_RENDERING
	#include "TutDeferredRendering.h"
	typedef TutDeferredRendering CHOSEN_APP;
#elif APP_CHOICE == TUT_SCENE_MANAGEMENT
	#include "TutSceneManagement.h"
	typedef TutSceneManagement CHOSEN_APP;
#elif APP_CHOICE == TUT_GUI
	#include "TutGui.h"
	typedef TutGui CHOSEN_APP;
#endif


int main() {
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	Application* app = new CHOSEN_APP();
	app->run();
	delete app;



	return 0;
}