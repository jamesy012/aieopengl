#include "TutPPEffects.h"

#include <gl_core_4_4.h>

#include <aie\Gizmos.h>

#include "Shader.h"
#include "Texture.h"
#include "TimeHandler.h"

void TutPPEffects::startUp() {
	glGenFramebuffers(1, &m_Fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, m_Fbo);

	//texture
	glGenTextures(1, &m_FboTexture);
	glBindTexture(GL_TEXTURE_2D, m_FboTexture);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, 1280, 720);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	//stop it from getting data from the other side of the screen
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	//depth
	glGenRenderbuffers(1, &m_FboDepth);
	glBindRenderbuffer(GL_RENDERBUFFER, m_FboDepth);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24,1280,720);


	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_FboTexture, 0);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_FboDepth);
	
	GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, drawBuffers);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glm::vec2 halfTexel = 1.0f / glm::vec2(1280, 720) * 0.5f;

	int vertexDataWidth = 6;
	int vertexDataHeight = 6;
	float vertexData[] = {
		-1, -1, 0, 1,     halfTexel.x,     halfTexel.y,
		 1,  1, 0, 1, 1 - halfTexel.x, 1 - halfTexel.y,
		-1,  1, 0, 1,     halfTexel.x, 1 - halfTexel.y,

		-1, -1, 0, 1,     halfTexel.x,     halfTexel.y,
		 1, -1, 0, 1, 1 - halfTexel.x,     halfTexel.y,
		 1,  1, 0, 1, 1 - halfTexel.x, 1 - halfTexel.y,
	};

	glGenVertexArrays(1, &m_Vao);
	glBindVertexArray(m_Vao);

	glGenBuffers(1, &m_Vbo);
	glBindBuffer(GL_ARRAY_BUFFER, m_Vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * vertexDataWidth * vertexDataHeight, vertexData, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(float) * vertexDataWidth, 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * vertexDataWidth, ((char*)0) + sizeof(float)*4);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	aie::Gizmos::create(1000, 1000, 0, 0);

	m_FrameBufferShader = new Shader();
	m_FrameBufferShader->setVertexShaderFromFile("Shaders/PostProcessing/vertex.vert");
	//m_FrameBufferShader->setFragmentShaderFromFile("Shaders/PostProcessing/simple.frag");
	//m_FrameBufferShader->setFragmentShaderFromFile("Shaders/PostProcessing/boxBlur.frag");
	m_FrameBufferShader->setFragmentShaderFromFile("Shaders/PostProcessing/distortion.frag");
	//m_FrameBufferShader->setFragmentShaderFromFile("Shaders/PostProcessing/edgeDetection.frag");
	m_FrameBufferShader->linkShader();
	m_FrameBufferShader->findCommonUniformLocations();

}

void TutPPEffects::shutDown() {
	aie::Gizmos::destroy();
	delete m_FrameBufferShader;
}

void TutPPEffects::update() {
	m_Camera->update();

	aie::Gizmos::clear();

	aie::Gizmos::addSphere(glm::vec3(0), 2, 10, 10, glm::vec4(1, 0, 0, 1));
	aie::Gizmos::addSphere(glm::vec3(0, 3, 0), 1, 10, 10, glm::vec4(0, 1, 0, 1));
	aie::Gizmos::addSphere(glm::vec3(0, 5, 0),0.5, 10, 10, glm::vec4(0, 0, 1, 1));
}

void TutPPEffects::draw() {

	glBindFramebuffer(GL_FRAMEBUFFER, m_Fbo);
	glViewport(0, 0, 1280, 720);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	aie::Gizmos::draw(m_Camera->getProjectionView());

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, 1280, 720);
	glClear(GL_DEPTH_BUFFER_BIT);

	m_FrameBufferShader->useProgram();

	m_FrameBufferShader->setUniformTime(TimeHandler::getCurrentTime()*3);

	Texture::useTexture(m_FboTexture, 0, "TexDiffuse1");

	glBindVertexArray(m_Vao);
	glDrawArrays(GL_TRIANGLES, 0, 6);

}
