#pragma once
#include "Application.h"

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

class Morph;
class Shader;
class Transform;

class TutAnimation :public Application {
public:
	TutAnimation();

	void startUp();
	void shutDown();
	void update();
	void draw();

private:
	//tut Quaternions part 1
	glm::vec3 m_Positions[4];
	glm::quat m_Rotations[4];

	//QUATERNIONS tut part 2
	struct KeyFrame {
		glm::vec3 position;
		glm::quat rotation;
	};
	KeyFrame m_HipFrames[2];
	KeyFrame m_KneeFrames[2];
	KeyFrame m_AnkleFrames[2];

	glm::mat4 m_HipBone;
	glm::mat4 m_KneeBone;
	glm::mat4 m_AnkelBone;

	//MORPHING
	Morph* m_Morph;
	Shader* m_MorphShader;
	Transform* m_MorphTransform;
	Transform* m_MorphTransformOffset;
};

