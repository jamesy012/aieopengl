#pragma once

#include "Application.h"

class Texture;
class Mesh;
class Shader;
class Material;

class TutAdvancedTexturing : public Application {

	void startUp();
	void shutDown();
	void update();
	void draw();

private:
	Texture* m_Diffuse;
	Texture* m_Normal;
	Mesh* m_Plane;
	Shader* m_Shader;
	Material* m_Material;
};