#pragma once
#include "Application.h"

class Shader;
class Transform;
class Mesh;
class Model;
class Texture;

class AppTesting :
	public Application {
public:
	void startUp();
	void shutDown();
	void update();
	void draw();
private:
	Shader* m_Shader;

	Transform* m_Transform;
	Transform* m_CopyTransform;

	Mesh* m_Mesh;
	Mesh* m_CopyMesh;

	Model* m_Model;
	Model* m_CopyModel;

	Texture* m_WorldDiff;
	Texture* m_WorldSpec;
	Texture* m_WorldDiffManagerLoad;

};

