#pragma once
#include "Application.h"

class Shader;

class TutPPEffects : public Application {
public:
	void startUp();
	void shutDown();
	void update();
	void draw();

private:
	unsigned int m_Fbo;
	unsigned int m_FboTexture;
	unsigned int m_FboDepth;

	unsigned int m_Vao;
	unsigned int m_Vbo;

	Shader* m_FrameBufferShader;
};

