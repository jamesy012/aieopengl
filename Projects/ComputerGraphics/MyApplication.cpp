#include "MyApplication.h"

#include <aie/Gizmos.h>

#include "Window.h"

using namespace aie;

MyApplication::MyApplication() {
}


MyApplication::~MyApplication() {
}

void MyApplication::startUp() {

	Gizmos::create(10000, 10000, 0, 0);

	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);
	glEnable(GL_DEPTH_TEST);

}

void MyApplication::shutDown() {
	Gizmos::destroy();
}

void MyApplication::update() {
	if (glfwGetKey(Window::getWindow(), GLFW_KEY_ESCAPE) == GLFW_PRESS) {
		m_Quit = true;
		return;
	}


	m_Camera->setLookAt(vec3(sin(glfwGetTime()*0.73f) * 15, sin(glfwGetTime()*0.537) * 10, cos(glfwGetTime() / 2) * 15), vec3(0), vec3(0, 1, 0));

	float time = (float)glfwGetTime();
	float earthDistFloat = 10.0f;

	
	vec3 earthDist(earthDistFloat, 0, 0);

	//SUN
	sun = glm::rotate(mat4(1), time, vec3(0, 5, 1));
	//MERCURY
	mercury = glm::rotate(mat4(1), time / 0.241f, vec3(0, 1, 0));
	mercury = glm::translate(mercury, earthDist*0.387f);
	//VENUS
	venus = glm::rotate(mat4(1), time / 0.615f, vec3(0, 1, 0));
	venus = glm::translate(venus, earthDist*0.723);
	//EARTH
	earth = glm::rotate(mat4(1), time, vec3(0, 1, 0));//rotate around
	earth = glm::translate(earth, earthDist);//translate
											 //	earth moon
	mat4 moonRot;//get moon rotation around earth
	moonRot = glm::rotate(mat4(1), time * 5, vec3(0, 1, 0));
	moonRot = glm::translate(moonRot, vec3(2, 0, 0));

	moon = earth * moonRot;//add earths mat4 and moonRots mat4

	earth = glm::rotate(earth, time*0.2f, vec3(0, 1, 0.2f));//rotate on axis
															//MARS
	mars = glm::rotate(mat4(1), time / 1.88f, vec3(0, 1, 0));//rotate around
	mars = glm::translate(mars, earthDist*1.524f);//translate
												  //JUPITER
	jupiter = glm::rotate(mat4(1), time / 11.9f, vec3(0, 1, 0));//rotate around
	jupiter = glm::translate(jupiter, earthDist*5.20f);//translate
													   //SATURN
	saturn = glm::rotate(mat4(1), time / 29.4f, vec3(0, 1, 0));//rotate around
	saturn = glm::translate(saturn, earthDist*9.58f);//translate
													 //URANUS
	uranus = glm::rotate(mat4(1), time / 83.7f, vec3(0, 1, 0));//rotate around
	uranus = glm::translate(uranus, earthDist*19.20f);//translate
													  //NEPTUNE
	neptune = glm::rotate(mat4(1), time / 133.7f, vec3(0, 1, 0));//rotate around
	neptune = glm::translate(neptune, earthDist*30.05f);//translate
														//PLUTO
	pluto = glm::rotate(mat4(1), time / 247.9f, vec3(0, 1, 0));//rotate around
	pluto = glm::translate(pluto, earthDist*39.48f);//translate
}

void MyApplication::draw() {
	float earthDiameter = 1.0f;
	
	Gizmos::clear();


	Gizmos::addTransform(mat4(1));

	vec4 white(1);
	vec4 black(0, 0, 0, 1);

	for (int i = 0; i < 21; ++i) {
		Gizmos::addLine(vec3(-10 + i, 0, 10),
			vec3(-10 + i, 0, -10),
			i == 10 ? white : black);

		Gizmos::addLine(vec3(10, 0, -10 + i),
			vec3(-10, 0, -10 + i),
			i == 10 ? white : black);
	}

	Gizmos::addSphere(vec3(0), 2.5f, 10, 10, vec4(1, 0, 0, 1), &sun);
	Gizmos::addSphere(vec3(0), earthDiameter * 0.3830f, 10, 10, vec4(207 / 255.0f, 208 / 255.0f, 203 / 255.0f, 1), &mercury);
	Gizmos::addSphere(vec3(0), earthDiameter * 0.9490f, 10, 10, vec4(255 / 255.0f, 255 / 255.0f, 255 / 255.0f, 1), &venus);
	Gizmos::addSphere(vec3(0), earthDiameter * 1.0000f, 10, 10, vec4(73 / 255.0f, 47 / 255.0f, 144 / 255.0f, 1), &earth);
	Gizmos::addSphere(vec3(0), earthDiameter * 0.2724f, 10, 10, vec4(255 / 255.0f, 170 / 255.0f, 100 / 255.0f, 1), &moon);
	Gizmos::addSphere(vec3(0), earthDiameter * 0.5320f, 10, 10, vec4(188 / 255.0f, 93 / 255.0f, 39 / 255.0f, 1), &mars);
	Gizmos::addSphere(vec3(0), earthDiameter * 11.210f, 10, 10, vec4(212 / 255.0f, 210 / 255.0f, 127 / 255.0f, 1), &jupiter);
	Gizmos::addSphere(vec3(0), earthDiameter * 9.4500f, 10, 10, vec4(229 / 255.0f, 227 / 255.0f, 189 / 255.0f, 1), &saturn);
	Gizmos::addSphere(vec3(0), earthDiameter * 4.0100f, 10, 10, vec4(118 / 255.0f, 197 / 255.0f, 202 / 255.0f, 1), &uranus);
	Gizmos::addSphere(vec3(0), earthDiameter * 3.8800f, 10, 10, vec4(116 / 255.0f, 162 / 255.0f, 196 / 255.0f, 1), &neptune);
	Gizmos::addSphere(vec3(0), earthDiameter * 0.1860f, 10, 10, vec4(137 / 255.0f, 150 / 255.0f, 159 / 255.0f, 1), &pluto);

	Gizmos::draw(m_Camera->getProjectionView());
}

