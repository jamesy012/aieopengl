#include "TutCpuParticles.h"

#include "ParticleEmitter.h"
#include "Shader.h"

void TutCpuParticles::startUp() {
	m_PE = new ParticleEmitter();
	m_PE->initalise(1000, 500, 0.1f,1.0f, 1, 5, 1, 0.1f, glm::vec4(1, 0, 0, 1), glm::vec4(0, 1, 0, 1));

	m_Shader = new Shader();
	m_Shader->setVertexShaderFromFile("Shaders/Particles/particle.vert");
	m_Shader->setFragmentShaderFromFile("Shaders/Particles/particle.frag");
	m_Shader->linkShader();
	m_Shader->findCommonUniformLocations();
}

void TutCpuParticles::shutDown() {
	delete m_PE;
	delete m_Shader;
}

void TutCpuParticles::update() {
	m_Camera->update();
	m_PE->update(*m_Camera);
}

void TutCpuParticles::draw() {
	m_Shader->useProgram();
	m_Shader->setUniformProjectionView(m_Camera->getProjectionView());

	m_PE->draw();
}
