#include "TutAdvancedTexturing.h"

#include "TimeHandler.h"

#include "Texture.h"
#include "Material.h"
#include "Mesh.h"
#include "Shader.h"

void TutAdvancedTexturing::startUp() {
	m_Diffuse = new Texture();
	m_Normal = new Texture();
	m_Plane = new Mesh();
	m_Shader = new Shader();

	m_Diffuse->loadTexture("Textures/four_diffuse.tga");
	m_Normal->loadTexture("Textures/four_normal.tga");

	m_Material = new Material();
	m_Material->setTexture(TextureSlots::Diffuse, m_Diffuse);
	m_Material->setTexture(TextureSlots::Normal, m_Normal);

	m_Plane->createPlane();
	m_Plane->setMaterial(m_Material);

	m_Shader->setShaderFromFile("Shaders/tangent.vert","Shaders/tangent.frag");
	m_Shader->findCommonUniformLocations();


	m_Plane->m_Transform.setScale(glm::vec3(5, 1, 5));
}

void TutAdvancedTexturing::shutDown() {
	delete m_Diffuse;
	delete m_Normal;
	delete m_Plane;
	delete m_Shader;
	delete m_Material;
}

void TutAdvancedTexturing::update() {
	m_Camera->update();
}

void TutAdvancedTexturing::draw() {
	m_Shader->useProgram();

	m_Shader->setUniformProjectionView(m_Camera->getProjectionView());
	m_Shader->setModelAndNormalMatrix(m_Plane->m_Transform.getGlobalTransform());

	glm::vec3 light(sin(TimeHandler::getCurrentTime()), 1, cos(TimeHandler::getCurrentTime()));

	//m_Normal->useTexture(1, "normal");
	m_Shader->setUniform("lightDir", light.x,light.y,light.z);

	m_Plane->draw();
}
