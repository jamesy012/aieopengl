#include "ProjectTestApp.h"

#include "Shader.h"
#include "Model.h"

ProjectTestApp::ProjectTestApp() {
}


ProjectTestApp::~ProjectTestApp() {
}

void ProjectTestApp::startUp() {
	m_Shader = new Shader();
	m_Shader->setShaderFromFile("Shaders/basicVertexTex.vert", "Shaders/colorUniform.frag");

	m_Model = new Model();
	m_Model->loadModel("Models/stanford/", "bunny.obj");

}

void ProjectTestApp::shutDown() {
	delete m_Shader;
	delete m_Model;
}

void ProjectTestApp::update() {
	m_Camera->update();
}

void ProjectTestApp::draw() {
	m_Shader->useProgram();
	m_Shader->setUniformProjectionView(m_Camera);

	m_Shader->setUniform("color", 1, 0, 0, 1);
	m_Model->draw();
}
