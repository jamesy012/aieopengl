#pragma once
#include "Application.h"

#include <glm\glm.hpp>

class Model;
class Mesh;
class Shader;
class Texture;

class TutLighting :
	public Application {
public:
	TutLighting();

	void startUp();
	void shutDown();
	void update();
	void draw();
private:
	Model* m_Model;
	Shader* m_Shader;

	Mesh* m_Box;
	Shader* m_Shader2;
	Texture* m_BoxTexture;

	Mesh* m_Plane;
	Shader* m_Shader3;
	Texture* m_PlaneDiffuse;
	Texture* m_PlaneSpecular;
	Texture* m_PlaneNormal;


	glm::vec3 m_LightColor, m_LightDirection, m_AmbientLight;
};

