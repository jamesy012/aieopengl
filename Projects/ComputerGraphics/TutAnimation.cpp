#include "TutAnimation.h"

#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/transform.hpp>

#include <stdio.h>

#include <aie\Gizmos.h>

#include "Input.h"
#include "TimeHandler.h"

#include "Morph.h"
#include "Shader.h"
#include "Transform.h"

TutAnimation::TutAnimation() {
}

void TutAnimation::startUp() {
	aie::Gizmos::create(10000, 10000, 0, 0);

	//QUATERNIONS tut part 1
	m_Positions[0] = glm::vec3(0, 0, -10);
	m_Positions[1] = glm::vec3(0, 0, 5);
	m_Positions[2] = glm::vec3(5, 0, 0);
	m_Positions[3] = glm::vec3(0, -5, 0);
	m_Rotations[0] = glm::quat(glm::vec3(0, -1, 0));
	m_Rotations[1] = glm::quat(glm::vec3(0, 1, 0));
	m_Rotations[2] = glm::quat(glm::vec3(0, 90, 0));
	m_Rotations[3] = glm::quat(glm::vec3(0, 90, 0));

	//QUATERNIONS tut part 2
	m_HipFrames[0].position = glm::vec3(0, 5, 0);
	m_HipFrames[0].rotation = glm::quat(glm::vec3(1, 0, 0));
	m_HipFrames[1].position = glm::vec3(0, 5, 0);
	m_HipFrames[1].rotation = glm::quat(glm::vec3(-1, 0, 0));

	m_KneeFrames[0].position = glm::vec3(0, -2.5, 0);
	m_KneeFrames[0].rotation = glm::quat(glm::vec3(1, 0, 0));
	m_KneeFrames[1].position = glm::vec3(0, -2.5, 0);
	m_KneeFrames[1].rotation = glm::quat(glm::vec3(-1, 0, 0));

	m_AnkleFrames[0].position = glm::vec3(0, -2.5, 0);
	m_AnkleFrames[0].rotation = glm::quat(glm::vec3(1, 0, 0));
	m_AnkleFrames[1].position = glm::vec3(0, -2.5, 0);
	m_AnkleFrames[1].rotation = glm::quat(glm::vec3(2, 0, 0));

	//MORPHING
	m_Morph = new Morph();
	m_Morph->load("Models/hand/", "hand_00.obj","hand_25.obj");

	m_MorphShader = new Shader();
	m_MorphShader->setShaderFromFile("Shaders/Morph.vert", "Shaders/basicDiffuse.frag");
	m_MorphShader->findCommonUniformLocations();

	m_MorphTransform = new Transform("Morph Transform");
	m_MorphTransformOffset = new Transform("Morph Transform offset");

	m_MorphTransformOffset->translate(glm::vec3(0, -0.5f, 0.25f));
	m_MorphTransformOffset->setParent(m_MorphTransform);

	m_MorphTransform->setScale(glm::vec3(10));
	m_MorphTransform->rotate(glm::vec3(0, 0, -90));
}

void TutAnimation::shutDown() {
	aie::Gizmos::destroy();
	delete m_Morph;
	delete m_MorphShader;
	delete m_MorphTransform;
	delete m_MorphTransformOffset;
}

void TutAnimation::update() {
	m_Camera->update();
	aie::Gizmos::clear();

	//QUATERNIONS tut part 1
	{
		int line2 = (int)TimeHandler::getCurrentTime() % 8;
		if (line2 > 4) {
			line2 = 8 - line2;
		}
		float s2 = (glm::cos(TimeHandler::getCurrentTime()*4) + 1.0f) /2.0f;
		//printf("line: %i  s: %f ... %f\n", line2, s2, TimeHandler::getCurrentTime());

		float s = glm::cos(TimeHandler::getCurrentTime()/5) * 0.5f + 0.5f;
		int line = (int)(s * 3);
		s -= (1.0f / 3) * line;
		s *= 3;

		glm::vec3 p = (1 - s) * m_Positions[line] + s * m_Positions[line + 1];

		glm::quat r = glm::slerp(m_Rotations[line], m_Rotations[line + 1], s);

		glm::mat4 m = glm::translate(p) * glm::toMat4(r);



		aie::Gizmos::addTransform(m);
		aie::Gizmos::addAABBFilled(p, glm::vec3(.5f), glm::vec4(1, 0, 0, 1), &m);
	}
	//QUATERNIONS tut part 2
	{

		float s = glm::cos(TimeHandler::getCurrentTime()) * 0.5f + 0.5f;

		glm::vec3 p = (1.0f - s) * m_HipFrames[0].position + s * m_HipFrames[1].position;
		glm::quat r = glm::slerp(m_HipFrames[0].rotation, m_HipFrames[1].rotation, s);
		m_HipBone = glm::translate(p) * glm::toMat4(r);

		p = (1.0f - s) * m_KneeFrames[0].position + s * m_KneeFrames[1].position;
		r = glm::slerp(m_KneeFrames[0].rotation, m_KneeFrames[1].rotation, s);
		m_KneeBone = m_HipBone * glm::translate(p) * glm::toMat4(r);
		m_KneeBone *= glm::scale(glm::vec3(0.2f));

		p = (1.0f - s) * m_AnkleFrames[0].position + s * m_AnkleFrames[1].position;
		r = glm::slerp(m_AnkleFrames[0].rotation, m_AnkleFrames[1].rotation, s);
		m_AnkelBone = m_KneeBone *  glm::translate(p) * glm::toMat4(r);


		glm::vec3 hipPos = glm::vec3(m_HipBone[3].x, m_HipBone[3].y, m_HipBone[3].z);
		glm::vec3 kneePos = glm::vec3(m_KneeBone[3].x, m_KneeBone[3].y, m_KneeBone[3].z);
		glm::vec3 anklePos = glm::vec3(m_AnkelBone[3].x, m_AnkelBone[3].y, m_AnkelBone[3].z);

		glm::vec3 half = glm::vec3(0.5f);
		glm::vec4 pink = glm::vec4(1, 0, 1, 1);

		aie::Gizmos::addAABBFilled(hipPos, half, pink, &m_HipBone);
		aie::Gizmos::addAABBFilled(kneePos, half, pink, &m_KneeBone);
		aie::Gizmos::addAABBFilled(anklePos, half, pink, &m_AnkelBone);
	}

	//MORPH
	m_MorphTransform->rotate(glm::vec3(30, 0, 0) * TimeHandler::getDeltaTime());

}

void TutAnimation::draw() {

	//MORPH
	m_MorphShader->useProgram();
	m_MorphShader->setUniformProjectionView(m_Camera->getProjectionView());
	m_MorphShader->setModelAndNormalMatrix(m_MorphTransformOffset->getGlobalTransform());

	m_MorphShader->setUniform("keyTime",
		cosf(TimeHandler::getCurrentTime() * 3) * 0.5f + 0.5f);

	m_Morph->draw();

	aie::Gizmos::addTransform(m_MorphTransform->getLocalTransform());
	aie::Gizmos::addTransform(m_MorphTransformOffset->getLocalTransform());

	aie::Gizmos::draw(m_Camera->getProjectionView());
}
