#pragma once
#include "Application.h"

class Grid;

class TutRenderingGeometry : public Application {
public:
	TutRenderingGeometry();
	~TutRenderingGeometry();

	void startUp();
	void shutDown();
	void draw();
	void update();

	Grid* m_Grid;
};

