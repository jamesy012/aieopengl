#include "TutGui.h"

#include <gl_core_4_4.h>
#include <GLFW\glfw3.h>

#include <imgui\imgui.h>

#include <glm\ext.hpp>

#include "Input.h"
#include "TimeHandler.h"
#include "Window.h"
#include "Transform.h"
#include "Mesh.h"
#include "Shader.h"
#include "Gui.h"

void TutGui::startUp() {

	m_Model = new Mesh();
	m_Plane = new Mesh();
	m_LightBox = new Mesh();
	m_UseShadowProgram = new Shader();
	m_ShadowGenProgram = new Shader();

	m_Plane->createPlane();
	m_Plane->m_Transform.m_Name = "Ground Plane";
	m_Plane->m_Transform.setScale(glm::vec3(10, 1, 10));

	m_LightBox->createBox();
	m_LightBox->m_Transform.m_Name = "Light Direction example";
	m_LightBox->m_Transform.setPosition(glm::vec3(-5.0f, 8.5f, 5.0f));
	m_LightBox->m_Transform.setParent(&m_Model->m_Transform);

	m_UseShadowProgram->setVertexShaderFromFile("Shaders/drawShadow.vert");
	m_UseShadowProgram->setFragmentShaderFromFile("Shaders/drawShadow.frag");
	m_UseShadowProgram->linkShader();

	m_ShadowGenProgram->setVertexShaderFromFile("Shaders/depthOnlyVert.vert");
	m_ShadowGenProgram->setFragmentShaderFromFile("Shaders/depthOnlyFrag.frag");
	m_ShadowGenProgram->linkShader();

	m_UseShadowProgram->m_ShowUniformErrorMessages = false;

	m_Model->createBox();
	m_Model->m_Transform.m_Name = "Shading Box";
	m_Model->m_Transform.setPosition(glm::vec3(0, 1, 0));

	glGenFramebuffers(1, &m_Fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, m_Fbo);

	glGenTextures(1, &m_FboDepth);
	glBindTexture(GL_TEXTURE_2D, m_FboDepth);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, 1024, 1024, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, m_FboDepth, 0);

	glDrawBuffer(GL_NONE);

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE) {
		printf("Framebuffer Error\n");
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);



}

void TutGui::shutDown() {
	delete m_Model;
	delete m_LightBox;
	delete m_Plane;
	delete m_UseShadowProgram;
	delete m_ShadowGenProgram;
}

void TutGui::update() {
	m_Camera->update();

	m_LightDirection = glm::normalize(m_LightBox->m_Transform.getGlobalPosition());
	glm::mat4 lightProjection = glm::ortho<float>(-10, 10, -10, 15, -10, 10);
	glm::mat4 lightView = glm::lookAt(m_LightDirection, glm::vec3(0), glm::vec3(0, 1, 0));

	m_LightMatrix = lightProjection * lightView;

	if (Input::isKeyDown(GLFW_KEY_LEFT)) {
		m_Model->m_Transform.rotate(glm::vec3(0, -50, 0) * TimeHandler::getDeltaTime());
	}
	if (Input::isKeyDown(GLFW_KEY_RIGHT)) {
		m_Model->m_Transform.rotate(glm::vec3(0, 50, 0) * TimeHandler::getDeltaTime());
	}

	// 1. Show a simple window
	// Tip: if we don't call ImGui::Begin()/ImGui::End() the widgets appears in a window automatically called "Debug"
	{
		static float f = 0.0f;
		ImGui::Text("Hello, world!");
		ImGui::SliderFloat("float", &f, 0.0f, 1.0f);
		ImGui::ColorEdit3("clear color", (float*) &clear_color);
		if (ImGui::Button("Test Window")) show_test_window ^= 1;
		if (ImGui::Button("Another Window")) show_another_window ^= 1;
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
	}

	// 2. Show another simple window, this time using an explicit Begin/End pair
	if (show_another_window) {
		ImGui::SetNextWindowSize(ImVec2(200, 100), ImGuiSetCond_Appearing);
		ImGui::Begin("Another Window", &show_another_window);
		ImGui::Text("Hello");
		ImGui::End();
	}

	// 3. Show the ImGui test window. Most of the sample code is in ImGui::ShowTestWindow()
	if (show_test_window) {
		ImGui::SetNextWindowPos(ImVec2(650, 20), ImGuiSetCond_Appearing);
		ImGui::ShowTestWindow(&show_test_window);
	}

	//Gui::drawHierarchy();
	//Gui::drawInspector();
	Gui::m_ShowHierachy = true;


	ImGuiIO& io = ImGui::GetIO();
	ImGui::Text("over window? %s", io.WantCaptureMouse ? "True" : "False");

}

void TutGui::draw() {
	glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);


	glViewport(0, 0, 1024, 1024);
	glBindFramebuffer(GL_FRAMEBUFFER, m_Fbo);
	glClear(GL_DEPTH_BUFFER_BIT);

	m_ShadowGenProgram->useProgram();
	m_ShadowGenProgram->setUniformProjectionView(m_LightMatrix);

	//DRAW ALL SHADOW CASTING GEROMERTY
	m_ShadowGenProgram->setModelAndNormalMatrix(m_Model->m_Transform.getGlobalTransform());
	m_Model->draw();
	//
	//m_ShadowGenProgram->setModelAndNormalMatrix(m_Plane->m_Transform.getGlobalTransform());
	//m_Plane->draw(false);
	//NO MORE SHADOWS PAST THIS POINT!

	glViewport(0, 0, Window::getFramebufferWidth(), Window::getFramebufferHeight());
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	m_UseShadowProgram->useProgram();
	m_UseShadowProgram->setUniformProjectionView(m_Camera);

	glm::mat4 textureSpaceOffset = glm::mat4(
		0.5f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.5f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.5f, 0.0f,
		0.5f, 0.5f, 0.5f, 1.0f
		);

	glm::mat4 lightMatrix = textureSpaceOffset * m_LightMatrix;
	m_UseShadowProgram->setUniform("lightMatrix", lightMatrix);
	m_UseShadowProgram->setUniform("lightDir", m_LightDirection);

	//Texture::useTexture(m_FboDepth, 1, "shadowMap");


	unsigned int loc = glGetUniformLocation(m_UseShadowProgram->getProgramId(), "shadowMap");
	glUniform1i(loc, 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_FboDepth);


	m_UseShadowProgram->setModelAndNormalMatrix(m_Plane->m_Transform.getGlobalTransform());
	m_Plane->draw();

	m_UseShadowProgram->setModelAndNormalMatrix(m_Model->m_Transform.getGlobalTransform());
	m_Model->draw();



	//glm::mat4 lightmat;
	//lightmat = glm::translate(m_LightDirection * 20);

	m_UseShadowProgram->setModelAndNormalMatrix(m_LightBox->m_Transform.getGlobalTransform());
	m_LightBox->draw();

}