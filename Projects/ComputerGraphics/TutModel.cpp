#include "TutModel.h"

#include <GLFW\glfw3.h>

#include "Input.h"
#include "TimeHandler.h"

#include "Model.h"
#include "Shader.h"

TutModel::TutModel() {
}


TutModel::~TutModel() {
}

void TutModel::startUp() {

	m_Shader = new Shader();
	//m_Shader->setShaderFromFile("Shaders/basicVertexTex.vert","Shaders/basicDiffuse.frag");
	m_Shader->setShaderFromFile("Shaders/basicVertexTex.vert", "Shaders/depthFragment.frag");
	m_Shader->findCommonUniformLocations();

	m_Model = new Model();
	//m_Model->loadModel("Models/","boxTex.obj");
	//m_Model->loadModel("Models/stanford/","bunny.obj");
	//m_Model->loadModel("Models/stanford/","dragon.obj");
	m_Model->loadModel("Models/Nanosuit/","nanosuit.obj");
	//m_Model->loadModel("Models/truck/", "truck.obj");

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	m_Model->m_Transform.setParent(&m_ModelTransform);
	m_Model->m_Transform.setRotation(glm::vec3(0, -90, 0));
	m_ModelTransform.m_Name = "Model Transform";
}

void TutModel::shutDown() {
	delete m_Model;
	delete m_Shader;
}

void TutModel::update() {
	m_Camera->update();

	//glm::vec3 forward = glm::vec3(0, 0, -1) * 10.0f;
	glm::vec3 forward = glm::vec3(-1, 0, 0) * 10.0f;
	glm::vec3 rotateSpeed = glm::vec3(0, 60, 0);

	if (Input::isKeyDown(GLFW_KEY_UP)) {
		m_ModelTransform.translate(forward*TimeHandler::getDeltaTime(),false);
	}
	if (Input::isKeyDown(GLFW_KEY_DOWN)) {
		m_ModelTransform.translate(-forward*TimeHandler::getDeltaTime(),false);
	}
	if (Input::isKeyDown(GLFW_KEY_LEFT)) {
		m_ModelTransform.rotate(rotateSpeed * TimeHandler::getDeltaTime());
	}
	if (Input::isKeyDown(GLFW_KEY_RIGHT)) {
		m_ModelTransform.rotate(-rotateSpeed * TimeHandler::getDeltaTime());
	}

}

void TutModel::draw() {
	m_Shader->useProgram();
	m_Shader->setUniformProjectionView(m_Camera->getProjectionView());
	m_Shader->setModelAndNormalMatrix(m_ModelTransform.getGlobalTransform());
	unsigned int loc = glGetUniformLocation(m_Shader->getProgramId(), "color");
	glUniform4f(loc, 1.0f,0.0f,0.0f, 1.0f);
	m_Model->draw();
}
