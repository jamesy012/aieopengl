#pragma once
#include "Application.h"

class ParticleEmitter;
class Shader;

class TutCpuParticles :
	public Application {
public:
	void startUp();
	void shutDown();
	void update();
	void draw();

private:
	ParticleEmitter* m_PE;
	Shader* m_Shader;
};

