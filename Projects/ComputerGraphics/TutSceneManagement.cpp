#include "TutSceneManagement.h"

#include <aie\Gizmos.h>

#include "Model.h"
#include "Mesh.h"
#include "Shader.h"
#include "BoundingSphere.h"
#include "Transform.h"
#include "TimeHandler.h"

void TutSceneManagement::startUp() {
	aie::Gizmos::create(1000, 1000, 0, 0);

	m_Shader = new Shader();
	m_Shader->setShaderFromFile("Shaders/basicVertexTex.vert", "Shaders/basicDiffuse.frag");


	m_Model = new Model();
	//m_Model->loadModel("Models/","boxTex.obj");
	//m_Model->loadModel("Models/stanford/","bunny.obj");
	//m_Model->loadModel("Models/stanford/", "dragon.obj");
	m_Model->loadModel("Models/Nanosuit/", "nanosuit.obj");
	//m_Model->loadModel("Models/truck/", "truck.obj");
	//m_Model->loadModel("Models/hand/", "hand_25.obj");

	m_BoundingSphere = new BoundingSphere();
	m_Model->fit(m_BoundingSphere);


}

void TutSceneManagement::shutDown() {
	aie::Gizmos::destroy();
	delete m_Model;
	delete m_Shader;
	delete m_BoundingSphere;
}

void TutSceneManagement::update() {
	m_Camera->update();
}

void TutSceneManagement::draw() {
	glm::vec4 frustumPlanes[6];
	getFrustumPlanes(m_Camera->getProjectionView(), frustumPlanes);

	aie::Gizmos::clear();

	m_Shader->useProgram();
	m_Shader->setUniformProjectionView(m_Camera->getProjectionView());

	if (checkBoundingSphere(m_BoundingSphere, &m_Model->m_Transform , frustumPlanes)) {
		//printf("%i\n", TimeHandler::getCurrentFrameNumber());
		aie::Gizmos::addSphere(m_BoundingSphere->getCenter() + m_Model->m_Transform.getGlobalPosition(), m_BoundingSphere->getRadius(), 8, 8, glm::vec4(1, 0, 1, 1));
		m_Model->draw();
	}

	aie::Gizmos::draw(m_Camera->getProjectionView());
}

void TutSceneManagement::getFrustumPlanes(const glm::mat4 & a_Transform, glm::vec4 * a_Planes) {
	a_Planes[0] = glm::vec4(
		a_Transform[0][3] - a_Transform[0][0],
		a_Transform[1][3] - a_Transform[1][0],
		a_Transform[2][3] - a_Transform[2][0],
		a_Transform[3][3] - a_Transform[3][0]
	);
	a_Planes[1] = glm::vec4(
		a_Transform[0][3] + a_Transform[0][0],
		a_Transform[1][3] + a_Transform[1][0],
		a_Transform[2][3] + a_Transform[2][0],
		a_Transform[3][3] + a_Transform[3][0]
	);
	a_Planes[2] = glm::vec4(
		a_Transform[0][3] - a_Transform[0][1],
		a_Transform[1][3] - a_Transform[1][1],
		a_Transform[2][3] - a_Transform[2][1],
		a_Transform[3][3] - a_Transform[3][1]
	);
	a_Planes[3] = glm::vec4(
		a_Transform[0][3] + a_Transform[0][1],
		a_Transform[1][3] + a_Transform[1][1],
		a_Transform[2][3] + a_Transform[2][1],
		a_Transform[3][3] + a_Transform[3][1]
	);
	a_Planes[4] = glm::vec4(
		a_Transform[0][3] - a_Transform[0][2],
		a_Transform[1][3] - a_Transform[1][2],
		a_Transform[2][3] - a_Transform[2][2],
		a_Transform[3][3] - a_Transform[3][2]
	);
	a_Planes[5] = glm::vec4(
		a_Transform[0][3] + a_Transform[0][2],
		a_Transform[1][3] + a_Transform[1][2],
		a_Transform[2][3] + a_Transform[2][2],
		a_Transform[3][3] + a_Transform[3][2]
	);

	//normalize planes
	for (int i = 0; i < 6; i++) {
		float d = glm::length(glm::vec3(a_Planes[i]));
		a_Planes[i] /= d;
	}
}

bool TutSceneManagement::checkBoundingSphere(BoundingSphere * a_Sphere, Transform* a_Transform, glm::vec4 * a_Planes) {
	glm::vec3 pos = a_Transform->getGlobalPosition();
	glm::vec3 scale = a_Transform->getGlobalScale();
	glm::vec3 center = (pos + a_Sphere->getCenter());
	float radius = a_Sphere->getRadius();

	//float maxScale = scale.x;
	//if (scale.y > maxScale) {
	//	maxScale = scale.y;
	//}
	//if (scale.z > maxScale) {
	//	maxScale = scale.z;
	//}
	//radius *= abs(maxScale);
	//
	//radius = glm::distance(a_Sphere->getMin() * (abs(maxScale/2)), center);
	
	for (int i = 0; i < 6; i++) {
		float d = glm::dot(glm::vec3(a_Planes[i]), center) + a_Planes[i].w;

		if (d < -radius) {
			//behind
			return false;
		}
		else if (d < radius) {
			//touching plane, still render
			//return true;
		} else {
			//in front of plane, render object
			//return true;
		}
	}

	return true;
}
