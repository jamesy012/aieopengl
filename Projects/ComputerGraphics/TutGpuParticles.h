#pragma once
#include "Application.h"

class GPUParticleEmitter;
class Texture;
class Model;
class Mesh;
class Shader;

class TutGpuParticles :
	public Application {
public:
	void startUp();
	void shutDown();
	void update();
	void draw();
private:

	Model* m_Model;
	Shader* m_Shader;
	
	GPUParticleEmitter* m_Particles;
	Texture* m_Texture;

	Texture* m_Eyes;
	Mesh* m_Plane;
};

