#pragma once
#include "Application.h"

#include <glm\glm.hpp>

class Model;
class Mesh;
class Shader;

class TutShadows :
	public Application {
public:
	void startUp();
	void shutDown();
	void update();
	void draw();

private:
	Model* m_Model;
	Mesh* m_Plane;
	Mesh* m_Plane2;
	Mesh* m_LightBox;
	Shader* m_UseShadowProgram;
	Shader* m_ShadowGenProgram;

	unsigned int m_Fbo;
	unsigned int m_FboDepth;

	glm::vec3 m_LightDirection;
	glm::mat4 m_LightMatrix;
};

