#pragma once

#include "Application.h"

class Shader;
class Model;

class ProjectTestApp : public Application {
public:
	ProjectTestApp();
	~ProjectTestApp();

	// Inherited via Application
	virtual void startUp() override;
	virtual void shutDown() override;
	virtual void update() override;
	virtual void draw() override;
private:
	Shader* m_Shader;

	Model* m_Model;
};

