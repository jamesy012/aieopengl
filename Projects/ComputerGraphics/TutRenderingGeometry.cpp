#include "TutRenderingGeometry.h"

#include <gl_core_4_4.h>
#include <GLFW\glfw3.h>
#include <glm\glm.hpp>
#include <glm/ext.hpp>

#include "Grid.h"

TutRenderingGeometry::TutRenderingGeometry() {
}


TutRenderingGeometry::~TutRenderingGeometry() {
}

void TutRenderingGeometry::startUp() {

	m_Grid = new Grid();
	m_Grid->loadShader();
	m_Grid->generateGrid(30, 30);
}

void TutRenderingGeometry::shutDown() {
	delete m_Grid;
}

void TutRenderingGeometry::draw() {
	m_Grid->draw(m_Camera->getProjectionView());
}

void TutRenderingGeometry::update() {
	m_Camera->setLookAt(glm::vec3(
		sin(glfwGetTime()*0.73f) * 20, sin(glfwGetTime()*0.537) * 10 + 20, cos(glfwGetTime() / 2) * 20),
		glm::vec3(15, 0, 15),
		glm::vec3(0, 1, 0));
	//m_Grid->generateGrid(30+rand()%10 -5, 30 + rand() % 10 - 5);
	m_Camera->setPerspective(glm::pi<float>() * float((sin(glfwGetTime())+1.25f)*0.2f), 16 / 9.f, 0.1f, 1000.f);
}
