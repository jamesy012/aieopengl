#include "TutLighting.h"

#include <GLFW\glfw3.h>

#include <aie\Gizmos.h>

#include "Input.h"
#include "TimeHandler.h"

#include "Model.h"
#include "Mesh.h"
#include "Shader.h"
#include "Texture.h"
#include "Gui.h"

TutLighting::TutLighting() {
}


void TutLighting::startUp() {
	Gui::m_ShowHierachy = true;
	m_HandleEscapeQuit = false;

	m_Shader = new Shader();
	//m_Shader->setShaderFromFile("Shaders/basicVertexTex.vert","Shaders/basicDiffuse.frag");
	//m_Shader->setShaderFromFile("Shaders/basicVertexTex.vert", "Shaders/depthFragment.frag");
	m_Shader->setShaderFromFile("Shaders/basicVertexTex.vert", "Shaders/basicLighting.frag");
	m_Shader->findCommonUniformLocations();
	m_Shader->m_ShowUniformErrorMessages = false;



	m_Model = new Model();
	//m_Model->loadModel("Models/","boxTex.obj");
	//m_Model->loadModel("Models/stanford/","bunny.obj");
	//m_Model->loadModel("Models/stanford/", "dragon.obj");
	m_Model->loadModel("Models/Nanosuit/","nanosuit.obj");
	//m_Model->loadModel("Models/truck/", "truck.obj");
	//m_Model->loadModel("Models/hand/", "hand_25.obj");

	m_LightColor = glm::vec3(1, 1, 1);
	m_LightDirection = glm::vec3(0, 0, -1);
	m_AmbientLight = glm::vec3(0.25f, 0.25f, 0.25f);

	//m_LightDirection = glm::vec3(0, 1, 0.0f);

	m_Shader2 = new Shader();
	//m_Shader2->setShaderFromFile("Shaders/basicVertexTex.vert", "Shaders/ColorFragment.frag");
	m_Shader2->setShaderFromFile("Shaders/basicVertexTex.vert", "Shaders/basicDiffuse.frag");
	m_Shader2->findCommonUniformLocations();

	m_BoxTexture = new Texture();
	m_BoxTexture->loadTexture("Models/checkerMap.png");

	m_Box = new Mesh();
	m_Box->createBox();
	//m_Box->m_Transform.setParent(&m_Model->m_Transform);
	m_Box->m_Transform.translate(glm::vec3(0, 0, 5));
	m_Box->setDiffuse(m_BoxTexture);

	m_Shader3 = new Shader();
	m_Shader3->setShaderFromFile("Shaders/tangent.vert", "Shaders/tangent.frag");
	m_Shader3->findCommonUniformLocations();

	m_PlaneDiffuse = new Texture();
	m_PlaneNormal = new Texture();
	m_PlaneSpecular = new Texture();
	m_PlaneDiffuse->loadTexture("Textures/earth_diffuse.jpg");
	m_PlaneNormal->loadTexture("Textures/earth_normal.jpg");
	m_PlaneSpecular->loadTexture("Textures/earth_specular.jpg");

	m_Plane = new Mesh();
	m_Plane->createPlane();
	m_Plane->setDiffuse(m_PlaneDiffuse);
	m_Plane->setSpecular(m_PlaneSpecular);
	m_Plane->setNormal(m_PlaneNormal);
	m_Plane->m_Transform.setScale(glm::vec3(150, 1, 100));


	//m_Camera->setParent(&m_Model->m_Transform);
	m_Camera->setLookAt(glm::vec3(0, 0, -5), glm::vec3(0));

	aie::Gizmos::create(1000,1000,1000,1000);
}

void TutLighting::shutDown() {
	aie::Gizmos::destroy();
	delete m_Model;
	delete m_Shader;
	
	delete m_Box;
	delete m_Shader2;
	delete m_BoxTexture;

	delete m_Plane;
	delete m_Shader3;
	delete m_PlaneDiffuse;
	delete m_PlaneSpecular;
	delete m_PlaneNormal;
}

void TutLighting::update() {

	m_Camera->update();

	glm::vec3 forward = glm::vec3(0, 0, 1) * 20.0f * TimeHandler::getDeltaTime();
	//glm::vec3 forward = glm::vec3(-1, 0, 0) * 10.0f;
	glm::vec3 rotateSpeed = glm::vec3(0, 90, 0) * TimeHandler::getDeltaTime();

	if (Input::isKeyDown(GLFW_KEY_UP)) {
		m_Model->m_Transform.translate(forward, false);
	}
	if (Input::isKeyDown(GLFW_KEY_DOWN)) {
		m_Model->m_Transform.translate(-forward, false);
	}
	if (Input::isKeyDown(GLFW_KEY_LEFT)) {
		m_Model->m_Transform.rotate(rotateSpeed);
	}
	if (Input::isKeyDown(GLFW_KEY_RIGHT)) {
		m_Model->m_Transform.rotate(-rotateSpeed);
	}

	//m_LightDirection.x = sinf(TimeHandler::getCurrentTime());
	//m_LightDirection.z = cosf(TimeHandler::getCurrentTime());

	//m_Model->m_Transform.setScale(glm::vec3(1, 0.6+sinf(TimeHandler::getCurrentTime())/2, 1));
	//m_Model->m_Transform.rotate(glm::vec3(1 * TimeHandler::getDeltaTime(), 0, 0));
		
	if (Input::wasKeyPressed(GLFW_KEY_R)) {
		m_Shader->deleteShader();
		m_Shader->setShaderFromFile("Shaders/basicVertexTex.vert", "Shaders/basicDiffuse.frag");
		m_Shader->findCommonUniformLocations();
		printf("Shader reloaded\n");
	}
	if (Input::wasKeyPressed(GLFW_KEY_T)) {
		m_Shader->deleteShader();
		m_Shader->setShaderFromFile("Shaders/basicVertexTex.vert", "Shaders/depthFragment.frag");
		m_Shader->findCommonUniformLocations();
		printf("Shader reloaded\n");
	}
	if (Input::wasKeyPressed(GLFW_KEY_Y)) {
		m_Shader->deleteShader();
		m_Shader->setShaderFromFile("Shaders/basicVertexTex.vert", "Shaders/basicLighting.frag");
		m_Shader->findCommonUniformLocations();
		printf("Shader reloaded\n");
	}


	if (Input::wasKeyPressed(GLFW_KEY_P)) {
		m_Box->m_Transform.removeParent();
	}
	if (Input::wasKeyPressed(GLFW_KEY_O)) {
		m_Box->m_Transform.setParent(&m_Model->m_Transform);
	}

	if (Input::wasKeyPressed(GLFW_KEY_L)) {
		m_Camera->removeParent();
	}
	if (Input::wasKeyPressed(GLFW_KEY_K)) {
		m_Camera->setParent(&m_Model->m_Transform);
	}
	if (Input::wasKeyPressed(GLFW_KEY_ENTER)) {
		glm::vec3 euler = m_Camera->getGlobalRotationEulers();
		euler = m_Camera->getGlobalUnitVector();
		euler = glm::degrees(euler);
		printf("CAMERA Euler (%f,%f,%f)\n", euler.x, euler.y, euler.z);
	}
	
	glm::vec3 up = glm::vec3(0, 1, 0) * 50.0f * TimeHandler::getDeltaTime();
	glm::vec3 right = glm::vec3(1, 0, 0) * 50.0f * TimeHandler::getDeltaTime();
	forward = glm::vec3(0, 0, -1) * 50.0f * TimeHandler::getDeltaTime();

	if (Input::isKeyDown(GLFW_KEY_KP_8)) {
		m_Box->m_Transform.translate(forward, true);
	}
	if (Input::isKeyDown(GLFW_KEY_KP_5)) {
		m_Box->m_Transform.translate(-forward, true);
	}
	if (Input::isKeyDown(GLFW_KEY_KP_6)) {
		m_Box->m_Transform.translate(right, true);
	}
	if (Input::isKeyDown(GLFW_KEY_KP_4)) {
		m_Box->m_Transform.translate(-right, true);
	}
	if (Input::isKeyDown(GLFW_KEY_KP_9)) {
		m_Box->m_Transform.translate(up, true);
	}
	if (Input::isKeyDown(GLFW_KEY_KP_7)) {
		m_Box->m_Transform.translate(-up, true);
	}

	//float time = TimeHandler::getCurrentTime();
	//m_Box->m_Transform.setPosition(glm::vec3(sin(time*1.131f) * 130,
	//	5.0f+(sin(TimeHandler::getCurrentTime())*0.5f + 0.5f) * 40,
	//	5 + cos(time*0.931f)*80));

	//m_Model->m_Transform.updateTransform();
	
}

void TutLighting::draw() {

	aie::Gizmos::clear();

	m_Shader->useProgram();
	m_Shader->setUniformProjectionView(m_Camera->getProjectionView());
	m_Shader->setModelAndNormalMatrix(m_Model->m_Transform.getGlobalTransform());

	//m_Shader->setUniform("lightPos", m_LightDirection);
	m_Shader->setUniform("lightPos", m_Box->m_Transform.getGlobalPosition());
	m_Shader->setUniform("camPos", m_Camera->getGlobalPosition());
	//m_Shader->setUniform("roughness", 0.2f);

	m_Model->draw();

	m_Shader->setModelAndNormalMatrix(m_Plane->m_Transform.getGlobalTransform());

	m_Plane->draw();

	m_Shader2->useProgram();
	m_Shader2->setUniformProjectionView(m_Camera->getProjectionView());
	m_Shader2->setModelAndNormalMatrix(m_Box->m_Transform.getGlobalTransform());
	
	m_Box->draw();


	//m_Shader3->useProgram();
	//m_Shader3->setUniformProjectionView(m_Camera->getProjectionView());
	//m_Shader3->setModelAndNormalMatrix(m_Plane->m_Transform.getGlobalTransform());
	//m_Shader3->setUniform("lightDir", m_Box->m_Transform.getGlobalPosition()-m_Plane->m_Transform.getGlobalPosition());
	//m_Plane->draw();
	
	aie::Gizmos::addLine(glm::vec3(0), m_Box->m_Transform.getGlobalPosition(), glm::vec4(1, 1, 1, 1));
	aie::Gizmos::addLine(glm::vec3(0), m_Camera->getGlobalUnitVector(), glm::vec4(1, 0, 0, 1));
	//aie::Gizmos::addLine(glm::vec3(0), m_Camera->getGlobalPosition() + glm::vec3(0,-1,0),glm::vec4(1,1,1,1));
	aie::Gizmos::draw(m_Camera->getProjectionView());

	//printf("(%f,%f,%f)\n", m_Camera->getPosition().x, m_Camera->getPosition().y, m_Camera->getPosition().z);

}
