#include "PhysicsTestApp.h"

#include <time.h>

#include <aie\Gizmos.h>
#include <glm\glm.hpp>
#include <imgui\imgui.h>
#include <glm\ext.hpp>

#include "Shader.h"
#include "TimeHandler.h"

#include "PhysicsScene.h"
#include "Sphere.h"
#include "Box.h"


void PhysicsTestApp::startUp() {
	srand(time(NULL));

	aie::Gizmos::create(1000, 1000, 1000, 1000);

	m_Shader = new Shader();
	m_Shader->createBasicProgram(false);

	m_Pta = new PhysicsScene();
	m_Pta->setGravity(glm::vec3(0, -5, 0));
	m_Pta->setFriction(0.00005f);

	glm::vec3 cubeArea = glm::vec3(40, 40, 40);


	m_Sphere = new Sphere(glm::vec3(0), 1.0f, 1.5f, glm::vec4(0.25f, 0.25f, 0.75f, 1.0f));
	m_Sphere->m_Frozen = true;
	m_Sphere->setPosition(glm::vec3(0, -2.5f, 0));

	m_Sphere2 = new Sphere(glm::vec3(0), 1, 1, glm::vec4(0.25f, 0.75f, 0.25f, 1.0f));
	m_Sphere2->getTransform()->m_Name = "Sphere 2";
	m_Sphere2->setPosition(glm::vec3(0, 5, 0));

	m_Sphere3 = new Sphere(glm::vec3(0), 1.0f, 0.5f, glm::vec4(0.25f, 0.75f, 0.75f, 1.0f));
	m_Sphere3->getTransform()->m_Name = "Sphere 3";
	m_Sphere3->setPosition(glm::vec3(0, 10, 0));

	m_Sphere4 = new Sphere(glm::vec3(0), 1.0f, 2.0f, glm::vec4(0.55f, 0.55f, 0.75f, 1.0f));
	m_Sphere4->getTransform()->m_Name = "Sphere 4";
	m_Sphere4->setPosition(glm::vec3(0, 14, 0));

	m_Box3 = new Box(glm::vec3(0), 1.0f, 1.0f, 1.0f, 1.0f, glm::vec4(0.55f, 0.0f, 0.25f, 1.0f));
	m_Box3->setPosition(glm::vec3(0, 20, 0));
	m_Box3->getTransform()->m_Name = "moving box";

	m_Box = new Box(glm::vec3(0), 1.0f, cubeArea.x, 1.0f, cubeArea.z, glm::vec4(0.25f, 0.0f, 0.25f, 1.0f));
	m_Box->getTransform()->m_Name = "wall floor";
	m_Box->m_Frozen = true;
	m_Box->setPosition(glm::vec3(0, 0, 0));

	m_Box2 = new Box(glm::vec3(0), 1.0f,  cubeArea.x, 1.0f, cubeArea.z,  glm::vec4(0.55f, 0.0f, 0.25f, 1.0f));
	m_Box2->m_Frozen = true;
	m_Box2->setPosition(glm::vec3(-cubeArea.x, cubeArea.z, 0));
	m_Box2->getTransform()->setRotation(glm::vec3(0, 0, 90));
	m_Box2->getTransform()->m_Name = "wall left";


	m_Box4 = new Box(glm::vec3(0), 1.0f, cubeArea.x, 1.0f, cubeArea.z, glm::vec4(0.55f, 0.0f, 0.25f, 1.0f));
	m_Box4->m_Frozen = true;
	m_Box4->setPosition(glm::vec3(cubeArea.z, cubeArea.y, 0));
	m_Box4->getTransform()->setRotation(glm::vec3(0, 0, -90));
	m_Box4->getTransform()->m_Name = "wall right";

	m_Box5 = new Box(glm::vec3(0), 1.0f, cubeArea.x, 1.0f, cubeArea.z, glm::vec4(0.25f, 0.0f, 0.55f, 1.0f));
	m_Box5->m_Frozen = true;
	m_Box5->setPosition(glm::vec3(0, cubeArea.y, cubeArea.z));
	m_Box5->getTransform()->setRotation(glm::vec3(90, 0, 0));
	m_Box5->getTransform()->m_Name = "wall back";

	m_Box6 = new Box(glm::vec3(0), 1.0f, cubeArea.x, 1.0f, cubeArea.z, glm::vec4(0.25f, 0.0f, 0.55f, 1.0f));
	m_Box6->m_Frozen = true;
	m_Box6->setPosition(glm::vec3(0, cubeArea.y, -cubeArea.z));
	m_Box6->getTransform()->setRotation(glm::vec3(-90, 0, 0));
	m_Box6->getTransform()->m_Name = "wall forward";


	//m_Sphere2->setMass(3);
	//m_Sphere2->setVelocity(glm::vec3(0, 4, 0));
	//m_Sphere3->setMass(5);
	//m_Sphere3->setVelocity(glm::vec3(0, -6, 0));
	//m_Sphere2->m_Frozen = true;

	m_Sphere2->setVelocity(glm::vec3(0, -2, 0));
	m_Sphere3->setVelocity(glm::vec3(0, -2, 0));
	m_Sphere4->setVelocity(glm::vec3(0, 5, 0));

	m_Pta->addActor(m_Sphere);
	m_Pta->addActor(m_Sphere2);
	m_Pta->addActor(m_Sphere3);
	m_Pta->addActor(m_Sphere4);
	m_Pta->addActor(m_Box);
	m_Pta->addActor(m_Box2);
	m_Pta->addActor(m_Box3);
	m_Pta->addActor(m_Box4);
	m_Pta->addActor(m_Box5);
	m_Pta->addActor(m_Box6);

	for (int i = 0; i < m_NumBalls; i++) {
		float radi = (rand() % 1000)/1000.0f;
		radi *= 5.0f;
		Sphere* sp = new Sphere(glm::sphericalRand(1.0f),1.0f,radi,glm::vec4(glm::sphericalRand(1.0f),1.0f));
		glm::vec3 pos;
		pos.x = (((rand() % 1000) / 1000.0f) - 0.5) * cubeArea.x * 2;
		pos.y = (((rand() % 1000) / 1000.0f) * (cubeArea.y/4.0f)) + radi;
		pos.z = (((rand() % 1000) / 1000.0f) - 0.5) * cubeArea.z * 2;
		sp->setPosition(pos);
		m_Spheres[i] = sp;
		m_Pta->addActor(sp);
	}
}

void PhysicsTestApp::shutDown() {
	delete m_Shader;
	delete m_Pta;
	delete m_Sphere;
	delete m_Sphere2;
	delete m_Sphere3;
	delete m_Sphere4;

	delete m_Box;
	delete m_Box2;
	delete m_Box3;
	delete m_Box4;
	delete m_Box5;
	delete m_Box6;

	for (int i = 0; i < m_NumBalls; i++) {
		delete m_Spheres[i];
	}

	aie::Gizmos::destroy();
}

void PhysicsTestApp::update() {
	aie::Gizmos::clear();
	ImGui::Begin("PHYSICS",nullptr, ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings);
	m_Camera->update();

	static bool updatePhysics = false;


	ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

	ImGui::Checkbox("Update Physics", &updatePhysics);
	float timeScale = m_Pta->getTimeScale();
	if (ImGui::SliderFloat("Time Scale", &timeScale, 0, 10)) {
		m_Pta->setTimeScale(timeScale);
	}
	//todo show all rigidbody's attached to the physics scene

	if (updatePhysics) {
		m_Pta->update();
	}

	ImGui::End();


}

void PhysicsTestApp::draw() {
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	m_Shader->useProgram();
	m_Shader->setUniformProjectionView(m_Camera);

	m_Pta->updateGizmos();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	aie::Gizmos::draw(m_Camera->getProjectionView());
}
