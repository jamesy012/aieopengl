#pragma once
#include "Application.h"

class Shader;
class PhysicsScene;

class Sphere;
class Box;

class PhysicsTestApp : public Application {
public:
	// Inherited via Application
	virtual void startUp() override;
	virtual void shutDown() override;
	virtual void update() override;
	virtual void draw() override;
private:
	PhysicsScene* m_Pta;
	Shader* m_Shader;
	
	Sphere* m_Sphere;
	Sphere* m_Sphere2;
	Sphere* m_Sphere3;
	Sphere* m_Sphere4;

	Box* m_Box;
	Box* m_Box2;
	Box* m_Box3;
	Box* m_Box4;
	Box* m_Box5;
	Box* m_Box6;

	static const unsigned int m_NumBalls = 100;
	Sphere* m_Spheres[m_NumBalls];
};

