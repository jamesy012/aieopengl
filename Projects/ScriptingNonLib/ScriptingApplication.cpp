#include "ScriptingApplication.h"

#include <glm\glm.hpp>
#include <GLFW\glfw3.h>

#include "Window.h"
#include "Shader.h"
#include "Input.h"

#include "Character.h"
#include "ScriptedCharacterController.h"

void ScriptingApplication::startUp() {
	screenSizeChanged();

	m_Character = new Character();
	m_Character->setSpeechColour(glm::vec4(0, 0, 0, 1));
	m_Character->setPosition(glm::vec2(Window::getWidth() * 0.5f, Window::getHeight() * 0.35f));

	m_Shader = new Shader();
	m_Shader->createBasicProgram(true);

	m_Controller = new ScriptedCharacterController(*m_Character);
	m_Controller->loadScript("Scripts/story.script");

	m_Camera->setLookAt(glm::vec3(0, 0, 1), glm::vec3(0));
}

void ScriptingApplication::shutDown() {
	delete m_Shader;
	delete m_Character;
	delete m_Controller;
}

void ScriptingApplication::update() {

	m_Controller->update();
	m_Character->update();

	if (Input::wasKeyPressed(GLFW_KEY_R)) {
		m_Controller->reloadScript();
	}
}

void ScriptingApplication::draw() {
	m_Shader->useProgram();
	m_Shader->setUniformProjectionView(m_Camera);
	
	m_Character->render();
}

//screensize overwrite
void ScriptingApplication::screenSizeChanged() {
	m_Camera->setOthrographic(0, (const float)Window::getWidth(), 0, (const float)Window::getHeight());
}
