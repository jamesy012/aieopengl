#pragma once
#include <string>
#include <vector>
#include <functional>
#include <unordered_map>

class Character;

class ScriptedCharacterController {
public:
	ScriptedCharacterController() = delete;
	ScriptedCharacterController(Character& a_ControlledCharacter);
	~ScriptedCharacterController() = default;

	void loadScript(std::string a_Script);
	void reloadScript();

	void update();

private:
	std::vector<std::string> splitString(std::string a_Str);
	void setupCommands();

	Character* m_ControlledCharacter;
	bool m_IsScriptLoaded;
	std::string m_ScriptPath;
	unsigned int m_CommandLocation;

	bool m_IsWaiting;
	float m_RemainingWaitTime;

	using CommandData = std::vector<std::string>;
	using CommandList = std::vector<CommandData>;
	CommandList m_CommandsToExectute;

	//command following
	using ScriptFunction = std::function<void(const CommandData&)>;
	std::unordered_map<std::string, ScriptFunction> m_AllowedCommands;

	//COMMANDS
	void onSetSprite(const CommandData& a_Data);
	void onSetState(const CommandData& a_Data);
	void onWait(const CommandData& a_Data);
	void onSpeak(const CommandData& a_Data);

	void onDebug(const CommandData& a_Data);

};

