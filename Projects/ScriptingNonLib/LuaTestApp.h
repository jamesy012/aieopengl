#pragma once
#include "Application.h"

#include <vector>
#include <map>
#include <string>

#include "PacketHandling.h"

class Shader;
class LuaAgentTest;
class Mesh;
class Texture;

class NetworkedCars;

class Server;
class Client;
class Networking;

class LuaTestAppClientPacketHandler;
class LuaTestAppServerPacketHandler;

//client pong server stuct
struct PotentialServers {
	std::string Ip;
	std::string text;
};

class LuaTestApp : public Application {
	friend LuaTestAppClientPacketHandler;
	friend LuaTestAppServerPacketHandler;
public:

	// Inherited via Application
	virtual void startUp() override;
	virtual void shutDown() override;
	virtual void update() override;
	virtual void draw() override;

	void sendCarToServer();

protected:
	std::vector<NetworkedCars*> m_NetworkCars;
	//number of cars
	unsigned int m_CarsCreated = 1;
	//IP, PROTENTIALSERVERS
	std::map<const char*, PotentialServers> m_LanServers;

	float m_UpdateTime = 0;

private:
	//RENDERING STUFF
	Shader* m_Shader;
	LuaAgentTest* m_LuaAgent;
	Mesh* m_MeshTest;
	Texture* m_TrackTexture;

	//NETWORKING STUFF
	std::vector<std::string> m_LocalIps;

	bool m_SinglePlayer = true;
	bool m_IsClient = true;
	bool m_Connected = false;

	Client* m_Client = nullptr;
	Server* m_Server = nullptr;
	Networking* m_Networker = nullptr;

	//CLIENT STUFF
	unsigned int m_ClientCarId = 1;
	LuaTestAppClientPacketHandler* m_ClientPacketHander;
	char m_ClientIpText[50] = { '\0' };


	//SERVER STUFF
	LuaTestAppServerPacketHandler* m_ServerPacketHander;

};

class LuaTestAppClientPacketHandler : public PacketHandling {
public:
	LuaTestApp* m_App;
	// Inherited via PacketHandling
	virtual void packetReceived(FUNCTIONPARAMATERS) override;
	virtual void pongReceived(RakNet::Packet * a_Packet, uint32_t a_Time, std::string a_Text) override;
};



class LuaTestAppServerPacketHandler : public PacketHandling {
public:
	LuaTestApp* m_App;
	// Inherited via PacketHandling
	virtual void packetReceived(FUNCTIONPARAMATERS) override;
	virtual void pongReceived(RakNet::Packet * a_Packet, uint32_t a_Time, std::string a_Text) override;
};

