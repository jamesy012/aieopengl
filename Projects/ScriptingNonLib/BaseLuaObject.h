#pragma once

#include <lua.hpp>

class BaseLuaObject {
public:
	BaseLuaObject();
	~BaseLuaObject();

	bool loadScript(const char* a_Path);
	bool reloadScript();

	void registerLuaFunction(const char* a_FunctionName, lua_CFunction a_Function);

	bool callFunction(int a_ArgCount, int a_ReturnCount);

	static void setPointerVar(lua_State* a_State, const char* a_VaraibleName, void* a_Val);

	static void* getPointerVar(lua_State* a_State, const char* a_VariableName);

	void pushFloat(float a_Value);
	float popFloat();

	void pushBool(bool a_Value);

	void pushInt(int a_Value);
	int popInt();

	const char* popString();

	void pushFunction(const char* a_Function);

protected:
	virtual void scriptLoaded() = 0;

private:
	lua_State* m_LuaState;
	const char* m_FilePath;
};

