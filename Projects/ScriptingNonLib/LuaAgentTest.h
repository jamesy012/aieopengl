#pragma once
#include "BaseLuaObject.h"

#include <glm\glm.hpp>

class Mesh;
class Texture;

class LuaAgentTest : public BaseLuaObject {
public:
	LuaAgentTest();
	~LuaAgentTest();

	void update();
	void draw();

	const glm::vec2 getPostion();

	void applyForce(const glm::vec2& a_Force);
	void applyForceWorld(const glm::vec2& a_Force);
	void applyRotation(const float a_Force);

	float getRotation();

	void updateKeysDown();
	void updateScreenSize();

	static int luaGetPosition(lua_State* a_State);
	static int luaGetMousePosition(lua_State* a_State);
	static int luaGetRotation(lua_State* a_State);
	static int luaGetLocalForce(lua_State* a_State);

	static int luaApplyForce(lua_State* a_State);
	static int luaApplyRotation(lua_State* a_State);
	static int luaApplyForceWorld(lua_State* a_State);

	static int luaIsKeyDown(lua_State* a_State);
	static int luaWasKeyPressed(lua_State* a_State);

	Mesh* m_Mesh;

	glm::vec2 m_Force = glm::vec2(0);
	glm::vec2 m_ForceWorld = glm::vec2(0);
	float m_RotationForce =0;

protected:
	// Inherited via BaseLuaObject
	virtual void scriptLoaded() override;
private:
	Texture* m_MeshTexture;
};

