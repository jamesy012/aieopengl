#include "NetworkedCars.h"

#include <GetTime.h>

#include "Texture.h"
#include "Mesh.h"

#include "Window.h"

#include "CarLuaNetworkingCommon.h"

NetworkedCars::NetworkedCars() {
	m_Texture = new Texture();
	m_Texture->loadTexture("Textures/car.png");

	m_Mesh = new Mesh();
	//m_Mesh->createPlane(true);
	//m_Mesh->setColor(glm::vec4(0, 1, 0, 1));
	//m_Mesh->setDiffuse(m_Texture);

	//m_Mesh->m_Transform.setRotation(glm::vec3(90, 0, 0));
	float size = 2.0f;
	m_Mesh->m_Transform.setScale(glm::vec3(size * 9, 1, size * 16));
	m_Mesh->m_Transform.setPosition(glm::vec3(Window::getWidth() * 0.5f, 0, Window::getHeight() * 0.35f));
}


NetworkedCars::~NetworkedCars() {
	//delete m_Texture;
	m_Texture->release();
	delete m_Mesh;
}

void NetworkedCars::lerp() {

	float amount = RakNet::GetTimeMS() - m_LastTimeReceived;
	if (amount > CAR_SEND_DELAY) {
		//return;//no point updating??
		amount = CAR_SEND_DELAY;//<- could do this also
	}
	amount /= CAR_SEND_DELAY;

	glm::vec3 pos, rot;
	rot = m_Mesh->m_Transform.getGlobalRotationEulers();

	glm::vec3 difPos = m_DesiredPos - m_FromPos;
	difPos *= amount;
	m_Mesh->m_Transform.setPosition(m_FromPos + difPos);

	//doesn't work!
	//glm::quat difRot = glm::slerp(glm::quat(m_FromRot), glm::quat(m_DesiredRot), amount);
	//m_Mesh->m_Transform.setRotation(difRot);

	float difRot = m_DesiredRot.y - m_FromRot.y;
	difRot *= amount;

	//z if flipped
	if (abs(m_DesiredRot.z) > 10) {
		if (abs(rot.z) > 10) {
			m_Mesh->m_Transform.setRotation(m_FromRot + glm::vec3(0, difRot, 0));
		} else {
			m_Mesh->m_Transform.setRotation(m_FromRot + glm::vec3(180, difRot, 180));
		}
	} else {
		if (abs(rot.z) > 10) {
			m_Mesh->m_Transform.setRotation(m_FromRot + glm::vec3(180, difRot, 180));
		} else {
			m_Mesh->m_Transform.setRotation(m_FromRot + glm::vec3(0, difRot, 0));
		}
	}

	//if the z/x flipped to or from 180
	//we update the fromRot
	if (abs(m_DesiredRot.z - rot.z) > 10) {
		updateFrom();
	}
}

void NetworkedCars::draw() {
	if (!m_CreatedPlane) {
		m_CreatedPlane = true;
		m_Mesh->createPlane(true);
		m_Mesh->setColor(glm::vec4(0, 1, 0, 1));
		m_Mesh->setDiffuse(m_Texture);
	}
	m_Mesh->draw();
}

void NetworkedCars::updateFrom() {
	m_FromPos = m_Mesh->m_Transform.getGlobalPosition();
	m_FromRot = m_Mesh->m_Transform.getGlobalRotationEulers();

	m_LastTimeReceived = (float)RakNet::GetTimeMS();
}
