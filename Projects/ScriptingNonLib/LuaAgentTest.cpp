#include "LuaAgentTest.h"

#include <GLFW\glfw3.h>

#include "Mesh.h"
#include "Texture.h"

#include "Window.h"
#include "TimeHandler.h"
#include "Input.h"

LuaAgentTest::LuaAgentTest() {
	m_MeshTexture = new Texture();
	m_MeshTexture->loadTexture("Textures/car.png");

	m_Mesh = new Mesh();
	m_Mesh->createPlane(true);
	m_Mesh->setColor(glm::vec4(1, 0, 0, 1));
	m_Mesh->setDiffuse(m_MeshTexture);

	//m_Mesh->m_Transform.setRotation(glm::vec3(90, 0, 0));
	float size = 2.0f;
	m_Mesh->m_Transform.setScale(glm::vec3(size * 9, 1, size * 16));
	m_Mesh->m_Transform.setPosition(glm::vec3(Window::getWidth() * 0.5f, 0, Window::getHeight() * 0.35f));


	registerLuaFunction("GetPostion", luaGetPosition);
	registerLuaFunction("GetMousePostion", luaGetMousePosition);
	registerLuaFunction("GetRotation", luaGetRotation);
	registerLuaFunction("GetLocalForce", luaGetLocalForce);

	registerLuaFunction("ApplyForce", luaApplyForce);
	registerLuaFunction("ApplyRotation", luaApplyRotation);
	registerLuaFunction("ApplyForceWorld", luaApplyForceWorld);

	registerLuaFunction("IsKeyDown", luaIsKeyDown);
	registerLuaFunction("WasKeyPressed", luaWasKeyPressed);
}


LuaAgentTest::~LuaAgentTest() {
	delete m_Mesh;
	//delete m_MeshTexture;
	m_MeshTexture->release();
}

void LuaAgentTest::update() {

	m_Mesh->m_Transform.translate(glm::vec3(m_Force.x, 0, m_Force.y), false);
	m_Mesh->m_Transform.translate(glm::vec3(m_ForceWorld.x, 0, m_ForceWorld.y), true);
	m_Mesh->m_Transform.rotate(glm::vec3(0, m_RotationForce, 0));

	m_Force *= 0.95f;
	m_ForceWorld *= 0.95f;
	m_RotationForce *= 0.95f;

	pushFunction("Update");
	pushFloat(TimeHandler::getDeltaTime());
	callFunction(1, 0);

	//updateKeysDown();
}

void LuaAgentTest::draw() {
	m_Mesh->draw();
}

const glm::vec2 LuaAgentTest::getPostion() {
	// TODO: insert return statement here
	return glm::vec2(m_Mesh->m_Transform.getGlobalPosition());
}

void LuaAgentTest::applyForce(const glm::vec2 & a_Force) {
	m_Force += a_Force;
}

void LuaAgentTest::applyForceWorld(const glm::vec2 & a_Force) {
	m_ForceWorld += a_Force;
}

void LuaAgentTest::applyRotation(const float a_Force) {
	m_RotationForce += a_Force;
}

float LuaAgentTest::getRotation() {
	return m_Mesh->m_Transform.getGlobalRotationEulers().y;
}

void LuaAgentTest::updateKeysDown() {
	pushFunction("KeysDown");
	pushBool(Input::isKeyDown(GLFW_KEY_UP) || Input::isKeyDown(GLFW_KEY_W));
	pushBool(Input::isKeyDown(GLFW_KEY_RIGHT) || Input::isKeyDown(GLFW_KEY_D));
	pushBool(Input::isKeyDown(GLFW_KEY_DOWN) || Input::isKeyDown(GLFW_KEY_S));
	pushBool(Input::isKeyDown(GLFW_KEY_LEFT) || Input::isKeyDown(GLFW_KEY_A));
	callFunction(4, 0);
}

void LuaAgentTest::updateScreenSize() {
	pushFunction("UpdateScreenSize");
	pushInt(Window::getStartingWidth());
	pushInt(Window::getStartingHeight());
	callFunction(2, 0);
}

int LuaAgentTest::luaGetPosition(lua_State * a_State) {

	LuaAgentTest* agent = (LuaAgentTest*)getPointerVar(a_State, "self");

	agent->pushFloat(agent->m_Mesh->m_Transform.getGlobalPosition().x);
	agent->pushFloat(agent->m_Mesh->m_Transform.getGlobalPosition().z);

	return 2;
}

int LuaAgentTest::luaGetMousePosition(lua_State * a_State) {
	LuaAgentTest* agent = (LuaAgentTest*)getPointerVar(a_State, "self");

	agent->pushFloat(Input::getMouseX());
	agent->pushFloat(Input::getMouseY());

	return 2;
}

int LuaAgentTest::luaGetRotation(lua_State * a_State) {
	LuaAgentTest* agent = (LuaAgentTest*)getPointerVar(a_State, "self");

	agent->pushFloat(agent->getRotation());

	return 1;
}

int LuaAgentTest::luaGetLocalForce(lua_State * a_State) {
	LuaAgentTest* agent = (LuaAgentTest*)getPointerVar(a_State, "self");

	agent->pushFloat(agent->m_Force.x);
	agent->pushFloat(agent->m_Force.y);

	return 2;
}

int LuaAgentTest::luaApplyForce(lua_State * a_State) {
	LuaAgentTest* agent = (LuaAgentTest*)getPointerVar(a_State, "self");
	glm::vec2 force;
	force.x = agent->popFloat();
	force.y = agent->popFloat();
	agent->applyForce(force);
	return 0;
}

int LuaAgentTest::luaApplyRotation(lua_State * a_State) {
	LuaAgentTest* agent = (LuaAgentTest*)getPointerVar(a_State, "self");
	float rotation = agent->popFloat();
	agent->applyRotation(rotation);
	return 0;
}

int LuaAgentTest::luaApplyForceWorld(lua_State * a_State) {
	LuaAgentTest* agent = (LuaAgentTest*)getPointerVar(a_State, "self");
	glm::vec2 force;
	force.x = agent->popFloat();
	force.y = agent->popFloat();
	agent->applyForceWorld(force);
	return 0;
}

int LuaAgentTest::luaIsKeyDown(lua_State * a_State) {
	LuaAgentTest* agent = (LuaAgentTest*)getPointerVar(a_State, "self");

	int keyID;
	keyID = agent->popInt();

	agent->pushBool(Input::isKeyDown(keyID));

	return 1;
}

int LuaAgentTest::luaWasKeyPressed(lua_State * a_State) {
	LuaAgentTest* agent = (LuaAgentTest*)getPointerVar(a_State, "self");

	int keyID;
	keyID = agent->popInt();

	agent->pushBool(Input::wasKeyPressed(keyID));

	return 1;
}

void LuaAgentTest::scriptLoaded() {
	updateScreenSize();
}
