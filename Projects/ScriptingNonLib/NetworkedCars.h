#pragma once

#include "Transform.h"

#include <RakNetTypes.h>

class Mesh;
class Texture;

class NetworkedCars {
public:
	NetworkedCars();
	~NetworkedCars();

	void lerp();

	void draw();

	void updateFrom();


	float m_LastTimeReceived = 0;

	glm::vec3 m_DesiredPos, m_FromPos;
	glm::vec3 m_DesiredRot, m_FromRot;

	unsigned int m_Id;

	RakNet::RakNetGUID m_Guid;

	bool m_CreatedPlane = false;

	Mesh* m_Mesh = nullptr;
	Texture* m_Texture;
};