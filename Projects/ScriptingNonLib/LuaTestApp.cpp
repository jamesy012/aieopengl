#include "LuaTestApp.h"

#include <iostream>
#include <sstream>

#include <GLFW\glfw3.h>
#include <imgui\imgui.h>

#include <RakNetTypes.h>//PACKET
#include <RakPeerInterface.h>
#include <MessageIdentifiers.h>
#include <BitStream.h>
#include <GetTime.h>

#include "LuaAgentTest.h"
#include "Shader.h"
#include "Mesh.h"
#include "Texture.h"
#include "NetworkedCars.h"

#include "Input.h"
#include "Window.h"
#include "Gui.h"

#include "Server.h"
#include "Client.h"
#include "Networking.h"
#include "CarLuaNetworkingCommon.h"

void LuaTestApp::startUp() {
	Gui::m_ShowHierachy = false;

	m_Shader = new Shader();
	m_Shader->createBasicProgram(true);

	m_LuaAgent = new LuaAgentTest();
	if (m_LuaAgent->loadScript("scripts/rotate.lua") == false) {
		delete m_LuaAgent;
		m_LuaAgent = nullptr;
	}

	m_TrackTexture = new Texture();
	//m_TrackTexture->loadTexture("Textures/earth_diffuse.jpg");
	m_TrackTexture->loadTexture("Textures/track.png");
	//m_TrackTexture->createWhite();

	m_MeshTest = new Mesh();
	m_MeshTest->createPlane(true);
	m_MeshTest->m_Transform.setScale(glm::vec3(Window::getWidth()*0.5f, 0, Window::getHeight()*0.5f));
	m_MeshTest->m_Transform.setPosition(glm::vec3(Window::getWidth() * 0.5f, 0, Window::getHeight()*0.5f));
	m_MeshTest->setColor(glm::vec4(glm::vec3(0.5f), 1));
	m_MeshTest->setDiffuse(m_TrackTexture);

	//update camera
	m_Camera->setLookAt(glm::vec3(0, 1, 0), glm::vec3(0, 1, -1));
	m_Camera->rotate(glm::vec3(90, 0, 0));
	m_PerspetiveOrthographic = true;
	screenSizeChanged();//update camera Perspetive


	m_HandleMouseLock = false;
}

void LuaTestApp::shutDown() {
	delete m_Shader;
	if (m_LuaAgent != nullptr) {
		delete m_LuaAgent;
	}
	delete m_MeshTest;
	delete m_TrackTexture;

	for (size_t i = 0; i < m_NetworkCars.size(); i++) {
		delete m_NetworkCars[i];
	}

	if (m_Client != nullptr) {
		delete m_ClientPacketHander;
		delete m_Client;
	}
	if (m_Server != nullptr) {
		delete m_ServerPacketHander;
		delete m_Server;
	}
}

void LuaTestApp::update() {
	m_LuaAgent->update();

	if (Input::wasKeyPressed(GLFW_KEY_R)) {
		m_LuaAgent->reloadScript();
	}


	if (m_SinglePlayer) {
		ImGui::Begin("Connect", 0, ImGuiWindowFlags_AlwaysAutoResize);

		if (ImGui::Button("Server")) {
			m_ClientCarId = 0;
			m_Server = new Server();
			m_Server->startup(2, SERVERPORT);
			m_IsClient = false;

			m_ServerPacketHander = new LuaTestAppServerPacketHandler();
			m_ServerPacketHander->m_App = this;
			m_Server->startNetworkThread(m_ServerPacketHander);

			m_SinglePlayer = false;
			m_Networker = m_Server;
			m_Connected = true;
		}
		if (ImGui::Button("Client")) {
			m_Client = new Client();
			m_Client->startup();
			m_Client->pingServerOnPort(SERVERPORT);
			//ping my computer
			m_Client->pingServerOnPort("10.17.38.225", SERVERPORT);

			m_ClientPacketHander = new LuaTestAppClientPacketHandler();
			m_ClientPacketHander->m_App = this;
			m_Client->startNetworkThread(m_ClientPacketHander);

			m_SinglePlayer = false;
			m_Networker = m_Client;
		}

		//server or client was clicked
		if (m_Networker != nullptr) {
			const char* ip;
			int index = 0;
			while (true) {
				ip = m_Networker->getPeerInterafce()->GetLocalIP(index);
				if (ip[0] > '9' || ip[0] < '0') {//if not a number, break
					break;
				}
				//std::cout << "\t" << index << "\t" << ip << std::endl;
				m_LocalIps.push_back("IP #" + std::to_string(index) + ": " + ip);
				index++;
			}
		}

		ImGui::End();
	} else {
		if (m_Connected) {

			for (size_t i = 0; i < m_NetworkCars.size(); i++) {
				m_NetworkCars[i]->lerp();
			}

			if (m_UpdateTime + CAR_SEND_DELAY < RakNet::GetTimeMS()) {
				m_UpdateTime = (float)RakNet::GetTimeMS();
				sendCarToServer();
				//std::cout << "Car Sent!!" <<std::endl;
			}

			if (!m_IsClient) {//server
				ImGui::Begin("IP List", 0, ImGuiWindowFlags_AlwaysAutoResize);

				for (size_t i = 0; i < m_LocalIps.size(); i++) {
					ImGui::Text(m_LocalIps[i].c_str());
				}

				ImGui::End();
			}
		} else {//if not conected
			if (m_IsClient) {//client
				//LAN SERVER LISTING
				{
					ImGui::Begin("SERVER SELECT", 0, ImGuiWindowFlags_AlwaysAutoResize);
					int serverCounter = 0;
					for (auto const &server : m_LanServers) {
						serverCounter++;

						ImGui::Text(server.second.Ip.c_str());
						ImGui::SameLine();
						ImGui::Text(server.second.text.c_str());
						ImGui::SameLine();
						std::string buttonName = "Join server:" + std::to_string(serverCounter);
						if (ImGui::Button(buttonName.c_str())) {
							//joins the server
							m_Connected = m_Client->connect(server.second.Ip.c_str(), SERVERPORT);
						}
					}

					if (serverCounter == 0) {
						ImGui::Text("No servers found!");
					}

					ImGui::NewLine();

					if (ImGui::Button("Refresh")) {
						m_LanServers.clear();
						m_Client->pingServerOnPort(SERVERPORT);
					}

					ImGui::NewLine();

					ImGui::InputText("", m_ClientIpText, 50);

					if (ImGui::Button("Join Server IP")) {
						bool isIpCorrect = true;
						for (int i = 0; i < 50; i++) {
							if (m_ClientIpText[i] == '\0') {
								if (i <= 8) {//0.0.0.0 <- min size
									isIpCorrect = false;
								}
								break;
							}
							if ((m_ClientIpText[i] < '0' || m_ClientIpText[i] > '9') && m_ClientIpText[i] != '.') {//letter
								isIpCorrect = false;
								break;
							}
						}
						if (isIpCorrect) {
							m_Connected = m_Client->connect(m_ClientIpText, SERVERPORT);
						}
					}

					//ImGui::NewLine();
					//
					//if (ImGui::Button("Join James Server")) {
					//	m_Connected = m_Client->connect("10.17.38.225", SERVERPORT);
					//}

					ImGui::End();
				}
			}
		}
	}
}

void LuaTestApp::draw() {
	glDisable(GL_DEPTH_TEST);
	m_Shader->useProgram();
	m_Shader->setUniformProjectionView(m_Camera);

	m_MeshTest->draw();

	for (size_t i = 0; i < m_NetworkCars.size(); i++) {
		m_NetworkCars[i]->draw();
	}

	m_LuaAgent->draw();
}

void LuaTestApp::sendCarToServer() {
	if (!m_Connected) {
		return;
	}
	m_Networker->startMessage(UserGameMessages::ID_CAR_UPDATE);
	m_Networker->addToMessage(m_ClientCarId);

	glm::vec3 pos = m_LuaAgent->m_Mesh->m_Transform.getGlobalPosition();
	glm::vec3 rot = m_LuaAgent->m_Mesh->m_Transform.getGlobalRotationEulers();


	m_Networker->addToMessage(pos.x);
	m_Networker->addToMessage(pos.z);
	//all 3 rots because it rotates on the y, and y wont go past 90
	m_Networker->addToMessage(rot.x);
	m_Networker->addToMessage(rot.y);
	m_Networker->addToMessage(rot.z);

	m_Networker->sendMessage();


}


void LuaTestAppClientPacketHandler::packetReceived(FUNCTIONPARAMATERS) {
	switch (*a_MessageID) {
		case UserGameMessages::ID_GET_ID:
			{
				std::cout << "GOT ID" << std::endl;
				unsigned int number;
				a_BitStream->Read(number);
				m_App->m_ClientCarId = number;
				std::cout << number << std::endl;
				break;
			}
		case UserGameMessages::ID_USER_JOINED:
			{
				std::cout << "NEW CAR" << std::endl;
				unsigned int id;
				a_BitStream->Read(id);

				for (size_t i = 0; i < m_App->m_NetworkCars.size(); i++) {
					if (id == m_App->m_NetworkCars[i]->m_Id) {
						break;//id already exist's
					}
				}

				NetworkedCars* nc = new NetworkedCars();
				nc->m_Id = id;

				float x, y, z;
				//pos
				a_BitStream->Read(x);
				y = 0;
				a_BitStream->Read(z);

				//nc->m_Mesh->m_Transform.setPosition(glm::vec3(x, y, z));
				nc->m_DesiredPos = glm::vec3(x, y, z);
				//rot
				a_BitStream->Read(x);
				a_BitStream->Read(y);
				a_BitStream->Read(z);

				//nc->m_Mesh->m_Transform.setRotation(glm::vec3(x, y, z));
				nc->m_DesiredRot = glm::vec3(x, y, z);

				nc->updateFrom();

				m_App->m_NetworkCars.push_back(nc);

				break;
			}
		case UserGameMessages::ID_CAR_UPDATE:
			{
				unsigned int id;
				a_BitStream->Read(id);

				//don't update our position
				if (id == m_App->m_ClientCarId) {
					break;
				}

				for (size_t i = 0; i < m_App->m_NetworkCars.size(); i++) {
					if (id == m_App->m_NetworkCars[i]->m_Id) {
						NetworkedCars* nc = m_App->m_NetworkCars[i];

						float x, y, z;
						//pos
						a_BitStream->Read(x);
						y = 0;
						a_BitStream->Read(z);

						nc->m_DesiredPos = glm::vec3(x, y, z);
						//rot
						a_BitStream->Read(x);
						a_BitStream->Read(y);
						a_BitStream->Read(z);

						nc->m_DesiredRot = glm::vec3(x, y, z);
						
						nc->updateFrom();
						break;
					}
				}

				break;
			}
		case UserGameMessages::ID_USER_LEFT:
			{
				unsigned int id;
				a_BitStream->Read(id);

				for (size_t i = 0; i < m_App->m_NetworkCars.size(); i++) {
					if (m_App->m_NetworkCars[i]->m_Id == id) {
						NetworkedCars* nc = m_App->m_NetworkCars[i];

						m_App->m_NetworkCars.erase(m_App->m_NetworkCars.begin() + i);
						delete nc;
					}
				}
				break;
			}
		default:
			break;
	}
}

void LuaTestAppClientPacketHandler::pongReceived(RakNet::Packet * a_Packet, uint32_t a_Time, std::string a_Text) {
	a_Packet->systemAddress.ToString();
	std::cout << "PONG!! from " << a_Packet->systemAddress.ToString() << " " << a_Time << "ms | " << a_Text << std::endl;

	PotentialServers ps;

	std::vector<std::string> strings;
	//split 
	std::istringstream f(a_Packet->systemAddress.ToString());
	std::string s;
	while (getline(f, s, '|')) {
		strings.push_back(s);
	}

	ps.Ip = strings[0];
	ps.text = a_Text;

	m_App->m_LanServers[strings[0].c_str()] = ps;
}

void LuaTestAppServerPacketHandler::packetReceived(FUNCTIONPARAMATERS) {
	switch (*a_MessageID) {
		case ID_NEW_INCOMING_CONNECTION:
			{
				std::cout << "INCOMING CONNECTION " << std::endl;
				//send all current cars to new client
				NetworkedCars* newCar = new NetworkedCars();
				newCar->m_Guid = a_Packet->guid;
				newCar->m_Id = m_App->m_CarsCreated++;

				//send ID to new Player
				m_App->m_Server->startMessage(UserGameMessages::ID_GET_ID);
				m_App->m_Server->addToMessage(newCar->m_Id);
				m_App->m_Server->sendMessage(a_Packet);

				m_App->m_NetworkCars.push_back(newCar);

				glm::vec3 pos;
				glm::vec3 rot;

				for (size_t i = 0; i < m_App->m_NetworkCars.size() - 1; i++) {
					NetworkedCars* car = m_App->m_NetworkCars[i];
					pos = car->m_Mesh->m_Transform.getGlobalPosition();
					rot = car->m_Mesh->m_Transform.getGlobalRotationEulers();

					m_App->m_Server->startMessage(UserGameMessages::ID_USER_JOINED);
					m_App->m_Server->addToMessage(car->m_Id);
					m_App->m_Server->addToMessage(pos.x);
					m_App->m_Server->addToMessage(pos.z);
					//all 3 rots because it rotates on the y, and y wont go past 90
					m_App->m_Server->addToMessage(rot.x);
					m_App->m_Server->addToMessage(rot.y);
					m_App->m_Server->addToMessage(rot.z);
					m_App->m_Server->sendMessage(a_Packet);
				}

				//add servers car
				pos = m_App->m_LuaAgent->m_Mesh->m_Transform.getGlobalPosition();
				rot = m_App->m_LuaAgent->m_Mesh->m_Transform.getGlobalRotationEulers();

				m_App->m_Server->startMessage(UserGameMessages::ID_USER_JOINED);
				m_App->m_Server->addToMessage(0U);//server car ID
				m_App->m_Server->addToMessage(pos.x);
				m_App->m_Server->addToMessage(pos.z);
				//all 3 rots because it rotates on the y, and y wont go past 90
				m_App->m_Server->addToMessage(rot.x);
				m_App->m_Server->addToMessage(rot.y);
				m_App->m_Server->addToMessage(rot.z);
				m_App->m_Server->sendMessage(a_Packet);

				//send new car to all clients
				pos = newCar->m_Mesh->m_Transform.getGlobalPosition();
				rot = newCar->m_Mesh->m_Transform.getGlobalRotationEulers();

				m_App->m_Server->startMessage(UserGameMessages::ID_USER_JOINED);
				m_App->m_Server->addToMessage(newCar->m_Id);
				m_App->m_Server->addToMessage(pos.x);
				m_App->m_Server->addToMessage(pos.z);
				//all 3 rots because it rotates on the y, and y wont go past 90
				m_App->m_Server->addToMessage(rot.x);
				m_App->m_Server->addToMessage(rot.y);
				m_App->m_Server->addToMessage(rot.z);
				m_App->m_Server->sendMessageExclude(a_Packet->guid);

				break;
			}
		case UserGameMessages::ID_CAR_UPDATE:
			{
				unsigned int id;
				a_BitStream->Read(id);

				//don't update our position
				if (id == m_App->m_ClientCarId) {
					break;
				}
				NetworkedCars* nc = nullptr;
				for (size_t i = 0; i < m_App->m_NetworkCars.size(); i++) {
					if (id == m_App->m_NetworkCars[i]->m_Id) {
						nc = m_App->m_NetworkCars[i];

						float x, y, z;
						//pos
						a_BitStream->Read(x);
						y = 0;
						a_BitStream->Read(z);

						//nc->m_Mesh->m_Transform.setPosition(glm::vec3(x, y, z));
						nc->m_DesiredPos = glm::vec3(x, y, z);

						//rot
						a_BitStream->Read(x);
						a_BitStream->Read(y);
						a_BitStream->Read(z);

						//nc->m_Mesh->m_Transform.setRotation(glm::vec3(x, y, z));
						nc->m_DesiredRot = glm::vec3(x, y, z);

						nc->updateFrom();

						
						break;
					}
				}

				//don't update if the server doesn't even have the car :/
				if (nc == nullptr) {
					break;
				}

				//send new car to all clients
				glm::vec3 pos = nc->m_Mesh->m_Transform.getGlobalPosition();
				glm::vec3 rot = nc->m_Mesh->m_Transform.getGlobalRotationEulers();

				m_App->m_Server->startMessage(UserGameMessages::ID_CAR_UPDATE);
				m_App->m_Server->addToMessage(nc->m_Id);
				m_App->m_Server->addToMessage(pos.x);
				m_App->m_Server->addToMessage(pos.z);
				//all 3 rots because it rotates on the y, and y wont go past 90
				m_App->m_Server->addToMessage(rot.x);
				m_App->m_Server->addToMessage(rot.y);
				m_App->m_Server->addToMessage(rot.z);
				m_App->m_Server->sendMessageExclude(a_Packet->guid);

				break;
			}
		case ID_DISCONNECTION_NOTIFICATION:
		case ID_CONNECTION_LOST:
			{
				for (size_t i = 0; i < m_App->m_NetworkCars.size(); i++) {
					if (m_App->m_NetworkCars[i]->m_Guid == a_Packet->guid) {
						NetworkedCars* nc = m_App->m_NetworkCars[i];

						m_App->m_Server->startMessage(UserGameMessages::ID_USER_LEFT);
						m_App->m_Server->addToMessage(nc->m_Id);
						m_App->m_Server->sendMessageExclude(a_Packet);

						m_App->m_NetworkCars.erase(m_App->m_NetworkCars.begin() + i);
						delete nc;
					}
				}
				break;
			}
		default:
			break;
	}
}

void LuaTestAppServerPacketHandler::pongReceived(RakNet::Packet * a_Packet, uint32_t a_Time, std::string a_Text) {
	//nothing, server does not ping
}
