#include "ScriptedCharacterController.h"

#include <fstream>
#include <sstream>
#include <iterator>
#include <iostream>

#include <algorithm>

#include "TimeHandler.h"

#include "Character.h"

ScriptedCharacterController::ScriptedCharacterController(Character & a_ControlledCharacter) :
	m_ControlledCharacter(&a_ControlledCharacter),
	m_CommandLocation(0),
	m_IsWaiting(false),
	m_RemainingWaitTime(0.0f) {

	setupCommands();

}

void ScriptedCharacterController::loadScript(std::string a_Script) {
	m_IsScriptLoaded = false;
	m_ScriptPath = a_Script;
	m_CommandsToExectute.clear();

	std::ifstream scriptFile(a_Script, std::ios::in);

	std::string text;
	while (scriptFile.good()) {
		if (std::getline(scriptFile, text)) {
			m_CommandsToExectute.push_back(splitString(text));
		}
	}

	if (!m_CommandsToExectute.empty()) {
		m_IsScriptLoaded = true;
		m_CommandLocation = 0;
		m_IsWaiting = false;
	}
}

void ScriptedCharacterController::reloadScript() {
	loadScript(m_ScriptPath);
}

void ScriptedCharacterController::update() {
	if (m_IsScriptLoaded && !m_IsWaiting) {
		const CommandData& nextCommand = m_CommandsToExectute[m_CommandLocation];

		auto it = m_AllowedCommands.find(nextCommand[0]);
		if (it != m_AllowedCommands.end()) {
			//this will execute lamba function stored to this command

			it->second(nextCommand);
		} else {
			std::cout << "Invalid Command Ran '" << nextCommand[0] << "'\n";
		}
		m_CommandLocation++;
		if (m_CommandLocation >= m_CommandsToExectute.size()) {
			m_CommandLocation = 0;
		}
	}

	if (m_IsWaiting) {
		m_RemainingWaitTime -= TimeHandler::getDeltaTime();
		if (m_RemainingWaitTime <= 0) {
			m_IsWaiting = false;
		}
	}
}

std::vector<std::string> ScriptedCharacterController::splitString(std::string a_Str) {
	//some how splits every space? or the first space
	std::istringstream iss(a_Str);
	return std::vector<std::string>(std::istream_iterator<std::string>(iss), std::istream_iterator<std::string>());
}

void ScriptedCharacterController::setupCommands() {
	m_AllowedCommands["SET_SPRITE"] = [this](const CommandData& a_Data) { onSetSprite(a_Data); };
	m_AllowedCommands["SPEAK"] = [this](const CommandData& a_Data) { onSpeak(a_Data); };
	m_AllowedCommands["WAIT"] = [this](const CommandData& a_Data) { onWait(a_Data); };
	m_AllowedCommands["SET_STATE"] = [this](const CommandData& a_Data) { onSetState(a_Data); };
	m_AllowedCommands["DEBUG"] = [this](const CommandData& a_Data) { onDebug(a_Data); };
}

void ScriptedCharacterController::onSetSprite(const CommandData & a_Data) {
	assert(a_Data.size() == 2);
	assert(
		a_Data[1] == "adventurer" ||
		a_Data[1] == "female" ||
		a_Data[1] == "player" ||
		a_Data[1] == "zombie" ||
		a_Data[1] == "soldier"
	);
	m_ControlledCharacter->setSprite(a_Data[1]);
}

void ScriptedCharacterController::onSetState(const CommandData & a_Data) {

	std::unordered_map<std::string, Character::State> stringToState;
	stringToState["idle"] = Character::IDLE;
	stringToState["talking"] = Character::TALKING;
	stringToState["face_back"] = Character::FACE_BACK;
	stringToState["hurt"] = Character::HURT;

	assert(a_Data.size() == 2);
	assert(stringToState.find(a_Data[1]) != stringToState.end());

	m_ControlledCharacter->setState(stringToState[a_Data[1]]);
}

void ScriptedCharacterController::onWait(const CommandData & a_Data) {
	assert(a_Data.size() == 2);

	float waitTime = (float)std::atof(a_Data[1].c_str());

	//atof will return 0 of no numbers
	assert(waitTime > 0.0f);

	m_IsWaiting = true;
	m_RemainingWaitTime = waitTime;
}

void ScriptedCharacterController::onSpeak(const CommandData & a_Data) {
	assert(a_Data.size() > 2);

	float time = (float)atof(a_Data[1].c_str());
	assert(time > 0.0f);

	std::string toSay;
	for (size_t i = 2; i < a_Data.size(); i++) {
		toSay.append(a_Data[i]);
		toSay.append(" ");
	}

	m_IsWaiting = true;
	m_RemainingWaitTime = true;
	m_ControlledCharacter->speak(toSay, time);
}

void ScriptedCharacterController::onDebug(const CommandData & a_Data) {
	assert(a_Data.size() >= 2);

	std::string toSay;
	for (size_t i = 1; i < a_Data.size(); i++) {
		toSay.append(a_Data[i]);
		toSay.append(" ");
	}

	std::cout << toSay << std::endl;
}

