#pragma once

#include <string>
#include <unordered_map>
#include <glm\glm.hpp>

class Texture;
class Mesh;

class Character {
public:
	enum State {
		IDLE,
		TALKING,
		FACE_BACK,
		HURT
	};
private:
	using TextureMap = std::unordered_map<State, Texture*>;
	using SpriteMap = std::unordered_map<std::string, TextureMap>;
public:
	Character(std::string a_BaseSprite = "adventurer");
	~Character();

	void setSprite(std::string a_Sprite);
	std::string getSprite() const;

	void setState(State a_EState);
	State getState() const;

	void setPosition(const glm::vec2& a_Pos);
	glm::vec2 getPosition() const;

	void setSpeechColour(const glm::vec4& a_Color);
	void speak(std::string a_SpeechText, float a_SpeechTime);

	void update();
	void render();
private:
	void loadTextures();

	void loadTexturesForSpite(const std::string& a_Sprite);
	void unloadTextures();

	std::string m_BaseSprite;
	State		m_EPlayerState;

	bool		m_IsSpeaking;
	std::string m_SpeakingText;
	float		m_RemainingSpeechTime;
	glm::vec4	m_SpeechColor;

	SpriteMap	m_Textures;


	Mesh* m_Mesh;
};
