#include "BaseLuaObject.h"

#include <iostream>

BaseLuaObject::BaseLuaObject() {
	m_LuaState = luaL_newstate();
	luaL_openlibs(m_LuaState);

	setPointerVar(m_LuaState, "self", this);
}


BaseLuaObject::~BaseLuaObject() {
}

bool BaseLuaObject::loadScript(const char * a_Path) {
	m_FilePath = a_Path;
	int status = luaL_dofile(m_LuaState, a_Path);
	if (status) {
		std::cout << "Couldn't load script: " << a_Path << std::endl << lua_tostring(m_LuaState, -1) << std::endl;
		return false;
	}

	//run a script loaded function before init to pass through data that may be necessary 
	scriptLoaded();

	//run init
	lua_getglobal(m_LuaState, "Init");
	//execute
	status = lua_pcall(m_LuaState, 0, 0, NULL);
	if (status) {
		std::cout << "Cout not execute function Init " << lua_tostring(m_LuaState, -1) << std::endl;
	}

	std::cout << "Script Loaded " << a_Path <<std::endl;
	return true;

}

bool BaseLuaObject::reloadScript() {
	return loadScript(m_FilePath);
}

void BaseLuaObject::registerLuaFunction(const char * a_FunctionName, lua_CFunction a_Function) {
	lua_register(m_LuaState, a_FunctionName, a_Function);
}

bool BaseLuaObject::callFunction(int a_ArgCount, int a_ReturnCount) {
	if (!lua_isfunction(m_LuaState, -(a_ArgCount + 1))) {
		std::cout << "BaseLuaObject::callFunction - function is not in the correct position on the stack" << std::endl;
		return false;
	}

	int status = lua_pcall(m_LuaState, a_ArgCount, a_ReturnCount, NULL);

	if (status) {
		std::cout << "Couldn't execute function  " << lua_tostring(m_LuaState, -1) << std::endl;
		return false;
	}

	return true;
}

void BaseLuaObject::setPointerVar(lua_State * a_State, const char * a_VaraibleName, void * a_Val) {
	lua_pushlightuserdata(a_State, a_Val);
	lua_setglobal(a_State, a_VaraibleName);
}

void * BaseLuaObject::getPointerVar(lua_State * a_State, const char * a_VariableName) {
	lua_getglobal(a_State, a_VariableName);

	if (lua_isuserdata(a_State, -1) == false) {
		std::cout << "BaseLuaObject::getPointerVar: Variable is not a pointer" << std::endl;
		return NULL;
	}

	void* val = (void*)lua_topointer(a_State, -1);
	lua_pop(a_State,1);
	return val;
}

void BaseLuaObject::pushFloat(float a_Value) {
	lua_pushnumber(m_LuaState, a_Value);//???
}

float BaseLuaObject::popFloat() {
	if (lua_isnumber(m_LuaState, -1) == false) {
		int iStackSize = lua_gettop(m_LuaState);
		std::cout << "BaseLuaObject::popFloat: Variable is not a number" << std::endl;
		return 0.0f;
	}

	float val = (float)lua_tonumber(m_LuaState, -1);
	lua_pop(m_LuaState, 1);
	return val;

}

void BaseLuaObject::pushBool(bool a_Value) {
	lua_pushboolean(m_LuaState, a_Value);
}

void BaseLuaObject::pushInt(int a_Value) {
	lua_pushinteger(m_LuaState, a_Value);
}

int BaseLuaObject::popInt() {
	if (lua_isinteger(m_LuaState, -1) == false) {
		int iStackSize = lua_gettop(m_LuaState);
		std::cout << "BaseLuaObject::popInt: Variable is not a number" << std::endl;
		return 0;
	}

	int val = (int)lua_tointeger(m_LuaState, -1);
	lua_pop(m_LuaState, 1);
	return val;
}

const char * BaseLuaObject::popString() {
	if (lua_isstring(m_LuaState, -1) == false) {
		int iStackSize = lua_gettop(m_LuaState);
		std::cout << "BaseLuaObject::popString: Variable is not a number" << std::endl;
		return "";
	}

	const char* val = (const char*)lua_tostring(m_LuaState, -1);
	lua_pop(m_LuaState, 1);
	return val;
}

void BaseLuaObject::pushFunction(const char * a_Function) {
	lua_getglobal(m_LuaState, a_Function);
	if (lua_isfunction(m_LuaState, -1) == false) {
		std::cout << "BaseLuaObject::pushFunction: variable is not a function" << std::endl;
		return;
	}
}
