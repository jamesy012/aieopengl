#pragma once

#include <MessageIdentifiers.h>

#define SERVERPORT 4829
#define CAR_SEND_DELAY 100

enum UserGameMessages {
	ID_STARTMESSAGE = ID_USER_PACKET_ENUM + 1,
	ID_USER_JOINED,
	ID_GET_ID,
	ID_USER_LEFT,
	ID_CAR_UPDATE,
	ID_CAR_REMOVE,
};