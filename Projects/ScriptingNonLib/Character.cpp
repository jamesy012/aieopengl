#include "Character.h"

#include <assert.h>

#include "Texture.h"
#include "TimeHandler.h"
#include "Mesh.h"

Character::Character(std::string a_BaseSprite) : m_EPlayerState(IDLE), m_IsSpeaking(false),m_SpeakingText(), m_SpeechColor(0,0,0,1) {
	loadTextures();
	setSprite(a_BaseSprite);

	m_Mesh = new Mesh();
	m_Mesh->createBox();
	m_Mesh->m_Transform.setRotation(glm::vec3(90, 0, 0));
	float size = 10.0f;
	//mul by ratio
	m_Mesh->m_Transform.setScale(glm::vec3(size * 9, 1, size * 16));
}

Character::~Character() {
	unloadTextures();
	delete m_Mesh;
}

void Character::setSprite(std::string a_Sprite) {
	m_BaseSprite = a_Sprite;
}

std::string Character::getSprite() const {
	return m_BaseSprite;
}

void Character::setState(State a_EState) {
	m_EPlayerState = a_EState;
}

Character::State Character::getState() const {
	return m_EPlayerState;
}

void Character::setPosition(const glm::vec2 & a_Pos) {
	m_Mesh->m_Transform.setPosition(glm::vec3(a_Pos,0));
}

glm::vec2 Character::getPosition() const {
	return glm::vec2(m_Mesh->m_Transform.getGlobalPosition());
}

void Character::setSpeechColour(const glm::vec4 & a_Color) {
	m_SpeechColor = a_Color;
}

void Character::speak(std::string a_SpeechText, float a_SpeechTime) {
	m_IsSpeaking = true;
	m_SpeakingText = a_SpeechText;
	m_RemainingSpeechTime = a_SpeechTime;

	printf("SPEAKING %s: %s\n", m_BaseSprite.c_str(),  a_SpeechText.c_str());
}

void Character::update() {
	if (m_IsSpeaking && m_RemainingSpeechTime >= 0) {
		m_RemainingSpeechTime -= TimeHandler::getDeltaTime();

		m_Mesh->m_Transform.rotate(glm::vec3(0, 5, 0) * TimeHandler::getDeltaTime());

		if (m_RemainingSpeechTime < 0) {
			m_Mesh->m_Transform.setRotation(glm::vec3(90, 0, 0));
			m_IsSpeaking = false;
		}
	}
}

void Character::render() {

	m_Mesh->setDiffuse(m_Textures[m_BaseSprite][m_EPlayerState]);

	//m_Mesh->m_Transform.setPosition(glm::vec3(m_RenderPosition.x, 0, m_RenderPosition.y));
	m_Mesh->draw();

	//add speech??
}

void Character::loadTextures() {
	if (!m_Textures.empty()) return; //Already loaded!

	loadTexturesForSpite("adventurer");
	loadTexturesForSpite("female");
	loadTexturesForSpite("player");
	loadTexturesForSpite("zombie");
	loadTexturesForSpite("soldier");

	//speech?
}

void Character::loadTexturesForSpite(const std::string & a_Sprite) {
	std::string path = "./Textures/Characters/" + a_Sprite + "/" + a_Sprite + "_";
	m_Textures[a_Sprite][IDLE] = new Texture();
	m_Textures[a_Sprite][TALKING] = new Texture();
	m_Textures[a_Sprite][FACE_BACK] = new Texture();
	m_Textures[a_Sprite][HURT] = new Texture();

	m_Textures[a_Sprite][IDLE]->loadTexture((path + "idle.png").c_str());
	m_Textures[a_Sprite][TALKING]->loadTexture((path + "talk.png").c_str());
	m_Textures[a_Sprite][FACE_BACK]->loadTexture((path + "back.png").c_str());
	m_Textures[a_Sprite][HURT]->loadTexture((path + "hurt.png").c_str());
}

void Character::unloadTextures() {
	for (auto& spriteTextures : m_Textures)
	{
		for (auto& texture : spriteTextures.second)
		{
			//delete texture.second;
			texture.second->release();
		}
	}
	m_Textures.clear();

	//speech?
}
