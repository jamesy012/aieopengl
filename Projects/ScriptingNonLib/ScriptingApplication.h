#pragma once
#include "Application.h"

class Texture;
class Character;
class ScriptedCharacterController;
class Shader;

class ScriptingApplication : public Application {
public:
	// Inherited via Application
	virtual void startUp() override;

	virtual void shutDown() override;

	virtual void update() override;

	virtual void draw() override;

	virtual void screenSizeChanged() override;
private:
	Texture* m_Background;
	Character* m_Character;
	ScriptedCharacterController* m_Controller;
	Shader* m_Shader;
};

