#define CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

#include <vector>
#include <mutex>
#include <thread>

#include "Logger.h"
#include "DebugTimer.h"

void print() {
	static std::mutex myMutex;

	std::lock_guard<std::mutex> guard(myMutex);

	printf("Hello Thread\n");
	printf("I'm here...\n");
	printf("... not there.\n");

}

int main() {
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	DebugTimer::startTimer(0, "Logger creation");
	Logger* log = new Logger();
	DebugTimer::endTimer(0);
	DebugTimer::startTimer(1,"Timer");
	
	std::vector<std::thread> threads;
	int threadCount = 5;

	log->log("before creation");

	DebugTimer::startTimer(3, "threads creation");
	for (int i = 0; i < threadCount; i++) {
		threads.push_back(std::thread(print));
		DebugTimer::lapTimer(3);
	}
	DebugTimer::endTimer(3);

	DebugTimer::startTimer(2,"Log test");
	log->log("mid creation");
	DebugTimer::endTimer(2);

	DebugTimer::startTimer(4, "thread join");
	for (auto& thread : threads) {
		thread.join();
	}
	DebugTimer::endTimer(4);

	//DebugTimer::lapTimer(1);
	log->log("before delete");
	DebugTimer::lapTimer(1);
	delete log;
	DebugTimer::endTimer(1);
	getchar();
	return 0;
}